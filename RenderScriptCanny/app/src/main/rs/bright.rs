#pragma version(1)
#pragma rs_fp_relaxed
#pragma rs java_package_name(com.wade.wafer)

#include "rs_debug.rsh"

float brightPassThreshold;
// #define RS_KERNEL __attribute__((kernel))
uchar4 RS_KERNEL brightPass(uchar4 in) {
    float3 luminanceVector = { 0.2125, 0.7154, 0.0721 };
    float4 pixel = rsUnpackColor8888(in);
    float luminance = dot(luminanceVector, pixel.rgb);
    luminance = max(0.0f, luminance - brightPassThreshold);
    pixel.rgb *= sign(luminance);
    pixel.a = 1.0;
    return rsPackColorTo8888(clamp(pixel, 0.f, 1.0f));
}
