#pragma version(1)
#pragma rs java_package_name(com.wade.wafer)

const float THRESHOLD = 0.5;

uchar4 RS_KERNEL root(uchar4 in, uint32_t x, uint32_t y) {
        float4 f4 = rsUnpackColor8888(in);
        uchar4 out;
        if ((f4.r + f4.g + f4.b) / 3 > THRESHOLD) {
                float3 output = {0, 0, 0};
        out = rsPackColorTo8888(output);
        } else {
                float3 output = {1.0, 1.0, 1.0};
                out = rsPackColorTo8888(output);
        }
        return out;
}

void init(){
        rsDebug("Called init @ binarize: THRESHOLD = ", THRESHOLD);
}
