#pragma version(1)
#pragma rs java_package_name(com.wade.wafercv2)
#pragma rs_fp_relaxed

rs_allocation allocationSrc;
rs_allocation allocationPrev;
rs_allocation allocationSobel;      // sobel(u8, float2)
rs_allocation allocationCanny;      // canny(float2, float2)
rs_allocation allocationHysteresis; // hysteresis(float2, rgba)
rs_allocation allocationOut;
const int MESSAGE_OK = 1;
#define MODE_NONE  0
#define MODE_MONO  1
#define MODE_TWO   2
#define MODE_SOBEL 4

uint32_t width;
uint32_t height;
uint32_t mode=MODE_NONE;

typedef struct Variance {
  int32_t count;
} Variance_T;

Variance_T variance;

// #define SHOW_MONO_ONLY 1
// #define SHOW_DIFF_MONO 1
// mInputAllocation.ioReceive();
// mHdrMergeScript.set_allocationSrc(mInputAllocation);
// mHdrMergeScript.forEach_mergeHdrFrames(mPrevAllocation);
// 原圖(u8_4) -. blur(u8,u8) -. histEq(u8,u8) -. sobel(u8, float2) -. canny -. hysteresis
void RS_KERNEL rgbSobel(uchar4 prevPixel, uint32_t x, uint32_t y) {
    uchar gray = rsGetElementAtYuv_uchar_Y(allocationSrc, x, y);

    if (mode == MODE_NONE) {
        short Y = ((short)gray) - 16;
        short U = ((short)rsGetElementAtYuv_uchar_U(allocationSrc, x, y)) - 128;
        short V = ((short)rsGetElementAtYuv_uchar_V(allocationSrc, x, y)) - 128;

        short4 p;
        p.x = (Y * 298 + V * 409 + 128) >> 8;
        p.y = (Y * 298 - U * 100 - V * 208 + 128) >> 8;
        p.z = (Y * 298 + U * 516 + 128) >> 8;
        p.w = 255;
        if (p.x < 0) p.x = 0;
        if (p.x > 255) p.x = 255;
        if (p.y < 0) p.y = 0;
        if (p.y > 255) p.y = 255;
        if (p.z < 0) p.z = 0;
        if (p.z > 255) p.z = 255;

        rsSetElementAt_uchar4(allocationOut, (uchar4){p.x, p.y, p.z, p.w}, x, y);
    } else if (mode == MODE_MONO)
        rsSetElementAt_uchar4(allocationOut, (uchar4){ gray, gray, gray, 254}, x, y);
    else {
        if ((mode&MODE_SOBEL) == MODE_SOBEL && x > 0 && y > 0 && x < width-1 && y < height-1) {
            uchar topleft    = rsGetElementAtYuv_uchar_Y(allocationSrc, x-1,y-1);
            uchar left       = rsGetElementAtYuv_uchar_Y(allocationSrc, x-1,y);
            uchar bottomleft = rsGetElementAtYuv_uchar_Y(allocationSrc, x-1,y+1);
            uchar top        = rsGetElementAtYuv_uchar_Y(allocationSrc, x,y-1);
            uchar bottom     = rsGetElementAtYuv_uchar_Y(allocationSrc, x,y+1);
            uchar topright   = rsGetElementAtYuv_uchar_Y(allocationSrc, x+1,y-1);
            uchar right      = rsGetElementAtYuv_uchar_Y(allocationSrc, x+1,y);
            uchar bottomright= rsGetElementAtYuv_uchar_Y(allocationSrc, x+1,y+1);

            float xValue=0.5*(-topleft-2*left-bottomleft+topright+2*right+bottomright);
            float yValue=0.5*(-topleft-2*top-topright+bottomleft+2*bottom+bottomright);
            float d = atan2pi((float)yValue, (float)xValue);
            float i = native_sqrt(xValue*xValue+yValue*yValue);
            gray = (uchar)i;
        }
        if ((mode&MODE_TWO) == MODE_TWO) {
            uchar4 prev = rsGetElementAt_uchar4(allocationPrev, x, y);
            if (prev.a != 254) prev.r = gray;
            rsSetElementAt_uchar4(allocationPrev, (uchar4){ gray, gray, gray, 254}, x, y);

            if (prev.r > gray) gray = prev.r - gray;
            else gray = gray - prev.r;
//            if (gray > 127) gray = 255;
//            else gray = 0;
        }
        rsSetElementAt_uchar4(allocationOut, (uchar4){ gray, gray, gray, 254}, x, y);
    }
}

/* 底下是為了比對兩張圖的差值 */
void RS_KERNEL calcSum(const uchar4 in, uint32_t x, uint32_t y) {
    uchar src = rsGetElementAtYuv_uchar_Y(allocationSrc, x, y);
    uchar4 target = rsGetElementAt_uchar4(allocationPrev, x, y);
    if (src != target.g) {
        ++variance.count;
    }
}
void resetVariance() {
  variance.count = 0;
}
void getVariance() {
    rsSendToClient(MESSAGE_OK, (void*)&variance, sizeof(variance));
}
