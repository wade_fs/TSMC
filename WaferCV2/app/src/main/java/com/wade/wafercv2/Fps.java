package com.wade.wafercv2;

import android.content.Context;
import android.view.View;

import java.util.ArrayList;
import java.util.Locale;

public class Fps {
    private ArrayList<Long> data;
    Context context;
    GuiHandler guiHandler;
    ScriptC_bar scBar;
    int mode;
    Fps(Context context) {
        this.context = context;
        this.scBar = scBar;
        data = new ArrayList<Long>() {};
        guiHandler = ((HdrViewfinderActivity)context).guiHandler;
    }
    public void reset() {
        data.clear();
    }
    public void setScBar(ScriptC_bar bar) { scBar = bar; }
    public void setMode(int m) { mode = m; }
    public void add() {
        data.add(System.currentTimeMillis());
        if (data.size() > 100) data.remove(0);
        int status = guiHandler.getStatus();
        if (status == GuiHandler.STATE_NONE) {
            if (mode == 1 && scBar != null) {
                guiHandler.setStatusShowMsg(String.format(Locale.ENGLISH, "FPS = %.2f, Threshold = %d, Diff = %d",
                        get(), scBar.get_threshold(), scBar.get_diff()),
                        1000);
            } else {
                guiHandler.setStatusShowMsg(String.format(Locale.ENGLISH, "FPS = %.2f", get()), 1000);
            }
        }
    }
    private double get() {
        if (data.size() <= 1) return 0;
        double diff = data.get(data.size()-1) - data.get(0);
        return (data.size()-1)*1000 / diff;
    }
}
