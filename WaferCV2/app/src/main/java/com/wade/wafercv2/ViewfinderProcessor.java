/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.wade.wafercv2;

import android.app.Activity;
import android.content.Context;
import android.graphics.ImageFormat;
import android.os.Handler;
import android.os.HandlerThread;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicConvolve5x5;
import android.renderscript.Type;
import android.util.Log;
import android.util.Size;
import android.view.Surface;
import android.view.View;
import android.widget.SeekBar;

/**
 * Renderscript-based merger for an HDR viewfinder
 * TODO: 感覺 RenderScript/Allocation 定義的地方亂亂的
 */
public class ViewfinderProcessor {
    public final static int MODE_NONE                   = 0;
    public final static int MODE_MONO                   = 1;
    public final static int MODE_TWODIFF                = 2;
    public final static int MODE_SKIP_TO_SOBEL          = 3;
    public final static int MODE_SOBEL                  = 4;
    public final static int MODE_SKIP_TO_TWODIFF_SOBEL  = 5;
    public final static int MODE_TWODIFF_SOBEL          = 6;

    private Allocation allocationSrc;
    private Allocation allocationPrev;
    private Allocation allocationOut;

    private Handler mProcessingHandler;
    private ScriptC_hdr_merge scHdrMerge;
    private ScriptC_bar scBar;
    private int width, height, halfWidth;
    private int mMode = MODE_NONE;
    private Context context;
    ProcessingTask processingTask;

    ViewfinderProcessor(Context context, RenderScript rs, Size dimensions) {
        this.context = context;
        // 將來會限制使用
//        SysUtils sysUtils = ((HdrViewfinderActivity)context).sysUtils;

        width = dimensions.getWidth();
        halfWidth = width >> 1;
        height = dimensions.getHeight();

        HandlerThread processingThread = new HandlerThread("ViewfinderProcessor");
        processingThread.start();
        mProcessingHandler = new Handler(processingThread.getLooper());
        seekBar();
        processingTask = new ProcessingTask(context, rs);
        processingTask.setMode(MODE_NONE);
    }
    public SeekBar sbThreshold;
    public SeekBar sbDiff;

    public void seekBar() {
        sbThreshold = ((Activity)context).findViewById(R.id.sbThreshold);
        sbThreshold.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                scBar.set_threshold(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        sbDiff = ((Activity)context).findViewById(R.id.sbDiff);
        sbDiff.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                scBar.set_diff(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }
    public void startTraining(int target, int ms) {
        processingTask.startTraining(target, ms);
    }
    public Surface getInputNormalSurface() {
        return allocationSrc.getSurface();
    }

    public void setOutputSurface(Surface output) {
        allocationOut.setSurface(output);
    }

    /**
     * Simple class to keep track of incoming frame count,
     * and to process the newest one in the processing thread
     */
    class ProcessingTask implements Runnable, Allocation.OnBufferAvailableListener {
        private int mPendingFrames = 0;

        private RenderScript mRS;
        Context context;

        private Fps fps;
        GuiHandler guiHandler;
        Variance variance;
        float onePerSize;
        float lastRatio = 0.0f;
        boolean isTrained = false;
        boolean afterTrained = false;
        int target=GuiHandler.TARGET_ALLOCATION_EMPTY;

        ProcessingTask(Context context, RenderScript rs) {
            this.context = context;
            mRS = rs;
            Type yuvType = new Type.Builder(rs, Element.YUV(rs))
                    .setX(width).setY(height).setYuvFormat(ImageFormat.YUV_420_888).create();
            allocationSrc = Allocation.createTyped(rs, yuvType,
                    Allocation.USAGE_IO_INPUT | Allocation.USAGE_SCRIPT);
            allocationSrc.setOnBufferAvailableListener(this);

            Type u8_4 = new Type.Builder(rs, Element.RGBA_8888(rs)).setX(width).setY(height).create();
            allocationPrev = Allocation.createTyped(rs, u8_4, Allocation.USAGE_SCRIPT);
            allocationOut = Allocation.createTyped(rs, u8_4,
                    Allocation.USAGE_IO_OUTPUT | Allocation.USAGE_SCRIPT);

            scHdrMerge = new ScriptC_hdr_merge(rs);
            scHdrMerge.set_allocationSrc(allocationSrc);
            scHdrMerge.set_allocationOut(allocationOut);
            scHdrMerge.set_allocationPrev(allocationPrev);
            scHdrMerge.set_width(width);
            scHdrMerge.set_height(height);

            guiHandler = ((HdrViewfinderActivity)context).guiHandler;

            fps = new Fps(context);
            variance = new Variance(scHdrMerge);
            onePerSize = 1/(width*height);
            mRS.setMessageHandler(new RenderScript.RSMessageHandler() {
                @Override
                public void run() {
                    switch(mID) {
                        case ScriptC_hdr_merge.const_MESSAGE_OK:
                            variance.set(mData);
                            if (guiHandler.getStatus() == GuiHandler.STATE_COMPLETED) {
                                Log.d("MyLog", "COMPLETED 2");

                                lastRatio = variance.getCount() * onePerSize;
                            }
                            break;
                    }
                }
            });

            scBar = new ScriptC_bar(rs);
            scBar.set_allocationIn(allocationSrc);
            scBar.set_width(width);
            scBar.set_height(height);
            scBar.set_minX(halfWidth-5);
            scBar.set_maxX(halfWidth+5);
            scBar.set_minY(1);
            scBar.set_maxY(height-1);
            scBar.set_threshold(150);
            scBar.set_diff(10);
            fps.setScBar(scBar);
        }

        public void startTraining(int target, int ms) {
            this.target = target;
            isTrained = true;
            afterTrained = false;
            guiHandler.setStatusTraining(target, ms);
        }
        @Override
        public void onBufferAvailable(Allocation a) {
            synchronized(this) {
                mPendingFrames++;
                mProcessingHandler.post(this);
            }
        }
        long count=0;

        @Override
        public void run() {

            // Find out how many frames have arrived
            int pendingFrames;
            synchronized(this) {
                pendingFrames = mPendingFrames;
                mPendingFrames = 0;

                // Discard extra messages in case processing is slower than frame rate
                mProcessingHandler.removeCallbacks(this);
            }

            // Get to newest input
            for (int i = 0; i < pendingFrames; i++) {
                allocationSrc.ioReceive();
            }

            // Run processing pass
            if (mMode == MODE_MONO) { // 把 Bar 放到這兒
                scBar.forEach_bar1(allocationPrev, allocationOut);
            } else {
                scHdrMerge.forEach_rgbSobel(allocationPrev);
            }

            // 接下來要自行判斷狀態
            // Log.d("MyLog", "GuiHandler("+sysUtils.now()+") : status = "+guiHandler.getStatusString()+", waiting = "+(guiHandler.waitingMs/1000));

            allocationOut.ioSend();
            fps.add();

            int status = guiHandler.getStatus();
            ++count;
            if (!afterTrained && status == GuiHandler.STATE_COMPLETED) {
                Log.d("MyLog", "COMPLETED 1");
                isTrained = afterTrained = true;
            } else if (isTrained && afterTrained &&
                    status == GuiHandler.STATE_NONE || status == GuiHandler.STATE_SHOW_MSG || status == GuiHandler.STATE_COMPLETED) {
                scHdrMerge.invoke_resetVariance();
                // 繼續 variance 的計算
                // if (count%20 == 0) Log.d("MyLog", "照片比對中");
            } else {
                if (count%20 == 0) Log.d("MyLog", "應該還在訓練中或意外狀況");
            }
        }
        public void setMode(int mode) {
            fps.setMode(mode);
            if (mMode == MODE_MONO) {
                sbThreshold.setVisibility(View.VISIBLE);
                sbDiff.setVisibility(View.VISIBLE);
            } else {
                sbThreshold.setVisibility(View.INVISIBLE);
                sbDiff.setVisibility(View.INVISIBLE);
            }
        }
    }

    public int nextMode() {
        ++mMode;
        if (mMode == MODE_SKIP_TO_SOBEL) mMode = MODE_SOBEL;
        else if (mMode == MODE_SKIP_TO_TWODIFF_SOBEL) mMode = MODE_TWODIFF_SOBEL;
        else if (mMode > MODE_TWODIFF_SOBEL) mMode = MODE_NONE;
        scHdrMerge.set_mode(mMode);
        processingTask.setMode(mMode);
        return mMode;
    }
}
