package com.wade.wafercv2;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.lang.ref.WeakReference;

import static android.os.SystemClock.sleep;

public class GuiHandler {
    public final static int TARGET_ALLOCATION_EMPTY  = 0;
    public final static int TARGET_ALLOCATION_BOTTOM = 1;
    public final static int TARGET_ALLOCATION_TOP    = 2;
    public final static int TARGET_ALLOCATION_FULL   = 3;

    private String[] targetString = new String[]{ "empty", "bottom", "top", "full" };
    private int targetAllocation = TARGET_ALLOCATION_EMPTY;

    private HdrViewfinderActivity HdrViewfinderActivity;
    private Handler myHandler;
    private int status = 0;

    public static int waitingMs = 0;
    private static String waitingMsg = "";
    SysUtils sysUtils;

    GuiHandler(Context context) {
        HdrViewfinderActivity = (HdrViewfinderActivity)context;
        myHandler = new MyHandler(HdrViewfinderActivity);
        status = STATE_NONE;
        waitingMs = 0;
        waitingMsg = "";
        sysUtils = new SysUtils(context);
        heartBeat();
    }

    public int getStatus() {
        return this.status;
    }
    public void setStatus(int status) {
        this.status = status;
    }
    public void setStatusShowMsg(String msg, int ms) {
        status = STATE_SHOW_MSG;
        waitingMs = ms;
        waitingMsg = msg;
    }
    public void setStatusHidePicView(int ms) {
        status = STATE_HIDE_PICV;
        waitingMs = ms;
    }
    public void setStatusTraining(int target, int ms) {
        targetAllocation = target;
        status = STATE_TRAINING;
        waitingMs = ms;
        waitingMsg = targetString[target];
    }
    public int getTargetAllocation() {
        return targetAllocation;
    }

    final public static int STATE_NONE      = 0;
    final public static int STATE_CLEAR_MSG = 1;
    final public static int STATE_SHOW_MSG  = 2;
    final public static int STATE_COUNTDOWN = 3;
    final public static int STATE_COMPLETED = 4;
    final public static int STATE_HIDE_PICV = 5;
    final public static int STATE_TRAINING  = 6;
    final public static int STATE_TAKE_PIC  = 7;

    public String getStatusString() {
        final String[] statusString = new String[] {
                "None", "Clear Msg", "Show Msg", "Count Down", "Completed",
                "Hide Pic View", "Training", "Take Picture"
        };
        return statusString[status];
    }
    // HEART BEAT 是『待辦事項』，需要處理狀態
    // 也就是在狀態改變時，透過送訊息讓 guiHandler 辦事
    private void heartBeat() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                while(true) {
                    if (waitingMs > 0) waitingMs -= 1000;
                    switch (status) {
                        case STATE_CLEAR_MSG:
                            status = STATE_NONE;
                            myHandler.sendEmptyMessage(STATE_CLEAR_MSG);
                            break;
                        case STATE_SHOW_MSG: // 有些訊息要倒數，有些不必(如 fps)
                            if (waitingMs > 0) status = STATE_COUNTDOWN;
                            else status = STATE_NONE;
                            myHandler.sendEmptyMessage(STATE_SHOW_MSG);
                            break;
                        case STATE_TRAINING: // TRAINING 也需要倒數計時，但是它需要在完成時『拍照』
                            if (waitingMs <= 0) {
                                status = STATE_COMPLETED;
                                myHandler.sendEmptyMessage(STATE_TAKE_PIC);
                                break;
                            } else {
                                myHandler.sendEmptyMessage(STATE_TRAINING);
                                break;
                            }
                        case STATE_COUNTDOWN: // 純粹只是顯示訊息倒數後再清掉
                            if (waitingMs <= 0) {
                                status = STATE_COMPLETED;
                                myHandler.sendEmptyMessage(STATE_COMPLETED);
                                break;
                            } else {
                                myHandler.sendEmptyMessage(STATE_COUNTDOWN);
                                break;
                            }
                        case STATE_COMPLETED: // 倒數完，訊息要清掉
                            status = STATE_NONE;
                            break;
                        case STATE_HIDE_PICV:
                            if (waitingMs <= 0) {
                                status = STATE_NONE;
                                myHandler.sendEmptyMessage(STATE_HIDE_PICV);
                            }
                            break;
                        default: // STATE_NONE??
                            break;
                    }
                    sleep(1000);
                }
            }
        }).start();
    }

    private static class MyHandler extends Handler {
        private HdrViewfinderActivity HdrViewfinderActivity;
        private final WeakReference<HdrViewfinderActivity> HdrViewfinderActivityWeakReference;
        TextView tvMsg;

        public MyHandler(HdrViewfinderActivity activity) {
            HdrViewfinderActivity = activity;
            HdrViewfinderActivityWeakReference = new WeakReference<>(activity);
            tvMsg = HdrViewfinderActivity.findViewById(R.id.tvMsg);
        }

        @Override
        public void handleMessage(Message message) {
            switch (message.what) {
                case STATE_CLEAR_MSG:
                    tvMsg.setText("");
                    break;
                case STATE_SHOW_MSG:
                    tvMsg.setText(waitingMsg);
                    break;
                case STATE_TRAINING:
                case STATE_COUNTDOWN:
                    tvMsg.setText("時間("+(waitingMs/1000)+"秒)倒數中\n請確認晶盤狀態是否正確");
                    break;
                case STATE_TAKE_PIC: // 原本有個拍照動作，但是在 HdrViewfinderActivity#process() 中會因狀態，將照片存到 Allocation, 目前不拍照
                    tvMsg.setText("");
                    break;
                case STATE_COMPLETED: // 倒數完，訊息要清掉
                    tvMsg.setText("");
                    break;
                default: // STATE_NONE??
                    break;
            }
        }
    }
}
