package com.wade.wafercv2;

public class Variance {
    private int count;
    Variance(ScriptC_hdr_merge scriptCHdrMerge) {
        ScriptField_Variance.Item item = new ScriptField_Variance.Item();
        item.count = 0;
        scriptCHdrMerge.set_variance(item);
    }
    public void set(int[] data) {
        count = data[0];
    }
    public int getCount() { return count; }
}
