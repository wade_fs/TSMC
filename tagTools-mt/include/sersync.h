#ifndef __SERSYNC_H__
#define __SERSYNC_H__

#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>

#define onlyexpect(a) expchar(a, 1)
#define isnumber(a) (a <='9' && a >= '0')
class serial_port;
class watcher;

class ring_buffer {
  /* Warning, we assume no overlapping when writting, 
     I mean, process reads enough, If not, a buffer of data will be lost.
   */

  int size;
  //  int needs_flush;
  unsigned char *buffer;
  unsigned char *read;
  unsigned char *write;

  pthread_mutex_t mutex;
  void LOCK();
  int TRYLOCK();
  void UNLOCK();
  int unlocked_putchar(char in);  /* Use only in a critical section */

public:
  //  int istime;
   watcher * owner;
  //  int flush();
  //  void notime();
  int clear();
  int ready();
  char get();
  int gettimeout(char *dest);
  int put(char in);
  int gets(char *, unsigned int size);
  int getstimeout(char *, unsigned int size);
  int puts(char *buff);
  //  int   getnumber();
  int hasdataavailable();
   ring_buffer(int size);
  ~ring_buffer();
};

class watcher {
  int needs_flush;              // output flushing mechanism
  int rfd;
  int wfd;
  int getout;
  int pipefd[2];

  /* Thread vars */
  pthread_attr_t tattr;
  pthread_t tid;
  void *arg;

  /* Mutex vars */
  pthread_mutex_t mutex;
  //  pthread_mutex_t flushmutex;
  void LOCK();
  int TRYLOCK();
  void UNLOCK();

  int please_flush();
  int is_flushed();
  int init_flush();

public:
   ring_buffer * recv_buffer;
  ring_buffer *send_buffer;
   watcher(int rfd, int wfd);
  ~watcher();
  serial_port *serial;          //int (*dataavailable)();

  void *wloop();
  int Spawn();
  int Terminate();

  /* Should wait up to some chars since last recchar and also up to some number of chars */
  int slowsend(char *cad);      /* Must wait 'til output is flushed */
  int slowsend(char *cad, unsigned long bytes);
  int echosend(char *cad);
  int echosend(char *cad, unsigned long bytes);
  int activesend(char *cad, unsigned long bytes);

  int expect(char *needle);
  int expchar(char a, int tryout);

  int get(char *);              // Locks
  int tryread(char *);          // tryes

  int flush_input();
  int flush_output();           /* Write all data to port */
  //  int fast_flush();
  int whendatabecomesavailable();
};

void *newthread(void *);        //class watcher *);

#endif // __SERSYNC_H__
