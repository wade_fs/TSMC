
#include <time.h>

extern "C" {

  int bogoloop(int it);

  int scalibrate();

  int sscalibrate(int a);

  int ssleep(int usecs);

}
