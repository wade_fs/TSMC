/**************************************************************************
 *                  rs232ioctls.h                                         *
 *                                                                        *
 *   This file is distributed under the GNU LGPL License, see LICENSE.TXT *
 *   for further details.                                                 *
 *                                                                        *
 *   2004 Santiago Palomino S�nchez-Manjavacas sps@acm.asoc.fi.upm.es     *
 **************************************************************************/

#ifndef __RS232IOCTLS_H__
#define __RS232IOCTLS_H__
extern "C" {
  int term_open(char *devname);

  int term_start(int fd);
  int term_stop();

  int rs232_setspeed(int fd, int spd);
  int rs232_getspeed(int fd);
}
#include <sys/param.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <stdio.h>
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#endif                          /* __ioctls__ */
