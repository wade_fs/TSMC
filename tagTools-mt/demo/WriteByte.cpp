#include <iostream>
#include <string>
#include <cstdlib>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <signal.h>
#include <dlfcn.h>
#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>

#include "ssleep.h"
#include "serial.h"
#include "rs232ioctls.h"
#include "cmd.h"
#include "utils.h"

int main(int argc, char** argv)
{   
  int speed;
  int ret = 0;
  int ch;
  int cfg_timeout = 1000000; // 0 for unlimited, unit is us (1.0e-6 s)

  cmd c = cmd();
  packet p;
  std::string res="";
  serial_port* port;

  if (argc != 3) {
    std::cerr << "Usage: " << argv[0] << " PORT SPEED\n\tPORT: /dev/ttyUSB0, SPEED: 9600\n";
    exit(1);
  }

  if ((speed = strtol(argv[2],NULL,10)) == NULL) {
    std::cerr << "Error parsing speed: " << argv[2] << std::endl;
    exit(2);
  }

  port = new serial_port(argv[1]);
  if (!port->isopened()) {
    std::cerr << "Error opening tty: " << argv[2] << std::endl;
    exit(3);
  }

  sscalibrate(4194);

  port->set_flow(FLOW_SSLEEP);
  if (port->setspeed(speed)) {
    std::cerr << "Error configuring port to " << speed << " bauds!" << std::endl;
    exit(4);
  }

  std::string write_string="";
  write_string.reserve(256);
  for (int c=0; c<=255; c++) write_string += std::string(1, static_cast<char>(c));

  std::cout << "Writing...." << std::endl;
  // ret = write(tty_fd, write_string.data(), write_string.size());
  ret = port->send((char*)write_string.data(), write_string.size());
  std::cout << "Write:\t" << utils::toHex(write_string) << std::endl;
  sync();

  ///////////////////////////////////////

  std::cout << "Reading...." << std::endl;
  res = c.readCmd(port);
  std::cout << "Read :\t" << utils::toHex(res) << std::endl;
  lseek(tty_fd, 0, SEEK_END);
}
