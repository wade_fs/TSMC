#include <iostream>
#include "cmd.h"
using namespace std;

int main(int argc, char* argv[])
{
  cmd c = cmd(CMD_DISPLAY_TIME);
  vector<packet> P = c.getPackets();

  for (auto&& p : P) {
    cout << "id=" << p.getId() << ", dir=" << p.dir() << ": " << p.getPinAscii() << " : " << p.getPinHex(' ') << endl;
  }

  cout << string(80, '=') << endl;
  vector<string> S = c.getPacketsString();

  for (auto&& s : S) {
    cout << s.size() << endl;
  }

  return 0;
}

