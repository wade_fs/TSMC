#include <iostream>
#include <functional>
#include <chrono>
#include <future>
#include <cstdio>

using namespace std;
class later
{
public:
    template <class callable, class... arguments>
    later(int after, bool async, callable&& f, arguments&&... args)
    {
        std::function<typename std::result_of<callable(arguments...)>::type()> task(std::bind(std::forward<callable>(f), std::forward<arguments>(args)...));

        // 這邊都是用 sleep_for(), 也可以改用 sleep_until()
        // this_thread::sleep_until(chrono::system_clock::now() + chrono::seconds(10));
        if (async)
        {
            std::thread([after, task]() {
                std::this_thread::sleep_for(std::chrono::milliseconds(after));
                task();
            }).detach();
        }
        else
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(after));
            task();
        }
    }

};

void test1(void)
{
    for (int i=0; i<10; i++) printf ("%d ", i);
    cout << endl;
    return;
}

void test2(int a)
{
    cout << a << endl;
    return;
}

int main()
{
    cout << "step 1" << endl;
    later later_test1(1000, false, &test1);
    cout << "step 2" << endl;
    later later_test2(1000, false, &test2, 101);
    cout << "step 3" << endl;

    return 0;
}
