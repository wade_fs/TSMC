/**************************************************************************
 *                  term.c                                                *
 *                                                                        *
 *   This file is distributed under the GNU LGPL License, see LICENSE.TXT *
 *   for further details.                                                 *
 *                                                                        *
 *   2004 Santiago Palomino S�nchez-Manjavacas sps@acm.asoc.fi.upm.es     *
 **************************************************************************/
#include <string>
#include "rs232ioctls.h"

#include <dlfcn.h>
/* According to POSIX 1003.1-2001 */
#include <sys/select.h>

/* According to earlier standards */
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>


#define SIOCSKEEPALIVE 0
#define MAX_B 1024
#define VERSION "2.0.1"

char BACK[] = { 0x08, 0x00};

struct termios tty_console;
struct termios tty_line;

struct termios tty_old_console;
struct termios tty_old_line;


int tty_fd;
void sigint(int a)
{
  term_stop();
  /*  term_stop(tty_fd); WARNING restored?*/

  fprintf(stderr,"Exitting by sigint double CTRL+C keypress\n");
  close(tty_fd);
  exit (0);
}

#define INIT 4
#define MAY_BE 2 
#define NORMAL_CODE       "[00m"
#define CONSOLE_CODE      "[01;34m"
#define LINE_CODE         "[01;31m"
#define NORMAL_CODE_LEN   6
#define CONSOLE_CODE_LEN  9
#define LINE_CODE_LEN     9

#define NOEXITVARNAME "TERMNOEXIT"  

using namespace std;
string toHex(char ca[], int n, bool withSpace) {
  const string lut = "0123456789ABCDEF";
  string output;
  output.reserve((withSpace?3:2) * n);
  for (int i=0; i<n; ++i) {
    const unsigned char c = ca[i];
    output.push_back(lut[c >> 4]);
    output.push_back(lut[c & 0x0F]);
    if (withSpace) output.push_back(' ');
  }
  return output;
}

int main(int argc, char **argv)
{
  int slow_on=0;
  char b[MAX_B]; 
  int r;
  fd_set readfs;       /* file descriptor set */                        
  int    maxfd;        /* maximum file desciptor used */                
  int    loop=INIT;    /* loop while TRUE */                            
  char echo_on = 0;
  //  char *speed="57600";
  char *devname;
  char byte;
  int longbyte;
  int speed;
  char * noexit = getenv(NOEXITVARNAME);
  signal(SIGINT, sigint);


  if(argc == 3)
    {
      devname=argv[1];

      if((speed = strtol(argv[2],NULL,10)))
	  {

	  }
      else
	{
	  fprintf(stderr, "Error parsing speed: %s", argv[2]);
	  exit(2);
	}
    }
  else
    {
      fprintf(stderr, "Usage: %s DEVICE SPEED",argv[0]);
      exit (1);
    }

  if ((tty_fd = term_open(devname)) < 0) 
    {
      fprintf(stderr, "Error opening: %s", devname);
      return(255); 
    }

  term_start(0);
  term_start(tty_fd);

  rs232_setspeed(tty_fd, speed);

  /* open_input_source opens a device, sets the port correctly, and  
     returns a file descriptor */                                    
  maxfd = (tty_fd)+1;  /* maximum bit entry (fd) to test */    
  loop  = INIT;

  printf("SPSTERM Ready MAXFD: %s\n\rSantiago Palomino 2005 <libserial-mt sf.net>\r\n\n\r", VERSION);

  /* loop for input */                                               
  while (loop) 
    {
      FD_ZERO(&readfs);
      FD_SET(tty_fd, &readfs);  /* set testing for source 1 */  
      FD_SET(0, &readfs);  /* set testing for source 2 */                                                                 
      /* block until input becomes available */                        
      select(maxfd, &readfs, NULL, NULL, NULL);                        
      if (slow_on)
	usleep(1000000);
      if (FD_ISSET(tty_fd, &readfs))         /* input from LINE available */   
	{
	  r=read(tty_fd,&b,MAX_B);
	  string s = toHex(b, r, true);
	  write(1,LINE_CODE, LINE_CODE_LEN);
	  write(1, s.data(), s.size());
	  write(1,NORMAL_CODE, NORMAL_CODE_LEN);
	}
      if (FD_ISSET(0, &readfs))         /* input from CONSOLE available */   
	{
	  r=read(0,&b,MAX_B);
	  if (slow_on)
	    usleep(1000000);
	  if (echo_on)
	    {

	      fprintf(stderr,"[%2X]", b[0]);
	      //write(1,&b,r); // echo
	      write(1,NORMAL_CODE, NORMAL_CODE_LEN);
	    }
	  if (loop == MAY_BE && r == 1)
	    {
	      switch(tolower(b[0]))
		{
		case 'e':
		  loop = INIT;
		  fprintf(stderr,"ECHO TOGGLE");
		  if (echo_on)
		    echo_on=0;
		  else
		    echo_on=1;
		  continue;
		case 's':
		  loop = INIT;
		  fprintf(stderr,"SLOW MODE");
		  if (slow_on)
		    slow_on=0;
		  else
		    slow_on=1;
		  continue;
		case 'x':
		  loop = INIT;
		  fprintf(stderr, "ENTER HEX BYTE");
		  scanf("%2x", &longbyte);
		  byte = (char)longbyte;
		  write(tty_fd,&byte,1); // echo
		  continue;
		case 0x03:
		  if (!noexit)
		    {
		      sigint(1);
		      exit(1);
		      break;
		    }
		  else
		    {
		      fprintf(stderr, "Not exitting since %s is set!\n", NOEXITVARNAME);
		    }
		default:
		  loop = INIT;
		  continue;
		};

	    }

	  if (loop == INIT && r==1 && b[0] == 0x03) 
	    {
	      loop=MAY_BE;
	      continue;
	    }
	  /*		if(dofirstwrite)
			{
			printf("as\r");
			dofirstwrite--;
			}*/
	  //write(2,&b,r);
	  //		write(0,BACK,1);
	  if (slow_on)
	    usleep(1000000);
	  write(2,CONSOLE_CODE, CONSOLE_CODE_LEN);
	  write(2,NORMAL_CODE, NORMAL_CODE_LEN);
	  write(tty_fd,&b,r);
	  
	}
	    

      // handle_input_from_source2();                        
    }
  /*	while (1)
	{
	a=getchar();
	if (a!=-1)
	printf("%c",a);
	}*/
  close(tty_fd);
  return 0;
}
