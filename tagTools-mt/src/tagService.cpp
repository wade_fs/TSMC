// http://libserial.sourceforge.net/x27.html
#include <iostream>
#include <string>
#include <cstdlib>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <signal.h>
#include <dlfcn.h>
#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>

#include "rs232ioctls.h"
#include "cmd.h"
#include "utils.h"

////////// #define //////////
#define clrscr() printf("\e[1;1H\e[2J");

#define DISPLAY_NONE	0
#define DISPLAY_RAW	1
#define DISPLAY_HEX	2
#define MAX_B		10

////////// 整體變數區 ///////////

int ret = 0;
int cfg_timeout = 1000000; // 0 for unlimited, unit is us (1.0e-6 s)

////////// 自訂函數區 ///////////

int main(int argc, char **argv)
{
  int speed;
  struct timeval timeout;
  cmd c = cmd();
  packet p;
  int ch;

  if (argc != 3) {
    cerr << "Usage: " << argv[0] << " PORT SPEED\n\tPORT: /dev/ttyUSB0, SPEED: 9600\n";
    exit(1);
  }

  if ((tty_fd = term_open(argv[1])) < 0) {
    fprintf(stderr, "Error opening: %s", argv[1]);
    return (255);
  }

  term_start(tty_fd);
  speed = strtol(argv[2], NULL, 10);
  rs232_setspeed(tty_fd, speed);

  std::string write_string="";
  write_string.reserve(256);
  for (int c=0; c<=255; c++) write_string += std::string(1, static_cast<char>(c));

  while (true) {
    std::string res = c.readCmd(tty_fd);
    if (res.compare(packets[PK_START_CMD]) == 0) {
      ret = write(tty_fd, packets[PK_ACK_START_CMD].data(), packets[PK_ACK_START_CMD].length());
      std::cout << "<PK_START_CMD>     " << utils::toHex(packets[PK_ACK_START_CMD], true) << std::endl;
    } else if (res.compare(packets[PK_ACK_START_CMD]) == 0) {
      std::cout << "<PK_ACK_START_CMD> ";
      ret = write(tty_fd, write_string.data(), write_string.length());
      std::cout << std::endl << "Write:\t" << utils::toHex(write_string) << std::endl;
    } else {
      ret = write(tty_fd, packets[PK_CMD_DONE].data(), packets[PK_CMD_DONE].length());
      std::cout << "<Other>     " << utils::toHex(packets[PK_CMD_DONE], true) << std::endl;
    }
    usleep(25000) ;
  };

  close(tty_fd);
  return 0;
}
