/**************************************************************************
 *                  serial.cpp                     $$                     *
 *                                                                        *
 *   This file is distributed under the GNU LGPL License, see LICENSE.TXT *
 *   for further details.                                                 *
 *                                                                        *
 *   2005 Santiago Palomino S�nchez-Manjavacas spssps@gmail.com           *
 **************************************************************************/

#include "serial.h"
#include <pthread.h>
#include "ssleep.h"

int serial_port::terminate()
{
  // cancel port thread
  if (W) {
    W->Terminate();
    fprintf(stderr, "Watcher is finished, so we unlock\n");
    W->recv_buffer->put('q');
    pthread_cond_signal(&condition_var);
  }

  fprintf(stderr, "Board is terminated\n");
  return true;
}

int serial_port::flush_input()
{
  int ret = 0;
  pthread_mutex_lock(&lock);
  if (W) {
    ret = W->flush_input();
  } else
    ret = 0;
  pthread_mutex_unlock(&lock);
  return ret;
}

int serial_port::flush_output()
{
  if (W) {
    return W->flush_output();
  } else
    return 0;
}

bool serial_port::set_flow(FLOW_TYPE a)
{
  if (a >= FLOW_NONE && a <= FLOW_XON) {
    flow_type = a;
    return true;
  } else {
    return false;
  }
}

int serial_port::send(char *cad)
{
  return send(cad, strlen(cad));
}

/*
cond_wait()
{
  // ATOMIC
  unlock();
  bloquear_hasta(condition_var_signal);
  lock();
}
*/
int serial_port::acquire()
{
  pthread_mutex_lock(&lock);
  while (owner)
    pthread_cond_wait(&condition_var, &lock);

  owner = 1;

  pthread_mutex_unlock(&lock);
  return 0;
}

int serial_port::release()
{
  pthread_mutex_lock(&lock);
  owner = 0;

  // Since we will not use the pending data on buffers, we allow it to be used.

  if (W->recv_buffer->hasdataavailable())
    pthread_cond_signal(&condition_var);

  pthread_mutex_unlock(&lock);

  return 0;
}

int serial_port::whendatabecomesavailable() // This is a callback
{
  /* This function is used when the serial port has something available.
     If there is no owner, we signal the waiter thread so it can read. */
#warning Not implemented. Needed?
  return 0;
}

int serial_port::acquirewithdata()
{
  pthread_mutex_lock(&lock);

  // Wait until two conditions:
  // (a) Something is available to be read, AND
  // (B) AND there is no one reading from the serial port.
  while (!(W->recv_buffer->hasdataavailable()) || owner)
    pthread_cond_wait(&condition_var, &lock);

  owner = 1;

  pthread_mutex_unlock(&lock);
  return 0;
}

int serial_port::send(char *cad, unsigned long bytes)
{
  //  fprintf(stderr, "Sending char*:[%x]", cad[0]);

  int ret = 0;
  switch (flow_type) {
  case FLOW_ECHO:
    ret = W->echosend(cad, bytes);
    break;
  case FLOW_NANO:
    ret = W->slowsend(cad, bytes);
    break;
  case FLOW_SSLEEP:
    ret = W->activesend(cad, bytes);
    break;
  default:
    ret = NOT_IMPLEMENTED;
    break;
  }
  return ret;
}

int serial_port::gets(char *cad, unsigned int max)
{
  int ret = 0;
  ret = W->recv_buffer->gets(cad, max);
  //  if (!W -> recv_buffer -> ready())
  //  dataavailable = 0;

  return ret;
}

int serial_port::getstimeout(char *cad, unsigned int max)
{
  int ret = 0;
  //printf("gt:%d\n", dataavailable);
  ret = W->recv_buffer->getstimeout(cad, max);
  //if (!ret)
  //  dataavailable = 0;
  return ret;
}

serial_port::serial_port(char *name)
{
  pthread_mutex_init(&lock, NULL);
  pthread_cond_init(&condition_var, NULL);
  owner = 0;
  //  pthread_mutex_attr att;
  // lock = PTHREAD_MUTEX_INITIALIZER;
  //  pthread_mutex_init(&lock, NULL);
  if ((rfd = term_open(name)) < 0) {
    wfd = -1;
    rfd = -1;
    W = NULL;
    //      fprintf(stderr,"Couldn't open port\n");
  } else {
    realtty = 1;
    wfd = rfd;
    //      term_start(0);
    rs232_setspeed(rfd, 9600);
    term_start(rfd);

    W = new watcher(rfd, wfd);
    W->serial = this;           //whendatabecomesavailable callback 
    //      fprintf(stderr, "Spawning Watcher\n");
    W->Spawn();
  }
  //  W = new watcher(rfd);
}

bool serial_port::setspeed(int spd)
{
  if (isopened())
    return rs232_setspeed(rfd, spd);
  else
    return 2;
}

serial_port::serial_port(int r, int w)
{
  pthread_mutex_init(&lock, NULL);
  pthread_cond_init(&condition_var, NULL);
  owner = 0;
  rfd = r;
  wfd = w;
  realtty = 0;
  W = new watcher(rfd, wfd);
  W->serial = this;             //whendatabecomesavailable; 
}

serial_port::~serial_port()
{
  if (W)
    delete(W);
  fprintf(stderr, "~serial_port\n");
}

bool serial_port::isopened()
{
  return (rfd != -1);
}
