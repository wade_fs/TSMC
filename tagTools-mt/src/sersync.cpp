/**************************************************************************
 *                  sersync.c                                             *
 *                                                                        *
 *   This file is distributed under the GNU LGPL License, see LICENSE.TXT *
 *   for further details.                                                 *
 *                                                                        *
 *   2004 Santiago Palomino S�nchez-Manjavacas spssps@gmail.com           *
 **************************************************************************/

#include <stdlib.h>
#include <pthread.h>

#include <stdio.h>
#include <string.h>
#include <time.h>

#ifndef pthread_yield
#include "sched.h"
#define pthread_yield sched_yield
#endif

#ifndef QTDEBUGGER

#ifdef DEBUGCOMMS
#define commdbgprint fprintf
#else
#define commdbgprint nulla
#endif

#ifdef DEBUGLIBSERIAL
#define stddbgser stderr
#define SERDEBUG 1
#define DEBUG fprintf
#define MUTEXDEBUG nulla
#else
#define DEBUG nulla
#define stddbgser stdnull
#define MUTEXDEBUG nulla
#endif

using namespace std;

int nulla(FILE *, const char *, ...)
{
  return 0;
}

#else
// Debug with QT app
#include "fds.h"

#endif
#define ERROR(a); /**/
#define INIT 1
#define OVERLAP 2
/* According to POSIX 1003.1-2001 */
#include <sys/select.h>
/* According to earlier standards */
#include <sys/time.h>
#include <sys/types.h>
/* Callback on nothing ? */
/* Timer for nothing */
#include "sersync.h"
#include "serial.h"
#include "ssleep.h"

void *newthread(void *o)
{
  ((class watcher *) o)->wloop();

  pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
  pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);
  return NULL;
}

//int pthread_yield(){return 0;}

#define RED_COLOR     "\033[0;31m"
#define BLUE_COLOR    "\033[0;34m"
#define NORMAL_COLOR  "\033[00m"
// PRE: All file descriptors are open and valid.
void *watcher::wloop()
{
  //until getout
  fd_set rfds;
  struct timeval tv;
  //  fprintf(stderr, "wloop pre\n");
  while (!(getout)) {
    int ret = 1;
    char aux;
    //      printf("loop rfd: %d wfd: %d\n", rfd, wfd);

    tv.tv_sec = 0;
    tv.tv_usec = 10000;
    FD_ZERO(&rfds);
    FD_SET(rfd, &rfds);
    FD_SET(pipefd[0], &rfds);

    ret = select((rfd > pipefd[0] ? rfd : pipefd[0]) + 1, &rfds, NULL, NULL, &tv);
    //      fprintf(stderr, ".");
    LOCK();
    // This is incorrect      is_flushed();
    switch (ret) {
    case 0:
      // Time out
      //    recv_buffer -> notime();
      break;
    case -1:
      fprintf(stderr, "Select err -1\n");
      break;
    default:
      //  DEBUG(stderr,"passing\n");
      if (FD_ISSET(rfd, &rfds)) {

        ret =::read(rfd, &aux, 1);
        //        usleep(1);

        // Receive
        commdbgprint(stderr, "%s%c%s", RED_COLOR, aux, NORMAL_COLOR);

        //needs_flush = 0;
        //if(needs_flush)
        //        fprintf(stderr, "from serial HEX[%x]\n", aux);
        //recv_buffer -> istime=-1;

        recv_buffer->put(aux);
        serial->whendatabecomesavailable(); //Callback

      }
      if (FD_ISSET(pipefd[0], &rfds)) {
        ret =::read(pipefd[0], &aux, 1);

        // Send
        //fprintf(stderr, "from pipe to heaven [%x] wfd:%d\n", aux, wfd);
        commdbgprint(stderr, "%s%c%s", BLUE_COLOR, aux, NORMAL_COLOR);

        ret =::write(wfd, &aux, 1);
        fflush(NULL);
        is_flushed();
        //        sleep (10);
      }
      /*
         else
         {
         if (ret == 1)
         {
         fprintf(stderr, "preSelect err else %d %x %d %d\n", ret, aux, wfd, pipefd[1]);
         ret = ::read(1, &aux, 1);
         fprintf(stderr, "Select err else %d %x %d %d\n", ret, aux, wfd, pipefd[1]);
         }
         } */
      break;
    };

    UNLOCK();
    pthread_yield();
  }

  DEBUG(stderr, "getting out\n");

  getout = 0;

  return NULL;

}

int watcher::Terminate()
{
  // This is called from another thread
  // first: Lock
  // second: Finish watch thread
  /*  getout = 1;

     // third: delete subobjects
     // fourth: Unlock
     fprintf(stderr, "watcher::Termination req\n");
     while (getout && tid)
     {
     pthread_yield();
     }
   */// Serial thread has now finished
  if (tid != 0)
    pthread_cancel(tid);

  recv_buffer->put('q');

  fprintf(stderr, "watcher is finished\n");
  return 0;
}

int watcher::Spawn()
{

  pthread_attr_init(&tattr);

  int ret = pthread_create(&tid, &tattr, newthread, (void *)this);
  switch (ret) {
  case 0:                      // Parent
    //    fprintf(stderr, "Spawn Parent\n");
    return ret;
    break;
  case -1:                     // Error
  default:
    fprintf(stderr, "libserial watcher::Spawn ERROR on pthread create [%d]\n", ret);
    ERROR(INIT);
    break;
  };
  return 0;
}

watcher::watcher(int r, int w)
{
  rfd = r;
  wfd = w;
  tid = 0;
  getout = 0;
  pipe(pipefd);
  recv_buffer = new ring_buffer(8192);
  send_buffer = new ring_buffer(8192);
  recv_buffer->owner = this;
  send_buffer->owner = this;
  serial = NULL;
}

watcher::~watcher()
{
  fprintf(stderr, "delete watcher thread\n");
  if (tid != 0)
    pthread_cancel(tid);

  //  getout = 1;
  //  pthread_join(th, retval);

  // Wait 'til thread finishes
  //  while(getout && tid);
  //  while(1)
  //  pthread_yield();

  //if (tid != 0)
  //    pthread_cancel(tid);

  //  if (recv_buffer)
  //  delete(recv_buffer);

}

int watcher::flush_input()
{
  DEBUG(stderr, "buffer cleared \n");
  return recv_buffer->clear();
}

int watcher::flush_output()
{
  DEBUG(stderr, "serial flush \n");
  fflush(NULL);
  //  return send_buffer->flush();
  return true;
}

int watcher::expect(char *needle)
{
  int salir = 0;
  char aux[512];
  char byte;
  int pos = 0;

  while (!salir) {
    byte = recv_buffer->get();

    //      printf("catenate [%c]",byte);
    /* catenate */
    aux[pos++] = byte;

    if (byte != 0x0a)
      continue;                 /* no end of string */

    aux[pos] = 0x00;

    //      DEBUG(stderr, "expecting complete cycle [%s] needed {%s} {%d}\n",aux, needle, ret);
    if (strstr(aux, needle))
      salir = 1;
    else
      pos = 0;
  }
  return salir;
}

/* add timeout */
int watcher::expchar(char a, int tryout)
{
  char aux;

  while (tryout) {
    aux = recv_buffer->get();
    if (aux == a) {
      DEBUG(stderr, "expecting char cycle ok[%c] [%c]\n", a, aux);

      return 1;
    } else
      DEBUG(stderr, "expecting char cycle failed[%c] [%c] -%d\n", a, aux, tryout);
    tryout--;
  }

  return 0;
  /* lacks EOF control. */
}

int watcher::slowsend(char *cad)
{
  while (*cad) {
    LOCK();
    ::write(pipefd[1], cad, 1);
    UNLOCK();
    fflush(NULL);
    usleep(1);
    DEBUG(stderr, "sent cad[%c]\n", *cad);
    /**/ cad++;
  }
  return 0;
}

int watcher::slowsend(char *cad, unsigned long bytes)
{
  unsigned long sent_bytes = 0;
  while (sent_bytes < bytes) {
    LOCK();
    needs_flush = 1;
    ::write(pipefd[1], cad, 1);
    usleep(1);
    UNLOCK();
    cad++;
    sent_bytes++;
  }
  return 0;
}

int watcher::activesend(char *cad, unsigned long bytes)
{
  unsigned long sent_bytes = 0;
  while (sent_bytes < bytes) {

    LOCK();

    init_flush();
    ::write(pipefd[1], cad, 1);
    //      fflush(NULL);

    UNLOCK();
    please_flush();

    ssleep(100);

    cad++;
    sent_bytes++;
  }

  return 0;
}

int watcher::echosend(char *cad)
{
  char c;
  while (*cad) {
    //      flush_input(rfd);
    recv_buffer->clear();
    LOCK();
    //      printf("we are locked\n");
    //sleep(1);
    ::write(pipefd[1], cad, 1);
    UNLOCK();
    printf("we are unlocked and waiting for char\n");
    c = recv_buffer->get();

    //sleep(10);
    if (*cad != c)
      DEBUG(stderr, "echosend error [%x][%x]\n", *cad, c);
    //  DEBUG(stderr," cad[%c]\n", *cad);/**/

    // flush_input(rfd);/*expchar(*cad, 3);*/

    /* read(0, &aux, 1); */
    //      sleep(1);
    cad++;
  }
  return 0;
}

int watcher::echosend(char *cad, unsigned long bytes)
{
  char c;
  unsigned long sent_bytes = 0;
  while (sent_bytes < bytes) {
    recv_buffer->clear();
    LOCK();

    ::write(pipefd[1], cad, 1);
    UNLOCK();
    printf("we are unlocked and waiting for char\n");
    c = recv_buffer->get();

    if (*cad != c)
      DEBUG(stderr, "echosend error [%x][%x]\n", *cad, c);

    cad++;
    sent_bytes++;
  }
  return 0;
}

void watcher::LOCK()
{

  //  fprintf(stderr, "w:Lock %d\n", pthread_self());
  pthread_mutex_lock(&mutex);
}

void watcher::UNLOCK()
{
  //  fprintf(stderr, "w:unLock\n");
  pthread_mutex_unlock(&mutex);
  //  pthread_yield();
}

int watcher::TRYLOCK()
{
  //  fprintf(stderr, "w:tryLock\n");
  return pthread_mutex_trylock(&mutex);
}

int watcher::whendatabecomesavailable()
{
  return serial->whendatabecomesavailable();
}

/************************************************************************/
/* Ring Buffer                                                          */
/************************************************************************/

int ring_buffer::hasdataavailable()
{
  return (read != write);
}

ring_buffer::ring_buffer(int isize) //, int pipes[2], int r, int w)
{
  size = isize;

  buffer = (unsigned char *)malloc(isize * sizeof(unsigned char));  // this shold use new instead
  pthread_mutex_init(&mutex, NULL); //x&attr); /* no data has been read */
  read = buffer;
  write = buffer;
  // printf("Ring this %d, r %d %d\n", this, read, write);
}

void ring_buffer::LOCK()
{
  MUTEXDEBUG(stderr, "RLock\n");
  pthread_mutex_lock(&mutex);
}

void ring_buffer::UNLOCK()
{
  MUTEXDEBUG(stderr, "RULock\n");
  pthread_mutex_unlock(&mutex);
  //  pthread_yield();
}

int ring_buffer::TRYLOCK()
{
  MUTEXDEBUG(stderr, "RTLock\n");
  return pthread_mutex_trylock(&mutex);
}

#define EOL 0x0a
int ring_buffer::gets(char *dest, unsigned int lim)
{
  char c = 0x00;
  unsigned int pos = 0;
  while (c != EOL && (pos < lim)) {
    c = get();
    dest[pos] = c;
    pos++;
  }
  dest[pos] = 0x00;
  return pos;

}

int ring_buffer::getstimeout(char *dest, unsigned int lim)
{
  char c = 0x00;
  int gotit = 0;
  unsigned int pos = 0;
  while (c != EOL && (pos < lim)) {
    gotit = gettimeout(&c);
    if (!gotit)
      return pos;
    dest[pos] = c;
    pos++;
  }
  dest[pos] = 0x00;
  return pos;

}

char ring_buffer::get()
{
  char r;
  while (read == write) ;

  LOCK();
  r = *read;
  read = (read + 1);            // increment
  if (read == buffer + size)    // overlap
  {
    read = buffer;
  }
  if (read != write)
    owner->whendatabecomesavailable();  //Callback
  UNLOCK();
  return r;
}

#include <sys/time.h>
#include <time.h>
int ring_buffer::gettimeout(char *dest)
{
  char r;
  struct timeval a0, a1;
  int distance;
  //  *write = 0x00;
  //  LOCK();
  int istime = 2;
  //  UNLOCK();

  gettimeofday(&a0, NULL);
  //printf("get this %d\n", this);
  while (read == write && (istime > 0)) {
    gettimeofday(&a1, NULL);
    distance = a1.tv_usec - a0.tv_usec + 1000000 * (a1.tv_sec - a0.tv_sec);
    if (distance > 300000)      // 300 milliseconds
    {
      istime = 0;
    }

#ifdef SERDEBUG
    //            fprintf(stderr, "gettimeout loop %d w-r[%d]\n", istime, write-read);
#endif
    pthread_yield();
  }

  if (!istime) {
#ifdef SERDEBUG
    fprintf(stderr, "timeout\n");
#endif
    return 0;
  }

  LOCK();

  r = *read;
  read++;                       // increment
  if (read == buffer + size)    // overlap
    read = buffer;

  if (read != write)
    owner->whendatabecomesavailable();  //Callback

  UNLOCK();

  *dest = r;
  return 1;
}

int ring_buffer::put(char in)
{
  //  fprintf(stderr, "rput\n");
  int ret = 1;
  LOCK();
  //data is available:
  ret = unlocked_putchar(in);
  UNLOCK();
  //fprintf(stderr, "rput ends\n");
  return ret;
}

inline int ring_buffer::unlocked_putchar(char in)
{
  int ret = 1;

  //data is available:
  *write = in;
  write++;
  if (write == buffer + size)
    write = buffer;
  if (write == read) {          // OVERLAPPED
    ERROR(OVERLAP);
    ret = 0;
  }

  return ret;
}

int watcher::init_flush()
{
  //needs_flush = 0;
  return 0;
}

int watcher::is_flushed()
{

  needs_flush = 0;
  return 0;
}

int watcher::please_flush()
{
  LOCK();
  UNLOCK();
  while (needs_flush) {
    fflush(NULL);
    pthread_yield();

  }
  return 0;
}

int ring_buffer::clear()
{
  int lost_bytes;

  //  fprintf(stderr, "pre clear\n");
  LOCK();
  //  fprintf(stderr, "post clear\n");
  //data is NOT available:
  if (read > write) {
    lost_bytes = write + size - read;
  } else
    lost_bytes = write - read;
  read = write;

  UNLOCK();
  return lost_bytes;
}

ring_buffer::~ring_buffer()
{
  pthread_mutex_destroy(&mutex);
  free(buffer);
}

int ring_buffer::ready()
{
  return (read != write);
}
