#include <ctype.h>
#include "utils.h"

using namespace std;
const string utils::lut = "0123456789ABCDEF";
string utils::fromBytes(char ca[], int n) {
  string tmp(ca, n);
  return tmp;
}

string utils::toHex(std::vector<unsigned char>ca, bool withSpace) {
  string output;
  output.reserve((withSpace?3:2) * ca.size());
  for (std::vector<unsigned char>::iterator i=ca.begin(); i!=ca.end(); ++i) {
    const unsigned char c = *i;
    output.push_back(lut[c >> 4]);
    output.push_back(lut[c & 0x0F]);
    if (withSpace) output.push_back(' ');
  }
  return trim(output);
}
string utils::toHex(char ca[], int n, bool withSpace) {
  string output;
  output.reserve((withSpace?3:2) * n);
  for (int i=0; i<n; ++i) {
    const unsigned char c = ca[i];
    output.push_back(lut[c >> 4]);
    output.push_back(lut[c & 0x0F]);
    if (withSpace) output.push_back(' ');
  }
  return trim(output);
}
string utils::toHex(string s, bool withSpace) {
  int n = s.size();
  string output;
  output.reserve((withSpace?3:2) * n);
  for (int i=0; i<n; ++i) {
    const unsigned char c = s[i];
    output.push_back(lut[c >> 4]);
    output.push_back(lut[c & 0x0F]);
    if (withSpace) output.push_back(' ');
  }
  return trim(output);
}
string utils::fromHex(string in, bool withDelim) {
  string output;

  size_t cnt = (in.length()+1) / (withDelim?3:2);

  for (size_t i = 0; cnt > i; ++i) {
    uint32_t s = 0;
    stringstream ss;
    ss << std::hex << in.substr(i * (withDelim?3:2), 2);
    ss >> s;

    output.push_back(static_cast<unsigned char>(s));
  }

  return output;
}
string utils::toAscii(string s, char delim) {
  int n = s.size();
  string output;
  output.reserve(n);
  for (int i=0; i<n; ++i) {
    const unsigned char c = s[i];
    if (isprint(c)) output.push_back(c);
    else output.push_back(delim);
  }
  return output;
}
string utils::trim(const std::string& s, int which) {
  size_t first = 0, last = s.size()-1;
  if (last < 0) return "";
  if (which != 2) { // head
    first = s.find_first_not_of(' ');
  }
  if (which != 1) { // tail
    last = s.find_last_not_of(' ');
  }
  if (first < last)
    return s.substr(first, (last - first + 1));
  else return "";
}
