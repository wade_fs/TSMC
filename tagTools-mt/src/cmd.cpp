#include <iostream>
#include <iomanip>
#include <string>
#include <unistd.h>

#include "serial.h"
#include "cmd.h"

using namespace std;

unordered_map <int, vector<int>> cmds = {
  { CMD_DISPLAY_TIME, {
      PK_START_CMD,
      PK_ACK_START_CMD,
      PK_DISPLAY_TIME,
      PK_CMD_DONE
  }},
  { CMD_READ_TIME, {
      PK_START_CMD,
      PK_ACK_START_CMD,
      PK_READ_DONE,
      PK_CMD_DONE,
      PK_READ_TIME1,
      PK_READ_DONE
  }},
  { CMD_SET_TIME, {
      PK_START_CMD,
      PK_ACK_START_CMD,
      PK_SET_TIME,
      PK_CMD_DONE
  }},
  { CMD_NORMAL_STATUS_READ, {
      PK_START_CMD,
      PK_ACK_START_CMD,
      PK_NORMAL_STATUS_READ,
      PK_CMD_DONE,
      PK_NORMAL_STATUS,
      PK_READ_DONE
  }},
  { CMD_EXTENDED_STATUS1_READ, {
      PK_START_CMD,
      PK_ACK_START_CMD,
      PK_NORMAL_STATUS_READ,
      PK_CMD_DONE,
      PK_NORMAL_STATUS,
      PK_READ_DONE
  }},
  { CMD_EXTENDED_STATUS2_READ, {
      PK_START_CMD,
      PK_ACK_START_CMD,
      PK_ENTENDED_STATUS2_READ,
      PK_CMD_DONE,
      PK_EXTENDED_STATUS2,
      PK_READ_DONE
  }},
  { CMD_PROTECTED_DATA_READ, {
      PK_START_CMD,
      PK_ACK_START_CMD,
      PK_PROTECTED_DATA_READ,
      PK_CMD_DONE,
      PK_PROTECTED_DATA,
      PK_READ_DONE
  }},
  { CMD_WRITE_LCD_DATA, {
      PK_START_CMD,
      PK_ACK_START_CMD,
      PK_PROTECTED_DATA_READ,
      PK_CMD_DONE,
      PK_PROTECTED_DATA,
      PK_READ_DONE
  }},
  { CMD_READ_LCD_DATA, {
      PK_START_CMD,
      PK_START_CMDr,
      PK_ACK_START_CMD,
      PK_READ_LCD_DATA,
      PK_CMD_DONE,
      PK_LCD_DATAvr
  }},
  { CMD_LCD_DISPLAY_ON, {
      PK_START_CMD,
      PK_ACK_START_CMD,
      PK_LCD_DISPLAY_ON,
      PK_CMD_DONE
  }},
  { CMD_LCD_DISPLAY_OFF, {
      PK_START_CMD,
      PK_ACK_START_CMD,
      PK_LCD_DISPLAY_OFF,
      PK_CMD_DONE
  }},
  { CMD_CLEAR_LCD, {
      PK_START_CMD,
      PK_ACK_START_CMD,
      PK_CLEAR_LCD,
      PK_CMD_DONE
  }},
  { CMD_ENABLE_TIMEOUT, {
      PK_START_CMD,
      PK_ACK_START_CMD,
      PK_ENABLE_TIME_OUT,
      PK_CMD_DONE
  }},
  { CMD_DISABLE_TIMEOUT, {
      PK_START_CMD,
      PK_ACK_START_CMD,
      PK_DISABLE_TIME_OUT,
      PK_CMD_DONE
  }},
  { CMD_DISPLAY_SERIAL_NUMBER, {
      PK_START_CMD,
      PK_ACK_START_CMD,
      PK_DISPLAY_SERIAL_NUMBER,
      PK_CMD_DONE
  }},
  { CMD_GET_TAG_FORMAT, {
      PK_START_CMD,
      PK_ACK_START_CMD,
      PK_GET_TAG_FORMAT,
      PK_CMD_DONE,
      PK_TAG_FORMAT,
      PK_READ_DONE
  }},
  { CMD_FORMAT_TAG, {
      PK_START_CMD,
      PK_ACK_START_CMD,
      PK_FORMAT_TAG,
      PK_CMD_DONE,
      PK_FORMAT,
      PK_CMD_DONE
  }},
};

cmd::cmd() {
  id = -1;
}

cmd::cmd(int ID) {
  if (cmds.find(ID) != cmds.end()) id = ID;
  else id = -1;
}

std::string cmd::readCmd(serial_port* port) {
  char buf[1024];
  int c;

  c = port->getstimeout(buf, 1);
  if (c > 0) return string(buf, c);
  return "";
}

int cmd::doCmd(serial_port* port) {
  if (id < 0) return -1;
  packet p;
  std::string res="";
  int ret;

  cout << "cmd length = " << cmds[id].size() << "\r\n";
  for (auto&& i:cmds[id]) { // packet(i) 產生 packet, packet[i] 產生 該 packet 的 string
    p = packet(i);
    cout << setw(4) << p.getId() << " : ";
    if (p.dir() == PACKET_DIR_OUT) {
      // ret = write(tty_fd, packets[i].data(), packets[i].length());
      ret = port->send((char*)packets[i].data(), packets[i].length());
    } else {
      res = readCmd(port);
      ret = res.compare("") == 0 ? 0 : res.size();
    }
    // if (res != packet::PK_OK) break; // send() read() 到底應該傳回 n 還是 0?
  }
  return ret;
}

////////////// 底下應該是測試才會用到 /////////////////////
vector<int> cmd::getPacketIDs() {
  if (id >= 0) return cmds[id];
  else return vector<int>();
}

vector<packet> cmd::getPackets() {
  if (id < 0) return vector<packet>();
  vector<packet> res;
  for (auto&& i:cmds[id]) {
    res.push_back(packet(i)); // 這邊是 packet(i), 傳回 packet
  }
  return res;
}

vector<string> cmd::getPacketsString() {
  if (id < 0) return vector<string>();
  vector<string> res;
  for (auto&& i:cmds[id]) {
    res.push_back(packets[i]); // 這邊是 packet[i], 傳回 string
  }
  return res;
}
