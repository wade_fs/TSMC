#include <iostream>
#include <iomanip>
#include <string>
#include "cmd.h"

using namespace std;
using namespace LibSerial;

int cmd::CMD_OK = 0;
unordered_map <int, vector<int>> cmds = {
  { CMD_DISPLAY_TIME, {
      PK_START_CMD,
      PK_ACK_START_CMD,
      PK_DISPLAY_TIME,
      PK_CMD_DONE
  }},
  { CMD_READ_TIME, {
      PK_START_CMD,
      PK_ACK_START_CMD,
      PK_READ_DONE,
      PK_CMD_DONE,
      PK_READ_TIME1,
      PK_READ_DONE
  }},
  { CMD_SET_TIME, {
      PK_START_CMD,
      PK_ACK_START_CMD,
      PK_SET_TIME,
      PK_CMD_DONE
  }},
  { CMD_NORMAL_STATUS_READ, {
      PK_START_CMD,
      PK_ACK_START_CMD,
      PK_NORMAL_STATUS_READ,
      PK_CMD_DONE,
      PK_NORMAL_STATUS,
      PK_READ_DONE
  }},
  { CMD_EXTENDED_STATUS1_READ, {
      PK_START_CMD,
      PK_ACK_START_CMD,
      PK_NORMAL_STATUS_READ,
      PK_CMD_DONE,
      PK_NORMAL_STATUS,
      PK_READ_DONE
  }},
  { CMD_EXTENDED_STATUS2_READ, {
      PK_START_CMD,
      PK_ACK_START_CMD,
      PK_ENTENDED_STATUS2_READ,
      PK_CMD_DONE,
      PK_EXTENDED_STATUS2,
      PK_READ_DONE
  }},
  { CMD_PROTECTED_DATA_READ, {
      PK_START_CMD,
      PK_ACK_START_CMD,
      PK_PROTECTED_DATA_READ,
      PK_CMD_DONE,
      PK_PROTECTED_DATA,
      PK_READ_DONE
  }},
  { CMD_WRITE_LCD_DATA, {
      PK_START_CMD,
      PK_ACK_START_CMD,
      PK_PROTECTED_DATA_READ,
      PK_CMD_DONE,
      PK_PROTECTED_DATA,
      PK_READ_DONE
  }},
  { CMD_READ_LCD_DATA, {
      PK_START_CMD,
      PK_START_CMDr,
      PK_ACK_START_CMD,
      PK_READ_LCD_DATA,
      PK_CMD_DONE,
      PK_LCD_DATAvr
  }},
  { CMD_LCD_DISPLAY_ON, {
      PK_START_CMD,
      PK_ACK_START_CMD,
      PK_LCD_DISPLAY_ON,
      PK_CMD_DONE
  }},
  { CMD_LCD_DISPLAY_OFF, {
      PK_START_CMD,
      PK_ACK_START_CMD,
      PK_LCD_DISPLAY_OFF,
      PK_CMD_DONE
  }},
  { CMD_CLEAR_LCD, {
      PK_START_CMD,
      PK_ACK_START_CMD,
      PK_CLEAR_LCD,
      PK_CMD_DONE
  }},
  { CMD_ENABLE_TIMEOUT, {
      PK_START_CMD,
      PK_ACK_START_CMD,
      PK_ENABLE_TIME_OUT,
      PK_CMD_DONE
  }},
  { CMD_DISABLE_TIMEOUT, {
      PK_START_CMD,
      PK_ACK_START_CMD,
      PK_DISABLE_TIME_OUT,
      PK_CMD_DONE
  }},
  { CMD_DISPLAY_SERIAL_NUMBER, {
      PK_START_CMD,
      PK_ACK_START_CMD,
      PK_DISPLAY_SERIAL_NUMBER,
      PK_CMD_DONE
  }},
  { CMD_GET_TAG_FORMAT, {
      PK_START_CMD,
      PK_ACK_START_CMD,
      PK_GET_TAG_FORMAT,
      PK_CMD_DONE,
      PK_TAG_FORMAT,
      PK_READ_DONE
  }},
  { CMD_FORMAT_TAG, {
      PK_START_CMD,
      PK_ACK_START_CMD,
      PK_FORMAT_TAG,
      PK_CMD_DONE,
      PK_FORMAT,
      PK_CMD_DONE
  }},
};

cmd::cmd() {
  id = -1;
}

cmd::cmd(int ID) {
  if (cmds.find(ID) != cmds.end()) id = ID;
  else id = -1;
}

//// TODO 主要的部份 ////
int cmd::doCmd(SerialPort& serial_port) {
  if (id < 0) return -1;
  packet p;
  int res = CMD_OK;
  cout << "cmd length = " << cmds[id].size() << "\r\n";
  for (auto&& i:cmds[id]) { // packet(i) 產生 packet, packet[i] 產生 該 packet 的 string
    p = packet(i);
    cout << setw(4) << p.getId() << " : ";
    if (p.dir() == PACKET_DIR_OUT) {
      // p.send(); res = 0; // send() 沒有傳回值
      serial_port.Write(packets[i]);
    } else {
      int r=-1;
      try {
        std::string str = "";
        serial_port.Read(str, 0, 25);
        cout << "R[ " << str << " ]\r\n";
        cout << "O< " << p.getPinHex(' ') << " >\r\n";
        if (packets[i].compare(str) != 0) r = -1;
        else r = str.size();
      } catch (ReadTimeout e) {
        cerr << "catch read timeout exception!" << endl;
      }

      cout << "\r\n";
    }
    // if (res != packet::PK_OK) break; // send() read() 到底應該傳回 n 還是 0?
  }
  return res;
}

////////////// 底下應該是測試才會用到 /////////////////////
vector<int> cmd::getPacketIDs() {
  if (id >= 0) return cmds[id];
  else return vector<int>();
}

vector<packet> cmd::getPackets() {
  if (id < 0) return vector<packet>();
  vector<packet> res;
  for (auto&& i:cmds[id]) {
    res.push_back(packet(i)); // 這邊是 packet(i), 傳回 packet
  }
  return res;
}

vector<string> cmd::getPacketsString() {
  if (id < 0) return vector<string>();
  vector<string> res;
  for (auto&& i:cmds[id]) {
    res.push_back(packets[i]); // 這邊是 packet[i], 傳回 string
  }
  return res;
}
