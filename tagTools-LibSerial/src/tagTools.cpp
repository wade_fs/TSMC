// http://libserial.sourceforge.net/x27.html
#include <iostream>
#include <cstdlib>

#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <string.h>

#include "cmd.h"

using namespace LibSerial;

////////// #define //////////

#define NORMAL_CODE       "\e[00m"
#define CONSOLE_CODE      "\e[01;34m"
#define LINE_CODE         "\e[01;31m"
#define NORMAL_CODE_LEN   6
#define CONSOLE_CODE_LEN  9
#define LINE_CODE_LEN     9
#define NOEXITVARNAME "TERMNOEXIT"

#define clrscr() printf("\e[1;1H\e[2J");

#define DISPLAY_NONE    0
#define DISPLAY_RAW     1
#define DISPLAY_HEX     2
#define MAX_B           10

////////// 整體變數區 ///////////

int ret=0;
int cfg_display_com_in_mode = DISPLAY_RAW;
int cfg_timeout = 1000000; // 0 for unlimited, unit is us (1.0e-6 s)

////////// 自訂函數區 ///////////

void menu() {
  printf ("o) 切換顯示從 Com Port 讀到的資料的模式 : None / Hex / Raw(%s)\r\n"
          "a) 增加遞延時間(%d)\r\n"
          "m) 減少遞延時間(%d)\r\n"
          "c) cmd: Clear LCD\r\n"
          "+) cmd: LCD Display On\r\n"
          "-) cmd: LCD Display Off\r\n"
          "r) cmd: Read LCD Data\r\n"
          "w) cmd: Write LCD Data\r\n"
          "t) cmd: Display Time\r\n"
          "T) cmd: Read Time\r\n"
          "s) cmd; Set Time\r\n"
          "n) cmd: Normal Status Read\r\n"
          "1) cmd: Extended Status1 Read\r\n"
          "2) cmd: Extended Status2 Read\r\n"
          "p) cmd: Protected Data Read\r\n"
          "e) cmd: Enable Timeout\r\n"
          "d) cmd: Disable Timeout\r\n"
          "#) cmd: Display Serial Number\r\n"
          "f) cmd: Get Tag Format\r\n"
          "F) cmd: Format Tag\r\n"
          "q) Quit\r\n"
          "(%d)> ",
          (cfg_display_com_in_mode==DISPLAY_NONE?"None": (cfg_display_com_in_mode==DISPLAY_RAW?"Raw":"Hex")),
          cfg_timeout, cfg_timeout, ret
  );
  fflush(stdout);
}
#if 0
void readReply(SerialStream& serial_stream) {
#ifdef COMMENT_ONLY
  // 具有 timeout 機制的作法如下:
  int timeout_ms = 25; // timeout value in milliseconds
  char next_char; // variable to store the ReadByte() result
  next_char = my_serial_port.ReadByte( timeout_ms );
  // 如果不要 timeout 的話，可以用 my_serial_stream.read( next_char ); 代替
  // 同樣，以 c++ 標準寫法的話，同 my_serial_stream >> next_char; 
  // 也可以用來讀不同型態的: int d; my_serial_stream >> d;
  // 同樣的，也可以用 string 型態: my_serial_port.Write( my_string ); 或 my_serial_stream << my_string << std::endl ;
#endif
  // Wait for some data to be available at the serial port.
  while( serial_stream.rdbuf()->in_avail() == 0 ) {
    usleep(100) ;
  }

  // Keep reading data from serial port and print it to the screen.
  while( serial_stream.rdbuf()->in_avail() > 0 ) {
    char nextByte;
    serial_stream.get(nextByte);
    std::cerr << std::hex << static_cast<int>( nextByte ) << " " ;
    usleep(100) ;
  }
  // 如果要一次讀多個 bytes:
  /*
    const int BUFFER_SIZE = 256 ;
    char buffer[BUFFER_SIZE] ; 
    my_serial_stream.read(buffer, BUFFER_SIZE);
    my_serial_stream.write(buffer, BUFFER_SIZE);
  */
}
#endif
int kb_hit(void) {
  struct termios oldt, newt;
  struct timeval tv;
  fd_set fds;
  int hit, oldf;

  // 設定成 non-blocking，並存起來，後面要還原
  tcgetattr(STDIN_FILENO, &oldt);
  newt = oldt;
  newt.c_lflag &= ~(ICANON | ECHO);
  tcsetattr(STDIN_FILENO, TCSANOW, &newt);
  oldf = fcntl(STDIN_FILENO, F_GETFL, 0);
  fcntl(STDIN_FILENO, F_SETFL, oldf | O_NONBLOCK);

  while (1) {
    tv.tv_sec = 0;
    tv.tv_usec = 0;
    FD_ZERO(&fds);
    FD_SET(STDIN_FILENO, &fds);
    select(STDIN_FILENO+1, &fds, NULL, NULL, &tv);
    if ((hit = FD_ISSET(STDIN_FILENO, &fds))) {
      // 然後把該按鍵傳回去
      int ch = fgetc(stdin);
      if (ch == 27) {
        ch = fgetc(stdin);
        if (ch == 79 || ch == 91) ch = fgetc(stdin); // ESC 鍵是複合鍵，鍵值有時是兩個 bytes, 我們不處理
        FD_SET(STDIN_FILENO, &fds);
        select(STDIN_FILENO+1, &fds, NULL, NULL, &tv);
      }
      while (FD_ISSET(STDIN_FILENO, &fds)) {
        fgetc(stdin);
        FD_SET(STDIN_FILENO, &fds);
        select(STDIN_FILENO+1, &fds, NULL, NULL, &tv);
      }
      // 還原 stdin 的 non-blocking 模式為正常模式
      fcntl(STDIN_FILENO, F_SETFL, oldf);
      tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
      return ch;
    }
  }
}

////////// 主程式區 //////////

int main(int argc, char* argv[])
{
  cmd c;
  packet p;
  int ch;

  if (argc != 2) {
    cerr << "Usage: " << argv[0] << " PORT\r\n\tPORT: /dev/ttyUSB0 or /dev/ttyS2\r\n";
    exit(1);
  }
  SerialPort serial_port;
  serial_port.Open(argv[1]);
  if ( !serial_port.IsOpen() ) {
    cerr << "[" << __FILE__ << ":" << __LINE__ << "] "
         << "Error: Could not open serial port."
         << endl ;
    exit(2) ;
  }
  
  serial_port.SetBaudRate(BaudRate::BAUD_9600);
  serial_port.SetCharacterSize(CharacterSize::CHAR_SIZE_8 ) ;
  serial_port.SetParity(Parity::PARITY_NONE ) ;
  serial_port.SetStopBits(StopBits::STOP_BITS_1);
  serial_port.SetFlowControl(FlowControl::FLOW_CONTROL_NONE);

  while (true) {
    clrscr();
    menu();
    ch = kb_hit();
    // dispatch command
    switch (ch) {
    case 'o':		// 切換顯示從 Com Port 讀到的資料的模式 : None / Hex / Raw
      cfg_display_com_in_mode = (cfg_display_com_in_mode + 1) % 3;
      continue;	// 這邊用 continue 則會立刻下一個迴圈，不會執行 switch() 後面的敘述
    case 'a':		// 增加遞延時間
      if (cfg_timeout == 0) cfg_timeout = 1000;
      else if (cfg_timeout < 512000) cfg_timeout <<= 1;
      else cfg_timeout += 100000;
      continue;	// 這邊用 continue 則會立刻下一個迴圈，不會執行 switch() 後面的敘述
    case 'm':		// 減少遞延時間
      if (cfg_timeout <= 1000) cfg_timeout = 0;
      else if (cfg_timeout <= 512000) cfg_timeout >>= 1;
      else cfg_timeout -= 100000;
      continue;	// 這邊用 continue 則會立刻下一個迴圈，不會執行 switch() 後面的敘述
    case 'c':		// cmd: Clear LCD
      cout << "\r\n";
      c = cmd(CMD_CLEAR_LCD);
      ret = c.doCmd(serial_port);
      continue;
    case '+':		// cmd: LCD Display On
      cout << "\r\n";
      c = cmd(CMD_LCD_DISPLAY_ON);
      ret = c.doCmd(serial_port);
      continue;
    case '-':		// cmd: LCD Display Off
      cout << "\r\n";
      c = cmd(CMD_LCD_DISPLAY_OFF);
      ret = c.doCmd(serial_port);
      continue;
    case 'r':		// cmd: Read LCD Data
      cout << "\r\n";
      c = cmd(CMD_READ_LCD_DATA);
      ret = c.doCmd(serial_port);
      continue;
    case 'w':		// cmd: Write LCD Data
      cout << "\r\n";
      c = cmd(CMD_WRITE_LCD_DATA);
      ret = c.doCmd(serial_port);
      continue;
    case 't':		// cmd: Display Time
      cout << "\r\n";
      c = cmd(CMD_DISPLAY_TIME);
      ret = c.doCmd(serial_port);
      continue;
    case 'T':		// cmd: Read Time
      cout << "\r\n";
      c = cmd(CMD_READ_TIME);
      ret = c.doCmd(serial_port);
      continue;
    case 's':		// cmd: Set Time
      cout << "\r\n";
      c = cmd(CMD_SET_TIME);
      ret = c.doCmd(serial_port);
      continue;
    case 'n':		// cmd: Normal Status Read
      cout << "\r\n";
      c = cmd(CMD_NORMAL_STATUS_READ);
      ret = c.doCmd(serial_port);
      continue;
    case '1':		// cmd: Extended Status1 Read
      cout << "\r\n";
      c = cmd(CMD_EXTENDED_STATUS1_READ);
      ret = c.doCmd(serial_port);
      continue;
    case '2':		// cmd: Extended Status2 Read
      cout << "\r\n";
      c = cmd(CMD_EXTENDED_STATUS2_READ);
      ret = c.doCmd(serial_port);
      continue;
    case 'p':		// cmd: Protected Data Read
      cout << "\r\n";
      c = cmd(CMD_PROTECTED_DATA_READ);
      ret = c.doCmd(serial_port);
      continue;
    case 'e':		// cmd: Enable Timeout
      cout << "\r\n";
      c = cmd(CMD_ENABLE_TIMEOUT);
      ret = c.doCmd(serial_port);
      continue;
    case 'd':		// cmd: Disnable Timeout
      cout << "\r\n";
      c = cmd(CMD_DISABLE_TIMEOUT);
      ret = c.doCmd(serial_port);
      continue;
    case '#':		// cmd: Display Serial Number
      cout << "\r\n";
      c = cmd(CMD_DISPLAY_SERIAL_NUMBER);
      ret = c.doCmd(serial_port);
      continue;
    case 'f':		// cmd: Get Tag Format
      cout << "\r\n";
      c = cmd(CMD_GET_TAG_FORMAT);
      ret = c.doCmd(serial_port);
      continue;
    case 'F':		// cmd: Format Tag
      cout << "\r\n";
      c = cmd(CMD_FORMAT_TAG);
      ret = c.doCmd(serial_port);
      continue;
    case 'x':		// send packet : Start Command = FC FF
      cout << "\r\n";
      serial_port.Write(packets[PK_START_CMD]);
      continue;
    case 'y':		// read packet : PK_READ_LCD_DATA = FF FF EF FF F7 FF FF 07 EF 7F 7F 63 FF FF FF
      cout << "\r\n";
      serial_port.Write(packets[PK_READ_LCD_DATA]);
      continue;
    case 'z':		// send packet : PK_GET_TAG_FORMAT = FF FF EF FF F7 FF 7F FF 87 FF 7F F3 FF FF FF
      cout << "\r\n";
      serial_port.Write(packets[PK_GET_TAG_FORMAT]);
      continue;
    case 'q':
      return EXIT_SUCCESS;
    default:
      ret = -1;
      continue;
    };
  };

  std::cerr << std::endl;
  return EXIT_SUCCESS;
}
