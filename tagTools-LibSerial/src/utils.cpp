#include "utils.h"

using namespace std;
const string utils::lut = "0123456789ABCDEF";
string utils::fromBytes(char ca[], int n) {
  string tmp(ca, n);
  return tmp;
}

string utils::toHex(std::vector<unsigned char>ca, bool withSpace) {
  string output;
  output.reserve((withSpace?3:2) * ca.size());
  for (std::vector<unsigned char>::iterator i=ca.begin(); i!=ca.end(); ++i) {
    const unsigned char c = *i;
    output.push_back(lut[c >> 4]);
    output.push_back(lut[c & 0x0F]);
    if (withSpace) output.push_back(' ');
  }
  return output;
}
string utils::toHex(char ca[], int n, bool withSpace) {
  string output;
  output.reserve((withSpace?3:2) * n);
  for (int i=0; i<n; ++i) {
    const unsigned char c = ca[i];
    output.push_back(lut[c >> 4]);
    output.push_back(lut[c & 0x0F]);
    if (withSpace) output.push_back(' ');
  }
  return output;
}
string utils::toHex(unsigned char c) {
  string output;
  output.reserve(2);
  output.push_back(lut[((unsigned char)c) >> 4]);
  output.push_back(lut[c & 0x0F]);
  return output;
}
string utils::toHex(string s, bool withSpace) {
  int n = s.size();
  string output;
  output.reserve((withSpace?3:2) * n);
  for (int i=0; i<n; ++i) {
    const unsigned char c = s[i];
    output.push_back(lut[c >> 4]);
    output.push_back(lut[c & 0x0F]);
    if (withSpace) output.push_back(' ');
  }
  return output;
}
string utils::fromHex(string in, bool withDelim) {
  string output;

  size_t cnt = (in.length()+1) / (withDelim?3:2);

  for (size_t i = 0; cnt > i; ++i) {
    uint32_t s = 0;
    stringstream ss;
    ss << std::hex << in.substr(i * (withDelim?3:2), 2);
    ss >> s;

    output.push_back(static_cast<unsigned char>(s));
  }

  return output;
}
