// http://libserial.sourceforge.net/x27.html
#include <iostream>
#include <cstdlib>
#include <string>

#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include "SerialPort.h"
#include "utils.h"

using namespace LibSerial;

////////// #define //////////

#define NORMAL_CODE       "\e[00m"
#define CONSOLE_CODE      "\e[01;34m"
#define LINE_CODE         "\e[01;31m"
#define NORMAL_CODE_LEN   6
#define CONSOLE_CODE_LEN  9
#define LINE_CODE_LEN     9
#define NOEXITVARNAME "TERMNOEXIT"

#define clrscr() printf("\e[1;1H\e[2J");

#define DISPLAY_NONE    0
#define DISPLAY_RAW     1
#define DISPLAY_HEX     2
#define MAX_B           10

////////// 整體變數區 ///////////

int ret=0;
int cfg_display_com_in_mode = DISPLAY_RAW;
int cfg_timeout = 1000000; // 0 for unlimited, unit is us (1.0e-6 s)

void readReply(SerialPort& serial_port) {
  std::cout.put(':').flush();
  while(!serial_port.IsDataAvailable()) {
    usleep(1000);
  }
  try {
    std::string buf="";
    serial_port.Read(buf, 0, 25);
    std::cout << utils::toHex(buf, true) << std::endl;
  } catch (ReadTimeout e) {
    std::cerr << "\ncatch read timeout exception!" << std::endl;
  }
}

////////// 主程式區 //////////

int main(int argc, char* argv[])
{
  if (argc != 2) {
    std::cerr << "Usage: " << argv[0] << " PORT\r\n\tPORT: /dev/ttyUSB0 or /dev/ttyS2\r\n";
    exit(1);
  }
  SerialPort serial_port;
  serial_port.Open(argv[1]);
  if ( !serial_port.IsOpen() ) {
    std::cerr << "[" << __FILE__ << ":" << __LINE__ << "] "
         << "Error: Could not open serial port."
         << std::endl ;
    exit(2) ;
  }
  
  serial_port.SetBaudRate(BaudRate::BAUD_9600);
  serial_port.SetCharacterSize(CharacterSize::CHAR_SIZE_8 ) ;
  serial_port.SetParity(Parity::PARITY_NONE ) ;
  serial_port.SetStopBits(StopBits::STOP_BITS_1);
  serial_port.SetFlowControl(FlowControl::FLOW_CONTROL_NONE);

  while (true) {
    readReply(serial_port);
    usleep(25000) ;
  };

  std::cerr << std::endl;
  return EXIT_SUCCESS;
}
