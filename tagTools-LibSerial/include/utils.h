#ifndef __UTILS_H__
#define __UTILS_H__
#include <iostream>
#include <string>
#include <sstream>
#include <vector>

class utils {
private:
  static const std::string lut;
public:
  static std::string fromBytes(char ca[], int n);
  static std::string toHex(std::vector<unsigned char>ca, bool withSpace);
  static std::string toHex(char ca[], int n, bool withSpace=true);
  static std::string toHex(unsigned char c);
  static std::string toHex(std::string s, bool withSpace=true);
  static std::string fromHex(std::string s, bool withDelim=true);
};
#endif
