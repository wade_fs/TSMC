#ifndef __packet_H__
#define __packet_H__

#include <string>
#include <unordered_map>
#include <vector>

#include <SerialPort.h>
#include <sys/select.h>

#define PACKET_DIR_IN  0
#define PACKET_DIR_OUT 1

using namespace LibSerial;
using namespace std;

/////////// 宣告區 ///////////////
// 透過 PACKET_ID 可以判斷方向+是否可變
// < 100 為不可變寫入
// > 100 && < 1000 為不可變讀取
// >= 1000 && < 1100 為可變寫入
// >= 1100 為可變讀取, 此項的前2bytes 為單獨1Byte
/*
> START_CMD             : FC FF
> DISPLAY_TIME          : FF FF DF FF BB FF FF BB FF FF FF
> READ_TIME             : FF FF DF FF 3B FF FF 3B FF FF FF
> READ_DONE             : 9F FF
> NORMAL_STATUS_READ    : FF FF DF FF FD FF 55 56 FF FF FF
> ENTENDED_STATUS2_READ : FF FF DF FF FD FF 5A 58 FF FF FF
> PROTECTED_DATA_READ   : FF FF DF FF FD FF 55 56 FF FF FF
> WRITE_LCD_DATA        : FF FF 1F FF 77 FF FF 07 B0 FF BB FF 7F FF
> READ_LCD_DATA         : FF FF EF FF F7 FF FF 07 EF 7F 7F 63 FF FF FF
> LCD_DISPLAY_ON        : FF FF DF FF F9 FF FF F9 FF FF FF
> LCD_DISPLAY_OFF       : FF FF DF FF 33 FF FF 33 FF FF FF
> CLEAR_LCD             : FF FF DF FF D3 FF FF D3 FF FF FF
> ENABLE_TIME_OUT       : FF FF DF FF B3 FF FF B3 FF FF FF
> DISABLE_TIME_OUT      : FF FF DF FF 73 FF FF 73 FF FF FF
> DISPLAY_SERIAL_NUMBER : FF FF DF FF 09 FF FF 09 FF FF FF
> GET_TAG_FORMAT        : FF FF EF FF F7 FF 7F FF 87 FF 7F F3 FF FF FF
> FORMAT_TAG            : FF FF 1F FF 77 FF 7F FF F7 FF BB FF FF FF
< START_CMDr            : FC FF
< ACK_START_CMD         : CA
< CMD_DONE              : 60

// 底下感覺應該是個函數，如果是 Write(>) 則當參數傳入，如果是 Read(<) 則當傳回值
// Write(>) 只有 1 筆 string, 而 Read(<) 3 筆 string
> FORMAT()              : FF FF B7 FF F5 FF 73 FB FB FB FB FB FB FB FB FB FB FB FB FB FB 7D FF BF FF
> SET_TIME()            : FF FF AF FF 5B FF 5D 17 3F 1B 77 7F 5B B8 FF FF FF
> LCD_DATAv()            : FF FF D0 FF F5 FF FF FF FF FF 7F FF FF FF 8F FF FF .....
< READ_TIME1()          : 00, 10, 24 ... 00 94 C0 E0 88 40 E8 81 00
< NORMAL_STATUS()       : 00, A8, 42 ... FF 00 00 00 00 80 D0 FF FF 15 5A 13 00 0F 28 00 80 A4 E3 FF A0 10
< NORMAL_STATUS()       : 00, A8, 42 ... FF 00 00 00 00 80 D0 FF FF 15 5A 13 00 00 28 00 80 A4 E3 FF A8 E0
< NORMAL_STATUS()       : 00, A8, 42 ... 00 CD 0D 5B DC 80 C2 FF D6 C8 FF 13 AA 00 5E 00 00 BE 69 00 24 10
< PROTECTED_DATA()      : 00, A8, 42 ... 00 CD 0D 5B DC 80 C2 FF D6 C8 FF 13 AA 00 5E 00 00 BE 69 00 24 10
< PROTECTED_DATA()      : 00, A8, 42 ... FF 00 00 00 00 80 D0 FF FF 15 5A 13 00 00 28 00 80 A4 E3 FF A8 E0
< EXTENDED_STATUS2()    : 00, E8, 42 ... FF 00 00 00 00 80 D0 FF FF 15 5A 13 00 00 28 00 80 A4 E3 FF B8 01 4D E0
< LCD_DATAvr()           : 80, 80, 0A ... 00 00 00 00 80 00 00 00 70 00 00 00 00 00 00 00 .....
< LCD_DATA21()           : 00, 90, 0A ... C7 E2 58 DF C3 9B 5E 7B C1 A0
< LCD_DATA21()           : 00, 90, 0A ... 00 00 00 00 00 00 00 00 0A 00
< LCD_DATA22()          : 00, 50, 0A ... BF FF 88 BA 28 84 00 44 00 88 C0
< TAG_FORMAT()          : 00, F8, 0A ... 9B 6A 8D AF 77 DC D5 F1 EB FF 12 C5 D0 E7 FF D5 F3 BB BF 6F 73 27 F6 4F DC B9 19 CC 6F DE D5 28
< TAG_FORMAT()          : 00, F8, 0A ... 00 4C 00 CC 00 04 04 04 04 04 04 04 04 04 04 FF 8A 00 C8 F2 B0 F2 D9 FF 70 FF F0 00 88 00 53 60
 */
enum PACKET_ID {
  PK_START_CMD=0,		// 0
  PK_DISPLAY_TIME,		// 1
  PK_READ_TIME,			// 2
  PK_READ_DONE,			// 3
  PK_NORMAL_STATUS_READ,	// 4
  PK_ENTENDED_STATUS2_READ,	// 5
  PK_PROTECTED_DATA_READ,	// 6
  PK_WRITE_LCD_DATA,		// 7
  PK_READ_LCD_DATA,		// 8
  PK_LCD_DISPLAY_ON,		// 9
  PK_LCD_DISPLAY_OFF,		// 10
  PK_CLEAR_LCD,			// 11
  PK_ENABLE_TIME_OUT,		// 12
  PK_DISABLE_TIME_OUT,		// 13
  PK_DISPLAY_SERIAL_NUMBER,	// 14
  PK_GET_TAG_FORMAT,		// 15
  PK_FORMAT_TAG,		// 16
  PK_START_CMDr=100,		// 100
  PK_ACK_START_CMD,		// 101
  PK_CMD_DONE,			// 102
  PK_FORMAT=1000,  // 會有參數，但是封包不變？ // 1001
  PK_SET_TIME,			// 1002
  PK_LCD_DATAv,			// 1003
  PK_READ_TIME1=1100, // 要分成三段	// 1100
  PK_NORMAL_STATUS,		// 1101
  PK_PROTECTED_DATA,		// 1102
  PK_EXTENDED_STATUS2,		// 1103
  PK_LCD_DATAvr,		// 1104
  PK_LCD_DATA21,		// 1105
  PK_LCD_DATA22,		// 1106
  PK_TAG_FORMAT,		// 1107
};

extern unordered_map <int, string> packets;

class packet {
private:
  int id;
public:
  packet(int ID=-1);
  int getId();
  void setId(int Id);
  int dir();
  int var();
  vector<char> getP();
  string getPinAscii();
  string getPinHex(char delim='\0');
  static int PK_OK;
#if 0
  void send(SerialPort&);
  int read(SerialPort&);
#endif
};

#endif
