/**
 *  @example serial_port_read_write.cpp
 */

#include <SerialPort.h>
#include "utils.h"

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <unistd.h>

using namespace LibSerial;

/**
 * @brief This example demonstrates multiple methods to read and write
 *        serial stream data.
 */
int main(int argc, char* argv[])
{
  SerialPort serial_port;
  size_t ms_timeout = 25;
  serial_port.Open(argc==2?argv[1]:"/dev/ttyUSB0");
  unsigned char data_byte;

  if (!serial_port.IsOpen()) {
    std::cerr << "The serial ports did not open correctly." << std::endl;
    return EXIT_FAILURE;
  }

  // Set the serial port
  serial_port.SetBaudRate(BaudRate::BAUD_115200);
  serial_port.SetCharacterSize(CharacterSize::CHAR_SIZE_8);
  serial_port.SetFlowControl(FlowControl::FLOW_CONTROL_NONE);
  serial_port.SetParity(Parity::PARITY_NONE);
  serial_port.SetStopBits(StopBits::STOP_BITS_1);

  std::string read_string = "";
  std::string write_string = "";
  read_string.reserve(256);
  write_string.reserve(256);

  std::cout << "sent:\t";
  for (unsigned char c=0; c<=255; ++c) {
    serial_port.WriteByte(c);
    std::cout << utils::toHex(c) << " ";
  }
  std::cout << std::endl;

  usleep(25000);

  while(!serial_port.IsDataAvailable()) {
    usleep(1000);
  } 
  
  while(serial_port.IsDataAvailable()) {
    try {
      serial_port.ReadByte(data_byte, ms_timeout);
      read_string += data_byte;
    } catch (ReadTimeout) {
      std::cerr << "The ReadByte() call has timed out." << std::endl;
    }
    
    usleep(1000);
  } 
  std::cout << "\tRead:\t" << utils::toHex(read_string) << std::endl;

  serial_port.Close();

  std::cout << "The example program successfully completed!" << std::endl;
  return EXIT_SUCCESS;
}
