#include <SerialPort.h>
#include <utils.h>

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <unistd.h>

using namespace LibSerial;

int main(int argc, char** argv)
{   
  SerialPort serial_port;

  serial_port.Open(argc==2?argv[1]:"/dev/ttyUSB0");

  // Set the Serial Port
  serial_port.SetBaudRate(BaudRate::BAUD_57600);
  serial_port.SetCharacterSize(CharacterSize::CHAR_SIZE_8);
  serial_port.SetFlowControl(FlowControl::FLOW_CONTROL_NONE);
  serial_port.SetParity(Parity::PARITY_NONE);
  serial_port.SetStopBits(StopBits::STOP_BITS_1);

  std::cout << "\tWrite:\t";
  for (int c=0; c<=255; c++) {
    serial_port.WriteByte((unsigned char)c);
    printf ("%02X ", c);
    usleep(10000);
  }
  std::cout << std::endl;
  return EXIT_SUCCESS;
}
