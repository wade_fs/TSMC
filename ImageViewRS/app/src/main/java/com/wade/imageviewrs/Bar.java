package com.wade.imageviewrs;

import android.content.Context;
import android.graphics.Bitmap;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.Script;
import android.renderscript.Type;

public class Bar {
    Context me;
    RenderScript mRS;

    public Bar(Context context) {
        me = context;
        mRS = RenderScript.create(me);
    }

    public Bitmap rsBar(Bitmap bitmap) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        ScriptC_bar sc_bar = new ScriptC_bar(mRS);

        int halfWidth = width >> 1;

        Bitmap bitmapOut = Bitmap.createBitmap(width, height, bitmap.getConfig());
        Type u8    = new Type.Builder(mRS, Element.U8(mRS)).setX(5*2+1).setY(height).create(); // 含中間+左右各5點
        Allocation allocationIn     = Allocation.createFromBitmap(mRS, bitmap, Allocation.MipmapControl.MIPMAP_NONE, Allocation.USAGE_SCRIPT);
        Allocation allocationOut    = Allocation.createFromBitmap(mRS, bitmapOut, Allocation.MipmapControl.MIPMAP_NONE, Allocation.USAGE_SCRIPT);

        sc_bar.set_width(width);
        sc_bar.set_height(height);
        sc_bar.set_minX(halfWidth-15);
        sc_bar.set_maxX(halfWidth+15);
        sc_bar.set_minY(1);
        sc_bar.set_maxY(height-1);
        sc_bar.set_threshold(150);
        sc_bar.set_diff(10);
        sc_bar.set_allocationIn(allocationIn);
        sc_bar.forEach_bar1(allocationIn, allocationOut);

        allocationOut.copyTo(bitmapOut);
        return bitmapOut;
    }
}
