package com.wade.imageviewrs;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.renderscript.RenderScript;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;

import static java.lang.Math.abs;

public class MainActivity extends Activity {
    private Bitmap bitmapIn, bitmapEmpty;
    private ImageView[] imageViews = new ImageView[4];
    private RenderScript mRS;
    private RsUtils rsUtils;
    private Bar bar;
    private float blur = 10.5f;
    private float bright = 0.15f;
    public static int touchImageId;
    String[][] imgs = {
            new String[] {
                    "empty/IMAG0005.jpg", "empty/IMAG0006.jpg", "empty/IMAG0007.jpg", "empty/IMAG0008.jpg"
            },
            new String[] {
                    "bottom/IMAG0013.jpg", "bottom/IMAG0014.jpg", "bottom/IMAG0015.jpg", "bottom/IMAG0016.jpg"
            },
            new String[] {
                    "top/IMAG0009.jpg", "top/IMAG0012.jpg", "top/IMAG0010.jpg", "top/IMAG0011.jpg"
            },
            new String[] {
                    "full/IMAG0001.jpg", "full/IMAG0003.jpg", "full/IMAG0002.jpg", "full/IMAG0004.jpg"
            }
    };
    int[] curImgColumnImgIdx = new int[] { 2, 2, 0, 0 };
    String imageBg = "IMAG0017.jpg";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        rsUtils = new RsUtils(this);
        bar = new Bar(this);

        createScript();
    }

    private void createScript() {
        mRS = RenderScript.create(this);
        final int[] imageViewIds = new int[] { R.id.empty, R.id.bottom, R.id.top, R.id.full };
        final GestureDetector gdt = new GestureDetector(new GestureListener());

        for (int imgColumn=0; imgColumn<4; ++imgColumn) {
            imageViews[imgColumn] = findViewById(imageViewIds[imgColumn]);
            imageViews[imgColumn].setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    touchImageId = v.getId();
                    gdt.onTouchEvent(event);
                    return true;
                }
            });
            loadImgAndProcess(imgColumn, 1);
        }

        mRS.destroy();
    }
    int[] showModes = new int[] { 1, 1, 1, 1 };
    private void loadImgAndProcess(int column, int showOrigin) {
        int imgIdx = curImgColumnImgIdx[column];
        showModes[column] = showOrigin;
        if (showOrigin == 1) {
            bitmapIn = rsUtils.edgeResize(loadBitmap(imgs[column][imgIdx]), 0.25f, true, 5, false);
            imageViews[column].setImageBitmap(bitmapIn);
        } else if (showOrigin == 2) {
            bitmapIn = bar.rsBar(loadBitmap(imgs[column][imgIdx]));
            imageViews[column].setImageBitmap(bitmapIn);
        } else {
            bitmapIn = rsUtils.edgeResize(loadBitmap(imgs[column][imgIdx]), 0.25f, true, 5, false);
            imageViews[column].setImageBitmap(rsUtils.edgeCannyHysteresis(bitmapIn, 5, 64, 40));
        }
    }
    public int getImgIdx(int id) {
        switch(id) {
            case R.id.empty: return 0;
            case R.id.bottom: return 1;
            case R.id.top: return 2;
            case R.id.full: return 3;
        }
        return 0;
    }
    private class GestureListener extends GestureDetector.SimpleOnGestureListener {
        private static final int SWIPE_MIN_DISTANCE = 120;
        private static final int SWIPE_THRESHOLD_VELOCITY = 200;

        @Override
        public boolean onSingleTapUp(MotionEvent event) {
            return true;
        }

        @Override
        public boolean onDoubleTap(MotionEvent event) {
            Log.d("MyLog", "onDoubleTap");
            return true;
        }
        @Override
        public boolean onDoubleTapEvent(MotionEvent event) {
            Log.d("MyLog", "onDoubleTapEvent: " + event.toString());
            return true;
        }

        @Override
        public boolean onSingleTapConfirmed(MotionEvent event) {
            int imgColumn = getImgIdx(touchImageId);
            showModes[imgColumn] = (showModes[imgColumn]+1)%3;
            loadImgAndProcess(imgColumn, showModes[imgColumn]);
            return true;
        }
        @Override
        public void onLongPress(MotionEvent event) {
        }

        @Override
        public boolean onScroll(MotionEvent event1, MotionEvent event2, float distanceX, float distanceY) {
            return true;
        }

        @Override
        public void onShowPress(MotionEvent event) {
        }
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            int distX = (int)abs(e1.getX() - e2.getX());
            int distY = (int)abs(e1.getY() - e2.getY());
            int imgColumn = getImgIdx(touchImageId);

            if((distX > SWIPE_MIN_DISTANCE || distY > SWIPE_MIN_DISTANCE)
                    && (abs(velocityX) > SWIPE_THRESHOLD_VELOCITY || abs(velocityX) > SWIPE_THRESHOLD_VELOCITY)) {
                int imgIdx = curImgColumnImgIdx[imgColumn];
                ++imgIdx;
                if (imgIdx >= imgs[imgColumn].length) imgIdx = 0;
                curImgColumnImgIdx[imgColumn] = imgIdx;
                loadImgAndProcess(imgColumn, 1);
            }
            return false;
        }
    }
    private Bitmap loadBitmap(String fn) {
        Bitmap bitmap;
        try {
            InputStream is = getAssets().open(fn);
            return BitmapFactory.decodeStream(is);
        } catch (IOException e) {
            Log.d("MyLog", "Assets: load failed for "+fn+": "+e.getMessage());
            return null;
        }
    }
}
