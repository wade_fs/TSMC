package com.wade.imageviewrs;

import android.content.Context;
import android.graphics.Bitmap;
import android.renderscript.Script;
import android.renderscript.ScriptIntrinsicConvolve5x5;
import android.renderscript.ScriptIntrinsicResize;
import android.renderscript.Type;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;

/**
 * Created by wade on 2018/2/26.
 * RsUtils 剛好當範例，RenderScript 除了內建的 Allocation, Element, RenderScript 外，還有底下
 * ScriptIntrinsic3DLUT
 • ScriptIntrinsicBlend
 • ScriptIntrinsicBlur
 • ScriptIntrinsicColorMatrix
 • ScriptIntrinsicConvolve3x3
 • ScriptIntrinsicConvolve5x5
 • ScriptIntrinsicLUT
 • ScriptIntrinsicYuvToRGB
 • ScriptGroup
 */
public class RsUtils {

    Context me;
    private static final float BITMAP_SCALE = 0.4f;

    //Set the radius of the Blur. Supported range 0 < radius <= 25
    private static float BLUR_RADIUS = 10.5f;
    RenderScript mRS;
    Script.LaunchOptions fastLaunchOptions;

    MainActivity mainActivity;

    public RsUtils(Context context) {
        me = context;
        mRS = RenderScript.create(me);
    }

    public Bitmap fastOptimize(Bitmap bitmap) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        ScriptC_fast sc_fast = new ScriptC_fast(mRS);
        Script.LaunchOptions fastLaunchOptions = new Script.LaunchOptions();
        fastLaunchOptions.setX(3, width - 3);
        fastLaunchOptions.setY(3, height - 3);

        Bitmap bitmapOut = Bitmap.createBitmap(width, height, bitmap.getConfig());
        Type u8    = new Type.Builder(mRS, Element.U8(mRS)).setX(width).setY(height).create();
        Allocation allocationIn     = Allocation.createFromBitmap(mRS, bitmap, Allocation.MipmapControl.MIPMAP_NONE, Allocation.USAGE_SCRIPT);
        Allocation allocationOut    = Allocation.createFromBitmap(mRS, bitmapOut, Allocation.MipmapControl.MIPMAP_NONE, Allocation.USAGE_SCRIPT);
        Allocation allocationU8     = Allocation.createTyped(mRS, u8, Allocation.USAGE_SCRIPT);

        // step1. rgba => gray
        sc_fast.forEach_rgbaToGray(allocationIn, allocationU8);
        sc_fast.set_grayAllocation(allocationU8);
        sc_fast.forEach_fastOptimized(allocationU8, allocationOut, fastLaunchOptions);
        allocationOut.copyTo(bitmapOut);
        return bitmapOut;
    }
    public void init(int width, int height) {
        if (null == fastLaunchOptions) {
            fastLaunchOptions = new Script.LaunchOptions();
            fastLaunchOptions.setX(3, width - 3);
            fastLaunchOptions.setY(3, height - 3);
        }
    }
    public Bitmap edgeResize(Bitmap image, float ratio, boolean blur, float blurSize, boolean sharp) {
        int width = (int)(image.getWidth()*ratio);
        int height = (int)(image.getHeight()*ratio);

        Bitmap bitmapOut = Bitmap.createBitmap(width, height, image.getConfig());
        Type u8    = new Type.Builder(mRS, Element.U8(mRS)).setX(width).setY(height).create();
        Allocation allocationU8     = Allocation.createTyped(mRS, u8, Allocation.USAGE_SCRIPT);
        Allocation allocationSharp  = Allocation.createTyped(mRS, u8, Allocation.USAGE_SCRIPT);
        Allocation allocationBlur   = Allocation.createTyped(mRS, u8, Allocation.USAGE_SCRIPT);
        Allocation allocationIn     = Allocation.createFromBitmap(mRS, image, Allocation.MipmapControl.MIPMAP_NONE, Allocation.USAGE_SCRIPT);
        Allocation allocationResize = Allocation.createFromBitmap(mRS, bitmapOut, Allocation.MipmapControl.MIPMAP_NONE, Allocation.USAGE_SCRIPT);
        Allocation allocationOut    = Allocation.createFromBitmap(mRS, bitmapOut, Allocation.MipmapControl.MIPMAP_NONE, Allocation.USAGE_SCRIPT);

        ScriptC_edge sc_edge = new ScriptC_edge(mRS);
        sc_edge.set_height(height);
        sc_edge.set_width(width);
        sc_edge.set_size(width*height);

        ScriptIntrinsicResize si_resize = ScriptIntrinsicResize.create(mRS);
        si_resize.setInput(allocationIn);
        si_resize.forEach_bicubic(allocationResize);
        sc_edge.forEach_rgba2gray(allocationResize, allocationU8);

        if (blur) {
            ScriptIntrinsicBlur si_blur = ScriptIntrinsicBlur.create(mRS, Element.U8(mRS));
            si_blur.setRadius(blurSize);
            si_blur.setInput(allocationU8);
            si_blur.forEach(allocationBlur);
            // 第一步，先變黑白，順便變成 u8
        } else {
            // 第一步，先變黑白，順便變成 u8
            allocationBlur.copyFrom(allocationU8);
        }
        if (sharp) {
            ScriptIntrinsicConvolve5x5 scriptIntrinsicConvolve5x5 = ScriptIntrinsicConvolve5x5.create(mRS, Element.U8(mRS));
            scriptIntrinsicConvolve5x5.setCoefficients( new float[] {
                    -0.00391f, -0.01563f, -0.02344f, -0.01563f, -0.00391f,
                    -0.01563f, -0.06250f, -0.09375f, -0.06250f, -0.01563f,
                    -0.02344f, -0.09375f,  1.85980f, -0.09375f, -0.02344f,
                    -0.01563f, -0.06250f, -0.09375f, -0.06250f, -0.01563f,
                    -0.00391f, -0.01563f, -0.02344f, -0.01563f, -0.00391f });
            scriptIntrinsicConvolve5x5.setInput(allocationBlur);
            scriptIntrinsicConvolve5x5.forEach(allocationSharp);
        } else allocationSharp.copyFrom(allocationBlur);
        sc_edge.forEach_gray2rgba(allocationSharp, allocationOut);
        allocationOut.copyTo(bitmapOut);
        allocationOut.destroy();
        allocationIn.destroy();
        allocationResize.destroy();

        return bitmapOut;
    }

    public Bitmap edgeHistEq(Bitmap image) {
        int width = image.getWidth();
        int height = image.getHeight();
        init(width, height);
        Bitmap bitmapOut = Bitmap.createBitmap(width, height, image.getConfig());

        Type u8    = new Type.Builder(mRS, Element.U8(mRS)).setX(width).setY(height).create();
        Type u8_4  = new Type.Builder(mRS, Element.U8_4(mRS)).setX(width).setY(height).create();
        Type f32_2 = new Type.Builder(mRS, Element.F32_2(mRS)).setX(width).setY(height).create();
        Allocation allocationIn     = Allocation.createFromBitmap(mRS, image, Allocation.MipmapControl.MIPMAP_NONE, Allocation.USAGE_SCRIPT);
        Allocation allocationOut    = Allocation.createFromBitmap(mRS, bitmapOut, Allocation.MipmapControl.MIPMAP_NONE, Allocation.USAGE_SCRIPT);
        Allocation allocationU8     = Allocation.createTyped(mRS, u8, Allocation.USAGE_SCRIPT);
        Allocation allocationU8B    = Allocation.createTyped(mRS, u8, Allocation.USAGE_SCRIPT);

        ScriptC_edge sc_edge = new ScriptC_edge(mRS);

        sc_edge.set_height(height);
        sc_edge.set_width(width);
        sc_edge.set_size(width*height);

        // 第一步，先變黑白，順便變成 u8
        sc_edge.forEach_rgba2gray(allocationIn, allocationU8B);
        sc_edge.forEach_histEq(allocationU8B, allocationU8);
        sc_edge.invoke_createRemapArray();
        sc_edge.forEach_remap4(allocationU8, allocationOut);

        allocationOut.copyTo(bitmapOut);

        allocationU8B.destroy();
        allocationU8.destroy();
        allocationOut.destroy();
        allocationIn.destroy();
        sc_edge.destroy();

        return bitmapOut;
    }

    public Bitmap edgeSobel(Bitmap image) {
        int width = image.getWidth();
        int height = image.getHeight();
        init(width, height);
        Bitmap bitmapOut = Bitmap.createBitmap(width, height, image.getConfig());

        Type u8    = new Type.Builder(mRS, Element.U8(mRS)).setX(width).setY(height).create();
        Type u8_4  = new Type.Builder(mRS, Element.U8_4(mRS)).setX(width).setY(height).create();
        Type f32_2 = new Type.Builder(mRS, Element.F32_2(mRS)).setX(width).setY(height).create();
        Allocation allocationIn     = Allocation.createFromBitmap(mRS, image, Allocation.MipmapControl.MIPMAP_NONE, Allocation.USAGE_SCRIPT);
        Allocation allocationOut    = Allocation.createFromBitmap(mRS, bitmapOut, Allocation.MipmapControl.MIPMAP_NONE, Allocation.USAGE_SCRIPT);
        Allocation allocationU8     = Allocation.createTyped(mRS, u8, Allocation.USAGE_SCRIPT);
        Allocation allocationU8B    = Allocation.createTyped(mRS, u8, Allocation.USAGE_SCRIPT);

        ScriptIntrinsicBlur si_blur = ScriptIntrinsicBlur.create(mRS, Element.U8(mRS));
        ScriptC_edge sc_edge = new ScriptC_edge(mRS);

        sc_edge.set_height(height);
        sc_edge.set_width(width);
        sc_edge.set_size(width*height);
        si_blur.setRadius(3);

        // 第一步，先變黑白，順便變成 u8
        sc_edge.forEach_rgba2gray(allocationIn, allocationU8B);

        // 再來正式 HistEq 映照表查詢
        sc_edge.forEach_histEq(allocationU8B, allocationU8);
        sc_edge.invoke_createRemapArray();
        sc_edge.forEach_remap(allocationU8, allocationU8B);

        // 第三步，sobel
        sc_edge.set_allocationSobel(allocationU8B);
        sc_edge.forEach_sobel4(allocationOut, fastLaunchOptions);

        allocationOut.copyTo(bitmapOut);

        allocationU8B.destroy();
        allocationU8.destroy();
        allocationOut.destroy();
        allocationIn.destroy();
        sc_edge.destroy();

        return bitmapOut;
    }

    public Bitmap edgeCanny(Bitmap image, short upper, short lower) {
        int width = image.getWidth();
        int height = image.getHeight();
        init(width, height);
        Bitmap bitmapOut = Bitmap.createBitmap(width, height, image.getConfig());

        Type u8    = new Type.Builder(mRS, Element.U8(mRS)).setX(width).setY(height).create();
        Type u8_4  = new Type.Builder(mRS, Element.U8_4(mRS)).setX(width).setY(height).create();
        Type f32_2 = new Type.Builder(mRS, Element.F32_2(mRS)).setX(width).setY(height).create();
        Allocation allocationIn     = Allocation.createFromBitmap(mRS, image, Allocation.MipmapControl.MIPMAP_NONE, Allocation.USAGE_SCRIPT);
        Allocation allocationOut    = Allocation.createFromBitmap(mRS, bitmapOut, Allocation.MipmapControl.MIPMAP_NONE, Allocation.USAGE_SCRIPT);
        Allocation allocationU8     = Allocation.createTyped(mRS, u8, Allocation.USAGE_SCRIPT);
        Allocation allocationU8B    = Allocation.createTyped(mRS, u8, Allocation.USAGE_SCRIPT);
        Allocation allocationF32_2  = Allocation.createTyped(mRS, f32_2, Allocation.USAGE_SCRIPT);

        ScriptC_edge sc_edge = new ScriptC_edge(mRS);

        sc_edge.set_height(height);
        sc_edge.set_width(width);
        sc_edge.set_size(width*height);
        sc_edge.set_UPPER(upper);
        sc_edge.set_LOWER(lower);

        // 第一步，先變黑白，順便變成 u8
        sc_edge.forEach_rgba2gray(allocationIn, allocationU8B);

        // 再來正式 HistEq 映照表查詢
        sc_edge.forEach_histEq(allocationU8B, allocationU8);
        sc_edge.invoke_createRemapArray();
        sc_edge.forEach_remap(allocationU8, allocationU8B);

        // 第三步，sobel
        sc_edge.set_allocationSobel(allocationU8B);
        sc_edge.forEach_sobel(allocationF32_2, fastLaunchOptions);

        // 第四步，Canny
        sc_edge.set_allocationCanny(allocationF32_2);
        sc_edge.forEach_canny4(allocationOut, fastLaunchOptions);

        allocationOut.copyTo(bitmapOut);

        allocationF32_2.destroy();
        allocationU8B.destroy();
        allocationU8.destroy();
        allocationOut.destroy();
        allocationIn.destroy();
        sc_edge.destroy();

        return bitmapOut;
    }

    public Bitmap edgeCannyHysteresis(Bitmap image, int filterSize, int upper, int lower) {
        int width = image.getWidth();
        int height = image.getHeight();
        init(width, height);
        Bitmap bitmapOut = Bitmap.createBitmap(width, height, image.getConfig());

        Type u8    = new Type.Builder(mRS, Element.U8(mRS)).setX(width).setY(height).create();
        Type u8_4  = new Type.Builder(mRS, Element.U8_4(mRS)).setX(width).setY(height).create();
        Type f32_2 = new Type.Builder(mRS, Element.F32_2(mRS)).setX(width).setY(height).create();
        Allocation allocationIn     = Allocation.createFromBitmap(mRS, image, Allocation.MipmapControl.MIPMAP_NONE, Allocation.USAGE_SCRIPT);
        Allocation allocationOut    = Allocation.createFromBitmap(mRS, bitmapOut, Allocation.MipmapControl.MIPMAP_NONE, Allocation.USAGE_SCRIPT);
        Allocation allocationU8     = Allocation.createTyped(mRS, u8, Allocation.USAGE_SCRIPT);
        Allocation allocationU8B    = Allocation.createTyped(mRS, u8, Allocation.USAGE_SCRIPT);
        Allocation allocationF32_2  = Allocation.createTyped(mRS, f32_2, Allocation.USAGE_SCRIPT);
        Allocation allocationF32_2B = Allocation.createTyped(mRS, f32_2, Allocation.USAGE_SCRIPT);
        ScriptC_edge sc_edge = new ScriptC_edge(mRS);

        sc_edge.set_height(height);
        sc_edge.set_width(width);
        sc_edge.set_size(width*height);
        sc_edge.set_UPPER((short)upper);
        sc_edge.set_LOWER((short)lower);
        sc_edge.set_gradientThreshold(0.0f);
        // 第一步，先變黑白，順便變成 u8
        sc_edge.forEach_rgba2gray(allocationIn, allocationU8B);

        // 再來正式 HistEq 映照表查詢
        sc_edge.forEach_histEq(allocationU8B, allocationU8);
        sc_edge.invoke_createRemapArray();
        sc_edge.forEach_remap(allocationU8, allocationU8B);

        // 第三步，sobel
        sc_edge.set_allocationSobel(allocationU8B);
        if (filterSize == 3) {
            sc_edge.forEach_sobel(allocationF32_2, fastLaunchOptions);
        } else {
            sc_edge.forEach_sobel5x5(allocationF32_2, fastLaunchOptions);
        }
        // 第四步，Canny
        sc_edge.set_allocationCanny(allocationF32_2);
        sc_edge.forEach_canny(allocationF32_2B, fastLaunchOptions);

        // 第五步，Hysteresis
        sc_edge.set_allocationHysteresis(allocationF32_2B);
        sc_edge.forEach_hysteresis(allocationOut, fastLaunchOptions);

        allocationOut.copyTo(bitmapOut);

        allocationF32_2B.destroy();
        allocationF32_2.destroy();
        allocationU8B.destroy();
        allocationU8.destroy();
        allocationOut.destroy();
        allocationIn.destroy();
        sc_edge.destroy();

        return bitmapOut;
    }
}
