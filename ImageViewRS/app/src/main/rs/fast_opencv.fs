#pragma version(1)
#pragma rs java_package_name(com.wade.imageviewrs)
#pragma rs_fp_relaxed

int fastThreshold = 20;
rs_allocation grayAllocation;

#include "fast_opencv_util.rsh"

// Follows FAST features detection directly ported from OpenCV code
// https://github.com/Itseez/opencv/blob/master/modules/cudafeatures2d/src/cuda/fast.cu

uchar4 RS_KERNEL fastOpenCV(uchar in, uint x, uint y){
    int v;
    uint4 C = {0,0,0,0};
    C.z |= (uint)(rsGetElementAt_uchar(grayAllocation, x - 1, y - 3)) << 8;
    C.z |= (uint)(rsGetElementAt_uchar(grayAllocation, x, y - 3));
    C.y |= (uint)(rsGetElementAt_uchar(grayAllocation, x + 1, y - 3)) << (3 * 8);

    C.z |= (uint)(rsGetElementAt_uchar(grayAllocation, x - 2, y - 2)) << (2 * 8);
    C.y |= (uint)(rsGetElementAt_uchar(grayAllocation, x + 2, y - 2)) << (2 * 8);

    C.z |= (uint)(rsGetElementAt_uchar(grayAllocation, x - 3, y - 1)) << (3 * 8);
    C.y |= (uint)(rsGetElementAt_uchar(grayAllocation, x + 3, y - 1)) << 8;

    C.w |= (uint)(rsGetElementAt_uchar(grayAllocation, x - 3, y));
    v = (int) (rsGetElementAt_uchar(grayAllocation, x, y));
    C.y |= (uint)(rsGetElementAt_uchar(grayAllocation, x + 3, y));

    int d1 = diffType(v, C.y & 0xff, fastThreshold);
    int d2 = diffType(v, C.w & 0xff, fastThreshold);

    if ((d1 | d2) == 0) {
        return (uchar4) { in, in, in, 255 }; // { 0, 0, 0, 255 }
    }

    C.w |= (uint)(rsGetElementAt_uchar(grayAllocation, x - 3, y + 1)) << 8;
    C.x |= (uint)(rsGetElementAt_uchar(grayAllocation, x + 3, y + 1)) << (3 * 8);

    C.w |= (uint)(rsGetElementAt_uchar(grayAllocation, x - 2, y + 2)) << (2 * 8);
    C.x |= (uint)(rsGetElementAt_uchar(grayAllocation, x + 2, y + 2)) << (2 * 8);

    C.w |= (uint)(rsGetElementAt_uchar(grayAllocation, x - 1, y + 3)) << (3 * 8);
    C.x |= (uint)(rsGetElementAt_uchar(grayAllocation, x, y + 3));
    C.x |= (uint)(rsGetElementAt_uchar(grayAllocation, x + 1, y + 3)) << 8;

    int2 mask = calcMask(C, v, fastThreshold);

    if (isKeyPoint(mask)) {

        // Point is a FAST keypoint
        return (uchar4){ 255, 255, 255, 255 };

    }
    return (uchar4) { in, in, in, 255 };  // { 0, 0, 0, 255 }
}
const static float3 grayMultipliers = {0.299f, 0.587f, 0.114f};

uchar RS_KERNEL rgbaToGray(uchar4 in, uint32_t x, uint32_t y) {

    uchar out = (uchar) ((float)in.r*grayMultipliers.r) +
                        ((float)in.g*grayMultipliers.g) +
                        ((float)in.b*grayMultipliers.b);

    return out;
}