#pragma version(1)
#pragma rs java_package_name(com.wade.imageviewrs)
#pragma rs_fp_relaxed
#include "rs_debug.rsh"

// 原圖(u8_4) --> blur(u8,u8) --> histEq(u8,u8) --> sobel(u8, float2) --> canny --> hysteresis
rs_allocation allocationSobel;
rs_allocation allocationCanny;
rs_allocation allocationHysteresis;
rs_allocation allocationIn1;
rs_allocation allocationIn2;
rs_allocation allocationOut;

uint32_t width;
uint32_t height;
int32_t histo[256];
float remapArray[256];
int size;

// Canny
uchar UPPER=200;
uchar LOWER=10;

// Hysteresis
float gradientThreshold;

void init() {
    for (int i = 0; i < 256; i++) {
        histo[i] = 0;
        remapArray[i] = 0.0f;
    }
}

const static float3 gMonoMult = {0.299f, 0.587f, 0.114f};

void RS_KERNEL merge(uchar4 prevPixel, uint32_t x, uint32_t y) {
    uchar4 pixel1 = rsGetElementAt_uchar4(allocationIn1, x, y);
    uchar4 pixel2 = rsGetElementAt_uchar4(allocationIn2, x, y);
    float p = (float)abs((pixel1.r - pixel2.r));
    uchar gray = (uchar)p;
    rsSetElementAt_uchar4(allocationOut, (uchar4){ gray, gray, gray, 254}, x, y);
}

uchar RS_KERNEL rgba2gray(const uchar4 in, uint32_t x, uint32_t y) {
    float4 f4 = rsUnpackColor8888(in);

    float3 mono = dot(f4.rgb, gMonoMult);
    uchar4 rgba = rsPackColorTo8888(mono);
    return rgba.r;
}

uchar4 RS_KERNEL gray2rgba(const uchar in, uint32_t x, uint32_t y) {
    uchar4 rgba = { in, in, in, 254 };
    return rgba;
}

uchar RS_KERNEL histEq(const uchar in, uint32_t x, uint32_t y) {
    rsAtomicInc(&histo[in]);
    return in;
}

// 要先算完 histo[] 並且呼叫過 remap, 才能讓 HistEq 使用調整對應
void createRemapArray() {
    //create map for y
    float sum = 0;
    for (int i = 0; i < 256; i++) {
        sum += histo[i];
        remapArray[i] = sum / (size);
    }
}
uchar RS_KERNEL remap(const uchar in, uint32_t x, uint32_t y) {
    float3 res = { remapArray[in], remapArray[in], remapArray[in] };
    uchar4 rgba = rsPackColorTo8888(res);
    return rgba.r;
}
uchar4 RS_KERNEL remap4(const uchar in, uint32_t x, uint32_t y) {
    float3 res = { remapArray[in], remapArray[in], remapArray[in] };
    uchar4 rgba = rsPackColorTo8888(res);
    return rgba;
}

float2 __attribute__ ((kernel)) sobel(uint32_t x,uint32_t y){
  if(x>0 && y>0 && x<width-1 &&y<height-1){
    //get all values around pixel
    uchar n1n1= rsGetElementAt_uchar(allocationSobel, x-1,y-1);
    uchar n1z=  rsGetElementAt_uchar(allocationSobel, x-1,y);
    uchar n1p1= rsGetElementAt_uchar(allocationSobel, x-1,y+1);
    uchar zn1=  rsGetElementAt_uchar(allocationSobel, x,y-1);
    uchar zp1=  rsGetElementAt_uchar(allocationSobel, x,y+1);
    uchar p1n1= rsGetElementAt_uchar(allocationSobel, x+1,y-1);
    uchar p1z=  rsGetElementAt_uchar(allocationSobel, x+1,y);
    uchar p1p1= rsGetElementAt_uchar(allocationSobel, x+1,y+1);
    //get x kernel value
    float xValue=(-n1n1-2*n1z-n1p1+p1n1+2*p1z+p1p1)/4;
    //get y kernel value
    float yValue=(-n1n1-2*zn1-p1n1+n1p1+2*zp1+p1p1)/4;
    return (float2){half_sqrt(xValue*xValue+yValue*yValue),(float)atan2pi((float)yValue, (float)xValue)};
  } else {
    return (float2){0,0};
  }
}

float2 __attribute__ ((kernel)) sobel5x5(uint32_t x,uint32_t y){
    //get all values around pixel
    uchar n2n2 = rsGetElementAt_uchar(allocationSobel, x-2,y-2);
    uchar n2n1 = rsGetElementAt_uchar(allocationSobel, x-2,y-1);
    uchar n2z  = rsGetElementAt_uchar(allocationSobel, x-2,y);
    uchar n2p1 = rsGetElementAt_uchar(allocationSobel, x-2,y+1);
    uchar n2p2 = rsGetElementAt_uchar(allocationSobel, x-2,y+1);

    uchar n1n2 = rsGetElementAt_uchar(allocationSobel, x-1,y-2);
    uchar n1n1 = rsGetElementAt_uchar(allocationSobel, x-1,y-1);
    uchar n1z  = rsGetElementAt_uchar(allocationSobel, x-1,y);
    uchar n1p1 = rsGetElementAt_uchar(allocationSobel, x-1,y+1);
    uchar n1p2 = rsGetElementAt_uchar(allocationSobel, x-1,y+1);

    uchar zn2  = rsGetElementAt_uchar(allocationSobel, x,y-2);
    uchar zn1  = rsGetElementAt_uchar(allocationSobel, x,y-1);
//    uchar zz   = rsGetElementAt_uchar(allocationSobel, x,y);
    uchar zp1  = rsGetElementAt_uchar(allocationSobel, x,y+1);
    uchar zp2  = rsGetElementAt_uchar(allocationSobel, x,y+1);

    uchar p1n2 = rsGetElementAt_uchar(allocationSobel, x+1,y-2);
    uchar p1n1 = rsGetElementAt_uchar(allocationSobel, x+1,y-1);
    uchar p1z  = rsGetElementAt_uchar(allocationSobel, x+1,y);
    uchar p1p1 = rsGetElementAt_uchar(allocationSobel, x+1,y+1);
    uchar p1p2 = rsGetElementAt_uchar(allocationSobel, x+1,y+1);

    uchar p2n2 = rsGetElementAt_uchar(allocationSobel, x+2,y-2);
    uchar p2n1 = rsGetElementAt_uchar(allocationSobel, x+2,y-1);
    uchar p2z  = rsGetElementAt_uchar(allocationSobel, x+2,y);
    uchar p2p1 = rsGetElementAt_uchar(allocationSobel, x+2,y+1);
    uchar p2p2 = rsGetElementAt_uchar(allocationSobel, x+2,y+1);

    //get x kernel value
    float xValue=((  n2n2+4*n2n1+ 6*n2z+4*n2p1+  n2p2) - (  p2n2+4*p2n1+ 6*p2z+4*p2p1+  p2p2) +
                  (2*n1n2+8*n1n1+12*n1z+8*n1p1+2*n1p2) - (2*p1n2+8*p1n1+12*p1z+8*p1p1+2*p1p2)) / 16;
    //get y kernel value
    float yValue=((  n2n2+4*n1n2+ 6*zn2+4*p1n2+  p2n2) - (  n2p2+4*n1p2+ 6*zp2+4*p1p2+  p2p2) +
                  (2*n2n1+8*n1n1+12*zn1+8*p1n1+2*p2n1) - (2*n2p1+8*n1p1+12*zp1+8*p1p1+2*p2p1)) / 16;
    return (float2){half_sqrt(xValue*xValue+yValue*yValue),(float)atan2pi((float)yValue, (float)xValue)};
}

uchar4 __attribute__ ((kernel)) sobel4(uint32_t x,uint32_t y){
  if(x>0 && y>0 && x<width-1 &&y<height-1){
    //get all values around pixel
    uchar n1n1=     rsGetElementAt_uchar(allocationSobel, x-1,y-1);
    uchar n1z=        rsGetElementAt_uchar(allocationSobel, x-1,y);
    uchar n1p1=  rsGetElementAt_uchar(allocationSobel, x-1,y+1);
    uchar zn1=         rsGetElementAt_uchar(allocationSobel, x,y-1);
    uchar zp1=      rsGetElementAt_uchar(allocationSobel, x,y+1);
    uchar p1n1=    rsGetElementAt_uchar(allocationSobel, x+1,y-1);
    uchar p1z=       rsGetElementAt_uchar(allocationSobel, x+1,y);
    uchar p1p1= rsGetElementAt_uchar(allocationSobel, x+1,y+1);
    //get x kernel value
    float xValue=(-n1n1-2*n1z-n1p1+p1n1+2*p1z+p1p1)/8;
    //get y kernel value
    float yValue=(-n1n1-2*zn1-p1n1+n1p1+2*zp1+p1p1)/8;
    uchar gray = (uchar) half_sqrt(xValue*xValue+yValue*yValue);
    return (uchar4){ gray, gray, gray, 254 };
  }else{
    return (uchar4){ 0, 0, 0, 0 };
  }
}

float2 __attribute__ ((kernel)) canny(uint32_t x, uint32_t y){
        float2 input = rsGetElementAt_float2(allocationCanny, x, y);
        float d=input.y;
        uchar direction = (uchar)(round(d * 4.0f) + 4) % 4;
        uchar i=(uchar)input.x;
        uchar output;
        if (direction == 0) {
            // horizontal, check n1z and p1z
            float2 a = rsGetElementAt_float2(allocationCanny, x - 1, y);
            float2 b = rsGetElementAt_float2(allocationCanny, x + 1, y);
            output=(uchar)a.x < i && (uchar)b.x < i ? i : 0;
        } else if (direction == 2) {
            // vertical, check above and below
            float2 a = rsGetElementAt_float2(allocationCanny, x, y - 1);
            float2 b = rsGetElementAt_float2(allocationCanny, x, y + 1);
            output=(uchar)a.x < i && (uchar)b.x < i ? i : 0;
        } else if (direction == 1) {
            // NW-SE
            float2 a = rsGetElementAt_float2(allocationCanny, x - 1, y - 1);
            float2 b = rsGetElementAt_float2(allocationCanny, x + 1, y + 1);
            output=(uchar)a.x < i && (uchar)b.x < i ? i : 0;
        } else{
            // NE-SW
            float2 a = rsGetElementAt_float2(allocationCanny, x + 1, y - 1);
            float2 b = rsGetElementAt_float2(allocationCanny, x - 1, y + 1);
            output=(uchar)a.x < i && (uchar)b.x < i ? i : 0;
        }
        if(output>UPPER){
            return (float2){255,d};
        }else if(output<LOWER){
            return (float2){0,0};
        }else{
            return (float2){255,d};
        }
}

uchar4 __attribute__ ((kernel)) canny4(uint32_t x, uint32_t y){
    if(x>0 && y>0 && x<width-1 &&y<height-1){
                float2 input = rsGetElementAt_float2(allocationCanny, x, y);
                float d=input.y;
                uchar direction = (uchar)(round(d * 4.0f) + 4) % 4;
                uchar i=(uchar)input.x;
                uchar output;
        if (direction == 0) {
            // horizontal, check n1z and p1z
            float2 a = rsGetElementAt_float2(allocationCanny, x - 1, y);
            float2 b = rsGetElementAt_float2(allocationCanny, x + 1, y);
            output=(uchar)a.x < i && (uchar)b.x < i ? i : 0;
        } else if (direction == 2) {
            // vertical, check above and below
            float2 a = rsGetElementAt_float2(allocationCanny, x, y - 1);
            float2 b = rsGetElementAt_float2(allocationCanny, x, y + 1);
            output=(uchar)a.x < i && (uchar)b.x < i ? i : 0;
        } else if (direction == 1) {
            // NW-SE
            float2 a = rsGetElementAt_float2(allocationCanny, x - 1, y - 1);
            float2 b = rsGetElementAt_float2(allocationCanny, x + 1, y + 1);
            output=(uchar)a.x < i && (uchar)b.x < i ? i : 0;
        } else{
            // NE-SW
            float2 a = rsGetElementAt_float2(allocationCanny, x + 1, y - 1);
            float2 b = rsGetElementAt_float2(allocationCanny, x - 1, y + 1);
            output=(uchar)a.x < i && (uchar)b.x < i ? i : 0;
        }
        if(output>UPPER){
            return (uchar4){ 255, 255, 255, 254 };
        }else if(output<LOWER){
            return (uchar4){ 0, 0, 0, 0 };
        }else{
            return (uchar4){ 255, 255, 255, 254 };
        }
    }else{
        return (uchar4){ 0, 0, 0, 0 };
    }
}

uchar4 __attribute__ ((kernel)) hysteresis(uint32_t x, uint32_t y){
    float2 input=rsGetElementAt_float2(allocationHysteresis, x,y);
    if(input.x==255){
      if(input.y!=0){
        if(fabs(rsGetElementAt_float2(allocationHysteresis, x - 1, y - 1).y-input.y)<gradientThreshold){
          return (uchar4){255,255,255,255};
        }else if(fabs(rsGetElementAt_float2(allocationHysteresis, x, y - 1).y-input.y)<gradientThreshold ){
          return (uchar4){255,255,255,255};
        }else if(fabs(rsGetElementAt_float2(allocationHysteresis, x + 1, y - 1).y-input.y)<gradientThreshold ){
          return (uchar4){255,255,255,255};
        }else if(fabs(rsGetElementAt_float2(allocationHysteresis, x - 1, y).y-input.y)<gradientThreshold ){
          return (uchar4){255,255,255,255};
        }else if(fabs(rsGetElementAt_float2(allocationHysteresis, x + 1, y).y-input.y)<gradientThreshold ){
          return (uchar4){255,255,255,255};
        }else if(fabs(rsGetElementAt_float2(allocationHysteresis, x - 1, y + 1).y-input.y)<gradientThreshold){
          return (uchar4){255,255,255,255};
        }else if(fabs(rsGetElementAt_float2(allocationHysteresis, x, y + 1).y-input.y)<gradientThreshold){
          return (uchar4){255,255,255,255};
        }else if(fabs(rsGetElementAt_float2(allocationHysteresis, x + 1, y + 1).y-input.y)<gradientThreshold){
          return (uchar4){255,255,255,255};
        }else{
          return (uchar4){0,0,0,255};
        }
      }else{
        return (uchar4){0,0,0,255};
      }
    }else if(input.x==0){
      return (uchar4){255,255,255,255};
    }else if(input.x==1){
      float2 a = rsGetElementAt_float2(allocationHysteresis, x - 1, y - 1);
      float2 b= rsGetElementAt_float2(allocationHysteresis, x, y - 1);
      float2 c= rsGetElementAt_float2(allocationHysteresis, x + 1, y - 1);
      float2 d= rsGetElementAt_float2(allocationHysteresis, x - 1, y);
      float2 e= rsGetElementAt_float2(allocationHysteresis, x + 1, y);
      float2 f= rsGetElementAt_float2(allocationHysteresis, x - 1, y + 1);
      float2 g= rsGetElementAt_float2(allocationHysteresis, x, y + 1);
      float2 h= rsGetElementAt_float2(allocationHysteresis, x + 1, y + 1);
      if(a.x+b.x+c.x+d.x+e.x+f.x+g.x+h.x>20){
        return (uchar4){0,0,0,255};
      }else{
        float2 a= rsGetElementAt_float2(allocationHysteresis, x - 2, y - 2);
        float2 b= rsGetElementAt_float2(allocationHysteresis, x - 1, y - 2);
        float2 c= rsGetElementAt_float2(allocationHysteresis, x, y - 2);
        float2 d= rsGetElementAt_float2(allocationHysteresis, x + 1, y - 2);
        float2 e= rsGetElementAt_float2(allocationHysteresis, x + 2, y - 2);
        float2 f= rsGetElementAt_float2(allocationHysteresis, x - 2, y - 1);
        float2 g= rsGetElementAt_float2(allocationHysteresis, x + 2, y - 1);
        float2 h= rsGetElementAt_float2(allocationHysteresis, x - 2, y);
        float2 i= rsGetElementAt_float2(allocationHysteresis, x + 2, y);
        float2 j= rsGetElementAt_float2(allocationHysteresis, x - 2, y + 1);
        float2 k= rsGetElementAt_float2(allocationHysteresis, x + 2, y + 1);
        float2 l= rsGetElementAt_float2(allocationHysteresis, x - 2, y + 2);
        float2 m= rsGetElementAt_float2(allocationHysteresis, x - 1, y + 2);
        float2 n= rsGetElementAt_float2(allocationHysteresis, x, y + 2);
        float2 o= rsGetElementAt_float2(allocationHysteresis, x + 1, y + 2);
        float2 p= rsGetElementAt_float2(allocationHysteresis, x + 2, y + 2);
        if(a.x+b.x+c.x+d.x+e.x+f.x+g.x+h.x+i.x+j.x+k.x+l.x+m.x+n.x+o.x+p.x>50){
          return (uchar4){0,0,0,255};
        }else{
          return (uchar4){255,255,255,255};
        }
      }
    }else{
      return (uchar4){255,255,255,255};
    }
}

/* 一般 3x3 就要 1/9, 5x5 應該就是 1/25
sharp        = new float[] {  0f, -1f, 0f, -1f,    5f, -1f, 0f, -1f, 0f };
sharp 一般表示法: [ 0, 0, 0, 0, 1, 0, 0, 0, 0 ] + amount * [ 0, −1, 0, −1, 4, −1, 0, −1, 0 ] (當然是2維的，amount = 1 時等於上面的式子
sharp5x5     = new float[] { −0.00391, −0.01563, −0.02344, −0.01563, −0.00391,
                             −0.01563, −0.06250, −0.09375, −0.06250, −0.01563,
                             −0.02344, −0.09375,  1.85980, −0.09375, −0.02344,
                             −0.01563, −0.06250, −0.09375, −0.06250, −0.01563
                             −0.00391, −0.01563, −0.02344, −0.01563, −0.00391 };

lighten      = new float[] {  0f,  0f, 0f,  0f,  1.5f,  0f, 0f,  0f, 0f };
blur         = new float[] {  1f,  1f, 1f,  1f,    1f,  1f, 1f,  1f, 1f } / 9;
blurGaussian = new float[] { 0.003765f, 0.015019f, 0.023792f, 0.015019f, 0.003765f,
                             0.015019f, 0.059912f, 0.094907f, 0.059912f, 0.015019f,
                             0.023792f, 0.094907f, 0.150342f, 0.094907f, 0.023792f,
                             0.015019f, 0.059912f, 0.094907f, 0.059912f, 0.015019f,
                             0.003765f, 0.015019f, 0.023792f, 0.015019f, 0.003765f};
darken       = new float[] {  0f,  0f, 0f,  0f,  0.5f,  0f, 0f,  0f, 0f };
edge         = new float[] {  0f,  1f, 0f,  1f, -4.0f,  1f, 0f,  1f, 0f };
emboss       = new float[] { -2f, -1f, 0f, -1f,  1.0f,  1f, 0f,  1f, 2f };
*/