#pragma version(1)
#pragma rs java_package_name(com.wade.imageviewrs)
#pragma rs_fp_relaxed
#include "utils.rsh"

uint32_t width = 0;
uint32_t height = 0;
uint32_t minX = 0, maxX = 0, minY = 1, maxY = 0;
uint32_t threshold, diff;

rs_allocation allocationIn;
rs_allocation allocationOut;

// 預訂的動作:
// 1) 只取中間+左右各10點，共 11*height
//    直接取出 gray,
//    順便做 HistEq 的前半
// 2) HistoEq 後半
// 3) 求連續性 + threshold

void init() {
    width = height = minX = maxX = minY = maxY = 0;
    threshold = diff = 0;
}

// step1: allocationIn(uchar4), allocationBar(uchar)
// 只要傳回值是 void, 就只需要 sc_bar.forEach_bar1(ain)
// 需要順便複製到 allocationOut....或許將來直接用 allocationIn 當 out?
uchar4 RS_KERNEL bar1(uchar4 in, uint32_t x, uint32_t y) {
    uchar gray  = rsGetElementAt_uchar4(allocationIn, x, y).g;
    if (minX <= x && x < maxX && minY <= y && y < maxY) {
        uchar p1    = rsGetElementAt_uchar4(allocationIn, x,y-1).g;
        uchar p3    = rsGetElementAt_uchar4(allocationIn, x,y+1).g;
        uint32_t sum = (uint32_t)(p1+gray+p3);
        uchar mean = (uchar)(sum/3);
        uchar d = (abs(p1-mean)+abs(gray-mean)+abs(p3-mean));
        if (sum >= 3*threshold && d < diff) {
            return (uchar4){ 254, 0, 0, 254 };
        } else return (uchar4){ 0, 0, 254, 254 };
    } else {
        return in;
    }
}
