#pragma version(1)
#pragma rs java_package_name(com.wade.imageviewrs)
#pragma rs_fp_relaxed

const static int fastThreshold = 10;
rs_allocation grayAllocation;

const static float3 grayMultipliers = {0.299f, 0.587f, 0.114f};

uchar RS_KERNEL rgbaToGray(uchar4 in, uint32_t x, uint32_t y) {

    uchar out = (uchar) ((float)in.r*grayMultipliers.r) +
                        ((float)in.g*grayMultipliers.g) +
                        ((float)in.b*grayMultipliers.b);

    return out;
}

static uchar getFASTPixel(uchar offset, int x, int y)
{
    switch(offset) {
        case 0: return rsGetElementAt_uchar(grayAllocation, x , y + 3); break;
        case 1: return rsGetElementAt_uchar(grayAllocation, x + 1 , y + 3); break;
        case 2: return rsGetElementAt_uchar(grayAllocation, x + 2 , y + 2); break;
        case 3: return rsGetElementAt_uchar(grayAllocation, x + 3 , y + 1); break;
        case 4: return rsGetElementAt_uchar(grayAllocation, x + 3 , y); break;
        case 5: return rsGetElementAt_uchar(grayAllocation, x + 3 , y + -1); break;
        case 6: return rsGetElementAt_uchar(grayAllocation, x + 2 , y - 2); break;
        case 7: return rsGetElementAt_uchar(grayAllocation, x + 1 , y - 3); break;
        case 8: return rsGetElementAt_uchar(grayAllocation, x , y - 3); break;
        case 9: return rsGetElementAt_uchar(grayAllocation, x - 1 , y - 3); break;
        case 10: return rsGetElementAt_uchar(grayAllocation, x - 2 , y - 2); break;
        case 11: return rsGetElementAt_uchar(grayAllocation, x - 3 , y  - 1); break;
        case 12: return rsGetElementAt_uchar(grayAllocation, x - 3 , y ); break;
        case 13: return rsGetElementAt_uchar(grayAllocation, x - 3, y + 1); break;
        case 14: return rsGetElementAt_uchar(grayAllocation, x - 2 , y + 2); break;
        case 15: return rsGetElementAt_uchar(grayAllocation, x -1, y + 3); break;
        case 16: return rsGetElementAt_uchar(grayAllocation, x, y); break;
    }
    return 0;
}
uchar4 __attribute__((kernel)) fastOptimized(uchar in, uint x, uint y) {
  int cb = in + fastThreshold;
  int c_b = in - fastThreshold;

  if (getFASTPixel(0, x, y) > cb)
    if (getFASTPixel(1, x, y) > cb)
      if (getFASTPixel(2, x, y) > cb)
        if (getFASTPixel(3, x, y) > cb)
          if (getFASTPixel(4, x, y) > cb)
            if (getFASTPixel(5, x, y) > cb)
              if (getFASTPixel(6, x, y) > cb)
                if (getFASTPixel(7, x, y) > cb)
                  if (getFASTPixel(8, x, y) > cb) {
                  } else if (getFASTPixel(15, x, y) > cb) {
                  } else
                    return (uchar4){ 0, 0, 0, 255 };
                else if (getFASTPixel(7, x, y) < c_b)
                  if (getFASTPixel(14, x, y) > cb)
                    if (getFASTPixel(15, x, y) > cb) {
                    } else
                      return (uchar4){ 0, 0, 0, 255 };
                  else if (getFASTPixel(14, x, y) < c_b)
                    if (getFASTPixel(8, x, y) < c_b)
                      if (getFASTPixel(9, x, y) < c_b)
                        if (getFASTPixel(10, x, y) < c_b)
                          if (getFASTPixel(11, x, y) < c_b)
                            if (getFASTPixel(12, x, y) < c_b)
                              if (getFASTPixel(13, x, y) < c_b)
                                if (getFASTPixel(15, x, y) < c_b) {
                                } else
                                  return (uchar4){ 0, 0, 0, 255 };
                              else
                                return (uchar4){ 0, 0, 0, 255 };
                            else
                              return (uchar4){ 0, 0, 0, 255 };
                          else
                            return (uchar4){ 0, 0, 0, 255 };
                        else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else if (getFASTPixel(14, x, y) > cb)
                  if (getFASTPixel(15, x, y) > cb) {
                  } else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else if (getFASTPixel(6, x, y) < c_b)
                if (getFASTPixel(15, x, y) > cb)
                  if (getFASTPixel(13, x, y) > cb)
                    if (getFASTPixel(14, x, y) > cb) {
                    } else
                      return (uchar4){ 0, 0, 0, 255 };
                  else if (getFASTPixel(13, x, y) < c_b)
                    if (getFASTPixel(7, x, y) < c_b)
                      if (getFASTPixel(8, x, y) < c_b)
                        if (getFASTPixel(9, x, y) < c_b)
                          if (getFASTPixel(10, x, y) < c_b)
                            if (getFASTPixel(11, x, y) < c_b)
                              if (getFASTPixel(12, x, y) < c_b)
                                if (getFASTPixel(14, x, y) < c_b) {
                                } else
                                  return (uchar4){ 0, 0, 0, 255 };
                              else
                                return (uchar4){ 0, 0, 0, 255 };
                            else
                              return (uchar4){ 0, 0, 0, 255 };
                          else
                            return (uchar4){ 0, 0, 0, 255 };
                        else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else if (getFASTPixel(7, x, y) < c_b)
                  if (getFASTPixel(8, x, y) < c_b)
                    if (getFASTPixel(9, x, y) < c_b)
                      if (getFASTPixel(10, x, y) < c_b)
                        if (getFASTPixel(11, x, y) < c_b)
                          if (getFASTPixel(12, x, y) < c_b)
                            if (getFASTPixel(13, x, y) < c_b)
                              if (getFASTPixel(14, x, y) < c_b) {
                              } else
                                return (uchar4){ 0, 0, 0, 255 };
                            else
                              return (uchar4){ 0, 0, 0, 255 };
                          else
                            return (uchar4){ 0, 0, 0, 255 };
                        else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else if (getFASTPixel(13, x, y) > cb)
                if (getFASTPixel(14, x, y) > cb)
                  if (getFASTPixel(15, x, y) > cb) {
                  } else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else if (getFASTPixel(13, x, y) < c_b)
                if (getFASTPixel(7, x, y) < c_b)
                  if (getFASTPixel(8, x, y) < c_b)
                    if (getFASTPixel(9, x, y) < c_b)
                      if (getFASTPixel(10, x, y) < c_b)
                        if (getFASTPixel(11, x, y) < c_b)
                          if (getFASTPixel(12, x, y) < c_b)
                            if (getFASTPixel(14, x, y) < c_b)
                              if (getFASTPixel(15, x, y) < c_b) {
                              } else
                                return (uchar4){ 0, 0, 0, 255 };
                            else
                              return (uchar4){ 0, 0, 0, 255 };
                          else
                            return (uchar4){ 0, 0, 0, 255 };
                        else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else if (getFASTPixel(5, x, y) < c_b)
              if (getFASTPixel(14, x, y) > cb)
                if (getFASTPixel(12, x, y) > cb)
                  if (getFASTPixel(13, x, y) > cb)
                    if (getFASTPixel(15, x, y) > cb) {
                    } else if (getFASTPixel(6, x, y) > cb)
                      if (getFASTPixel(7, x, y) > cb)
                        if (getFASTPixel(8, x, y) > cb)
                          if (getFASTPixel(9, x, y) > cb)
                            if (getFASTPixel(10, x, y) > cb)
                              if (getFASTPixel(11, x, y) > cb) {
                              } else
                                return (uchar4){ 0, 0, 0, 255 };
                            else
                              return (uchar4){ 0, 0, 0, 255 };
                          else
                            return (uchar4){ 0, 0, 0, 255 };
                        else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else if (getFASTPixel(12, x, y) < c_b)
                  if (getFASTPixel(6, x, y) < c_b)
                    if (getFASTPixel(7, x, y) < c_b)
                      if (getFASTPixel(8, x, y) < c_b)
                        if (getFASTPixel(9, x, y) < c_b)
                          if (getFASTPixel(10, x, y) < c_b)
                            if (getFASTPixel(11, x, y) < c_b)
                              if (getFASTPixel(13, x, y) < c_b) {
                              } else
                                return (uchar4){ 0, 0, 0, 255 };
                            else
                              return (uchar4){ 0, 0, 0, 255 };
                          else
                            return (uchar4){ 0, 0, 0, 255 };
                        else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else if (getFASTPixel(14, x, y) < c_b)
                if (getFASTPixel(7, x, y) < c_b)
                  if (getFASTPixel(8, x, y) < c_b)
                    if (getFASTPixel(9, x, y) < c_b)
                      if (getFASTPixel(10, x, y) < c_b)
                        if (getFASTPixel(11, x, y) < c_b)
                          if (getFASTPixel(12, x, y) < c_b)
                            if (getFASTPixel(13, x, y) < c_b)
                              if (getFASTPixel(6, x, y) < c_b) {
                              } else if (getFASTPixel(15, x, y) < c_b) {
                              } else
                                return (uchar4){ 0, 0, 0, 255 };
                            else
                              return (uchar4){ 0, 0, 0, 255 };
                          else
                            return (uchar4){ 0, 0, 0, 255 };
                        else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else if (getFASTPixel(6, x, y) < c_b)
                if (getFASTPixel(7, x, y) < c_b)
                  if (getFASTPixel(8, x, y) < c_b)
                    if (getFASTPixel(9, x, y) < c_b)
                      if (getFASTPixel(10, x, y) < c_b)
                        if (getFASTPixel(11, x, y) < c_b)
                          if (getFASTPixel(12, x, y) < c_b)
                            if (getFASTPixel(13, x, y) < c_b) {
                            } else
                              return (uchar4){ 0, 0, 0, 255 };
                          else
                            return (uchar4){ 0, 0, 0, 255 };
                        else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else if (getFASTPixel(12, x, y) > cb)
              if (getFASTPixel(13, x, y) > cb)
                if (getFASTPixel(14, x, y) > cb)
                  if (getFASTPixel(15, x, y) > cb) {
                  } else if (getFASTPixel(6, x, y) > cb)
                    if (getFASTPixel(7, x, y) > cb)
                      if (getFASTPixel(8, x, y) > cb)
                        if (getFASTPixel(9, x, y) > cb)
                          if (getFASTPixel(10, x, y) > cb)
                            if (getFASTPixel(11, x, y) > cb) {
                            } else
                              return (uchar4){ 0, 0, 0, 255 };
                          else
                            return (uchar4){ 0, 0, 0, 255 };
                        else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else if (getFASTPixel(12, x, y) < c_b)
              if (getFASTPixel(7, x, y) < c_b)
                if (getFASTPixel(8, x, y) < c_b)
                  if (getFASTPixel(9, x, y) < c_b)
                    if (getFASTPixel(10, x, y) < c_b)
                      if (getFASTPixel(11, x, y) < c_b)
                        if (getFASTPixel(13, x, y) < c_b)
                          if (getFASTPixel(14, x, y) < c_b)
                            if (getFASTPixel(6, x, y) < c_b) {
                            } else if (getFASTPixel(15, x, y) < c_b) {
                            } else
                              return (uchar4){ 0, 0, 0, 255 };
                          else
                            return (uchar4){ 0, 0, 0, 255 };
                        else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else
              return (uchar4){ 0, 0, 0, 255 };
          else if (getFASTPixel(4, x, y) < c_b)
            if (getFASTPixel(13, x, y) > cb)
              if (getFASTPixel(11, x, y) > cb)
                if (getFASTPixel(12, x, y) > cb)
                  if (getFASTPixel(14, x, y) > cb)
                    if (getFASTPixel(15, x, y) > cb) {
                    } else if (getFASTPixel(6, x, y) > cb)
                      if (getFASTPixel(7, x, y) > cb)
                        if (getFASTPixel(8, x, y) > cb)
                          if (getFASTPixel(9, x, y) > cb)
                            if (getFASTPixel(10, x, y) > cb) {
                            } else
                              return (uchar4){ 0, 0, 0, 255 };
                          else
                            return (uchar4){ 0, 0, 0, 255 };
                        else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else if (getFASTPixel(5, x, y) > cb)
                    if (getFASTPixel(6, x, y) > cb)
                      if (getFASTPixel(7, x, y) > cb)
                        if (getFASTPixel(8, x, y) > cb)
                          if (getFASTPixel(9, x, y) > cb)
                            if (getFASTPixel(10, x, y) > cb) {
                            } else
                              return (uchar4){ 0, 0, 0, 255 };
                          else
                            return (uchar4){ 0, 0, 0, 255 };
                        else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else if (getFASTPixel(11, x, y) < c_b)
                if (getFASTPixel(5, x, y) < c_b)
                  if (getFASTPixel(6, x, y) < c_b)
                    if (getFASTPixel(7, x, y) < c_b)
                      if (getFASTPixel(8, x, y) < c_b)
                        if (getFASTPixel(9, x, y) < c_b)
                          if (getFASTPixel(10, x, y) < c_b)
                            if (getFASTPixel(12, x, y) < c_b) {
                            } else
                              return (uchar4){ 0, 0, 0, 255 };
                          else
                            return (uchar4){ 0, 0, 0, 255 };
                        else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else if (getFASTPixel(13, x, y) < c_b)
              if (getFASTPixel(7, x, y) < c_b)
                if (getFASTPixel(8, x, y) < c_b)
                  if (getFASTPixel(9, x, y) < c_b)
                    if (getFASTPixel(10, x, y) < c_b)
                      if (getFASTPixel(11, x, y) < c_b)
                        if (getFASTPixel(12, x, y) < c_b)
                          if (getFASTPixel(6, x, y) < c_b)
                            if (getFASTPixel(5, x, y) < c_b) {
                            } else if (getFASTPixel(14, x, y) < c_b) {
                            } else
                              return (uchar4){ 0, 0, 0, 255 };
                          else if (getFASTPixel(14, x, y) < c_b)
                            if (getFASTPixel(15, x, y) < c_b) {
                            } else
                              return (uchar4){ 0, 0, 0, 255 };
                          else
                            return (uchar4){ 0, 0, 0, 255 };
                        else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else if (getFASTPixel(5, x, y) < c_b)
              if (getFASTPixel(6, x, y) < c_b)
                if (getFASTPixel(7, x, y) < c_b)
                  if (getFASTPixel(8, x, y) < c_b)
                    if (getFASTPixel(9, x, y) < c_b)
                      if (getFASTPixel(10, x, y) < c_b)
                        if (getFASTPixel(11, x, y) < c_b)
                          if (getFASTPixel(12, x, y) < c_b) {
                          } else
                            return (uchar4){ 0, 0, 0, 255 };
                        else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else
              return (uchar4){ 0, 0, 0, 255 };
          else if (getFASTPixel(11, x, y) > cb)
            if (getFASTPixel(12, x, y) > cb)
              if (getFASTPixel(13, x, y) > cb)
                if (getFASTPixel(14, x, y) > cb)
                  if (getFASTPixel(15, x, y) > cb) {
                  } else if (getFASTPixel(6, x, y) > cb)
                    if (getFASTPixel(7, x, y) > cb)
                      if (getFASTPixel(8, x, y) > cb)
                        if (getFASTPixel(9, x, y) > cb)
                          if (getFASTPixel(10, x, y) > cb) {
                          } else
                            return (uchar4){ 0, 0, 0, 255 };
                        else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else if (getFASTPixel(5, x, y) > cb)
                  if (getFASTPixel(6, x, y) > cb)
                    if (getFASTPixel(7, x, y) > cb)
                      if (getFASTPixel(8, x, y) > cb)
                        if (getFASTPixel(9, x, y) > cb)
                          if (getFASTPixel(10, x, y) > cb) {
                          } else
                            return (uchar4){ 0, 0, 0, 255 };
                        else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else
              return (uchar4){ 0, 0, 0, 255 };
          else if (getFASTPixel(11, x, y) < c_b)
            if (getFASTPixel(7, x, y) < c_b)
              if (getFASTPixel(8, x, y) < c_b)
                if (getFASTPixel(9, x, y) < c_b)
                  if (getFASTPixel(10, x, y) < c_b)
                    if (getFASTPixel(12, x, y) < c_b)
                      if (getFASTPixel(13, x, y) < c_b)
                        if (getFASTPixel(6, x, y) < c_b)
                          if (getFASTPixel(5, x, y) < c_b) {
                          } else if (getFASTPixel(14, x, y) < c_b) {
                          } else
                            return (uchar4){ 0, 0, 0, 255 };
                        else if (getFASTPixel(14, x, y) < c_b)
                          if (getFASTPixel(15, x, y) < c_b) {
                          } else
                            return (uchar4){ 0, 0, 0, 255 };
                        else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else
              return (uchar4){ 0, 0, 0, 255 };
          else
            return (uchar4){ 0, 0, 0, 255 };
        else if (getFASTPixel(3, x, y) < c_b)
          if (getFASTPixel(10, x, y) > cb)
            if (getFASTPixel(11, x, y) > cb)
              if (getFASTPixel(12, x, y) > cb)
                if (getFASTPixel(13, x, y) > cb)
                  if (getFASTPixel(14, x, y) > cb)
                    if (getFASTPixel(15, x, y) > cb) {
                    } else if (getFASTPixel(6, x, y) > cb)
                      if (getFASTPixel(7, x, y) > cb)
                        if (getFASTPixel(8, x, y) > cb)
                          if (getFASTPixel(9, x, y) > cb) {
                          } else
                            return (uchar4){ 0, 0, 0, 255 };
                        else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else if (getFASTPixel(5, x, y) > cb)
                    if (getFASTPixel(6, x, y) > cb)
                      if (getFASTPixel(7, x, y) > cb)
                        if (getFASTPixel(8, x, y) > cb)
                          if (getFASTPixel(9, x, y) > cb) {
                          } else
                            return (uchar4){ 0, 0, 0, 255 };
                        else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else if (getFASTPixel(4, x, y) > cb)
                  if (getFASTPixel(5, x, y) > cb)
                    if (getFASTPixel(6, x, y) > cb)
                      if (getFASTPixel(7, x, y) > cb)
                        if (getFASTPixel(8, x, y) > cb)
                          if (getFASTPixel(9, x, y) > cb) {
                          } else
                            return (uchar4){ 0, 0, 0, 255 };
                        else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else
              return (uchar4){ 0, 0, 0, 255 };
          else if (getFASTPixel(10, x, y) < c_b)
            if (getFASTPixel(7, x, y) < c_b)
              if (getFASTPixel(8, x, y) < c_b)
                if (getFASTPixel(9, x, y) < c_b)
                  if (getFASTPixel(11, x, y) < c_b)
                    if (getFASTPixel(6, x, y) < c_b)
                      if (getFASTPixel(5, x, y) < c_b)
                        if (getFASTPixel(4, x, y) < c_b) {
                        } else if (getFASTPixel(12, x, y) < c_b)
                          if (getFASTPixel(13, x, y) < c_b) {
                          } else
                            return (uchar4){ 0, 0, 0, 255 };
                        else
                          return (uchar4){ 0, 0, 0, 255 };
                      else if (getFASTPixel(12, x, y) < c_b)
                        if (getFASTPixel(13, x, y) < c_b)
                          if (getFASTPixel(14, x, y) < c_b) {
                          } else
                            return (uchar4){ 0, 0, 0, 255 };
                        else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else if (getFASTPixel(12, x, y) < c_b)
                      if (getFASTPixel(13, x, y) < c_b)
                        if (getFASTPixel(14, x, y) < c_b)
                          if (getFASTPixel(15, x, y) < c_b) {
                          } else
                            return (uchar4){ 0, 0, 0, 255 };
                        else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else
              return (uchar4){ 0, 0, 0, 255 };
          else
            return (uchar4){ 0, 0, 0, 255 };
        else if (getFASTPixel(10, x, y) > cb)
          if (getFASTPixel(11, x, y) > cb)
            if (getFASTPixel(12, x, y) > cb)
              if (getFASTPixel(13, x, y) > cb)
                if (getFASTPixel(14, x, y) > cb)
                  if (getFASTPixel(15, x, y) > cb) {
                  } else if (getFASTPixel(6, x, y) > cb)
                    if (getFASTPixel(7, x, y) > cb)
                      if (getFASTPixel(8, x, y) > cb)
                        if (getFASTPixel(9, x, y) > cb) {
                        } else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else if (getFASTPixel(5, x, y) > cb)
                  if (getFASTPixel(6, x, y) > cb)
                    if (getFASTPixel(7, x, y) > cb)
                      if (getFASTPixel(8, x, y) > cb)
                        if (getFASTPixel(9, x, y) > cb) {
                        } else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else if (getFASTPixel(4, x, y) > cb)
                if (getFASTPixel(5, x, y) > cb)
                  if (getFASTPixel(6, x, y) > cb)
                    if (getFASTPixel(7, x, y) > cb)
                      if (getFASTPixel(8, x, y) > cb)
                        if (getFASTPixel(9, x, y) > cb) {
                        } else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else
              return (uchar4){ 0, 0, 0, 255 };
          else
            return (uchar4){ 0, 0, 0, 255 };
        else if (getFASTPixel(10, x, y) < c_b)
          if (getFASTPixel(7, x, y) < c_b)
            if (getFASTPixel(8, x, y) < c_b)
              if (getFASTPixel(9, x, y) < c_b)
                if (getFASTPixel(11, x, y) < c_b)
                  if (getFASTPixel(12, x, y) < c_b)
                    if (getFASTPixel(6, x, y) < c_b)
                      if (getFASTPixel(5, x, y) < c_b)
                        if (getFASTPixel(4, x, y) < c_b) {
                        } else if (getFASTPixel(13, x, y) < c_b) {
                        } else
                          return (uchar4){ 0, 0, 0, 255 };
                      else if (getFASTPixel(13, x, y) < c_b)
                        if (getFASTPixel(14, x, y) < c_b) {
                        } else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else if (getFASTPixel(13, x, y) < c_b)
                      if (getFASTPixel(14, x, y) < c_b)
                        if (getFASTPixel(15, x, y) < c_b) {
                        } else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else
              return (uchar4){ 0, 0, 0, 255 };
          else
            return (uchar4){ 0, 0, 0, 255 };
        else
          return (uchar4){ 0, 0, 0, 255 };
      else if (getFASTPixel(2, x, y) < c_b)
        if (getFASTPixel(9, x, y) > cb)
          if (getFASTPixel(10, x, y) > cb)
            if (getFASTPixel(11, x, y) > cb)
              if (getFASTPixel(12, x, y) > cb)
                if (getFASTPixel(13, x, y) > cb)
                  if (getFASTPixel(14, x, y) > cb)
                    if (getFASTPixel(15, x, y) > cb) {
                    } else if (getFASTPixel(6, x, y) > cb)
                      if (getFASTPixel(7, x, y) > cb)
                        if (getFASTPixel(8, x, y) > cb) {
                        } else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else if (getFASTPixel(5, x, y) > cb)
                    if (getFASTPixel(6, x, y) > cb)
                      if (getFASTPixel(7, x, y) > cb)
                        if (getFASTPixel(8, x, y) > cb) {
                        } else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else if (getFASTPixel(4, x, y) > cb)
                  if (getFASTPixel(5, x, y) > cb)
                    if (getFASTPixel(6, x, y) > cb)
                      if (getFASTPixel(7, x, y) > cb)
                        if (getFASTPixel(8, x, y) > cb) {
                        } else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else if (getFASTPixel(3, x, y) > cb)
                if (getFASTPixel(4, x, y) > cb)
                  if (getFASTPixel(5, x, y) > cb)
                    if (getFASTPixel(6, x, y) > cb)
                      if (getFASTPixel(7, x, y) > cb)
                        if (getFASTPixel(8, x, y) > cb) {
                        } else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else
              return (uchar4){ 0, 0, 0, 255 };
          else
            return (uchar4){ 0, 0, 0, 255 };
        else if (getFASTPixel(9, x, y) < c_b)
          if (getFASTPixel(7, x, y) < c_b)
            if (getFASTPixel(8, x, y) < c_b)
              if (getFASTPixel(10, x, y) < c_b)
                if (getFASTPixel(6, x, y) < c_b)
                  if (getFASTPixel(5, x, y) < c_b)
                    if (getFASTPixel(4, x, y) < c_b)
                      if (getFASTPixel(3, x, y) < c_b) {
                      } else if (getFASTPixel(11, x, y) < c_b)
                        if (getFASTPixel(12, x, y) < c_b) {
                        } else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else if (getFASTPixel(11, x, y) < c_b)
                      if (getFASTPixel(12, x, y) < c_b)
                        if (getFASTPixel(13, x, y) < c_b) {
                        } else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else if (getFASTPixel(11, x, y) < c_b)
                    if (getFASTPixel(12, x, y) < c_b)
                      if (getFASTPixel(13, x, y) < c_b)
                        if (getFASTPixel(14, x, y) < c_b) {
                        } else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else if (getFASTPixel(11, x, y) < c_b)
                  if (getFASTPixel(12, x, y) < c_b)
                    if (getFASTPixel(13, x, y) < c_b)
                      if (getFASTPixel(14, x, y) < c_b)
                        if (getFASTPixel(15, x, y) < c_b) {
                        } else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else
              return (uchar4){ 0, 0, 0, 255 };
          else
            return (uchar4){ 0, 0, 0, 255 };
        else
          return (uchar4){ 0, 0, 0, 255 };
      else if (getFASTPixel(9, x, y) > cb)
        if (getFASTPixel(10, x, y) > cb)
          if (getFASTPixel(11, x, y) > cb)
            if (getFASTPixel(12, x, y) > cb)
              if (getFASTPixel(13, x, y) > cb)
                if (getFASTPixel(14, x, y) > cb)
                  if (getFASTPixel(15, x, y) > cb) {
                  } else if (getFASTPixel(6, x, y) > cb)
                    if (getFASTPixel(7, x, y) > cb)
                      if (getFASTPixel(8, x, y) > cb) {
                      } else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else if (getFASTPixel(5, x, y) > cb)
                  if (getFASTPixel(6, x, y) > cb)
                    if (getFASTPixel(7, x, y) > cb)
                      if (getFASTPixel(8, x, y) > cb) {
                      } else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else if (getFASTPixel(4, x, y) > cb)
                if (getFASTPixel(5, x, y) > cb)
                  if (getFASTPixel(6, x, y) > cb)
                    if (getFASTPixel(7, x, y) > cb)
                      if (getFASTPixel(8, x, y) > cb) {
                      } else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else if (getFASTPixel(3, x, y) > cb)
              if (getFASTPixel(4, x, y) > cb)
                if (getFASTPixel(5, x, y) > cb)
                  if (getFASTPixel(6, x, y) > cb)
                    if (getFASTPixel(7, x, y) > cb)
                      if (getFASTPixel(8, x, y) > cb) {
                      } else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else
              return (uchar4){ 0, 0, 0, 255 };
          else
            return (uchar4){ 0, 0, 0, 255 };
        else
          return (uchar4){ 0, 0, 0, 255 };
      else if (getFASTPixel(9, x, y) < c_b)
        if (getFASTPixel(7, x, y) < c_b)
          if (getFASTPixel(8, x, y) < c_b)
            if (getFASTPixel(10, x, y) < c_b)
              if (getFASTPixel(11, x, y) < c_b)
                if (getFASTPixel(6, x, y) < c_b)
                  if (getFASTPixel(5, x, y) < c_b)
                    if (getFASTPixel(4, x, y) < c_b)
                      if (getFASTPixel(3, x, y) < c_b) {
                      } else if (getFASTPixel(12, x, y) < c_b) {
                      } else
                        return (uchar4){ 0, 0, 0, 255 };
                    else if (getFASTPixel(12, x, y) < c_b)
                      if (getFASTPixel(13, x, y) < c_b) {
                      } else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else if (getFASTPixel(12, x, y) < c_b)
                    if (getFASTPixel(13, x, y) < c_b)
                      if (getFASTPixel(14, x, y) < c_b) {
                      } else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else if (getFASTPixel(12, x, y) < c_b)
                  if (getFASTPixel(13, x, y) < c_b)
                    if (getFASTPixel(14, x, y) < c_b)
                      if (getFASTPixel(15, x, y) < c_b) {
                      } else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else
              return (uchar4){ 0, 0, 0, 255 };
          else
            return (uchar4){ 0, 0, 0, 255 };
        else
          return (uchar4){ 0, 0, 0, 255 };
      else
        return (uchar4){ 0, 0, 0, 255 };
    else if (getFASTPixel(1, x, y) < c_b)
      if (getFASTPixel(8, x, y) > cb)
        if (getFASTPixel(9, x, y) > cb)
          if (getFASTPixel(10, x, y) > cb)
            if (getFASTPixel(11, x, y) > cb)
              if (getFASTPixel(12, x, y) > cb)
                if (getFASTPixel(13, x, y) > cb)
                  if (getFASTPixel(14, x, y) > cb)
                    if (getFASTPixel(15, x, y) > cb) {
                    } else if (getFASTPixel(6, x, y) > cb)
                      if (getFASTPixel(7, x, y) > cb) {
                      } else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else if (getFASTPixel(5, x, y) > cb)
                    if (getFASTPixel(6, x, y) > cb)
                      if (getFASTPixel(7, x, y) > cb) {
                      } else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else if (getFASTPixel(4, x, y) > cb)
                  if (getFASTPixel(5, x, y) > cb)
                    if (getFASTPixel(6, x, y) > cb)
                      if (getFASTPixel(7, x, y) > cb) {
                      } else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else if (getFASTPixel(3, x, y) > cb)
                if (getFASTPixel(4, x, y) > cb)
                  if (getFASTPixel(5, x, y) > cb)
                    if (getFASTPixel(6, x, y) > cb)
                      if (getFASTPixel(7, x, y) > cb) {
                      } else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else if (getFASTPixel(2, x, y) > cb)
              if (getFASTPixel(3, x, y) > cb)
                if (getFASTPixel(4, x, y) > cb)
                  if (getFASTPixel(5, x, y) > cb)
                    if (getFASTPixel(6, x, y) > cb)
                      if (getFASTPixel(7, x, y) > cb) {
                      } else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else
              return (uchar4){ 0, 0, 0, 255 };
          else
            return (uchar4){ 0, 0, 0, 255 };
        else
          return (uchar4){ 0, 0, 0, 255 };
      else if (getFASTPixel(8, x, y) < c_b)
        if (getFASTPixel(7, x, y) < c_b)
          if (getFASTPixel(9, x, y) < c_b)
            if (getFASTPixel(6, x, y) < c_b)
              if (getFASTPixel(5, x, y) < c_b)
                if (getFASTPixel(4, x, y) < c_b)
                  if (getFASTPixel(3, x, y) < c_b)
                    if (getFASTPixel(2, x, y) < c_b) {
                    } else if (getFASTPixel(10, x, y) < c_b)
                      if (getFASTPixel(11, x, y) < c_b) {
                      } else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else if (getFASTPixel(10, x, y) < c_b)
                    if (getFASTPixel(11, x, y) < c_b)
                      if (getFASTPixel(12, x, y) < c_b) {
                      } else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else if (getFASTPixel(10, x, y) < c_b)
                  if (getFASTPixel(11, x, y) < c_b)
                    if (getFASTPixel(12, x, y) < c_b)
                      if (getFASTPixel(13, x, y) < c_b) {
                      } else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else if (getFASTPixel(10, x, y) < c_b)
                if (getFASTPixel(11, x, y) < c_b)
                  if (getFASTPixel(12, x, y) < c_b)
                    if (getFASTPixel(13, x, y) < c_b)
                      if (getFASTPixel(14, x, y) < c_b) {
                      } else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else if (getFASTPixel(10, x, y) < c_b)
              if (getFASTPixel(11, x, y) < c_b)
                if (getFASTPixel(12, x, y) < c_b)
                  if (getFASTPixel(13, x, y) < c_b)
                    if (getFASTPixel(14, x, y) < c_b)
                      if (getFASTPixel(15, x, y) < c_b) {
                      } else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else
              return (uchar4){ 0, 0, 0, 255 };
          else
            return (uchar4){ 0, 0, 0, 255 };
        else
          return (uchar4){ 0, 0, 0, 255 };
      else
        return (uchar4){ 0, 0, 0, 255 };
    else if (getFASTPixel(8, x, y) > cb)
      if (getFASTPixel(9, x, y) > cb)
        if (getFASTPixel(10, x, y) > cb)
          if (getFASTPixel(11, x, y) > cb)
            if (getFASTPixel(12, x, y) > cb)
              if (getFASTPixel(13, x, y) > cb)
                if (getFASTPixel(14, x, y) > cb)
                  if (getFASTPixel(15, x, y) > cb) {
                  } else if (getFASTPixel(6, x, y) > cb)
                    if (getFASTPixel(7, x, y) > cb) {
                    } else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else if (getFASTPixel(5, x, y) > cb)
                  if (getFASTPixel(6, x, y) > cb)
                    if (getFASTPixel(7, x, y) > cb) {
                    } else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else if (getFASTPixel(4, x, y) > cb)
                if (getFASTPixel(5, x, y) > cb)
                  if (getFASTPixel(6, x, y) > cb)
                    if (getFASTPixel(7, x, y) > cb) {
                    } else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else if (getFASTPixel(3, x, y) > cb)
              if (getFASTPixel(4, x, y) > cb)
                if (getFASTPixel(5, x, y) > cb)
                  if (getFASTPixel(6, x, y) > cb)
                    if (getFASTPixel(7, x, y) > cb) {
                    } else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else
              return (uchar4){ 0, 0, 0, 255 };
          else if (getFASTPixel(2, x, y) > cb)
            if (getFASTPixel(3, x, y) > cb)
              if (getFASTPixel(4, x, y) > cb)
                if (getFASTPixel(5, x, y) > cb)
                  if (getFASTPixel(6, x, y) > cb)
                    if (getFASTPixel(7, x, y) > cb) {
                    } else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else
              return (uchar4){ 0, 0, 0, 255 };
          else
            return (uchar4){ 0, 0, 0, 255 };
        else
          return (uchar4){ 0, 0, 0, 255 };
      else
        return (uchar4){ 0, 0, 0, 255 };
    else if (getFASTPixel(8, x, y) < c_b)
      if (getFASTPixel(7, x, y) < c_b)
        if (getFASTPixel(9, x, y) < c_b)
          if (getFASTPixel(10, x, y) < c_b)
            if (getFASTPixel(6, x, y) < c_b)
              if (getFASTPixel(5, x, y) < c_b)
                if (getFASTPixel(4, x, y) < c_b)
                  if (getFASTPixel(3, x, y) < c_b)
                    if (getFASTPixel(2, x, y) < c_b) {
                    } else if (getFASTPixel(11, x, y) < c_b) {
                    } else
                      return (uchar4){ 0, 0, 0, 255 };
                  else if (getFASTPixel(11, x, y) < c_b)
                    if (getFASTPixel(12, x, y) < c_b) {
                    } else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else if (getFASTPixel(11, x, y) < c_b)
                  if (getFASTPixel(12, x, y) < c_b)
                    if (getFASTPixel(13, x, y) < c_b) {
                    } else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else if (getFASTPixel(11, x, y) < c_b)
                if (getFASTPixel(12, x, y) < c_b)
                  if (getFASTPixel(13, x, y) < c_b)
                    if (getFASTPixel(14, x, y) < c_b) {
                    } else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else if (getFASTPixel(11, x, y) < c_b)
              if (getFASTPixel(12, x, y) < c_b)
                if (getFASTPixel(13, x, y) < c_b)
                  if (getFASTPixel(14, x, y) < c_b)
                    if (getFASTPixel(15, x, y) < c_b) {
                    } else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else
              return (uchar4){ 0, 0, 0, 255 };
          else
            return (uchar4){ 0, 0, 0, 255 };
        else
          return (uchar4){ 0, 0, 0, 255 };
      else
        return (uchar4){ 0, 0, 0, 255 };
    else
      return (uchar4){ 0, 0, 0, 255 };
  else if (getFASTPixel(0, x, y) < c_b)
    if (getFASTPixel(1, x, y) > cb)
      if (getFASTPixel(8, x, y) > cb)
        if (getFASTPixel(7, x, y) > cb)
          if (getFASTPixel(9, x, y) > cb)
            if (getFASTPixel(6, x, y) > cb)
              if (getFASTPixel(5, x, y) > cb)
                if (getFASTPixel(4, x, y) > cb)
                  if (getFASTPixel(3, x, y) > cb)
                    if (getFASTPixel(2, x, y) > cb) {
                    } else if (getFASTPixel(10, x, y) > cb)
                      if (getFASTPixel(11, x, y) > cb) {
                      } else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else if (getFASTPixel(10, x, y) > cb)
                    if (getFASTPixel(11, x, y) > cb)
                      if (getFASTPixel(12, x, y) > cb) {
                      } else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else if (getFASTPixel(10, x, y) > cb)
                  if (getFASTPixel(11, x, y) > cb)
                    if (getFASTPixel(12, x, y) > cb)
                      if (getFASTPixel(13, x, y) > cb) {
                      } else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else if (getFASTPixel(10, x, y) > cb)
                if (getFASTPixel(11, x, y) > cb)
                  if (getFASTPixel(12, x, y) > cb)
                    if (getFASTPixel(13, x, y) > cb)
                      if (getFASTPixel(14, x, y) > cb) {
                      } else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else if (getFASTPixel(10, x, y) > cb)
              if (getFASTPixel(11, x, y) > cb)
                if (getFASTPixel(12, x, y) > cb)
                  if (getFASTPixel(13, x, y) > cb)
                    if (getFASTPixel(14, x, y) > cb)
                      if (getFASTPixel(15, x, y) > cb) {
                      } else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else
              return (uchar4){ 0, 0, 0, 255 };
          else
            return (uchar4){ 0, 0, 0, 255 };
        else
          return (uchar4){ 0, 0, 0, 255 };
      else if (getFASTPixel(8, x, y) < c_b)
        if (getFASTPixel(9, x, y) < c_b)
          if (getFASTPixel(10, x, y) < c_b)
            if (getFASTPixel(11, x, y) < c_b)
              if (getFASTPixel(12, x, y) < c_b)
                if (getFASTPixel(13, x, y) < c_b)
                  if (getFASTPixel(14, x, y) < c_b)
                    if (getFASTPixel(15, x, y) < c_b) {
                    } else if (getFASTPixel(6, x, y) < c_b)
                      if (getFASTPixel(7, x, y) < c_b) {
                      } else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else if (getFASTPixel(5, x, y) < c_b)
                    if (getFASTPixel(6, x, y) < c_b)
                      if (getFASTPixel(7, x, y) < c_b) {
                      } else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else if (getFASTPixel(4, x, y) < c_b)
                  if (getFASTPixel(5, x, y) < c_b)
                    if (getFASTPixel(6, x, y) < c_b)
                      if (getFASTPixel(7, x, y) < c_b) {
                      } else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else if (getFASTPixel(3, x, y) < c_b)
                if (getFASTPixel(4, x, y) < c_b)
                  if (getFASTPixel(5, x, y) < c_b)
                    if (getFASTPixel(6, x, y) < c_b)
                      if (getFASTPixel(7, x, y) < c_b) {
                      } else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else if (getFASTPixel(2, x, y) < c_b)
              if (getFASTPixel(3, x, y) < c_b)
                if (getFASTPixel(4, x, y) < c_b)
                  if (getFASTPixel(5, x, y) < c_b)
                    if (getFASTPixel(6, x, y) < c_b)
                      if (getFASTPixel(7, x, y) < c_b) {
                      } else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else
              return (uchar4){ 0, 0, 0, 255 };
          else
            return (uchar4){ 0, 0, 0, 255 };
        else
          return (uchar4){ 0, 0, 0, 255 };
      else
        return (uchar4){ 0, 0, 0, 255 };
    else if (getFASTPixel(1, x, y) < c_b)
      if (getFASTPixel(2, x, y) > cb)
        if (getFASTPixel(9, x, y) > cb)
          if (getFASTPixel(7, x, y) > cb)
            if (getFASTPixel(8, x, y) > cb)
              if (getFASTPixel(10, x, y) > cb)
                if (getFASTPixel(6, x, y) > cb)
                  if (getFASTPixel(5, x, y) > cb)
                    if (getFASTPixel(4, x, y) > cb)
                      if (getFASTPixel(3, x, y) > cb) {
                      } else if (getFASTPixel(11, x, y) > cb)
                        if (getFASTPixel(12, x, y) > cb) {
                        } else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else if (getFASTPixel(11, x, y) > cb)
                      if (getFASTPixel(12, x, y) > cb)
                        if (getFASTPixel(13, x, y) > cb) {
                        } else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else if (getFASTPixel(11, x, y) > cb)
                    if (getFASTPixel(12, x, y) > cb)
                      if (getFASTPixel(13, x, y) > cb)
                        if (getFASTPixel(14, x, y) > cb) {
                        } else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else if (getFASTPixel(11, x, y) > cb)
                  if (getFASTPixel(12, x, y) > cb)
                    if (getFASTPixel(13, x, y) > cb)
                      if (getFASTPixel(14, x, y) > cb)
                        if (getFASTPixel(15, x, y) > cb) {
                        } else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else
              return (uchar4){ 0, 0, 0, 255 };
          else
            return (uchar4){ 0, 0, 0, 255 };
        else if (getFASTPixel(9, x, y) < c_b)
          if (getFASTPixel(10, x, y) < c_b)
            if (getFASTPixel(11, x, y) < c_b)
              if (getFASTPixel(12, x, y) < c_b)
                if (getFASTPixel(13, x, y) < c_b)
                  if (getFASTPixel(14, x, y) < c_b)
                    if (getFASTPixel(15, x, y) < c_b) {
                    } else if (getFASTPixel(6, x, y) < c_b)
                      if (getFASTPixel(7, x, y) < c_b)
                        if (getFASTPixel(8, x, y) < c_b) {
                        } else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else if (getFASTPixel(5, x, y) < c_b)
                    if (getFASTPixel(6, x, y) < c_b)
                      if (getFASTPixel(7, x, y) < c_b)
                        if (getFASTPixel(8, x, y) < c_b) {
                        } else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else if (getFASTPixel(4, x, y) < c_b)
                  if (getFASTPixel(5, x, y) < c_b)
                    if (getFASTPixel(6, x, y) < c_b)
                      if (getFASTPixel(7, x, y) < c_b)
                        if (getFASTPixel(8, x, y) < c_b) {
                        } else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else if (getFASTPixel(3, x, y) < c_b)
                if (getFASTPixel(4, x, y) < c_b)
                  if (getFASTPixel(5, x, y) < c_b)
                    if (getFASTPixel(6, x, y) < c_b)
                      if (getFASTPixel(7, x, y) < c_b)
                        if (getFASTPixel(8, x, y) < c_b) {
                        } else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else
              return (uchar4){ 0, 0, 0, 255 };
          else
            return (uchar4){ 0, 0, 0, 255 };
        else
          return (uchar4){ 0, 0, 0, 255 };
      else if (getFASTPixel(2, x, y) < c_b)
        if (getFASTPixel(3, x, y) > cb)
          if (getFASTPixel(10, x, y) > cb)
            if (getFASTPixel(7, x, y) > cb)
              if (getFASTPixel(8, x, y) > cb)
                if (getFASTPixel(9, x, y) > cb)
                  if (getFASTPixel(11, x, y) > cb)
                    if (getFASTPixel(6, x, y) > cb)
                      if (getFASTPixel(5, x, y) > cb)
                        if (getFASTPixel(4, x, y) > cb) {
                        } else if (getFASTPixel(12, x, y) > cb)
                          if (getFASTPixel(13, x, y) > cb) {
                          } else
                            return (uchar4){ 0, 0, 0, 255 };
                        else
                          return (uchar4){ 0, 0, 0, 255 };
                      else if (getFASTPixel(12, x, y) > cb)
                        if (getFASTPixel(13, x, y) > cb)
                          if (getFASTPixel(14, x, y) > cb) {
                          } else
                            return (uchar4){ 0, 0, 0, 255 };
                        else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else if (getFASTPixel(12, x, y) > cb)
                      if (getFASTPixel(13, x, y) > cb)
                        if (getFASTPixel(14, x, y) > cb)
                          if (getFASTPixel(15, x, y) > cb) {
                          } else
                            return (uchar4){ 0, 0, 0, 255 };
                        else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else
              return (uchar4){ 0, 0, 0, 255 };
          else if (getFASTPixel(10, x, y) < c_b)
            if (getFASTPixel(11, x, y) < c_b)
              if (getFASTPixel(12, x, y) < c_b)
                if (getFASTPixel(13, x, y) < c_b)
                  if (getFASTPixel(14, x, y) < c_b)
                    if (getFASTPixel(15, x, y) < c_b) {
                    } else if (getFASTPixel(6, x, y) < c_b)
                      if (getFASTPixel(7, x, y) < c_b)
                        if (getFASTPixel(8, x, y) < c_b)
                          if (getFASTPixel(9, x, y) < c_b) {
                          } else
                            return (uchar4){ 0, 0, 0, 255 };
                        else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else if (getFASTPixel(5, x, y) < c_b)
                    if (getFASTPixel(6, x, y) < c_b)
                      if (getFASTPixel(7, x, y) < c_b)
                        if (getFASTPixel(8, x, y) < c_b)
                          if (getFASTPixel(9, x, y) < c_b) {
                          } else
                            return (uchar4){ 0, 0, 0, 255 };
                        else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else if (getFASTPixel(4, x, y) < c_b)
                  if (getFASTPixel(5, x, y) < c_b)
                    if (getFASTPixel(6, x, y) < c_b)
                      if (getFASTPixel(7, x, y) < c_b)
                        if (getFASTPixel(8, x, y) < c_b)
                          if (getFASTPixel(9, x, y) < c_b) {
                          } else
                            return (uchar4){ 0, 0, 0, 255 };
                        else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else
              return (uchar4){ 0, 0, 0, 255 };
          else
            return (uchar4){ 0, 0, 0, 255 };
        else if (getFASTPixel(3, x, y) < c_b)
          if (getFASTPixel(4, x, y) > cb)
            if (getFASTPixel(13, x, y) > cb)
              if (getFASTPixel(7, x, y) > cb)
                if (getFASTPixel(8, x, y) > cb)
                  if (getFASTPixel(9, x, y) > cb)
                    if (getFASTPixel(10, x, y) > cb)
                      if (getFASTPixel(11, x, y) > cb)
                        if (getFASTPixel(12, x, y) > cb)
                          if (getFASTPixel(6, x, y) > cb)
                            if (getFASTPixel(5, x, y) > cb) {
                            } else if (getFASTPixel(14, x, y) > cb) {
                            } else
                              return (uchar4){ 0, 0, 0, 255 };
                          else if (getFASTPixel(14, x, y) > cb)
                            if (getFASTPixel(15, x, y) > cb) {
                            } else
                              return (uchar4){ 0, 0, 0, 255 };
                          else
                            return (uchar4){ 0, 0, 0, 255 };
                        else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else if (getFASTPixel(13, x, y) < c_b)
              if (getFASTPixel(11, x, y) > cb)
                if (getFASTPixel(5, x, y) > cb)
                  if (getFASTPixel(6, x, y) > cb)
                    if (getFASTPixel(7, x, y) > cb)
                      if (getFASTPixel(8, x, y) > cb)
                        if (getFASTPixel(9, x, y) > cb)
                          if (getFASTPixel(10, x, y) > cb)
                            if (getFASTPixel(12, x, y) > cb) {
                            } else
                              return (uchar4){ 0, 0, 0, 255 };
                          else
                            return (uchar4){ 0, 0, 0, 255 };
                        else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else if (getFASTPixel(11, x, y) < c_b)
                if (getFASTPixel(12, x, y) < c_b)
                  if (getFASTPixel(14, x, y) < c_b)
                    if (getFASTPixel(15, x, y) < c_b) {
                    } else if (getFASTPixel(6, x, y) < c_b)
                      if (getFASTPixel(7, x, y) < c_b)
                        if (getFASTPixel(8, x, y) < c_b)
                          if (getFASTPixel(9, x, y) < c_b)
                            if (getFASTPixel(10, x, y) < c_b) {
                            } else
                              return (uchar4){ 0, 0, 0, 255 };
                          else
                            return (uchar4){ 0, 0, 0, 255 };
                        else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else if (getFASTPixel(5, x, y) < c_b)
                    if (getFASTPixel(6, x, y) < c_b)
                      if (getFASTPixel(7, x, y) < c_b)
                        if (getFASTPixel(8, x, y) < c_b)
                          if (getFASTPixel(9, x, y) < c_b)
                            if (getFASTPixel(10, x, y) < c_b) {
                            } else
                              return (uchar4){ 0, 0, 0, 255 };
                          else
                            return (uchar4){ 0, 0, 0, 255 };
                        else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else if (getFASTPixel(5, x, y) > cb)
              if (getFASTPixel(6, x, y) > cb)
                if (getFASTPixel(7, x, y) > cb)
                  if (getFASTPixel(8, x, y) > cb)
                    if (getFASTPixel(9, x, y) > cb)
                      if (getFASTPixel(10, x, y) > cb)
                        if (getFASTPixel(11, x, y) > cb)
                          if (getFASTPixel(12, x, y) > cb) {
                          } else
                            return (uchar4){ 0, 0, 0, 255 };
                        else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else
              return (uchar4){ 0, 0, 0, 255 };
          else if (getFASTPixel(4, x, y) < c_b)
            if (getFASTPixel(5, x, y) > cb)
              if (getFASTPixel(14, x, y) > cb)
                if (getFASTPixel(7, x, y) > cb)
                  if (getFASTPixel(8, x, y) > cb)
                    if (getFASTPixel(9, x, y) > cb)
                      if (getFASTPixel(10, x, y) > cb)
                        if (getFASTPixel(11, x, y) > cb)
                          if (getFASTPixel(12, x, y) > cb)
                            if (getFASTPixel(13, x, y) > cb)
                              if (getFASTPixel(6, x, y) > cb) {
                              } else if (getFASTPixel(15, x, y) > cb) {
                              } else
                                return (uchar4){ 0, 0, 0, 255 };
                            else
                              return (uchar4){ 0, 0, 0, 255 };
                          else
                            return (uchar4){ 0, 0, 0, 255 };
                        else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else if (getFASTPixel(14, x, y) < c_b)
                if (getFASTPixel(12, x, y) > cb)
                  if (getFASTPixel(6, x, y) > cb)
                    if (getFASTPixel(7, x, y) > cb)
                      if (getFASTPixel(8, x, y) > cb)
                        if (getFASTPixel(9, x, y) > cb)
                          if (getFASTPixel(10, x, y) > cb)
                            if (getFASTPixel(11, x, y) > cb)
                              if (getFASTPixel(13, x, y) > cb) {
                              } else
                                return (uchar4){ 0, 0, 0, 255 };
                            else
                              return (uchar4){ 0, 0, 0, 255 };
                          else
                            return (uchar4){ 0, 0, 0, 255 };
                        else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else if (getFASTPixel(12, x, y) < c_b)
                  if (getFASTPixel(13, x, y) < c_b)
                    if (getFASTPixel(15, x, y) < c_b) {
                    } else if (getFASTPixel(6, x, y) < c_b)
                      if (getFASTPixel(7, x, y) < c_b)
                        if (getFASTPixel(8, x, y) < c_b)
                          if (getFASTPixel(9, x, y) < c_b)
                            if (getFASTPixel(10, x, y) < c_b)
                              if (getFASTPixel(11, x, y) < c_b) {
                              } else
                                return (uchar4){ 0, 0, 0, 255 };
                            else
                              return (uchar4){ 0, 0, 0, 255 };
                          else
                            return (uchar4){ 0, 0, 0, 255 };
                        else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else if (getFASTPixel(6, x, y) > cb)
                if (getFASTPixel(7, x, y) > cb)
                  if (getFASTPixel(8, x, y) > cb)
                    if (getFASTPixel(9, x, y) > cb)
                      if (getFASTPixel(10, x, y) > cb)
                        if (getFASTPixel(11, x, y) > cb)
                          if (getFASTPixel(12, x, y) > cb)
                            if (getFASTPixel(13, x, y) > cb) {
                            } else
                              return (uchar4){ 0, 0, 0, 255 };
                          else
                            return (uchar4){ 0, 0, 0, 255 };
                        else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else if (getFASTPixel(5, x, y) < c_b)
              if (getFASTPixel(6, x, y) > cb)
                if (getFASTPixel(15, x, y) < c_b)
                  if (getFASTPixel(13, x, y) > cb)
                    if (getFASTPixel(7, x, y) > cb)
                      if (getFASTPixel(8, x, y) > cb)
                        if (getFASTPixel(9, x, y) > cb)
                          if (getFASTPixel(10, x, y) > cb)
                            if (getFASTPixel(11, x, y) > cb)
                              if (getFASTPixel(12, x, y) > cb)
                                if (getFASTPixel(14, x, y) > cb) {
                                } else
                                  return (uchar4){ 0, 0, 0, 255 };
                              else
                                return (uchar4){ 0, 0, 0, 255 };
                            else
                              return (uchar4){ 0, 0, 0, 255 };
                          else
                            return (uchar4){ 0, 0, 0, 255 };
                        else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else if (getFASTPixel(13, x, y) < c_b)
                    if (getFASTPixel(14, x, y) < c_b) {
                    } else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else if (getFASTPixel(7, x, y) > cb)
                  if (getFASTPixel(8, x, y) > cb)
                    if (getFASTPixel(9, x, y) > cb)
                      if (getFASTPixel(10, x, y) > cb)
                        if (getFASTPixel(11, x, y) > cb)
                          if (getFASTPixel(12, x, y) > cb)
                            if (getFASTPixel(13, x, y) > cb)
                              if (getFASTPixel(14, x, y) > cb) {
                              } else
                                return (uchar4){ 0, 0, 0, 255 };
                            else
                              return (uchar4){ 0, 0, 0, 255 };
                          else
                            return (uchar4){ 0, 0, 0, 255 };
                        else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else if (getFASTPixel(6, x, y) < c_b)
                if (getFASTPixel(7, x, y) > cb)
                  if (getFASTPixel(14, x, y) > cb)
                    if (getFASTPixel(8, x, y) > cb)
                      if (getFASTPixel(9, x, y) > cb)
                        if (getFASTPixel(10, x, y) > cb)
                          if (getFASTPixel(11, x, y) > cb)
                            if (getFASTPixel(12, x, y) > cb)
                              if (getFASTPixel(13, x, y) > cb)
                                if (getFASTPixel(15, x, y) > cb) {
                                } else
                                  return (uchar4){ 0, 0, 0, 255 };
                              else
                                return (uchar4){ 0, 0, 0, 255 };
                            else
                              return (uchar4){ 0, 0, 0, 255 };
                          else
                            return (uchar4){ 0, 0, 0, 255 };
                        else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else if (getFASTPixel(14, x, y) < c_b)
                    if (getFASTPixel(15, x, y) < c_b) {
                    } else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else if (getFASTPixel(7, x, y) < c_b)
                  if (getFASTPixel(8, x, y) < c_b) {
                  } else if (getFASTPixel(15, x, y) < c_b) {
                  } else
                    return (uchar4){ 0, 0, 0, 255 };
                else if (getFASTPixel(14, x, y) < c_b)
                  if (getFASTPixel(15, x, y) < c_b) {
                  } else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else if (getFASTPixel(13, x, y) > cb)
                if (getFASTPixel(7, x, y) > cb)
                  if (getFASTPixel(8, x, y) > cb)
                    if (getFASTPixel(9, x, y) > cb)
                      if (getFASTPixel(10, x, y) > cb)
                        if (getFASTPixel(11, x, y) > cb)
                          if (getFASTPixel(12, x, y) > cb)
                            if (getFASTPixel(14, x, y) > cb)
                              if (getFASTPixel(15, x, y) > cb) {
                              } else
                                return (uchar4){ 0, 0, 0, 255 };
                            else
                              return (uchar4){ 0, 0, 0, 255 };
                          else
                            return (uchar4){ 0, 0, 0, 255 };
                        else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else if (getFASTPixel(13, x, y) < c_b)
                if (getFASTPixel(14, x, y) < c_b)
                  if (getFASTPixel(15, x, y) < c_b) {
                  } else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else if (getFASTPixel(12, x, y) > cb)
              if (getFASTPixel(7, x, y) > cb)
                if (getFASTPixel(8, x, y) > cb)
                  if (getFASTPixel(9, x, y) > cb)
                    if (getFASTPixel(10, x, y) > cb)
                      if (getFASTPixel(11, x, y) > cb)
                        if (getFASTPixel(13, x, y) > cb)
                          if (getFASTPixel(14, x, y) > cb)
                            if (getFASTPixel(6, x, y) > cb) {
                            } else if (getFASTPixel(15, x, y) > cb) {
                            } else
                              return (uchar4){ 0, 0, 0, 255 };
                          else
                            return (uchar4){ 0, 0, 0, 255 };
                        else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else if (getFASTPixel(12, x, y) < c_b)
              if (getFASTPixel(13, x, y) < c_b)
                if (getFASTPixel(14, x, y) < c_b)
                  if (getFASTPixel(15, x, y) < c_b) {
                  } else if (getFASTPixel(6, x, y) < c_b)
                    if (getFASTPixel(7, x, y) < c_b)
                      if (getFASTPixel(8, x, y) < c_b)
                        if (getFASTPixel(9, x, y) < c_b)
                          if (getFASTPixel(10, x, y) < c_b)
                            if (getFASTPixel(11, x, y) < c_b) {
                            } else
                              return (uchar4){ 0, 0, 0, 255 };
                          else
                            return (uchar4){ 0, 0, 0, 255 };
                        else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else
              return (uchar4){ 0, 0, 0, 255 };
          else if (getFASTPixel(11, x, y) > cb)
            if (getFASTPixel(7, x, y) > cb)
              if (getFASTPixel(8, x, y) > cb)
                if (getFASTPixel(9, x, y) > cb)
                  if (getFASTPixel(10, x, y) > cb)
                    if (getFASTPixel(12, x, y) > cb)
                      if (getFASTPixel(13, x, y) > cb)
                        if (getFASTPixel(6, x, y) > cb)
                          if (getFASTPixel(5, x, y) > cb) {
                          } else if (getFASTPixel(14, x, y) > cb) {
                          } else
                            return (uchar4){ 0, 0, 0, 255 };
                        else if (getFASTPixel(14, x, y) > cb)
                          if (getFASTPixel(15, x, y) > cb) {
                          } else
                            return (uchar4){ 0, 0, 0, 255 };
                        else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else
              return (uchar4){ 0, 0, 0, 255 };
          else if (getFASTPixel(11, x, y) < c_b)
            if (getFASTPixel(12, x, y) < c_b)
              if (getFASTPixel(13, x, y) < c_b)
                if (getFASTPixel(14, x, y) < c_b)
                  if (getFASTPixel(15, x, y) < c_b) {
                  } else if (getFASTPixel(6, x, y) < c_b)
                    if (getFASTPixel(7, x, y) < c_b)
                      if (getFASTPixel(8, x, y) < c_b)
                        if (getFASTPixel(9, x, y) < c_b)
                          if (getFASTPixel(10, x, y) < c_b) {
                          } else
                            return (uchar4){ 0, 0, 0, 255 };
                        else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else if (getFASTPixel(5, x, y) < c_b)
                  if (getFASTPixel(6, x, y) < c_b)
                    if (getFASTPixel(7, x, y) < c_b)
                      if (getFASTPixel(8, x, y) < c_b)
                        if (getFASTPixel(9, x, y) < c_b)
                          if (getFASTPixel(10, x, y) < c_b) {
                          } else
                            return (uchar4){ 0, 0, 0, 255 };
                        else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else
              return (uchar4){ 0, 0, 0, 255 };
          else
            return (uchar4){ 0, 0, 0, 255 };
        else if (getFASTPixel(10, x, y) > cb)
          if (getFASTPixel(7, x, y) > cb)
            if (getFASTPixel(8, x, y) > cb)
              if (getFASTPixel(9, x, y) > cb)
                if (getFASTPixel(11, x, y) > cb)
                  if (getFASTPixel(12, x, y) > cb)
                    if (getFASTPixel(6, x, y) > cb)
                      if (getFASTPixel(5, x, y) > cb)
                        if (getFASTPixel(4, x, y) > cb) {
                        } else if (getFASTPixel(13, x, y) > cb) {
                        } else
                          return (uchar4){ 0, 0, 0, 255 };
                      else if (getFASTPixel(13, x, y) > cb)
                        if (getFASTPixel(14, x, y) > cb) {
                        } else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else if (getFASTPixel(13, x, y) > cb)
                      if (getFASTPixel(14, x, y) > cb)
                        if (getFASTPixel(15, x, y) > cb) {
                        } else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else
              return (uchar4){ 0, 0, 0, 255 };
          else
            return (uchar4){ 0, 0, 0, 255 };
        else if (getFASTPixel(10, x, y) < c_b)
          if (getFASTPixel(11, x, y) < c_b)
            if (getFASTPixel(12, x, y) < c_b)
              if (getFASTPixel(13, x, y) < c_b)
                if (getFASTPixel(14, x, y) < c_b)
                  if (getFASTPixel(15, x, y) < c_b) {
                  } else if (getFASTPixel(6, x, y) < c_b)
                    if (getFASTPixel(7, x, y) < c_b)
                      if (getFASTPixel(8, x, y) < c_b)
                        if (getFASTPixel(9, x, y) < c_b) {
                        } else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else if (getFASTPixel(5, x, y) < c_b)
                  if (getFASTPixel(6, x, y) < c_b)
                    if (getFASTPixel(7, x, y) < c_b)
                      if (getFASTPixel(8, x, y) < c_b)
                        if (getFASTPixel(9, x, y) < c_b) {
                        } else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else if (getFASTPixel(4, x, y) < c_b)
                if (getFASTPixel(5, x, y) < c_b)
                  if (getFASTPixel(6, x, y) < c_b)
                    if (getFASTPixel(7, x, y) < c_b)
                      if (getFASTPixel(8, x, y) < c_b)
                        if (getFASTPixel(9, x, y) < c_b) {
                        } else
                          return (uchar4){ 0, 0, 0, 255 };
                      else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else
              return (uchar4){ 0, 0, 0, 255 };
          else
            return (uchar4){ 0, 0, 0, 255 };
        else
          return (uchar4){ 0, 0, 0, 255 };
      else if (getFASTPixel(9, x, y) > cb)
        if (getFASTPixel(7, x, y) > cb)
          if (getFASTPixel(8, x, y) > cb)
            if (getFASTPixel(10, x, y) > cb)
              if (getFASTPixel(11, x, y) > cb)
                if (getFASTPixel(6, x, y) > cb)
                  if (getFASTPixel(5, x, y) > cb)
                    if (getFASTPixel(4, x, y) > cb)
                      if (getFASTPixel(3, x, y) > cb) {
                      } else if (getFASTPixel(12, x, y) > cb) {
                      } else
                        return (uchar4){ 0, 0, 0, 255 };
                    else if (getFASTPixel(12, x, y) > cb)
                      if (getFASTPixel(13, x, y) > cb) {
                      } else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else if (getFASTPixel(12, x, y) > cb)
                    if (getFASTPixel(13, x, y) > cb)
                      if (getFASTPixel(14, x, y) > cb) {
                      } else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else if (getFASTPixel(12, x, y) > cb)
                  if (getFASTPixel(13, x, y) > cb)
                    if (getFASTPixel(14, x, y) > cb)
                      if (getFASTPixel(15, x, y) > cb) {
                      } else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else
              return (uchar4){ 0, 0, 0, 255 };
          else
            return (uchar4){ 0, 0, 0, 255 };
        else
          return (uchar4){ 0, 0, 0, 255 };
      else if (getFASTPixel(9, x, y) < c_b)
        if (getFASTPixel(10, x, y) < c_b)
          if (getFASTPixel(11, x, y) < c_b)
            if (getFASTPixel(12, x, y) < c_b)
              if (getFASTPixel(13, x, y) < c_b)
                if (getFASTPixel(14, x, y) < c_b)
                  if (getFASTPixel(15, x, y) < c_b) {
                  } else if (getFASTPixel(6, x, y) < c_b)
                    if (getFASTPixel(7, x, y) < c_b)
                      if (getFASTPixel(8, x, y) < c_b) {
                      } else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else if (getFASTPixel(5, x, y) < c_b)
                  if (getFASTPixel(6, x, y) < c_b)
                    if (getFASTPixel(7, x, y) < c_b)
                      if (getFASTPixel(8, x, y) < c_b) {
                      } else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else if (getFASTPixel(4, x, y) < c_b)
                if (getFASTPixel(5, x, y) < c_b)
                  if (getFASTPixel(6, x, y) < c_b)
                    if (getFASTPixel(7, x, y) < c_b)
                      if (getFASTPixel(8, x, y) < c_b) {
                      } else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else if (getFASTPixel(3, x, y) < c_b)
              if (getFASTPixel(4, x, y) < c_b)
                if (getFASTPixel(5, x, y) < c_b)
                  if (getFASTPixel(6, x, y) < c_b)
                    if (getFASTPixel(7, x, y) < c_b)
                      if (getFASTPixel(8, x, y) < c_b) {
                      } else
                        return (uchar4){ 0, 0, 0, 255 };
                    else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else
              return (uchar4){ 0, 0, 0, 255 };
          else
            return (uchar4){ 0, 0, 0, 255 };
        else
          return (uchar4){ 0, 0, 0, 255 };
      else
        return (uchar4){ 0, 0, 0, 255 };
    else if (getFASTPixel(8, x, y) > cb)
      if (getFASTPixel(7, x, y) > cb)
        if (getFASTPixel(9, x, y) > cb)
          if (getFASTPixel(10, x, y) > cb)
            if (getFASTPixel(6, x, y) > cb)
              if (getFASTPixel(5, x, y) > cb)
                if (getFASTPixel(4, x, y) > cb)
                  if (getFASTPixel(3, x, y) > cb)
                    if (getFASTPixel(2, x, y) > cb) {
                    } else if (getFASTPixel(11, x, y) > cb) {
                    } else
                      return (uchar4){ 0, 0, 0, 255 };
                  else if (getFASTPixel(11, x, y) > cb)
                    if (getFASTPixel(12, x, y) > cb) {
                    } else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else if (getFASTPixel(11, x, y) > cb)
                  if (getFASTPixel(12, x, y) > cb)
                    if (getFASTPixel(13, x, y) > cb) {
                    } else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else if (getFASTPixel(11, x, y) > cb)
                if (getFASTPixel(12, x, y) > cb)
                  if (getFASTPixel(13, x, y) > cb)
                    if (getFASTPixel(14, x, y) > cb) {
                    } else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else if (getFASTPixel(11, x, y) > cb)
              if (getFASTPixel(12, x, y) > cb)
                if (getFASTPixel(13, x, y) > cb)
                  if (getFASTPixel(14, x, y) > cb)
                    if (getFASTPixel(15, x, y) > cb) {
                    } else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else
              return (uchar4){ 0, 0, 0, 255 };
          else
            return (uchar4){ 0, 0, 0, 255 };
        else
          return (uchar4){ 0, 0, 0, 255 };
      else
        return (uchar4){ 0, 0, 0, 255 };
    else if (getFASTPixel(8, x, y) < c_b)
      if (getFASTPixel(9, x, y) < c_b)
        if (getFASTPixel(10, x, y) < c_b)
          if (getFASTPixel(11, x, y) < c_b)
            if (getFASTPixel(12, x, y) < c_b)
              if (getFASTPixel(13, x, y) < c_b)
                if (getFASTPixel(14, x, y) < c_b)
                  if (getFASTPixel(15, x, y) < c_b) {
                  } else if (getFASTPixel(6, x, y) < c_b)
                    if (getFASTPixel(7, x, y) < c_b) {
                    } else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else if (getFASTPixel(5, x, y) < c_b)
                  if (getFASTPixel(6, x, y) < c_b)
                    if (getFASTPixel(7, x, y) < c_b) {
                    } else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else if (getFASTPixel(4, x, y) < c_b)
                if (getFASTPixel(5, x, y) < c_b)
                  if (getFASTPixel(6, x, y) < c_b)
                    if (getFASTPixel(7, x, y) < c_b) {
                    } else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else if (getFASTPixel(3, x, y) < c_b)
              if (getFASTPixel(4, x, y) < c_b)
                if (getFASTPixel(5, x, y) < c_b)
                  if (getFASTPixel(6, x, y) < c_b)
                    if (getFASTPixel(7, x, y) < c_b) {
                    } else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else
              return (uchar4){ 0, 0, 0, 255 };
          else if (getFASTPixel(2, x, y) < c_b)
            if (getFASTPixel(3, x, y) < c_b)
              if (getFASTPixel(4, x, y) < c_b)
                if (getFASTPixel(5, x, y) < c_b)
                  if (getFASTPixel(6, x, y) < c_b)
                    if (getFASTPixel(7, x, y) < c_b) {
                    } else
                      return (uchar4){ 0, 0, 0, 255 };
                  else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else
              return (uchar4){ 0, 0, 0, 255 };
          else
            return (uchar4){ 0, 0, 0, 255 };
        else
          return (uchar4){ 0, 0, 0, 255 };
      else
        return (uchar4){ 0, 0, 0, 255 };
    else
      return (uchar4){ 0, 0, 0, 255 };
  else if (getFASTPixel(7, x, y) > cb)
    if (getFASTPixel(8, x, y) > cb)
      if (getFASTPixel(9, x, y) > cb)
        if (getFASTPixel(6, x, y) > cb)
          if (getFASTPixel(5, x, y) > cb)
            if (getFASTPixel(4, x, y) > cb)
              if (getFASTPixel(3, x, y) > cb)
                if (getFASTPixel(2, x, y) > cb)
                  if (getFASTPixel(1, x, y) > cb) {
                  } else if (getFASTPixel(10, x, y) > cb) {
                  } else
                    return (uchar4){ 0, 0, 0, 255 };
                else if (getFASTPixel(10, x, y) > cb)
                  if (getFASTPixel(11, x, y) > cb) {
                  } else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else if (getFASTPixel(10, x, y) > cb)
                if (getFASTPixel(11, x, y) > cb)
                  if (getFASTPixel(12, x, y) > cb) {
                  } else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else if (getFASTPixel(10, x, y) > cb)
              if (getFASTPixel(11, x, y) > cb)
                if (getFASTPixel(12, x, y) > cb)
                  if (getFASTPixel(13, x, y) > cb) {
                  } else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else
              return (uchar4){ 0, 0, 0, 255 };
          else if (getFASTPixel(10, x, y) > cb)
            if (getFASTPixel(11, x, y) > cb)
              if (getFASTPixel(12, x, y) > cb)
                if (getFASTPixel(13, x, y) > cb)
                  if (getFASTPixel(14, x, y) > cb) {
                  } else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else
              return (uchar4){ 0, 0, 0, 255 };
          else
            return (uchar4){ 0, 0, 0, 255 };
        else if (getFASTPixel(10, x, y) > cb)
          if (getFASTPixel(11, x, y) > cb)
            if (getFASTPixel(12, x, y) > cb)
              if (getFASTPixel(13, x, y) > cb)
                if (getFASTPixel(14, x, y) > cb)
                  if (getFASTPixel(15, x, y) > cb) {
                  } else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else
              return (uchar4){ 0, 0, 0, 255 };
          else
            return (uchar4){ 0, 0, 0, 255 };
        else
          return (uchar4){ 0, 0, 0, 255 };
      else
        return (uchar4){ 0, 0, 0, 255 };
    else
      return (uchar4){ 0, 0, 0, 255 };
  else if (getFASTPixel(7, x, y) < c_b)
    if (getFASTPixel(8, x, y) < c_b)
      if (getFASTPixel(9, x, y) < c_b)
        if (getFASTPixel(6, x, y) < c_b)
          if (getFASTPixel(5, x, y) < c_b)
            if (getFASTPixel(4, x, y) < c_b)
              if (getFASTPixel(3, x, y) < c_b)
                if (getFASTPixel(2, x, y) < c_b)
                  if (getFASTPixel(1, x, y) < c_b) {
                  } else if (getFASTPixel(10, x, y) < c_b) {
                  } else
                    return (uchar4){ 0, 0, 0, 255 };
                else if (getFASTPixel(10, x, y) < c_b)
                  if (getFASTPixel(11, x, y) < c_b) {
                  } else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else if (getFASTPixel(10, x, y) < c_b)
                if (getFASTPixel(11, x, y) < c_b)
                  if (getFASTPixel(12, x, y) < c_b) {
                  } else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else if (getFASTPixel(10, x, y) < c_b)
              if (getFASTPixel(11, x, y) < c_b)
                if (getFASTPixel(12, x, y) < c_b)
                  if (getFASTPixel(13, x, y) < c_b) {
                  } else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else
              return (uchar4){ 0, 0, 0, 255 };
          else if (getFASTPixel(10, x, y) < c_b)
            if (getFASTPixel(11, x, y) < c_b)
              if (getFASTPixel(12, x, y) < c_b)
                if (getFASTPixel(13, x, y) < c_b)
                  if (getFASTPixel(14, x, y) < c_b) {
                  } else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else
              return (uchar4){ 0, 0, 0, 255 };
          else
            return (uchar4){ 0, 0, 0, 255 };
        else if (getFASTPixel(10, x, y) < c_b)
          if (getFASTPixel(11, x, y) < c_b)
            if (getFASTPixel(12, x, y) < c_b)
              if (getFASTPixel(13, x, y) < c_b)
                if (getFASTPixel(14, x, y) < c_b)
                  if (getFASTPixel(15, x, y) < c_b) {
                  } else
                    return (uchar4){ 0, 0, 0, 255 };
                else
                  return (uchar4){ 0, 0, 0, 255 };
              else
                return (uchar4){ 0, 0, 0, 255 };
            else
              return (uchar4){ 0, 0, 0, 255 };
          else
            return (uchar4){ 0, 0, 0, 255 };
        else
          return (uchar4){ 0, 0, 0, 255 };
      else
        return (uchar4){ 0, 0, 0, 255 };
    else
      return (uchar4){ 0, 0, 0, 255 };
  else
    return (uchar4){ 0, 0, 0, 255 };

  // Point is a FAST keypoint
  return (uchar4){ 255, 0, 0, 255 };
}
