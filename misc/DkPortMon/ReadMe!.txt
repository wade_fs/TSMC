==============================================================================
DkPortMon
------------------------------------------------------------------------------

==============================================================================
1. Structure of this packet
------------------------------------------------------------------------------
|
+-Article
|
+-Bin
|  +-Win7
|  +-WinXP
|    
+-Sources
     +-DkPortMon02
     +-DkPortMonGui

Bin directory contain binary (PE file) DkPortMonGui.exe, and it's sub directory
contain driver file, WinXP -> driver file for Windows XP plus inf file, 
Win7 -> driver file for Windows 7 (UNTESTED!) plus inf file.
Source directory contain sources for the driver and DkPortMon client (GUI).


==============================================================================
2. Warning!
------------------------------------------------------------------------------
This software is provided "AS IS". I will not responsible for any efects that 
may caused by using this software.
Because this is driver software, you may experience the (un)famous "Blue Screen
of Death" in your computer, and in the worst case may lost your 
data and even damage you computer. So you have been warned.


==============================================================================
3. License
------------------------------------------------------------------------------
Use BSD License