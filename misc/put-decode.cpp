#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <errno.h>

inline unsigned char inv(unsigned char m) { int r, i=8; for (; i--; r=r*2|m&1,m>>=1); return (unsigned char)r; }
inline unsigned int inv2(unsigned int m) { int r, i=16; for (; i--; r=r*2|m&1,m>>=1); return (unsigned int)r; }
inline void dump(unsigned char* buf, int bufLen) { printf ("> \""); for (int i=0; i < bufLen; i++) { printf ("%c", buf[i]); } printf ("\"\n"); }
inline int readHexByte(unsigned int *inp) {
#ifdef DEBUG
  int ret = scanf("%x", inp);
  printf ("%02X ", (unsigned int)(*inp & 0xFF));
  return (ret == 1);
#else
  return (scanf("%x", inp) == 1);
#endif
}

int main(int argc, char* argv[])
{
  unsigned char buf[2048];
  unsigned char buf2[32];
  unsigned char ch, *bufp;
  unsigned int inp, len;
  int blockIndex=0, gen;
  int isStartFC = 0, isStartF5 = 0, isEndPack = 0;
  // pch 每個封包的資料段都要重設為 0，指向這個資料段的位置,
  // 而 bufLen 則指向整個 buf 目前長度，也就是儲放資料的現在位置，所以除非是 Multi 的第一個資料段，其他的資料段會一直遞增不會重設
  int pch = 0, bufLen = 0;
  int checksum = 0, myChecksum = 80; // 80 = inv(0x0A) for init
  int dataSecNum = 0;

  // 00. 這邊有一段應該初始化變數，目前寫在宣告區
  //     ...... init() ........

  bzero(buf, sizeof(buf));

  // 01. 一直讀到 F5 FF 為止
  pch = 0;
  while (readHexByte(&inp)) {
    buf2[pch++] = (unsigned char)(inp & 0xFF);
    if (inp == (unsigned int)0xF5) {
      // F5 後面還有個 0xFF
      if (!readHexByte(&inp)) break;
      buf2[pch++] = (unsigned char)(inp & 0xFF);
      if (inp != (unsigned int)0xFF) return 1; // error package
      gen = buf2[pch - 13] == (unsigned char) 0x07? 1 : 2; // 應該只有兩種 07(single), 7B(Multi)
      len = inv(~buf2[pch - 3] & 0xFF) << 8 | inv(~buf2[pch - 4] & 0xFF);
      if (gen == 1)
        len -= 4; // 記得，它的 len 要略過 len(x2) 本身 + F5 FF, 所以是減 4
      else
        len -= 3; // 第2+代的話，它的長度比較奇怪，要修正 1, 當然計算時一樣要略過 len(x2) 本身 + F5 FF, 所以是減 3 = 4-1
      pch = 0;
      break;
    } 
  }
 
  while (readHexByte(&inp)) { // scanf("%x", (unsigned int*)&inp) == 1) {
    ch = (unsigned char)(inp & 0xFF);
    if (!isEndPack) {
      if (pch < len) {
        buf[bufLen] = (unsigned char)inv(~ch);
        myChecksum += (int)(buf[bufLen] & 0xFF);
        ++pch; ++bufLen;
      }
      if (pch >= len) { // 如果讀夠了，就要設定 isEndPack, 以便進行下一件事囉
        if (!readHexByte(&inp)) break;
        isEndPack = 1;
        if (dataSecNum == 0) { // 只有第一個資料段才會有 blockIndex
          // 05. 接下來就比較複雜，要根據 gen 07(single), 7B(Multi)
          if (gen == 1) { // single 要讀 block index(x1)
            blockIndex = (unsigned char)(inv((unsigned char)(~inp & 0xFF)));
            if (!readHexByte(&inp)) break;
          }
          myChecksum += blockIndex;
        }

        // 06. 到這邊要讀進 2 組 2 個 bytes 的 checksum
        //     這邊一樣採用自己讀進兩個 checksum byte 的方式，而不是透過 while() 讀進來，這樣程式比較好理解
        checksum = (unsigned)(inv((unsigned char)(~inp & 0xFF)));
        // 略過一個沒用的 byte
        if (!readHexByte(&inp)) break;
        if (!readHexByte(&inp)) break;
        checksum |= (unsigned)(inv((unsigned char)(~inp & 0xFF)) << 8);
        // 略過一個沒用的 byte
        if (!readHexByte(&inp)) break;
        // 6.1 檢查 checksum 是否正確
        if (checksum != myChecksum) {
          printf ("Error checksum %d (%04X) != %d(%04X)\n", checksum, (unsigned int)~inv2(checksum), myChecksum, (unsigned int)~inv2(myChecksum));
          return 1;
        }
      }
      else continue;
    }
    // 07. 到了這邊，如果是 single 的話，已經是讀完了, 如果是 Multi, 則還要略過兩個 bytes
    if (gen == 1) {
      // 把 single 的資料往前搬
      bufLen = 224-blockIndex;
      memcpy(buf, buf+blockIndex, bufLen);
      break;
    }

    // 讀到下一個 FC FF 為止
    pch = 0;
    /* 先讀3個 byte, 有可能是 length, 而這個 length 有可能剛剛好是 F5 FF 那就會誤判
       而且前面的 bytes 有可能含有 B5 FF ... 那就是另外的封包了
    */
    if (readHexByte(&inp)) buf2[pch++] = (unsigned char)(inp & 0xFF);
    if (readHexByte(&inp)) buf2[pch++] = (unsigned char)(inp & 0xFF);
    if (readHexByte(&inp)) buf2[pch++] = (unsigned char)(inp & 0xFF);
    if (buf2[0] == (unsigned)0xB5 || buf2[1] == (unsigned)0xB5 || buf2[2] == (unsigned)0xB5) {
      while (readHexByte(&inp));
    }
    while (readHexByte(&inp)) {
      buf2[pch++] = (unsigned char)(inp & 0xFF);
      if (inp == (unsigned int)0xF5) {
        // F5 後面還有個 0xFF
        if (!readHexByte(&inp)) break;
        buf2[pch++] = (unsigned char)(inp & 0xFF);
        if (inp != (unsigned int)0xFF) return 1; // error package
        len = inv(~buf2[pch-3]) << 8 | inv(~buf2[pch-4]);
        if (gen == 1)
          len -= 4; // 記得，它的 len 要略過 len(x2) 本身 + F5 FF, 所以是減 4
        else
          len -= 3; // 第2+代的話，它的長度比較奇怪，要修正 1, 當然計算時一樣要略過 len(x2) 本身 + F5 FF, 所以是減 3 = 4-1
        break;
      }
      // 遇到 B5 FF 後面就停止...
      if (inp == (unsigned int)0xB5) {
        while (readHexByte(&inp));
      }
    }

    // 7.1. 因為第一個封包段其實不含任何資訊，所以要重設 buf
    ++dataSecNum;
    isStartF5 = isEndPack = pch = checksum = 0;
    myChecksum = 80; // 80 = inv(~F5) + inv(~FF)
  }
  dump(buf, bufLen);
  return 0;
}
