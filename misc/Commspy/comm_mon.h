
// comm_mon.h

#ifndef _INC_COMM_MON_
#define _INC_COMM_MON_



#define ZERO_MEMORY(s)  ::ZeroMemory(&s, sizeof(s))

#define MAX_RXBLOCK   80

// flow control
#define FC_DTRDSR  0x01
#define FC_RTSCTS  0x02
#define FC_XONXOFF 0x04

#define ASCII_XON  0x11
#define ASCII_XOFF 0x13


typedef struct _TTYSTRUCT
{
   BYTE byCommPort;  // zero based port - 3 or higher implies TELNET
   BYTE byXonXoff;
   BYTE byByteSize; 
   BYTE byFlowCtrl;
   BYTE byParity; 
   BYTE byStopBits;
   DWORD dwBaudRate;

} TTYSTRUCT, *LPTTYSTRUCT;



typedef struct _CONNECTION 
{
   HANDLE hCommDev;
   BOOL bConnected;
   OVERLAPPED osWrite;
   OVERLAPPED osRead;
   DWORD dwThreadID;

} CONNECTION, *LPCONNECTION;




typedef int CALLBACK MONITORPROC(HGLOBAL, int, LPARAM);
typedef MONITORPROC *LPMONITORPROC;


typedef struct _MONITORPROCPARMS
{
   LPCONNECTION lpConn;
   LPMONITORPROC lpCallback;
   LPARAM lpCallbackParam;

} MONITORPROCPARAMS, *LPMONITORPROCPARAMS;





class GCommMonitor
{
public:
	GCommMonitor();

   // connect to a comm port
   BOOL Connect(LPTTYSTRUCT lpTTY);
   // disconnect 
   BOOL Disconnect();

   // monitor (read) data at open connection
   BOOL Monitor(LPMONITORPROC lpMonitorProc, LPARAM lParam=NULL);
   BOOL IsConnected() const;

protected:
   // thread entry point
   static DWORD CALLBACK CommMonitorProc(LPVOID lpThreadParameter);
   // helper function to monitor comm port
   static BOOL CALLBACK CommReadBlock(LPCONNECTION lpConn, LPBYTE lpszBlock, int nLen);

   // information about the current connection and
   // monitoring thread
   LPCONNECTION m_pConn;
   HGLOBAL m_hConn;
   LPMONITORPROCPARAMS m_pMPP;
   HGLOBAL m_hMPP;

public:
	virtual ~GCommMonitor();
};


#endif


