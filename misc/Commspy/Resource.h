//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by Commspy.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_COMMSPY_DIALOG              102
#define IDR_MAINFRAME                   128
#define IDB_BITMAP_BLACK                129
#define IDB_BITMAP_DKGRAY               130
#define IDC_DISPLAY                     1000
#define IDC_START                       1001
#define IDC_COLOR_BK                    1002
#define IDC_COLOR_FG                    1003
#define IDC_BAUD                        1004
#define IDC_DATA                        1005
#define IDC_PARITY                      1006
#define IDC_STOP                        1007
#define IDC_STOPBITS                    1007
#define IDC_PORT                        1008
#define IDC_FONT                        1009
#define IDC_CLEAR                       1010
#define IDC_HEX                         1011
#define IDC_DECIMAL                     1012
#define IDC_OCTAL                       1013
#define IDC_ASCII                       1014
#define IDC_RESUME_SCROLL               1015
#define IDC_TRIGGER                     1016
#define IDC_PRINT                       1017
#define IDC_GROUP_DISPLAY               1018
#define IDC_ONTOP                       1019
#define IDC_BUTTON1                     1020
#define IDC_FLOW                        1022

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1023
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
