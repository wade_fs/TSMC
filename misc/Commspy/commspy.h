// commspy.h : main header file for the COMMSPY application
//

#ifndef _INC_COMMSPY_
#define _INC_COMMSPY_

#include "resource.h"		// main symbols


void ReportLastError();


/////////////////////////////////////////////////////////////////////////////
// CCommspyApp:
class CCommspyApp : public CWinApp
{
public:
	CCommspyApp();

public:
	virtual BOOL InitInstance();

	DECLARE_MESSAGE_MAP()
};


#endif
