
// util_gdi.h

#ifndef _INC_UTIL_GDI_
#define _INC_UTIL_GDI_


typedef struct tag3DPens
{
   CPen *pPenBlack;
   CPen *pPenDarkGray;
   CPen *pPenWhite;
   CPen *pPenGray;

   tag3DPens();
   ~tag3DPens();

} _3DPENS, *LP3DPENS;
   


int GfxDraw3DBorder(CDC *pDC, LP3DPENS p3DPens, 
                     CRect rectBorder, BOOL bInside=TRUE);
void GfxDrawBorder(CDC *pDC, CRect rectBorder, CPen *pPen=NULL); 
void GfxDrawBorder(CDC *pDC, CRect rectBorder, 
                  COLORREF clFill, CPen *pPen=NULL);



#endif
