
// util_gdi.cpp

#include "stdafx.h"
#include "util_gdi.h"



tag3DPens::tag3DPens()
{
   pPenBlack = new CPen(PS_SOLID, 1, RGB(0,0,0));
   pPenDarkGray = new CPen(PS_SOLID, 1, GetSysColor(COLOR_BTNSHADOW));
   pPenWhite = new CPen(PS_SOLID, 1, RGB(255,255,255));
   pPenGray = new CPen(PS_SOLID, 1, GetSysColor(COLOR_BTNFACE));
}


tag3DPens::~tag3DPens()
{
   delete pPenBlack;
   delete pPenDarkGray;
   delete pPenWhite;
   delete pPenGray;
}




int GfxDraw3DBorder(CDC *pDC, LP3DPENS p3DPens, CRect rectBorder, BOOL bInside)
{
   if(bInside)
   {
      CPen *pOldPen = pDC->SelectObject(p3DPens->pPenDarkGray);

      pDC->MoveTo(rectBorder.left, rectBorder.top);
      pDC->LineTo(rectBorder.right, rectBorder.top);
      pDC->MoveTo(rectBorder.left, rectBorder.top);
      pDC->LineTo(rectBorder.left, rectBorder.bottom);

      pDC->SelectObject(p3DPens->pPenBlack);

      rectBorder.DeflateRect(1, 1);
      pDC->MoveTo(rectBorder.left, rectBorder.top);
      pDC->LineTo(rectBorder.right, rectBorder.top);
      pDC->MoveTo(rectBorder.left, rectBorder.top);
      pDC->LineTo(rectBorder.left, rectBorder.bottom);

      pDC->SelectObject(p3DPens->pPenWhite);

      rectBorder.InflateRect(1, 1);
      pDC->MoveTo(rectBorder.right-1, rectBorder.top);
      pDC->LineTo(rectBorder.right-1, rectBorder.bottom);
      pDC->MoveTo(rectBorder.left, rectBorder.bottom-1);
      pDC->LineTo(rectBorder.right, rectBorder.bottom-1);

      pDC->SelectObject(p3DPens->pPenGray);

      rectBorder.DeflateRect(1, 1);
      pDC->MoveTo(rectBorder.right-1, rectBorder.top);
      pDC->LineTo(rectBorder.right-1, rectBorder.bottom);
      pDC->MoveTo(rectBorder.left, rectBorder.bottom-1);
      pDC->LineTo(rectBorder.right, rectBorder.bottom-1);

      pDC->SelectObject(pOldPen);
   }
   else
   {
      // draw the borders
      CPen *pOldPen = pDC->SelectObject(p3DPens->pPenGray);

      pDC->MoveTo(rectBorder.left, rectBorder.top);
      pDC->LineTo(rectBorder.right, rectBorder.top);
      pDC->MoveTo(rectBorder.left, rectBorder.top);
      pDC->LineTo(rectBorder.left, rectBorder.bottom);

      pDC->SelectObject(p3DPens->pPenWhite);

      rectBorder.DeflateRect(1, 1);
      pDC->MoveTo(rectBorder.left, rectBorder.top);
      pDC->LineTo(rectBorder.right, rectBorder.top);
      pDC->MoveTo(rectBorder.left, rectBorder.top);
      pDC->LineTo(rectBorder.left, rectBorder.bottom);

      pDC->SelectObject(p3DPens->pPenBlack);

      rectBorder.InflateRect(1, 1);
      pDC->MoveTo(rectBorder.right, rectBorder.top);
      pDC->LineTo(rectBorder.right, rectBorder.bottom+1);
      pDC->MoveTo(rectBorder.left, rectBorder.bottom);
      pDC->LineTo(rectBorder.right, rectBorder.bottom);

      pDC->SelectObject(p3DPens->pPenDarkGray);

      rectBorder.DeflateRect(1, 1);
      pDC->MoveTo(rectBorder.right, rectBorder.top);
      pDC->LineTo(rectBorder.right, rectBorder.bottom+1);
      pDC->MoveTo(rectBorder.left, rectBorder.bottom);
      pDC->LineTo(rectBorder.right, rectBorder.bottom);

      pDC->SelectObject(pOldPen);
   }

   return 2;
}



void GfxDrawBorder(CDC *pDC, CRect rectBorder, 
                   CPen *pPen)
{
   CPen *pOldPen = NULL;
   if(pPen)
      pOldPen = pDC->SelectObject(pPen);

   pDC->MoveTo(rectBorder.left, rectBorder.top);
   pDC->LineTo(rectBorder.right-1, rectBorder.top);
   pDC->LineTo(rectBorder.right-1, rectBorder.bottom-1);
   pDC->LineTo(rectBorder.left, rectBorder.bottom-1);
   pDC->LineTo(rectBorder.left, rectBorder.top);

   if(pOldPen)
      pDC->SelectObject(pOldPen);
}


void GfxDrawBorder(CDC *pDC, CRect rectBorder, 
                   COLORREF clFill, CPen *pPen)
{
   CPen *pOldPen = NULL;
   if(pPen)
      pOldPen = pDC->SelectObject(pPen);

   pDC->MoveTo(rectBorder.left, rectBorder.top);
   pDC->LineTo(rectBorder.right-1, rectBorder.top);
   pDC->LineTo(rectBorder.right-1, rectBorder.bottom-1);
   pDC->LineTo(rectBorder.left, rectBorder.bottom-1);
   pDC->LineTo(rectBorder.left, rectBorder.top);

   if(pOldPen)
      pDC->SelectObject(pOldPen);

   CBrush brush(clFill);
   rectBorder.DeflateRect(1,1);

   pDC->FillRect(rectBorder, &brush);
}



