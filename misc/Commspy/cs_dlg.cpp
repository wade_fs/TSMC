
//
// cs_dlg.cpp : implementation file
//

#include "stdafx.h"
#include "commspy.h"
#include "cs_dlg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


#define IDM_RX WM_USER + 1000

static int CALLBACK MonitorCallback(HGLOBAL hBuf, int nLen, LPARAM lParam);



/////////////////////////////////////////////////////////////////////////////
// CCommspyDlg dialog

CCommspyDlg::CCommspyDlg(CWnd* pParent) : CDialog(IDD_COMMSPY_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}



void CCommspyDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

   if(!pDX->m_bSaveAndValidate)
   {
      LoadSettings();
      m_settings.nFormat -= IDC_HEX;
   }

   DDX_Control(pDX, IDC_FLOW, m_flow);
   DDX_Control(pDX, IDC_DISPLAY, m_display);
   DDX_CBIndex(pDX, IDC_PORT, m_settings.nPort);
   DDX_CBIndex(pDX, IDC_PARITY, m_settings.nParity);
   DDX_CBIndex(pDX, IDC_BAUD, m_settings.nBaud);
   DDX_CBIndex(pDX, IDC_DATA, m_settings.nData);
   DDX_CBIndex(pDX, IDC_STOP, m_settings.nStop);
   DDX_Radio(pDX, IDC_HEX, m_settings.nFormat);
   DDX_Check(pDX, IDC_ONTOP, m_settings.bOnTop);

   if(pDX->m_bSaveAndValidate)
   {
      m_settings.nFormat += IDC_HEX;
      m_settings.nFlow = 0;

      if(m_flow.GetCheck(0))
         m_settings.nFlow |= FC_DTRDSR;
      if(m_flow.GetCheck(1))
         m_settings.nFlow |= FC_RTSCTS;
      if(m_flow.GetCheck(2))
         m_settings.nFlow |= FC_XONXOFF;
   }
   else
   {
      m_flow.ResetContent();
      m_flow.AddString(_T("DTR/DSR"));
      m_flow.AddString(_T("RTS/CTS"));
      m_flow.AddString(_T("XON/XOFF"));

      if(m_settings.nFlow & FC_DTRDSR)
         m_flow.SetCheck(0, TRUE);
      if(m_settings.nFlow & FC_RTSCTS)
         m_flow.SetCheck(1, TRUE);
      if(m_settings.nFlow & FC_XONXOFF)
         m_flow.SetCheck(2, TRUE);
   }
}


BEGIN_MESSAGE_MAP(CCommspyDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
   ON_WM_CLOSE()
	ON_WM_QUERYDRAGICON()
   ON_BN_CLICKED(IDC_FONT, OnFont)
   ON_BN_CLICKED(IDC_START, OnStart)
   ON_BN_CLICKED(IDC_ONTOP, OnOnTop)
   ON_BN_CLICKED(IDC_CLEAR, OnClear)
   ON_WM_CTLCOLOR()
 	ON_CONTROL(CCN_SELCHANGE, IDC_COLOR_FG, OnFgColorChange)
 	ON_CONTROL(CCN_SELCHANGE, IDC_COLOR_BK, OnBkColorChange)
   ON_MESSAGE(IDM_RX, OnRX)
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CCommspyDlg message handlers

BOOL CCommspyDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

   m_fontDisplay.CreatePointFont(m_settings.nPointSize, m_settings.strFontName);
   m_display.SetFont(&m_fontDisplay);
   m_display.LimitText(LONG_MAX);

   m_display.GetWindowRect(&m_rectDisplayStop);
   ScreenToClient(&m_rectDisplayStop);

   m_rectDisplayStart = m_rectDisplayStop;
   CRect rectEdge;
   GetDlgItem(IDC_GROUP_DISPLAY)->GetWindowRect(&rectEdge);
   ScreenToClient(&rectEdge);
   m_rectDisplayStart.right = rectEdge.right;
   
   // update background color control
   CRect rect;
   CWnd *pWnd = GetDlgItem(IDC_COLOR_BK);
   pWnd->SetDlgCtrlID(0xffff);
   pWnd->GetWindowRect(rect);
   ScreenToClient(&rect);
   m_colorBK.SetColor(m_settings.clBK);
   m_colorBK.Create(WS_VISIBLE|WS_CHILD, rect, this, IDC_COLOR_BK);

   // update foreground color control
   pWnd = GetDlgItem(IDC_COLOR_FG);
   pWnd->GetWindowRect(rect);
   pWnd->SetDlgCtrlID(0xffff);
   ScreenToClient(&rect);
   m_colorFG.SetColor(m_settings.clFG);
   m_colorFG.Create(WS_VISIBLE|WS_CHILD, rect, this, IDC_COLOR_FG);

	// Add "About..." menu item to system menu.
	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

   // reposition the window
   UINT nFlags = SWP_NOSIZE;
   CPoint pt(0,0);
   if(m_settings.dwPos == 0xFFFFFFFF)
      nFlags |= SWP_NOMOVE;
   else
      pt = CPoint(m_settings.dwPos); 

   const CWnd *pWndInsertAfter = NULL;
   if(m_settings.bOnTop)
      pWndInsertAfter = &wndTopMost;

   SetWindowPos(pWndInsertAfter, pt.x, pt.y, 0, 0, nFlags);

	return TRUE;  // return TRUE  unless you set the focus to a control
}



void CCommspyDlg::OnClose()
{
   if(m_comm.IsConnected() && !m_comm.Disconnect())
      return;

   UpdateData();

   CRect wr;
   GetWindowRect(&wr);
   m_settings.dwPos = MAKELPARAM(wr.left, wr.top);

   StoreSettings();

   CDialog::OnClose();
}




// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.
void CCommspyDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}


// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CCommspyDlg::OnQueryDragIcon()
{
	return (HCURSOR)m_hIcon;
}



void CCommspyDlg::OnFont()
{
   CFontDialog dlg;

   if(dlg.DoModal() == IDOK)
   {
      m_fontDisplay.DeleteObject();

      m_settings.nPointSize = dlg.GetSize();
      m_settings.strFontName = dlg.GetFaceName();

      m_fontDisplay.CreatePointFont(m_settings.nPointSize, m_settings.strFontName);
      
      m_display.SetFont(&m_fontDisplay);
   }
}



int _nDataValues[] = {5,6,7,8};

int _nBaudRates[] = {CBR_110, CBR_300, CBR_600, CBR_1200, CBR_2400,
                     CBR_4800, CBR_9600, CBR_14400, CBR_19200,
                     CBR_38400, CBR_56000, CBR_57600, CBR_115200,
                     CBR_128000, CBR_256000};



void CCommspyDlg::OnStart()
{
   BOOL bDisconnect = m_comm.IsConnected();

   if(!bDisconnect)
   {
      UpdateData();

      // initialize tty info
      TTYSTRUCT tty;
      ZERO_MEMORY(tty);

      tty.byCommPort = (BYTE)m_settings.nPort;
      tty.byXonXoff = FALSE;
      tty.byByteSize = (BYTE)_nDataValues[m_settings.nData]; 
      tty.byFlowCtrl = (BYTE)m_settings.nFlow;;
      tty.byParity = (BYTE)m_settings.nParity;; 
      tty.byStopBits = (BYTE)m_settings.nStop;;
      tty.dwBaudRate = (DWORD)_nBaudRates[m_settings.nBaud];;

      if(m_comm.Connect(&tty))
      {
         ShowControls(SW_HIDE, &m_rectDisplayStart);

         // we're about to start up a thread...if we don't force all control
         // repaints before then, the display will repaint sluggishly
         MSG msg;
         while(PeekMessage(&msg, NULL, WM_PAINT, WM_PAINT, PM_NOREMOVE))
         {
            ::TranslateMessage(&msg);
            ::DispatchMessage(&msg);
         }

         // begin monitoring
         if(!m_comm.Monitor(&MonitorCallback, (LPARAM)m_hWnd))
         {
            bDisconnect = TRUE;
         }
      }  
      else
      {
         ReportLastError();
      }
   }
   
   if(bDisconnect)
   {
      m_comm.Disconnect();
      ShowControls(SW_SHOW, &m_rectDisplayStop);
   }
}





void CCommspyDlg::ShowControls(int nShow, CRect *pNewDisplayRect)
{
   int nControls[] = {IDC_COLOR_FG, IDC_COLOR_BK, IDC_BAUD, IDC_DATA, IDC_PARITY,
                      IDC_STOP, IDC_PORT, IDC_FONT, IDC_HEX, IDC_DECIMAL,
                      IDC_OCTAL, IDC_ASCII, IDC_ONTOP};

   int nCount = sizeof(nControls)/sizeof(nControls[0]);
   for(int i = 0; i < nCount; i++)
   {
      CWnd *pWnd = GetDlgItem(nControls[i]);
      if(pWnd)
         pWnd->ShowWindow(nShow);
   }

   if(pNewDisplayRect)
   {
      m_display.MoveWindow(pNewDisplayRect);
   }

   CString strStartStop(nShow == SW_SHOW ? _T("Start!") : _T("Stop!"));
   CWnd *pWnd = GetDlgItem(IDC_START);
   if(pWnd)
      pWnd->SetWindowText(strStartStop);
}




LRESULT CCommspyDlg::OnRX(WPARAM wLen, LPARAM hBuf)
{
   LPBYTE lpBuf = (LPBYTE)::GlobalLock((HGLOBAL)hBuf);
   if(!lpBuf)
      return 1;

   CString strOutput;

   if(m_settings.nFormat == IDC_ASCII)
   {
      strOutput = lpBuf;
   }
   else
   {
      CString strCvt;

      for(int i = 0; i < (int)wLen; i++)
      {
         switch(m_settings.nFormat)
         {
            case IDC_DECIMAL:
               strCvt.Format("%3d ", (BYTE)lpBuf[i]);
               break;
            case IDC_OCTAL:
               strCvt.Format("%3o ", (BYTE)lpBuf[i]);
               break;
            // hex
            default: 
               strCvt.Format("%02x ", (BYTE)lpBuf[i]);
               break;
         }

         strOutput += strCvt;
      }
   }

   strOutput += "\r\n";

   m_display.SetSel(0,0);
   m_display.ReplaceSel(strOutput);
   m_display.SetSel(0,0);

   ::GlobalUnlock((HGLOBAL)hBuf);
   return 1;
}



int CALLBACK MonitorCallback(HGLOBAL hBuf, int nLen, LPARAM lParam)
{
   HWND hDlg = (HWND)lParam;
   ::SendMessage(hDlg, IDM_RX, nLen, (LPARAM)hBuf);

   return 1;
}


void CCommspyDlg::OnOnTop()
{
   UpdateData();

   if(m_settings.bOnTop)
   {
      SetWindowPos(&wndTopMost, 0, 0, 0, 0, SWP_NOMOVE|SWP_NOSIZE);
   }
   else
   {
      SetWindowPos(&wndBottom, 0, 0, 0, 0, SWP_NOMOVE|SWP_NOSIZE|SWP_NOREDRAW);
      BringWindowToTop();
   }
}


void CCommspyDlg::OnClear()
{
   m_display.SetWindowText("");
}


HBRUSH CCommspyDlg::OnCtlColor(CDC* pDC, CWnd *pWnd, UINT nCtlColor)
{
   if(pWnd->m_hWnd == m_display.m_hWnd)
   {
      pDC->SetTextColor(m_settings.clFG);
      pDC->SetBkColor(m_settings.clBK);
   
      if(m_brushDisplay.m_hObject)
         m_brushDisplay.DeleteObject();

      m_brushDisplay.CreateSolidBrush(m_settings.clBK);
      return m_brushDisplay;
   }

   return CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
}


void CCommspyDlg::OnBkColorChange()
{
   m_settings.clBK = m_colorBK.GetColor();
   m_display.Invalidate();
}


void CCommspyDlg::OnFgColorChange()
{
   m_settings.clFG = m_colorFG.GetColor();
   m_display.Invalidate();
}


void CCommspyDlg::LoadSettings()
{
   CWinApp *pApp = AfxGetApp();

   m_settings.clBK = (COLORREF)pApp->GetProfileInt(CS_REGKEY_SETTINGS, CS_REGENTRY_BKCOLOR, (int)RGB(0,0,255));
   m_settings.clFG = (COLORREF)pApp->GetProfileInt(CS_REGKEY_SETTINGS, CS_REGENTRY_FGCOLOR, (int)RGB(0,255,255));
   m_settings.nPointSize = pApp->GetProfileInt(CS_REGKEY_SETTINGS, CS_REGENTRY_POINTSIZE, 90);
   m_settings.strFontName = pApp->GetProfileString(CS_REGKEY_SETTINGS, CS_REGENTRY_FACENAME, _T("Terminal"));
   m_settings.bOnTop = pApp->GetProfileInt(CS_REGKEY_SETTINGS, CS_REGENTRY_ONTOP, FALSE);

   m_settings.nPort = pApp->GetProfileInt(CS_REGKEY_SETTINGS, CS_REGENTRY_PORT, 1);
   m_settings.nParity = pApp->GetProfileInt(CS_REGKEY_SETTINGS, CS_REGENTRY_PARITY, 0);
   m_settings.nBaud = pApp->GetProfileInt(CS_REGKEY_SETTINGS, CS_REGENTRY_BAUD, 11);
   m_settings.nData = pApp->GetProfileInt(CS_REGKEY_SETTINGS, CS_REGENTRY_DATABITS, 3);
   m_settings.nStop = pApp->GetProfileInt(CS_REGKEY_SETTINGS, CS_REGENTRY_STOPBITS, 0);
   m_settings.nFlow = pApp->GetProfileInt(CS_REGKEY_SETTINGS, CS_REGENTRY_FLOW, 0);
   m_settings.nFormat = pApp->GetProfileInt(CS_REGKEY_SETTINGS, CS_REGENTRY_FORMAT, IDC_HEX);
   m_settings.dwPos = (DWORD)pApp->GetProfileInt(CS_REGKEY_SETTINGS, CS_REGENTRY_POS, 0xFFFFFFFF);
}



void CCommspyDlg::StoreSettings()
{
   CWinApp *pApp = AfxGetApp();

   pApp->WriteProfileInt(CS_REGKEY_SETTINGS, CS_REGENTRY_BKCOLOR, (int)m_settings.clBK);
   pApp->WriteProfileInt(CS_REGKEY_SETTINGS, CS_REGENTRY_FGCOLOR, (int)m_settings.clFG);

   pApp->WriteProfileInt(CS_REGKEY_SETTINGS, CS_REGENTRY_POINTSIZE, (int)m_settings.nPointSize);
   pApp->WriteProfileString(CS_REGKEY_SETTINGS, CS_REGENTRY_FACENAME, m_settings.strFontName);
   pApp->WriteProfileInt(CS_REGKEY_SETTINGS, CS_REGENTRY_ONTOP, m_settings.bOnTop);

   pApp->WriteProfileInt(CS_REGKEY_SETTINGS, CS_REGENTRY_PORT, m_settings.nPort);
   pApp->WriteProfileInt(CS_REGKEY_SETTINGS, CS_REGENTRY_PARITY, m_settings.nParity);
   pApp->WriteProfileInt(CS_REGKEY_SETTINGS, CS_REGENTRY_BAUD, m_settings.nBaud);
   pApp->WriteProfileInt(CS_REGKEY_SETTINGS, CS_REGENTRY_DATABITS, m_settings.nData);
   pApp->WriteProfileInt(CS_REGKEY_SETTINGS, CS_REGENTRY_STOPBITS, m_settings.nStop);
   pApp->WriteProfileInt(CS_REGKEY_SETTINGS, CS_REGENTRY_FLOW, m_settings.nFlow);

   pApp->WriteProfileInt(CS_REGKEY_SETTINGS, CS_REGENTRY_FORMAT, m_settings.nFormat);
   pApp->WriteProfileInt(CS_REGKEY_SETTINGS, CS_REGENTRY_POS, m_settings.dwPos);
}

void CCommspyDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
      CDialog dlg(IDD_ABOUTBOX);
      dlg.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}









