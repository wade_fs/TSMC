

// cs_dlg.h : header file
//

#ifndef _INC_CS_DLG_
#define _INC_CS_DLG_

#include "util_clr.h"
#include "comm_mon.h"


typedef struct _CSSETTINGS
{
   COLORREF clBK;
   COLORREF clFG;
   DWORD dwPos;
   int nPointSize;
   CString strFontName;
   BOOL bOnTop;
   int nFormat;
   int nPort;
   int nParity;
   int nBaud;
   int nData;
   int nStop;
   int nFlow;

} CSSETTINGS, *LPCSSETTINGS;


// registry stuff
#define CS_REGKEY_SETTINGS     _T("Settings")

#define CS_REGENTRY_BKCOLOR    _T("bkcolor")
#define CS_REGENTRY_FGCOLOR    _T("fgcolor")
#define CS_REGENTRY_POINTSIZE  _T("pointsize")
#define CS_REGENTRY_FACENAME   _T("facename")
#define CS_REGENTRY_ONTOP      _T("ontop")
#define CS_REGENTRY_PORT       _T("port")
#define CS_REGENTRY_PARITY     _T("parity")
#define CS_REGENTRY_BAUD       _T("baud")
#define CS_REGENTRY_DATABITS   _T("databits")
#define CS_REGENTRY_STOPBITS   _T("stopbits")
#define CS_REGENTRY_FLOW       _T("flow")
#define CS_REGENTRY_FORMAT     _T("format")
#define CS_REGENTRY_POS        _T("pos")





/////////////////////////////////////////////////////////////////////////////
// CCommspyDlg dialog

class CCommspyDlg : public CDialog
{
// Construction
public:
   // standard constructor
	CCommspyDlg(CWnd* pParent = NULL);	

protected:
   // DDX/DDV support
	virtual void DoDataExchange(CDataExchange* pDX);	

   void ShowControls(int nShow, CRect *pNewDisplayRect);
   void LoadSettings();
   void StoreSettings();
   // called when data received from comm port
   LRESULT OnRX(WPARAM wLen, LPARAM lBuf);

// Implementation
protected:
	HICON m_hIcon;
   CRect m_rectDisplayStart;
   CRect m_rectDisplayStop;

   // subclassed controls
   GColorComboBox m_colorBK;
   GColorComboBox m_colorFG;
   CCheckListBox m_flow;
   CEdit m_display;
   // brushes, fonts, settings
   CFont m_fontDisplay;
   CBrush m_brushDisplay;
   CSSETTINGS m_settings;
   // monitoring object
   GCommMonitor m_comm;

	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
   afx_msg void OnFont();
   afx_msg void OnStart();
   afx_msg void OnStop();
   afx_msg void OnOnTop();
   afx_msg void OnClose();
   afx_msg void OnClear();
   afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd *pWnd, UINT nCtlColor);
   afx_msg void OnBkColorChange();
   afx_msg void OnFgColorChange();
	

	DECLARE_MESSAGE_MAP()
};



#endif 
