#include <stdio.h>
#include <string.h>

#define WIDTH  (8 * sizeof(unsigned char))
#define TOPBIT (1 << (WIDTH - 1))
static unsigned char POLYNOMIAL=0x07, INITIAL=0x00, FINAL_XOR=0x00;
static unsigned char crcTable[256];
static int made_table=0;

static void init_crc8(int debug)
{
  unsigned char remainder;

  if (!made_table) {
    if (debug) printf ("=========================== L o o k u p  T a b l e ============================\n");
    for (int dividend = 0; dividend < 256; ++dividend) {
      remainder = dividend << (WIDTH - 8);
  
      for (unsigned char bit = 8; bit > 0; --bit) {
        if (remainder & TOPBIT) {
          remainder = (remainder << 1) ^ POLYNOMIAL;
        }
        else {
          remainder = (remainder << 1);
        }
      }
  
      crcTable[dividend] = remainder;
      if (debug) {
        printf ("0x%02X ", crcTable[dividend]);
        if (dividend % 16 == 15) printf ("\n");
      }
    }
    made_table = 1;
    if (debug) printf ("=========================== * * * * * *  * * * * * ============================\n");
  }
}

void showAlgorithms() {
  printf ("Algorithm   Polynomial Initial FinalXor\n"
          "CRC8         7          0       0\n"
          "SAE_J1850   1D         FF      FF\n"
          "DAE_J1850_Z 1D          0       0\n"
          "8H2F        2F         FF      FF\n"
          "CDMA2000    9B         FF       0\n"
          "DARC        39          0       0\n"
          "DVB_S2      D5          0       0\n"
          "EBU         1D         FF       0\n"
          "ICODE       1D         FD       0\n"
          "ITU          7          0      55\n"
          "MAXIM       31          0       0\n"
          "ROHC         7         FF       0\n"
          "WCDMA       9B          0       0\n");
}

void crc8(unsigned char *crc, unsigned char m)
     /*
      * For a byte array whose accumulated crc value is stored in *crc, computes
      * resultant crc obtained by appending m to the byte array
      */
{
  *crc = crcTable[(*crc) ^ m];
  *crc &= 0xFF;
}

int main(int argc, char* argv[])
{
  unsigned int inp=0, ai=1, debug=0, autoinput=0;
  unsigned char m, crc=0;

  if (argc == 1) {
    printf ("Usage: echo HH1 [HH2] ... | %s [-d] [-a] {-l or Polynomial Initial Xor}\n"
            "\tWhere -d to show Search CRC Table, "
            "-a to auto use all 256 uchar as input\n"
            "  Example:\n"
            "\t%s -l # for show as known CRC8 algorithn parameter\n"
            "\techo 40 FE | %s -d 07 0 0 # echo 0x40 0xFE as input and show CRC Table for debug\n"
            "\t%s -a 07 0 55 # for CRC8_ITU and auto input 0-255 uchar for input\n"
            , argv[0], argv[0], argv[0], argv[0]);
    return 1;
  }

  for (ai=1; ai<=argc; ai++) {
    if (strstr(argv[ai], "-l")) {
      showAlgorithms();
      return 2;
    } else if (strstr(argv[ai], "-s")) {
      autoinput = 1;
    } else if (strstr(argv[ai], "-d")) {
      debug = 1;
    } else break;
  }
  sscanf (argv[ai], "%x", (unsigned int*)&POLYNOMIAL);
  sscanf (argv[ai+1], "%x", (unsigned int*)&INITIAL);
  sscanf (argv[ai+2], "%x", (unsigned int*)&FINAL_XOR);

  if (!made_table) init_crc8(debug);

  if (autoinput) {
    for (int i=0; i<256; i++) {
      crc = INITIAL;
      crc8(&crc, i);
      crc ^= FINAL_XOR;
      printf ("0x%02X ", crc);
      if (i%16 == 15) printf ("\n");
    }
  }

  printf ("Input: ");
  crc = INITIAL;
  while (scanf ("%x", &inp) == 1) {
    m = (unsigned char)inp;
    printf ("%02X ", m);
    crc8(&crc, m);
  }
  crc ^= FINAL_XOR;
  printf ("\nCRC ==> 0x%02X\n", crc);
  return 0;
}
