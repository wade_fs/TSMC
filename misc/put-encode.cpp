#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <errno.h>

inline unsigned char inv(unsigned char m) { int r, i=8; for (; i--; r=r*2|m&1,m>>=1); return (unsigned char)r; }
inline void printFC0() { printf ("FC FF "); }
inline void printFC() { printf ("FC FF FF FF 1F FF 77 FF FF "); }
inline void printGen1(int gen) { if (gen == 1) printf ("07 "); else printf ("7B "); }
// Total Gen 1 = bufLen+2; gen 2 = bufLen + (data sec number)*3 -1
inline void printTotal(int bufLen, int dataSec, int gen) { int l; if (gen == 1) l = 242; else l = bufLen + ((dataSec << 1) + dataSec) - 1; printf ("%02X %02X ", inv(~(l & 0xFF)), inv(~((l>>8)&0xFF))); }
inline void printUnknown() { printf ("BB FF "); } // 跟內容有關，卻又不知道關係, 不像是長度函數，懷疑是 total checksum 之類的
inline void printGen2(int gen) { if (gen != 1) printf ("FF FF FF FF "); else printf ("7F FF FF FF "); } // 這邊順帶印2 個 FF FF
inline void printLength(int bufLen, int gen) { int l; if (gen == 1) l = 244; else l = bufLen + 3; printf ("%02X %02X ", inv(~(l & 0xFF)), inv(~(l>>8)&0xFF)); }
inline void printF5() { printf ("F5 FF "); }
inline void println() { printf ("\n"); }

void printMsgGen1 (char* buf, int bufLen, int blockIndex) {
  int i, len=0, checksum=80; // checksum init with 80 = inv(~F5)
  // 前 224 是放資料的，後面有 16 個空白, 再後面是...
  for (i=0; i<blockIndex; i++) printf ("FF ");
  for (len=blockIndex, i=0; i<bufLen; i++) {
    printf ("%02X ", (unsigned char)inv(~(buf[i] & 0xFF)));
    checksum += (unsigned int) buf[i];
  }
  // 這邊是放空白直到 240 結尾
  for (len+=bufLen; len<240; len++) {
    printf ("FB ");
    checksum += (unsigned int) 0x20; // 0x20 = inv(0x04)
  }
  printf ("%02X ", (unsigned char)inv(~blockIndex));
  checksum += (unsigned int)blockIndex; // 10 = inv(0x50), 255 = inv(0xFF)
  printf ("%02X FF %02X FF ", (unsigned char)inv(~checksum & 0xFF), (unsigned char)inv(~(checksum >> 8) & 0xFF));
}

void printMsgGen3 (char* buf, int bufLen) {
  int i, checksum=80; // checksum init with 80 = inv(0A)
  for (i=0; i<bufLen; i++) {
    printf ("%02X ", (unsigned char)~inv(buf[i]));
    checksum += (unsigned int) buf[i];
  }
  printf ("%02X FF %02X FF ", (unsigned char)inv(~checksum & 0xFFFF), (unsigned char)inv((~checksum>>8) & 0xFFFF));
  printf ("FF FF ");
}

int main(int argc, char* argv[])
{
  // 00. message buffer 目前假設存在 argv[1] 中
  // 實際應用應該是類似 buf[] 中
  if (argc != 2) {
    printf ("usage: %s \"訊息字串\"\n", argv[0]);
    return 1;
  }
  int bufLen, gen, dataSec, i;
  char* pch;

  bufLen = strlen(argv[1]);
  if (bufLen <= 224) gen = 1;
  else gen = 3;  // 故意不要出現 gen2, 因為沒必要
  dataSec = (bufLen-1) / 250 + 1;
  pch = argv[1];

  printFC(); // FC FF FF FF 1F FF 77 FF FF 
  printGen1(gen);
  printTotal(bufLen, dataSec, gen);
  printUnknown();
  printGen2(gen);
  if (gen == 1) {
    printLength(240, 1);
    printF5();
    printMsgGen1(pch, bufLen, 224 - ((1+ (bufLen-1) / 16)<< 4));
  }
  else {
    int bl;
    for (i=0; i<dataSec; pch += 250, i++) {
      if (i != (dataSec-1)) bl = 250;
      else bl = bufLen % 250;
      printLength(bl, gen);
      printF5();
      printMsgGen3(pch, bl);
    }
  }
  println();

  return 0;
}
