#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <errno.h>

unsigned char inv(unsigned char m) { int r, i=8; for (; i--; r=r*2|m&1,m>>=1); return (unsigned char)(r&0xFF); }
void dump(unsigned char* buf, int bufLen) {
  unsigned int sum;

  sum = 0;
  printf ("raw  %d> \"", bufLen);
  for (int i=0; i < bufLen; i++) {
    sum += (unsigned)(buf[i] & 0xFF);
    printf ("%02X ", (unsigned char)(buf[i] & 0xFF));
  }
  printf ("\"\n%X(%d)\n", sum, sum);

  sum = 0;
  printf ("~raw %d> \"", bufLen);
  for (int i=0; i < bufLen; i++) {
    sum += (unsigned)(~buf[i] & 0xFF);
    printf ("%02X ", (unsigned char)(~buf[i] & 0xFF));
  }
  printf ("\"\n%X(%d)\n", sum, sum);

  sum = 0;
  printf ("inv  %d> \"", bufLen);
  for (int i=0; i < bufLen; i++) {
    sum += (unsigned)(inv(buf[i]) & 0xFF);
    printf ("%02X ", (unsigned char)(inv(buf[i]) & 0xFF));
  }
  printf ("\"\n%X(%d)\n", sum, sum);

  sum = 0;
  printf ("~inv %d> \"", bufLen);
  for (int i=0; i < bufLen; i++) {
    sum += (unsigned)(~inv(buf[i]) & 0xFF);
    printf ("%02X ", (unsigned char)(inv(buf[i]) & 0xFF));
  }
  printf ("\"\n%X(%d)\n", sum, sum);
}
int readHexByte(unsigned int *inp) {
  return (scanf("%x", inp) == 1);
}

int main(int argc, char* argv[])
{
  unsigned char buf[2048];
  unsigned char buf2[32];
  unsigned char ch, *bufp;
  unsigned int inp, len;
  int blockIndex=0, gen;
  int isStartFC = 0, isStartF5 = 0, isEndPack = 0;
  // pch 每個封包的資料段都要重設為 0，指向這個資料段的位置,
  // 而 bufLen 則指向整個 buf 目前長度，也就是儲放資料的現在位置，所以除非是 Multi 的第一個資料段，其他的資料段會一直遞增不會重設
  int pch = 0, bufLen = 0;
  int checksum = 0, myChecksum = 80; // 80 = inv(0x0A) for init
  int dataSecNum = 0;

  // 00. 這邊有一段應該初始化變數，目前寫在宣告區
  //     ...... init() ........

  bzero(buf, sizeof(buf));

  // 01. 一直讀到 F5 FF 為止
  pch = 0;
  while (readHexByte(&inp)) {
    buf2[pch++] = (unsigned char)(inp & 0xFF);
  }
 
  dump(buf2, pch);
  return 0;
}
