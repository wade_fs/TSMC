#!/usr/bin/env python
#coding:utf-8
import serial
import sys
import time

import kbhit
import menu
import cmd
from switch import switch

if 2 <= len(sys.argv):
  port = sys.argv[1]
else:
  port = '/dev/ttyUSB0'

if 3 == len(sys.argv):
  baudrate = int(sys.argv[2])
else:
  baudrate = 9600
 
try:
  ser = serial.Serial(port, baudrate, timeout=10)
  ser.flush()
except serial.SerialException:
  print( "請檢查 {}，例如是否存在，以及權限是否可讀寫".format(port))
  exit(1)

Running = True

res = -1
while Running:
  key = menu.menu(res)

  for case in switch(key):
    # NORMAL
    if case('d'): # cmd: Display Time"
      res = cmd.doCmd(ser, cmd.ENUM_CMD_DISPLAY_TIME)
      break
    if case('+'): # cmd: LCD Display On"
      res = cmd.doCmd(ser, cmd.ENUM_CMD_LCD_DISPLAY_ON)
      break
    if case('-'): # cmd: LCD Display Off"
      res = cmd.doCmd(ser, cmd.ENUM_CMD_LCD_DISPLAY_OFF)
      break
    if case('c'): # cmd: Clear LCD"
      res = cmd.doCmd(ser, cmd.ENUM_CMD_CLEAR_LCD)
      break
    if case('t'): # cmd: Enable Timeout"
      res = cmd.doCmd(ser, cmd.ENUM_CMD_ENABLE_TIMEOUT)
      break
    if case('T'): # cmd: Disable Timeout"
      res = cmd.doCmd(ser, cmd.ENUM_CMD_DISABLE_TIMEOUT)
      break
    if case('#'): # cmd: Display Serial Number"
      res = cmd.doCmd(ser, cmd.ENUM_CMD_DISPLAY_SERIAL_NUMBER)
      break
    # READ
    if case('1'): # cmd: Normal Status Read"
      res = cmd.doCmd(ser, cmd.ENUM_CMD_NORMAL_STATUS_READ)
      break
    if case('2'): # cmd: Extended Status2 Read"
      res = cmd.doCmd(ser, cmd.ENUM_CMD_EXTENDED_STATUS2_READ)
      break
    if case('3'): # cmd: Protected Data Read"
      res = cmd.doCmd(ser, cmd.ENUM_CMD_PROTECTED_DATA_READ)
      break
    if case('f'): # cmd: Get Tag Format"
      res = cmd.doCmd(ser, cmd.ENUM_CMD_GET_TAG_FORMAT)
      break
    # FORMAT
    if case('F'): # cmd: Format Tag"
      res = cmd.doCmd(ser, cmd.ENUM_CMD_FORMAT_TAG)
      break
    if case('r'): # cmd: Read LCD Data"
      res = cmd.doCmd(ser, cmd.ENUM_CMD_READ_LCD_DATA)
      break
    if case('w'): # cmd: Write LCD Data"
      res = cmd.doCmd(ser, cmd.ENUM_CMD_WRITE_LCD_DATA)
      break
    if case('q'): # 離開
      cmd.writeByteArray(ser, [113])
      time.sleep(0.030)
      cmd.writeByteArray(ser, [113])
      Running = False
      break 
    if case():
      print( "Something else....")

ser.close()
