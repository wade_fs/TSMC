#!/usr/bin/env python
import serial
import sys

if 2 <= len(sys.argv):
  port = sys.argv[1]
else:
  port = '/dev/ttyS0'

if 3 == len(sys.argv):
  baudrate = int(sys.argv[2])
else:
  baudrate = 9600

ser = serial.Serial(port, baudrate, timeout=10)
print("baudrate: ", ser.baudrate)
print("bytesize: ", ser.bytesize)
print("parity  : ", ser.parity)
print("stopbits: ", ser.stopbits)
print("xonoff  : ", ser.xonxoff)
print("rtscts  : ", ser.rtscts)
print("dsrdtr  : ", ser.dsrdtr)
print("timeout : ", ser.timeout)
print("writeTimeout    : ", ser.writeTimeout)
print("interCharTimeout: ", ser.interCharTimeout)
ba = bytearray()

for x in range(256):
  ba.append(x)
for x in range(256):
  ba.append(x)

ser.flushOutput()
ser.write(ba)
print("write to SerialPort "+port)
ser.close()

