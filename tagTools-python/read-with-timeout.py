#!/usr/bin/env python
import serial
import binascii
import sys

if 2 <= len(sys.argv):
  port = sys.argv[1]
else:
  port = '/dev/ttyUSB0'

if 3 == len(sys.argv):
  baudrate = int(sys.argv[2])
else:
  baudrate = 9600

ser = serial.Serial(port, baudrate)

print( "baudrate: ", ser.baudrate)
print( "bytesize: ", ser.bytesize)
print( "parity  : ", ser.parity)
print( "stopbits: ", ser.stopbits)
print( "xonoff  : ", ser.xonxoff)
print( "rtscts  : ", ser.rtscts)
print( "dsrdtr  : ", ser.dsrdtr)
print( "timeout : ", ser.timeout)
print( "writeTimeout    : ", ser.writeTimeout)
print( "interCharTimeout: ", ser.interCharTimeout)

#ser.flushInput()
#ser.timeout = None
#line = ser.read(1)
#print( " ".join("{:02X}".format(ord(x)) for x in line))
#ser.timeout = 1
#line = ser.read()
#print( " ".join("{:02X}".format(ord(x)) for x in line))


line = "";
ser.flushInput()
ser.timeout = None
ch = ser.read(1)
print( "read {} bytes:".format(len(ch)), " ".join("{:02X}".format(ord(x)) for x in ch))

ser.timeout = 0.025
while ch:
  line += ch
  ch = ser.read()
  print( "read : ", " ".join("{:02X}".format(ord(x)) for x in ch))
print( "read {} bytes:".format(len(line)), " ".join("{:02X}".format(ord(x)) for x in line))

ser.close()

