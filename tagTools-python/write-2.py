#!/usr/bin/env python
import serial
import sys
import time

if 2 <= len(sys.argv):
  port = sys.argv[1]
else:
  port = '/dev/ttyUSB0'

if 3 == len(sys.argv):
  baudrate = int(sys.argv[2])
else:
  baudrate = 9600

ser = serial.Serial(port, baudrate, timeout=10)
print("baudrate: ", ser.baudrate)
print("bytesize: ", ser.bytesize)
print("parity  : ", ser.parity)
print("stopbits: ", ser.stopbits)
print("xonoff  : ", ser.xonxoff)
print("rtscts  : ", ser.rtscts)
print("dsrdtr  : ", ser.dsrdtr)
print("timeout : ", ser.timeout)
print("writeTimeout    : ", ser.writeTimeout)
print("interCharTimeout: ", ser.interCharTimeout)

ser.flushOutput()
ser.write([0x00])
time.sleep(0.002)
ser.write([0x01, 0x02, 0x03, 0x04, 0x05])
print("write to SerialPort "+port)
ser.close()

