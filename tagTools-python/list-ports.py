#!/usr/bin/env python
import sys
import serial

from serial.tools import list_ports

#if 2 <= len(sys.argv):
#  port = sys.argv[1]
#else:
#  port = '/dev/ttyS0'


ports = list(list_ports.comports())
for port_no, description, address in ports:
  print(port_no)
