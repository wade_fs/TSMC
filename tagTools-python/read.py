#!/usr/bin/env python
import serial
import binascii
import sys

if 2 <= len(sys.argv):
  port = sys.argv[1]
else:
  port = '/dev/ttyS0'

if 3 == len(sys.argv):
  baudrate = int(sys.argv[2])
else:
  baudrate = 9600

ser = serial.Serial(port, baudrate, timeout=10)

print( "baudrate: ", ser.baudrate)
print( "bytesize: ", ser.bytesize)
print( "parity  : ", ser.parity)
print( "stopbits: ", ser.stopbits)
print( "xonoff  : ", ser.xonxoff)
print( "rtscts  : ", ser.rtscts)
print( "dsrdtr  : ", ser.dsrdtr)
print( "timeout : ", ser.timeout)
print( "writeTimeout    : ", ser.writeTimeout)
print( "interCharTimeout: ", ser.interCharTimeout)

ser.flushInput()
line = ser.read(512)
ser.flushInput()
# print(binascii.hexlify(line), sep=',')
print( ",".join("{:02x}".format(ord(x)) for x in line))
ser.close()

