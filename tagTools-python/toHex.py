def toHex(line):
  return ",".join("0x{:02X} ".format(ord(x)) for x in line)

