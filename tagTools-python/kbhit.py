#coding:utf-8
import signal
import sys
import termios
from select import select;
'''範例
import kbhit, time;
kbhit.init();

running = True;
while running:
    if kbhit.kbhit():
        ch = kbhit.getch();
        if 'q' == ch:
                running = False;
        print ("ch:", ch)
    time.sleep(0.1);

kbhit.restore();
'''
# save the terminal settings

_kbhit_fd = sys.stdin.fileno();
_kbhit_new_term = termios.tcgetattr(_kbhit_fd);
_kbhit_old_term = termios.tcgetattr(_kbhit_fd);

# new terminal setting unbuffered
_kbhit_new_term[3] = (_kbhit_new_term[3] & ~termios.ICANON & ~termios.ECHO);

def init():
    signal.signal(signal.SIGINT, signal.SIG_IGN)
    signal.signal(signal.SIGTSTP, signal.SIG_IGN)
    termios.tcsetattr(_kbhit_fd, termios.TCSAFLUSH, _kbhit_new_term);

def restore():
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    signal.signal(signal.SIGTSTP, signal.SIG_DFL)
    termios.tcsetattr(_kbhit_fd, termios.TCSAFLUSH, _kbhit_old_term);

def getch():
    return sys.stdin.read(1);

def kbhit():
    dr,dw,de = select([sys.stdin], [], [], 0);
    return len(dr)>0;
