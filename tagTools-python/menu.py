#coding:utf-8
import kbhit
import time

def clrscr():
    # print( "\x1bc")
    # print( "\x1b[1;1H\x1b[2J")
    print( "\x1b[2J\x1b[1;1H")
    return 0

def menu(res):
    _kbhit_key = ""

    kbhit.init()
    # clrscr()

    # 底下7個命令都是固定的4封包格式 : 相當於 > Start, < ACK, > CMD, < DONE
    print( "d) cmd: Display Time")
    print( "+) cmd: LCD Display On")
    print( "-) cmd: LCD Display Off")
    print( "c) cmd: Clear LCD")
    print( "t) cmd: Enable Timeout")
    print( "T) cmd: Disable Timeout")
    print( "#) cmd: Display Serial Number")
    # 底下5個命令算是同一類，前4個封包後面，接上變動長度的內容，最後再加上 ENUM_PK_READ_DONE/ENUM_PK_CMD_DONE
    print( "1) cmd: Normal Status Read")
    # print( "1) cmd: Extended Status1 Read")
    print( "2) cmd: Extended Status2 Read")
    print( "3) cmd: Protected Data Read")
    print( "f) cmd: Get Tag Format")
    print( "F) cmd: Format Tag")
    # 底下命令特殊的地方是，後面並沒有 ..._DONE 的確認封包 
    print( "w) cmd: Write LCD Data")
    # 底下命令特殊的地方是，後面有兩段來回確認，我猜是因為資料被切成2段
    print( "r) cmd: Read LCD Data")
    # print( "T) cmd: Read Time")
    # print( "s) cmd; Set Time")
    if (res == 0):
      print( "q) Quit [CMD OK] >")
    elif (res > 0):
      print( "q) Quit [CMD ERROR] >")
    else:
      print( "q) Quit >")
    _kbhit_key = ""
    while not kbhit.kbhit():
      time.sleep(0.1)
    _kbhit_key = kbhit.getch()
    kbhit.restore()
    return _kbhit_key
