#coding:utf-8

import numpy
import time

# 比對兩個 array (1維或多維都一樣)的相等語法如下:
# (numpy.array(A)==numpy.array(B)).all() 或
# numpy.array_equal(A,B)

'''
  ========== PACKET ==========
  print package: print map(hex, pkt)
  例如: print map(hex, packets[ENUM_PK_START_CMD])
  也可以是: print " ".join("{:02X}".format(x) for x in packets[ENUM_PK_START_CMD])
'''
ENUM_PK_START_CMD		= 0 
ENUM_PK_DISPLAY_TIME	 	= 1
ENUM_PK_READ_TIME	 	= 2
ENUM_PK_READ_DONE	 	= 3
ENUM_PK_NORMAL_STATUS_READ	= 4
ENUM_PK_EXTENDED_STATUS2_READ	= 5
ENUM_PK_PROTECTED_DATA_READ	= 6
ENUM_PK_WRITE_LCD_DATA		= 7
ENUM_PK_READ_LCD_DATA		= 8
ENUM_PK_LCD_DISPLAY_ON		= 9
ENUM_PK_LCD_DISPLAY_OFF		= 10
ENUM_PK_CLEAR_LCD		= 11
ENUM_PK_ENABLE_TIME_OUT		= 12
ENUM_PK_DISABLE_TIME_OUT	= 13
ENUM_PK_DISPLAY_SERIAL_NUMBER	= 14
ENUM_PK_GET_TAG_FORMAT		= 15
ENUM_PK_FORMAT_TAG		= 16
ENUM_PK_START_CMDr		= 17
ENUM_PK_ACK_START_CMD		= 18
ENUM_PK_CMD_DONE		= 19
ENUM_PK_FORMAT			= 20
ENUM_PK_SET_TIME		= 21
ENUM_PK_LCD_DATAv		= 22
ENUM_PK_READ_TIME1		= 23
ENUM_PK_NORMAL_STATUS		= 24
ENUM_PK_PROTECTED_DATA		= 25
ENUM_PK_EXTENDED_STATUS2	= 26
ENUM_PK_LCD_DATAvr		= 27
ENUM_PK_LCD_DATA21		= 28
ENUM_PK_LCD_DATA22		= 29
ENUM_PK_TAG_FORMAT		= 30

packets = [
  # ENUM_PK_START_CMD               = 0
  [ 0xFC, 0xFF ],
  # ENUM_PK_DISPLAY_TIME            = 1
  [ 0xFF, 0xFF, 0xDF, 0xFF, 0xBB, 0xFF, 0xFF, 0xBB, 0xFF, 0xFF, 0xFF ],
  # ENUM_PK_READ_TIME               = 2
  [ 0xFF, 0xFF, 0xDF, 0xFF, 0x3B, 0xFF, 0xFF, 0x3B, 0xFF, 0xFF, 0xFF ],
  # ENUM_PK_READ_DONE               = 3
  [ 0x9F, 0xFF ],
  # ENUM_PK_NORMAL_STATUS_READ      = 4
  [ 0xFF, 0xFF, 0xDF, 0xFF, 0xFD, 0xFF, 0x55, 0x56, 0xFF, 0xFF, 0xFF ],
  # ENUM_PK_EXTENDED_STATUS2_READ   = 5
  [ 0xFF, 0xFF, 0xDF, 0xFF, 0xFD, 0xFF, 0x5A, 0x58, 0xFF, 0xFF, 0xFF ],
  # ENUM_PK_PROTECTED_DATA_READ     = 6
  [ 0xFF, 0xFF, 0xDF, 0xFF, 0xFD, 0xFF, 0x55, 0x56, 0xFF, 0xFF, 0xFF ],
  # ENUM_PK_WRITE_LCD_DATA	    = 7
  [ 0xFF, 0xFF, 0x1F, 0xFF, 0x77, 0xFF, 0xFF, 0x07, 0xB0, 0xFF, 0xBB, 0xFF, 0x7F, 0xFF ],
  # ENUM_PK_READ_LCD_DATA	    = 8
  [ 0xFF, 0xFF, 0xEF, 0xFF, 0xF7, 0xFF, 0xFF, 0x07, 0xEF, 0x7F, 0x7F, 0x63, 0xFF, 0xFF, 0xFF ],
  # ENUM_PK_LCD_DISPLAY_ON	    = 9
  [ 0xFF, 0xFF, 0xDF, 0xFF, 0xF9, 0xFF, 0xFF, 0xF9, 0xFF, 0xFF, 0xFF ],
  # ENUM_PK_LCD_DISPLAY_OFF	    = 10
  [ 0xFF, 0xFF, 0xDF, 0xFF, 0x33, 0xFF, 0xFF, 0x33, 0xFF, 0xFF, 0xFF ],
  # ENUM_PK_CLEAR_LCD		    = 11
  [ 0xFF, 0xFF, 0xDF, 0xFF, 0xD3, 0xFF, 0xFF, 0xD3, 0xFF, 0xFF, 0xFF ],
  # ENUM_PK_ENABLE_TIME_OUT	    = 12
  [ 0xFF, 0xFF, 0xDF, 0xFF, 0xB3, 0xFF, 0xFF, 0xB3, 0xFF, 0xFF, 0xFF ],
  # ENUM_PK_DISABLE_TIME_OUT	    = 13
  [ 0xFF, 0xFF, 0xDF, 0xFF, 0x73, 0xFF, 0xFF, 0x73, 0xFF, 0xFF, 0xFF ],
  # ENUM_PK_DISPLAY_SERIAL_NUMBER   = 14
  [ 0xFF, 0xFF, 0xDF, 0xFF, 0x09, 0xFF, 0xFF, 0x09, 0xFF, 0xFF, 0xFF ],
  # ENUM_PK_GET_TAG_FORMAT	    = 15
  [ 0xFF, 0xFF, 0xEF, 0xFF, 0xF7, 0xFF, 0x7F, 0xFF, 0x87, 0xFF, 0x7F, 0xF3, 0xFF, 0xFF, 0xFF ],
  # ENUM_PK_FORMAT_TAG		    = 16
  [ 0xFF, 0xFF, 0x1F, 0xFF, 0x77, 0xFF, 0x7F, 0xFF, 0xF7, 0xFF, 0xBB, 0xFF, 0xFF, 0xFF ],
  # ENUM_PK_START_CMDr		    = 17
  [ 0xFC, 0xFF ],
  # ENUM_PK_ACK_START_CMD	    = 18
  [ 0xCA ],
  # ENUM_PK_CMD_DONE		    = 19
  [ 0x60 ],
  # ENUM_PK_FORMAT		    = 20
  [ 0xFF, 0xFF, 0xB7, 0xFF, 0xF5, 0xFF, 0x73, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0x7D, 0xFF, 0xBF, 0xFF ],
  # ENUM_PK_SET_TIME		    = 21
  [ 0xFF, 0xFF, 0xAF, 0xFF, 0x5B, 0xFF, 0x5D, 0x17, 0x3F, 0x1B, 0x77, 0x7F, 0x5B, 0xB8, 0xFF, 0xFF, 0xFF ],
  # ENUM_PK_LCD_DATAv		    = 22
  [ 0xFF, 0xFF, 0xD0 ],
  # ENUM_PK_READ_TIME1		    = 23
  [ 0x00, 0x10, 0x24 ],
  # ENUM_PK_NORMAL_STATUS	    = 24
  [ 0x00, 0xA8, 0x42 ],
  # ENUM_PK_PROTECTED_DATA	    = 25
  [ 0x00, 0xA8, 0x42 ],
  # ENUM_PK_EXTENDED_STATUS2	    = 26
  [ 0x00, 0xE8, 0x42 ],
  # ENUM_PK_LCD_DATAvr		    = 27
  [ 0x80, 0x80, 0x0A ],
  # ENUM_PK_LCD_DATA21		    = 28
  [ 0x00, 0x90, 0x0A ],
  # ENUM_PK_LCD_DATA22		    = 29
  [ 0x00, 0x50, 0x0A ],
  # ENUM_PK_TAG_FORMAT		    = 30
  [ 0x00, 0xF8, 0x0A ]
]

'''
  ========== CMD ==========
  print( cmd:)
    print( [map(hex,p) for p in [packets[pkt] for pkt in cmds[ENUM_CMD_DISPLAY_TIME]])
'''
'''
  底下命令相當於 > Start, < ACK, > CMD, < DONE
'''
ENUM_CMD_DISPLAY_TIME		= 0
ENUM_CMD_LCD_DISPLAY_ON		= 1
ENUM_CMD_LCD_DISPLAY_OFF	= 2
ENUM_CMD_CLEAR_LCD		= 3
ENUM_CMD_ENABLE_TIMEOUT		= 4
ENUM_CMD_DISABLE_TIMEOUT	= 5
ENUM_CMD_DISPLAY_SERIAL_NUMBER	= 6
'''
  底下命令都是讀資料，所以在 前面的通訊完畢後，還會加上 < RESPONSE, > READ_DONE
  其中 RESPONSE 只是開頭3 bytes，用來比對，
'''
ENUM_CMD_NORMAL_STATUS_READ	= 7
ENUM_CMD_EXTENDED_STATUS2_READ	= 8
ENUM_CMD_PROTECTED_DATA_READ	= 9
ENUM_CMD_GET_TAG_FORMAT		= 10
ENUM_CMD_FORMAT_TAG		= 11
'''
  CMD_WRITE_LCD_DATA 通訊過程跟以上都不同, 後面還需要再一段真正的 LCD Data
'''
ENUM_CMD_WRITE_LCD_DATA		= 12
'''
  CMD_READ_LCD_DATA 通訊過程相當詭異, 不止開始有問題，後面在傳完 Data 後，還有一段
  我認為有問題，底下與 README-Aconfig.txt 記載並不相符
'''
ENUM_CMD_READ_LCD_DATA		= 13

'''
  底下是尚未驗證的命令
'''
# ENUM_CMD_READ_TIME			= [ ENUM_PK_START_CMD, ENUM_PK_ACK_START_CMD, ENUM_PK_READ_DONE,		ENUM_PK_CMD_DONE,		ENUM_PK_READ_TIME1, ENUM_PK_READ_DONE ]
# ENUM_CMD_SET_TIME			= [ ENUM_PK_START_CMD, ENUM_PK_ACK_START_CMD, ENUM_PK_SET_TIME,		ENUM_PK_CMD_DONE ]
# ENUM_CMD_SET_LCD_TIMEOUT		= 
# ENUM_CMD_READ_TAG			= 
# ENUM_CMD_WRITE_TAG			= 
ENUM_CMD_TESTING = -1

cmds = [
  # 底下7個命令都是固定的4封包格式 : 相當於 > Start, < ACK, > CMD, < DONE
  # ENUM_CMD_DISPLAY_TIME		= 0
  [ ENUM_PK_START_CMD, ENUM_PK_ACK_START_CMD, ENUM_PK_DISPLAY_TIME,             ENUM_PK_CMD_DONE ],
  # ENUM_CMD_LCD_DISPLAY_ON		= 1
  [ ENUM_PK_START_CMD, ENUM_PK_ACK_START_CMD, ENUM_PK_LCD_DISPLAY_ON,           ENUM_PK_CMD_DONE ],
  # ENUM_CMD_LCD_DISPLAY_OFF		= 2
  [ ENUM_PK_START_CMD, ENUM_PK_ACK_START_CMD, ENUM_PK_LCD_DISPLAY_OFF,          ENUM_PK_CMD_DONE ],
  # ENUM_CMD_CLEAR_LCD			= 3
  [ ENUM_PK_START_CMD, ENUM_PK_ACK_START_CMD, ENUM_PK_CLEAR_LCD,                ENUM_PK_CMD_DONE ],
  # ENUM_CMD_ENABLE_TIMEOUT		= 4
  [ ENUM_PK_START_CMD, ENUM_PK_ACK_START_CMD, ENUM_PK_ENABLE_TIME_OUT,		ENUM_PK_CMD_DONE ],
  # ENUM_CMD_DISABLE_TIMEOUT		= 5
  [ ENUM_PK_START_CMD, ENUM_PK_ACK_START_CMD, ENUM_PK_DISABLE_TIME_OUT,		ENUM_PK_CMD_DONE ],
  # ENUM_CMD_DISPLAY_SERIAL_NUMBER	= 6
  [ ENUM_PK_START_CMD, ENUM_PK_ACK_START_CMD, ENUM_PK_DISPLAY_SERIAL_NUMBER,	ENUM_PK_CMD_DONE ],
  # 底下5個命令算是同一類，前4個封包後面，接上變動長度的內容，最後再加上 ENUM_PK_READ_DONE/ENUM_PK_CMD_DONE
  # ENUM_CMD_NORMAL_STATUS_READ		= 7, 後面 ENUM_PK_NORMAL_STATUS 只有前3bytes
  [ ENUM_PK_START_CMD, ENUM_PK_ACK_START_CMD, ENUM_PK_NORMAL_STATUS_READ,	ENUM_PK_CMD_DONE, ENUM_PK_NORMAL_STATUS, ENUM_PK_READ_DONE ],
  # ENUM_CMD_EXTENDED_STATUS2_READ	= 8, 後面 ENUM_PK_EXTENDED_STATUS2 只有前3bytes
  [ ENUM_PK_START_CMD, ENUM_PK_ACK_START_CMD, ENUM_PK_EXTENDED_STATUS2_READ,	ENUM_PK_CMD_DONE, ENUM_PK_EXTENDED_STATUS2, ENUM_PK_READ_DONE ],
  # ENUM_CMD_PROTECTED_DATA_READ	= 9, 後面  ENUM_PK_PROTECTED_DATA 只有前3bytes
  [ ENUM_PK_START_CMD, ENUM_PK_ACK_START_CMD, ENUM_PK_PROTECTED_DATA_READ,	ENUM_PK_CMD_DONE, ENUM_PK_PROTECTED_DATA, ENUM_PK_READ_DONE ],
  # ENUM_CMD_GET_TAG_FORMAT		= 10, 後面 ENUM_PK_TAG_FORMAT 只有前3bytes
  [ ENUM_PK_START_CMD, ENUM_PK_ACK_START_CMD, ENUM_PK_GET_TAG_FORMAT,		ENUM_PK_CMD_DONE, ENUM_PK_TAG_FORMAT, ENUM_PK_READ_DONE ],
  # ENUM_CMD_FORMAT_TAG			= 11
  [ ENUM_PK_START_CMD, ENUM_PK_ACK_START_CMD, ENUM_PK_FORMAT_TAG,		ENUM_PK_CMD_DONE, ENUM_PK_FORMAT, ENUM_PK_CMD_DONE ],
  # 底下命令特殊的地方是，後面並沒有 ..._DONE 的確認封包
  # ENUM_CMD_WRITE_LCD_DATA		= 12
  [ ENUM_PK_START_CMD, ENUM_PK_ACK_START_CMD, ENUM_PK_WRITE_LCD_DATA,		ENUM_PK_CMD_DONE ],
  # 底下命令特殊的地方是，後面有兩段來回確認，我猜是因為資料被切成2段
  # ENUM_CMD_READ_LCD_DATA		= 13 後面 ENUM_PK_LCD_DATAvr, ENUM_PK_LCD_DATA21 都只有前3bytes
  [ ENUM_PK_START_CMD, ENUM_PK_ACK_START_CMD, ENUM_PK_READ_LCD_DATA,		ENUM_PK_CMD_DONE, ENUM_PK_LCD_DATAvr, ENUM_PK_READ_DONE, ENUM_PK_LCD_DATA21, ENUM_PK_READ_DONE ]
]

lcdData = [0xFF, 0xFF, 0xD0, 0xFF, 0xF5, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x7F, 0xFF, 0xFF, 0xFF, 0x8F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xEB, 0x3F, 0xFF, 0xFF, 0x87, 0xFF, 0xFF, 0xFF, 0x5F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x3B, 0x3F, 0xFF, 0xFF, 0x7F, 0xFF, 0xFF, 0xFF, 0x3F, 0xFF, 0xFF, 0xFF, 0x7F, 0xFF, 0xFF, 0xFF, 0x5B, 0x3F, 0xFF, 0xFF, 0xBF, 0xFF, 0xFF, 0xFF, 0xBF, 0xFF, 0xFF, 0xFF, 0xBF, 0xFF, 0xFF, 0xFF, 0x9B, 0x3F, 0xFF, 0xFF, 0x3F, 0xFF, 0xFF, 0xFF, 0x7F, 0xFF, 0xFF, 0xFF, 0x3F, 0xFF, 0xFF, 0xFF, 0x1B, 0x3F, 0xFF, 0xFF, 0xDF, 0xFF, 0xFF, 0xFF, 0x7F, 0xFF, 0xFF, 0xFF, 0xDF, 0xFF, 0xFF, 0xFF, 0x6F, 0xFF, 0xFF, 0xFF, 0x5F, 0xFF, 0xFF, 0xFF, 0x07, 0x3F, 0xFF, 0xFF, 0xDF, 0xFF, 0xFF, 0xFF, 0x1B, 0x3F, 0xFF, 0xFF, 0x5F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x5F, 0xFF, 0xFF, 0xFF, 0x1F, 0xFF, 0xFF, 0xFF, 0x9F, 0xFF, 0xFF, 0xFF, 0x7B, 0x3F, 0xFF, 0xFF, 0x5F, 0xFF, 0xFF, 0xFF, 0xEB, 0x3F, 0xFF, 0xFF, 0x9F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x9F, 0xFF, 0xFF, 0xFF, 0x9F, 0xFF, 0xFF, 0xFF, 0x1F, 0xFF, 0xFF, 0xFF, 0xBB, 0x3F, 0xFF, 0xFF, 0x9F, 0xFF, 0xFF, 0xFF, 0xEB, 0x3F, 0xFF, 0xFF, 0x1F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x1F, 0xFF, 0xFF, 0xFF, 0x5F, 0xFF, 0xFF, 0xFF, 0x6F, 0xFF, 0xFF, 0xFF, 0x73, 0xB3, 0x33, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xF4, 0x3C, 0xFF, 0x1F, 0xFF]

'''
  ========= 自定義函數 ==========
'''
def getCmdId(pkt):
  if type(pkt) is str:
    ba = bytearray()
    ba.extend(pkt)
    pkt = ba

  for c in range(len(cmds)):
    for p in range(len(cmds[c])):
      if numpy.array_equal(packets[cmds[c][p]],pkt):
        return c
  return -1

def getPktId(pkt):
  for p in range(len(packets)):
    if numpy.array_equal(packets[p],pkt):
      return p
  return -1

def doCmd(ser, cmdId):
  # 前4個封包算是固定的，跟 cmdId 無關
  # pack 1 : write packet
  writePkt(ser, cmds[cmdId][0]) # 每一個命令的前2個封包，其實都一樣，這邊是 ENUM_PK_START_CMD
  # pack 2 : read packet
  res = readPkt(ser, 1)
  if (not pktEqual(res, cmds[cmdId][1])):
    return 1
  # pack 3: write packet
  writePkt(ser, cmds[cmdId][2])
  # pack 4 : read packet
  res = readPkt(ser, 1)
  if (not pktEqual(res, cmds[cmdId][3])):
    return 1
  # 接下來需要區分 cmdId
  if cmdId < ENUM_CMD_NORMAL_STATUS_READ: # READ 之前是一般命令，只有兩次一來一回
    return 0
  elif cmdId < ENUM_CMD_FORMAT_TAG: # 命令 7, 8, 9, 10 是 read, 所以需要再讀回傳內容，然後傳送 ENUM_PK_READ_DONE。這邊是固定值
    time.sleep(0.002)
    res = readPkt(ser, 1)
    if (not pktEqual(res, cmds[cmdId][4])):
      return 1
    writePkt(ser, cmds[cmdId][5])
    return 0
  elif cmdId == ENUM_CMD_FORMAT_TAG: # 乍看跟 7/8/9/10 一樣，其實是相反方向的，還需要送命令、讀回(ENUM_PK_CMD_DONE)一次
    writePkt(ser, cmds[cmdId][4])
    res = readPkt(ser, 1)
    if (pktEqual(res, cmds[cmdId][5])):
      return 0
  elif cmdId == ENUM_CMD_WRITE_LCD_DATA:
    writeByteArray(ser, lcdData);
    return 0
  else: # READ_LCD_DATA 比較特殊, 還有 < ENUM_PK_LCD_DATAvr, > ENUM_PK_READ_DONE, < ENUM_PK_LCD_DATA21, >ENUM_PK_READ_DONE
    print( "TODO")
    return 0

  return 1

def readPkt(ser, t=None):
  line = "";
  ser.timeout = t
  ser.flushInput()
  ch = ser.read()
  ser.timeout = 0.025
  while ch:
    line += str(ch)
    ch = ser.read()
  print( "read : ", " ".join("{:02X}".format(ord(x)) for x in line))
  return line

def writePkt(ser, pktId):
  print( "write: ", " ".join("{:02X}".format(x) for x in packets[pktId]))
  ser.write(packets[pktId])

def writeByteArray(ser, pkt):
  print( "write: ", " ".join("{:02X}".format(x) for x in pkt))
  ser.write(pkt)

def pktEqual(pkt, pktId):
  if (len(pkt) < len(packets[pktId])):
    print( "cmp1: ", " ".join("{:02X}".format(ord(x)) for x in pkt), " <> ", " ".join("{:02X}".format(x) for x in packets[pktId]))
    return False
  if (len(pkt) > len(packets[pktId])):
    pkt = pkt[:len(packets[pktId])-len(pkt)]
  print( "cmp2: ", " ".join("{:02X}".format(ord(x)) for x in pkt), " <> ", " ".join("{:02X}".format(x) for x in packets[pktId]))
  ba = bytearray()
  ba.extend(pkt)
  return numpy.array_equal(ba, packets[pktId])
