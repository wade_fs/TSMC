#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>        
#include <stdlib.h> 
#include <unistd.h>

int baudFromStr(char* b) {
  int br = atoi(b);
  switch (br) {
    case 9600   : return B9600;
    case 19200  : return B19200;
    case 38499  : return B38400;
    case 57600  : return B57600;
    case 115200 : return B115200;
    case 230400 : return B230400;
    default: return B9600;
  }
}

#define BAUDRATE B115200 
#define MODEMDEVICE "/dev/ttyUSB0"/*UART NAME IN PROCESSOR*/
#define _POSIX_SOURCE 1 /* POSIX compliant source */
#define FALSE 0
#define TRUE 1
int openport(int, char**);
void sendport(void);
void readport(void);
int fd=0, n;
static int cnt, size, s_cnt;
unsigned char *var;
struct termios oldtp, newtp;
char sendcmd1[10]="\0";
FILE *file;
 
void readport(void)
{
  unsigned char buff;
  while (1) { 
    n = read(fd, &buff, 1);
    //  fcntl(fd,F_SETFL,0);
    if (n == -1) {
      switch(errno) {
        case EAGAIN: /* sleep() */ 
          continue;
        default: return;
      }
    }
    if (n ==0) break;
    printf("%d %c\n", n,buff);
  }
}

int openport(int argc, char* argv[])
{
  int baudrate;

  if (argc != 3) {
    printf ("Usage: %s PORT BAUDRATE\n", argv[0]);
    return 1;
  }
  baudrate = baudFromStr(argv[2]);
  fd = open(argv[1], O_RDWR | O_NOCTTY |O_NDELAY );
  printf("Open Serial Port %d\n",fd);
  if (fd <0) {
    perror(argv[1]);
    return 1;
  }

  fcntl(fd,F_SETFL,0);
  tcgetattr(fd,&oldtp); /* save current serial port settings */
  // tcgetattr(fd,&newtp); /* save current serial port settings */
  bzero(&newtp, sizeof(newtp));
  // bzero(&oldtp, sizeof(oldtp));

  newtp.c_cflag = baudrate | CRTSCTS | CS8 | CLOCAL | CREAD;

  newtp.c_iflag = IGNPAR | ICRNL;

  newtp.c_oflag = 0;                                                                        
  newtp.c_lflag = ICANON;                                                                    
  newtp.c_cc[VINTR]    = 0;     /* Ctrl-c */
  newtp.c_cc[VQUIT]    = 0;     /* Ctrl-\ */
  newtp.c_cc[VERASE]   = 0;     /* del */
  newtp.c_cc[VKILL]    = 0;     /* @ */
  // newtp.c_cc[VEOF]     = 4;     /* Ctrl-d */
  newtp.c_cc[VTIME]    = 0;     /* inter-character timer unused */
  newtp.c_cc[VMIN]     = 0;     /* blocking read until 1 character arrives */
  newtp.c_cc[VSWTC]    = 0;     /* '\0' */
  newtp.c_cc[VSTART]   = 0;     /* Ctrl-q */
  newtp.c_cc[VSTOP]    = 0;     /* Ctrl-s */
  newtp.c_cc[VSUSP]    = 0;     /* Ctrl-z */
  newtp.c_cc[VEOL]     = 0;     /* '\0' */
  newtp.c_cc[VREPRINT] = 0;     /* Ctrl-r */
  newtp.c_cc[VDISCARD] = 0;     /* Ctrl-u */
  newtp.c_cc[VWERASE]  = 0;     /* Ctrl-w */
  newtp.c_cc[VLNEXT]   = 0;     /* Ctrl-v */
  newtp.c_cc[VEOL2]    = 0;     /* '\0' */
                                                                                                                                                  
//    tcflush(fd, TCIFLUSH);
//   tcsetattr(fd,TCSANOW,&newtp);
  return 0;
}
 
int  main(int argc, char* argv[])
{
  if (openport(argc, argv) != 0) return 1;
  readport();
  return 0;
}
