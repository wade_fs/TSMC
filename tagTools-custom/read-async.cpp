#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <unistd.h>
#include <termios.h>
#include <fcntl.h>
#include <sys/types.h>
#include <signal.h>
#include <string.h>
#include <stdlib.h>

int baudFromStr(char* b) {
  int br = atoi(b);
  switch (br) {
    case 9600   : return B9600;
    case 19200  : return B19200;
    case 38499  : return B38400;
    case 57600  : return B57600;
    case 115200 : return B115200;
    case 230400 : return B230400;
    default: return B9600;
  }
}

#define BAUDRATE B38400
#define MODEMDEVICE "/dev/ttyS1"
#define _POSIX_SOURCE 1 /* POSIX 系統相容 */
#define FALSE 0
#define TRUE 1

volatile int STOP=FALSE; 

void signal_handler_IO (int status);   /* 定義訊號處理程序 */
int wait_flag=TRUE;                    /* 沒收到訊號的話就會是 TRUE */

int main(int argc, char* argv[])
{
  int fd,c, res;
  struct termios oldtio,newtio;
  struct sigaction saio;           /* definition of signal action */
  char buf[255];
  int baudrate;

  if (argc != 3) {
    printf ("Usage: %s PORT BAUDRATE\n", argv[0]);
    return 1;
  }
  baudrate = baudFromStr(argv[2]);

  /* 開啟裝置為 non-blocking (讀取功能會馬上結束返回) */
  fd = open(argv[1], O_RDWR | O_NOCTTY | O_NONBLOCK);
  if (fd <0) {perror(MODEMDEVICE); exit(-1); }

  /* 在使裝置非同步化前, 安裝訊號處理程序 */
  saio.sa_handler = signal_handler_IO;
  sigemptyset(&saio.sa_mask);
  saio.sa_flags = 0;
  saio.sa_restorer = NULL;
  sigaction(SIGIO,&saio,NULL);
  
  /* 允許行程去接收 SIGIO 訊號*/
  fcntl(fd, F_SETOWN, getpid());
  /* 使檔案ake the file descriptor 非同步 (使用手冊上說只有 O_APPEND 及
  O_NONBLOCK, 而 F_SETFL 也可以用...) */
  fcntl(fd, F_SETFL, FASYNC);

  tcgetattr(fd,&oldtio); /* 儲存目前的序列埠設定值 */
  /* 設定新的序列埠為標準輸入程序 */
  newtio.c_cflag = BAUDRATE | CRTSCTS | CS8 | CLOCAL | CREAD;
  newtio.c_iflag = IGNPAR | ICRNL;
  newtio.c_oflag = 0;
  newtio.c_lflag = ICANON;
  newtio.c_cc[VMIN]=1;
  newtio.c_cc[VTIME]=0;
  tcflush(fd, TCIFLUSH);
  tcsetattr(fd,TCSANOW,&newtio);
 
  /* 等待輸入訊號的迴圈. 很多有用的事我們將在這做 */ 
  while (STOP==FALSE) {
    printf(".\n");usleep(100000);
    /* 在收到 SIGIO 後, wait_flag = FALSE, 輸入訊號存在則可以被讀取 */
    if (wait_flag==FALSE) { 
      res = read(fd,buf,255);
      buf[res]=0;
      for (int i=0; i<res; i++)   /*printing only the received characters*/
        printf("%02X ",buf[i]);
      printf ("\n");

      if (res==1) STOP=TRUE; /* 如果只輸入 CR 則停止迴圈 */
      wait_flag = TRUE;      /* 等待新的輸入訊號 */
    }
  }
  /* 回存舊的序列埠設定值 */
  tcsetattr(fd,TCSANOW,&oldtio);
}

/***************************************************************************
* 訊號處理程序. 設定 wait_flag 為 FALSE, 以使上述的迴圈能接收字元          *
***************************************************************************/

void signal_handler_IO (int status)
{
  printf("received SIGIO signal.\n");
  wait_flag = FALSE;
}
