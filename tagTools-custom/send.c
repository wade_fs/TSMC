// http://linux.org.tw/CLDP/OLD/Serial-Programming-HOWTO-3.html
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

int baudFromStr(char* b) {
  int br = atoi(b);
  switch (br) {
    case 9600   : return B9600;
    case 19200  : return B19200;
    case 38499  : return B38400;
    case 57600  : return B57600;
    case 115200 : return B115200;
    case 230400 : return B230400;
    default: return B9600;
  }
}

// 建議 buffer 不要超過 255
 
  
#define BAUDRATE B115200 
#define MODEMDEVICE "/dev/ttyUSB0"
#define _POSIX_SOURCE 1 /* POSIX compliant source */
#define FALSE 0
#define TRUE 1
 
FILE *file;
int fileLen;
char *tmpbuffer;
int openport(int, char**);
void readport(void);
void sendport(void);
int fd=0;
struct termios oldtp, newtp;
char buffer[512];
 
int main(int argc, char* argv[])
{
  if (openport(argc, argv) != 0) return 1;
  sleep(1);
  sendport();
  return 0;
}
 
void sendport(void)
{
  printf("enter write\n");
  int n;
  //   sem_wait(&len);
  file = fopen("sample.txt", "r");
 
  //get file size
 
  fseek(file, 0, SEEK_END);
  fileLen = ftell(file);
  fseek(file, 0, SEEK_SET);
 
  tmpbuffer = (char *)malloc(fileLen + 1);
 
  //read file contents
  printf("Start send\n");
  fread(tmpbuffer, fileLen, 1, file);
  fclose(file);
 
  n = write(fd, tmpbuffer, fileLen + 1);
 
  if (n < 0) {
    fputs("write() of bytes failed!\n", stderr);
  } else {
    printf("Image sent successfully %d\n",n);
  }
  close(fd);
}
 
int openport(int argc, char* argv[])
{
  int baudrate;

  if (argc != 3) {
    printf ("Usage: %s PORT BAUDRATE\n", argv[0]);
    return 1;
  }
  baudrate = baudFromStr(argv[2]);
  fd = open(argv[1], O_RDWR | O_NOCTTY |O_NDELAY );
  printf("Open Serial Port %d\n",fd);
  if (fd <0) {
    perror(argv[1]);
    return 1;
  }

  fcntl(fd,F_SETFL,0);
  tcgetattr(fd,&oldtp); /* save current serial port settings */
  // tcgetattr(fd,&newtp); /* save current serial port settings */
  bzero(&newtp, sizeof(newtp));
  // bzero(&oldtp, sizeof(oldtp));

  newtp.c_cflag = baudrate | CRTSCTS | CS8 | CLOCAL | CREAD;

  newtp.c_iflag = IGNPAR | ICRNL;

  newtp.c_oflag = 0;

  newtp.c_lflag = ICANON;

  newtp.c_cc[VINTR]    = 0;     /* Ctrl-c */
  newtp.c_cc[VQUIT]    = 0;     /* Ctrl-\ */
  newtp.c_cc[VERASE]   = 0;     /* del */
  newtp.c_cc[VKILL]    = 0;     /* @ */
  //newtp.c_cc[VEOF]     = 4;     /* Ctrl-d */
  newtp.c_cc[VEOF]     = 0;     /* Ctrl-d */
  newtp.c_cc[VTIME]    = 0;     /* inter-character timer unused */
  newtp.c_cc[VMIN]     = 1;     /* blocking read until 1 character arrives */
  newtp.c_cc[VSWTC]    = 0;     /* '\0' */
  newtp.c_cc[VSTART]   = 0;     /* Ctrl-q */
  newtp.c_cc[VSTOP]    = 0;     /* Ctrl-s */
  newtp.c_cc[VSUSP]    = 0;     /* Ctrl-z */
  newtp.c_cc[VEOL]     = 0;     /* '\0' */
  newtp.c_cc[VREPRINT] = 0;     /* Ctrl-r */
  newtp.c_cc[VDISCARD] = 0;     /* Ctrl-u */
  newtp.c_cc[VWERASE]  = 0;     /* Ctrl-w */
  newtp.c_cc[VLNEXT]   = 0;     /* Ctrl-v */
  newtp.c_cc[VEOL2]    = 0;     /* '\0' */
                    
                    
  //     tcflush(fd, TCIFLUSH);
  //  tcsetattr(fd,TCSANOW,&newtp);
  return 0;
}

