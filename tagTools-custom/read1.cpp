#include <stdio.h>
#include <fcntl.h>   /* File Control Definitions           */
#include <termios.h> /* POSIX Terminal Control Definitions */
#include <unistd.h>  /* UNIX Standard Definitions      */ 
#include <errno.h>   /* ERROR Number Definitions           */
#include <string.h>
#include <stdlib.h>

int baudFromStr(char* b) {
  int br = atoi(b);
  switch (br) {
    case 9600   : return B9600;
    case 19200  : return B19200;
    case 38499  : return B38400;
    case 57600  : return B57600;
    case 115200 : return B115200;
    case 230400 : return B230400;
    default: return B9600;
  }
}

int main(int argc, char* argv[])
{
  int fd;/*File Descriptor*/
  int baudrate;
  
  if (argc != 3) {
    printf ("Usage: %s PORT BAUDRATE\n", argv[0]);
    return 1;
  }
  baudrate = baudFromStr(argv[2]);

  /*------------------------------- Opening the Serial Port -------------------------------*/

  /* Change /dev/ttyUSB0 to the one corresponding to your system */

  fd = open(argv[1], O_RDWR | O_NOCTTY);  /* ttyUSB0 is the FT232 based USB2SERIAL Converter   */
  /* O_RDWR   - Read/Write access to serial port       */
  /* O_NOCTTY - No terminal will control the process   */
  /* Open in blocking mode,read will wait              */
                
                                                        
                
  if(fd == -1)            /* Error Checking */
    printf("\n  Error! in Opening ttyUSB0  ");
  else
    printf("\n  ttyUSB0 Opened Successfully ");

  
  /*---------- Setting the Attributes of the serial port using termios structure --------- */
  
  struct termios SerialPortSettings;  /* Create the structure                          */

  tcgetattr(fd, &SerialPortSettings);  /* Get the current attributes of the Serial port */

  /* Setting the Baud rate */
  cfsetispeed(&SerialPortSettings,B9600); /* Set Read  Speed as 9600                       */
  cfsetospeed(&SerialPortSettings,B9600); /* Set Write Speed as 9600                       */

  /* 8N1 Mode */
  SerialPortSettings.c_cflag &= ~PARENB;   /* Disables the Parity Enable bit(PARENB),So No Parity   */
  SerialPortSettings.c_cflag &= ~CSTOPB;   /* CSTOPB = 2 Stop bits,here it is cleared so 1 Stop bit */
  SerialPortSettings.c_cflag &= ~CSIZE;   /* Clears the mask for setting the data size             */
  SerialPortSettings.c_cflag |=  CS8;      /* Set the data bits = 8                                 */
  
  SerialPortSettings.c_cflag &= ~CRTSCTS;       /* No Hardware flow Control                         */
  SerialPortSettings.c_cflag |= CREAD | CLOCAL; /* Enable receiver,Ignore Modem Control lines       */ 
  
  
  SerialPortSettings.c_iflag &= ~(IXON | IXOFF | IXANY);          /* Disable XON/XOFF flow control both i/p and o/p */
  SerialPortSettings.c_iflag &= ~(ICANON | ECHO | ECHOE | ISIG);  /* Non Cannonical mode                            */

  SerialPortSettings.c_oflag &= ~OPOST;/*No Output Processing*/
  
  /* Setting Time outs */
  SerialPortSettings.c_cc[VMIN] = 10; /* Read at least 10 characters */
  SerialPortSettings.c_cc[VTIME] = 0; /* Wait indefinetly   */


  if((tcsetattr(fd,TCSANOW,&SerialPortSettings)) != 0) /* Set the attributes to the termios structure*/
    printf("\n  ERROR ! in Setting attributes");
  else
    printf("\n  BaudRate = 9600 \n  StopBits = 1 \n  Parity   = none");
    
  /*------------------------------- Read data from serial port -----------------------------*/

  tcflush(fd, TCIFLUSH);   /* Discards old data in the rx buffer            */

  unsigned char read_buffer[255];   /* Buffer to store the data received              */
  int  bytes_read = 0;    /* Number of bytes read by the read() system call */
  int i = 0;

  bytes_read = read(fd,&read_buffer,255); /* Read the data                   */
    
  printf("\n\n  Bytes Rxed %d", bytes_read); /* Print the number of bytes read */
  printf("\n\n  ");

  for(i=0;i<bytes_read;i++)   /*printing only the received characters*/
    printf("%02X ",read_buffer[i]);
  
  printf("\n +----------------------------------+\n\n\n");

  close(fd); /* Close the serial port */
  return 0;
}
