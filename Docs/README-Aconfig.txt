1. 首先會發現亞士帝與 Jason的過程不同
  [wade] Jason 是怎麼想的？
2. 失敗的命令相當多，列表如下:
  (Display Time), (Read Time)，(Extended Status2 Read)，(Set LCD Time Out), (Read Tag), (Write Tag)
3. 好像只要有底下的訊息，亞士帝的都算失敗，但是 Jason 的都永遠會出現
   COM1 Write(hex): 2(Bytes)B5 FF 
   COM1 Read(hex): 2(Bytes) B5 FF 
4. Read 或 Display 時(例如 Display Serial Number, Display Time) 螢幕有顯示嗎？
5. Format Tag 時，Jason 都失敗，亞士帝則只支援 NOFORMAT, 8K, 32K 不支援
6. (Normal Status Read) 與 (Extended Status1 Read) 通訊過程都一樣，但是 Aconfig 顯示的內容卻不同?

(Display Time)		FF FF DF FF BB FF FF BB FF FF FF
(Read Time)		FF FF DF FF 3B FF FF 3B FF FF FF
(LCD Display ON)	FF FF DF FF F9 FF FF F9 FF FF FF
(LCD Display OFF)	FF FF DF FF 33 FF FF 33 FF FF FF
(Clear LCD)		FF FF DF FF D3 FF FF D3 FF FF FF
(Enable Time Out)	FF FF DF FF B3 FF FF B3 FF FF FF
(Disable Time Out)	FF FF DF FF 73 FF FF 73 FF FF FF
(Display Serial Number)	FF FF DF FF 09 FF FF 09 FF FF FF
======= 在此線之前，感覺命令有重複, 例如 09 FF FF 09 FF FF =======
(Extended Status2 Read)	FF FF DF FF FD FF 5A 58 FF FF FF
(Normal Status Read)	FF FF DF FF FD FF 55 56 FF FF FF
(Extended Status1 Read)	其實同(Normal Status Read)，但是 Aconfig 顯示的內容卻不同
(Protected Data Read)	其實同(Normal Status Read)，但是 Aconfig 顯示的內容卻不同
(Set Time)		FF FF AF FF 5B FF 15 73 EF 97 77 DF 5B 5F FF 7F FF
(Read LCD Data)		FF FF EF FF F7 FF FF 07 EF 7F 7F 63 FF FF FF
(Get Tag Format)	FF FF EF FF F7 FF 7F FF 87 FF 7F F3 FF FF FF
(Write LCD Data)	FF FF 1F FF 77 FF FF 07 B0 FF BB FF 7F FF
(Format Tag)->1 -> No Format:
			FF FF 1F FF 77 FF 7F FF F7 FF BB FF FF FF
			FF FF B7 FF F5 FF 73 FB FB FB FB FB FB FB FB FB FB FB FB FB FB 7D FF BF FF

(Display Time) 
失敗, 但是 Jason 的TAG是成功的
COM1 Write(hex): 2(Bytes)FC FF 
COM1 Read(hex): 1(Bytes) CA 
COM1 Write(hex): 11(Bytes)FF FF DF FF BB FF FF BB FF FF FF 
COM1 Read(hex): 1(Bytes) E0 	<-- 應該回 60...
COM1 Write(hex): 2(Bytes)FC FF 
COM1 Write(hex): 2(Bytes)B5 FF  <-- 有這個的時候，通常是失敗？
COM1 Read(hex): 2(Bytes) B5 FF 
COM1 Read(hex): 1(Bytes) CA 
COM1 Write(hex): 11(Bytes)FF FF DF FF BB FF FF BB FF FF FF 
COM1 Read(hex): 1(Bytes) E0 
COM1 Write(hex): 2(Bytes)FC FF 
COM1 Write(hex): 2(Bytes)B5 FF 
COM1 Read(hex): 2(Bytes) B5 FF 
COM1 Read(hex): 1(Bytes) CA 
COM1 Write(hex): 11(Bytes)FF FF DF FF BB FF FF BB FF FF FF 
COM1 Read(hex): 1(Bytes) E0 

Tag Protcole Message

上午 08:28:06  Issuing Display Time Command...

上午 08:28:07 Display Time Command Failed Error : TAG:Communication error with tag.

-----------------------------------------------------------------

\\\\\\\\\Time Related Functions \\\\\\\\\\
成功 from Jason
(Display Time)

COM1 Write(hex): 2(Bytes)FC FF 
COM1 Write(hex): 2(Bytes)B5 FF 
COM1 Read(hex): 2(Bytes) B5 FF 
COM1 Read(hex): 1(Bytes) CA 
COM1 Write(hex): 11(Bytes)FF FF DF FF BB FF FF BB FF FF FF 
COM1 Read(hex): 1(Bytes) 60 

Tag Protcole Message

上午 07:25:09  Issuing Display Time Command...

上午 07:25:09 Display Time Command Successful 

-----------------------------------------------------------------

(Read Time)
都失敗，過程不同
COM1 Write(hex): 2(Bytes)FC FF 
COM1 Read(hex): 1(Bytes) CA 
COM1 Write(hex): 11(Bytes)FF FF DF FF 3B FF FF 3B FF FF FF 
COM1 Read(hex): 1(Bytes) E0 
COM1 Write(hex): 2(Bytes)FC FF 
COM1 Write(hex): 2(Bytes)B5 FF <-- 有這個，通常就代表失敗
COM1 Read(hex): 2(Bytes) B5 FF 
COM1 Read(hex): 1(Bytes) CA 
COM1 Write(hex): 11(Bytes)FF FF DF FF 3B FF FF 3B FF FF FF 
COM1 Read(hex): 1(Bytes) E0 
COM1 Write(hex): 2(Bytes)FC FF 
COM1 Write(hex): 2(Bytes)B5 FF 
COM1 Read(hex): 2(Bytes) B5 FF 
COM1 Read(hex): 1(Bytes) CA 
COM1 Write(hex): 11(Bytes)FF FF DF FF 3B FF FF 3B FF FF FF 
COM1 Read(hex): 1(Bytes) E0 

Tag Protcole Message

上午 08:29:19  Issuing Read Time Command...

上午 08:29:20 Read Time Command Failed Error : TAG:Communication error with tag.

-----------------------------------------------------------------
(Read Time)
失敗, From Jason
COM1 Write(hex): 2(Bytes)FC FF 
COM1 Read(hex): 2(Bytes) FC FF 
COM1 Write(hex): 2(Bytes)B5 FF 
COM1 Read(hex): 2(Bytes) B5 FF 
COM1 Read(hex): 1(Bytes) CA 
COM1 Write(hex): 11(Bytes)FF FF DF FF 3B FF FF 3B FF FF FF 
COM1 Read(hex): 1(Bytes) 60 
COM1 Write(hex): 2(Bytes)FC FF 
COM1 Read(hex): 2(Bytes) FC FF 
COM1 Write(hex): 2(Bytes)B5 FF 
COM1 Read(hex): 2(Bytes) B5 FF 
COM1 Write(hex): 2(Bytes)FC FF 
COM1 Read(hex): 2(Bytes) FC FF 
COM1 Write(hex): 2(Bytes)B5 FF 
COM1 Read(hex): 2(Bytes) B5 FF 
COM1 Write(hex): 2(Bytes)FC FF 
COM1 Read(hex): 2(Bytes) FC FF 
COM1 Write(hex): 2(Bytes)B5 FF 
COM1 Read(hex): 2(Bytes) B5 FF 

Tag Protcole Message

上午 07:25:38  Issuing Read Time Command...

上午 07:25:39 Read Time Command Failed Error : TAG:Tag may not be in the pod (or) Tag is not responding.

-----------------------------------------------------------------

(Set Time)
都失敗, 懶得附 Jason的過程
COM1 Write(hex): 2(Bytes)FC FF 
COM1 Read(hex): 1(Bytes) CA 
COM1 Write(hex): 17(Bytes)FF FF AF FF 5B FF 15 73 EF 97 77 DF 5B 5F FF 7F FF 
COM1 Read(hex): 1(Bytes) E0 
COM1 Write(hex): 2(Bytes)FC FF 
COM1 Write(hex): 2(Bytes)B5 FF	<-- 有這個，就代表失敗？
COM1 Read(hex): 2(Bytes) B5 FF 
COM1 Read(hex): 1(Bytes) CA 
COM1 Write(hex): 17(Bytes)FF FF AF FF 5B FF 15 73 EF 97 77 DF 5B 5F FF 7F FF 
COM1 Read(hex): 1(Bytes) E0 
COM1 Write(hex): 2(Bytes)FC FF 
COM1 Write(hex): 2(Bytes)B5 FF 
COM1 Read(hex): 2(Bytes) B5 FF 
COM1 Read(hex): 1(Bytes) CA 
COM1 Write(hex): 17(Bytes)FF FF AF FF 5B FF 15 73 EF 97 77 DF 5B 5F FF 7F FF 
COM1 Read(hex): 1(Bytes) E0 

Tag Protcole Message

上午 08:31:57  Issuing Set Time Command...

上午 08:31:58 Set Time Command Failed 

-----------------------------------------------------------------

\\\\\\\\\Status Commands \\\\\\\\\\

(Normal Status Read)
成功，兩個 Tag 結果一樣，但過程不同
COM1 Write(hex): 2(Bytes)FC FF 
COM1 Read(hex): 1(Bytes) CA 
COM1 Write(hex): 11(Bytes)FF FF DF FF FD FF 55 56 FF FF FF 
COM1 Read(hex): 1(Bytes) 60 
COM1 Read(hex): 1(Bytes) 00 
COM1 Read(hex): 1(Bytes) A8 
COM1 Read(hex): 23(Bytes) 42 00 CD 0D 5B DC 80 C2 FF D6 C8 FF 13 AA 00 5E 00 00 BE 69 00 24 10 
COM1 Write(hex): 2(Bytes)9F FF 

Tag Protcole Message

上午 08:34:59 Normal Status Read Command Successful 
Battery is Good
Tag Software Version: 67
SMART-Tag has   32 KB RAM

SMART-Tag Clock Speed is 0

-----------------------------------------------------------------
\\\\\\\\\Status Commands \\\\\\\\\\

(Normal Status Read)
成功, From Jason
COM1 Write(hex): 2(Bytes)FC FF 
COM1 Write(hex): 2(Bytes)B5 FF 
COM1 Read(hex): 2(Bytes) B5 FF 
COM1 Read(hex): 1(Bytes) CA 
COM1 Write(hex): 11(Bytes)FF FF DF FF FD FF 55 56 FF FF FF 
COM1 Read(hex): 1(Bytes) 60 
COM1 Read(hex): 1(Bytes) 00 
COM1 Read(hex): 1(Bytes) A8 
COM1 Read(hex): 23(Bytes) 42 00 CD 0D 5B DC 80 C2 FF D6 C8 FF 13 AA 00 5E 00 00 BE 69 00 24 10 
COM1 Write(hex): 2(Bytes)9F FF 

Tag Protcole Message

上午 07:27:51 Normal Status Read Command Successful 
Battery is Good
Tag Software Version: 67
SMART-Tag has   32 KB RAM

SMART-Tag Clock Speed is 0

-----------------------------------------------------------------

(Extended Status1 Read)
成功，兩個 Tag 結果一樣，但過程不同
COM1 Write(hex): 2(Bytes)FC FF 
COM1 Read(hex): 1(Bytes) CA 
COM1 Write(hex): 11(Bytes)FF FF DF FF FD FF 55 56 FF FF FF 
COM1 Read(hex): 1(Bytes) 60 
COM1 Read(hex): 1(Bytes) 00 
COM1 Read(hex): 1(Bytes) A8 
COM1 Read(hex): 23(Bytes) 42 00 CD 0D 5B DC 80 C2 FF D6 C8 FF 13 AA 00 5E 00 00 BE 69 00 24 10 
COM1 Write(hex): 2(Bytes)9F FF 

Tag Protcole Message


上午 08:35:39 Extended Status1 Read Command Successful 

Extended Status 1 : 
	Real Time Clock = False
	Serial Number = 38525

-----------------------------------------------------------------
(Extended Status1 Read)
成功, from Jason
COM1 Write(hex): 2(Bytes)FC FF 
COM1 Write(hex): 2(Bytes)B5 FF 
COM1 Read(hex): 2(Bytes) B5 FF 
COM1 Read(hex): 1(Bytes) CA 
COM1 Write(hex): 11(Bytes)FF FF DF FF FD FF 55 56 FF FF FF 
COM1 Read(hex): 1(Bytes) 60 
COM1 Read(hex): 1(Bytes) 00 
COM1 Read(hex): 1(Bytes) A8 
COM1 Read(hex): 23(Bytes) 42 00 CD 0D 5B DC 80 C2 FF D6 C8 FF 13 AA 00 5E 00 00 BE 69 00 24 10 
COM1 Write(hex): 2(Bytes)9F FF 

Tag Protcole Message

上午 07:29:30 Extended Status1 Read Command Successful 

Extended Status 1 : 
	Real Time Clock = False
	Serial Number = 38525

-----------------------------------------------------------------

(Extended Status2 Read)
都失敗，無法參考正確通訊
COM1 Write(hex): 2(Bytes)FC FF 
COM1 Read(hex): 1(Bytes) CA 
COM1 Write(hex): 11(Bytes)FF FF DF FF FD FF 5A 58 FF FF FF 
COM1 Read(hex): 1(Bytes) 60 
COM1 Write(hex): 2(Bytes)FC FF 
COM1 Read(hex): 2(Bytes) FC FF 
COM1 Write(hex): 2(Bytes)B5 FF 
COM1 Read(hex): 2(Bytes) B5 FF 
COM1 Write(hex): 2(Bytes)FC FF 
COM1 Read(hex): 2(Bytes) FC FF 
COM1 Write(hex): 2(Bytes)B5 FF 
COM1 Read(hex): 2(Bytes) B5 FF 
COM1 Write(hex): 2(Bytes)FC FF 
COM1 Read(hex): 2(Bytes) FC FF 
COM1 Write(hex): 2(Bytes)B5 FF 
COM1 Read(hex): 2(Bytes) B5 FF 
COM1 Read(hex): 1(Bytes) 00 


Tag Protcole Message

上午 08:36:09 Extended Status2 Read Command Failed Error : TAG:Tag may not be in the pod (or) Tag is not responding.

-----------------------------------------------------------------
(Extended Status2 Read)
失敗，from Jason, 但是好像有跡可尋
COM1 Write(hex): 2(Bytes)FC FF 
COM1 Read(hex): 2(Bytes) FC FF 
COM1 Write(hex): 2(Bytes)B5 FF 
COM1 Read(hex): 2(Bytes) B5 FF 
COM1 Read(hex): 1(Bytes) CA 
COM1 Write(hex): 11(Bytes)FF FF DF FF FD FF 5A 58 FF FF FF 
COM1 Read(hex): 1(Bytes) 60 
COM1 Read(hex): 1(Bytes) 00 
COM1 Read(hex): 1(Bytes) A8 
COM1 Read(hex): 23(Bytes) 42 00 CD 0D 5B DC 80 C2 FF D6 C8 FF 13 AA 00 5E 00 00 BE 69 00 24 10 
COM1 Write(hex): 2(Bytes)9F FF 
COM1 Write(hex): 2(Bytes)FC FF 
COM1 Read(hex): 2(Bytes) FC FF 
COM1 Write(hex): 2(Bytes)B5 FF 
COM1 Read(hex): 2(Bytes) B5 FF 
COM1 Write(hex): 2(Bytes)FC FF 
COM1 Read(hex): 2(Bytes) FC FF 
COM1 Write(hex): 2(Bytes)B5 FF 
COM1 Read(hex): 2(Bytes) B5 FF 
COM1 Read(hex): 1(Bytes) CA 
COM1 Write(hex): 11(Bytes)FF FF DF FF FD FF 5A 58 FF FF FF 
COM1 Read(hex): 1(Bytes) 60 
COM1 Read(hex): 1(Bytes) 00 
COM1 Read(hex): 1(Bytes) A8 
COM1 Read(hex): 23(Bytes) 42 00 CD 0D 5B DC 80 C2 FF D6 C8 FF 13 AA 00 5E 00 00 BE 69 00 24 10 
COM1 Write(hex): 2(Bytes)9F FF 
COM1 Write(hex): 2(Bytes)FC FF 
COM1 Read(hex): 2(Bytes) FC FF 
COM1 Write(hex): 2(Bytes)B5 FF 
COM1 Read(hex): 2(Bytes) B5 FF 
COM1 Write(hex): 2(Bytes)FC FF 
COM1 Read(hex): 2(Bytes) FC FF 
COM1 Write(hex): 2(Bytes)B5 FF 
COM1 Read(hex): 2(Bytes) B5 FF 
COM1 Read(hex): 1(Bytes) CA 
COM1 Write(hex): 11(Bytes)FF FF DF FF FD FF 5A 58 FF FF FF 
COM1 Read(hex): 1(Bytes) 60 
COM1 Read(hex): 1(Bytes) 00 
COM1 Read(hex): 1(Bytes) A8 
COM1 Read(hex): 23(Bytes) 42 00 CD 0D 5B DC 80 C2 FF D6 C8 FF 13 AA 00 5E 00 00 BE 69 00 24 10 
COM1 Write(hex): 2(Bytes)9F FF 

Tag Protcole Message

上午 07:32:58 Extended Status2 Read Command Failed Error : TAG:Communication error with tag.

-----------------------------------------------------------------

(Protected Data Read)
成功，結果一樣，但是過程不同
COM1 Write(hex): 2(Bytes)FC FF 
COM1 Read(hex): 1(Bytes) CA 
COM1 Write(hex): 11(Bytes)FF FF DF FF FD FF 55 56 FF FF FF 
COM1 Read(hex): 1(Bytes) 60 
COM1 Read(hex): 1(Bytes) 00 
COM1 Read(hex): 1(Bytes) A8 
COM1 Read(hex): 23(Bytes) 42 00 CD 0D 5B DC 80 C2 FF D6 C8 FF 13 AA 00 5E 00 00 BE 69 00 24 10 
COM1 Write(hex): 2(Bytes)9F FF 

Tag Protcole Message

上午 08:36:42 Protected Data Read Command Successful 

Protected Data : 
	Battery Low Count = 0
	General Status    = 0
	Received Bytes    = 0
	Receive Errors    = 0
	Serial Number     = 38525
	Transmit Errors   = 0
	Transmitted Bytes = 0

-----------------------------------------------------------------

(Protected Data Read)
成功，From Jason
COM1 Write(hex): 2(Bytes)FC FF 
COM1 Read(hex): 2(Bytes) FC FF 
COM1 Write(hex): 2(Bytes)B5 FF 
COM1 Read(hex): 2(Bytes) B5 FF 
COM1 Read(hex): 1(Bytes) CA 
COM1 Write(hex): 11(Bytes)FF FF DF FF FD FF 55 56 FF FF FF 
COM1 Read(hex): 1(Bytes) 60 
COM1 Read(hex): 1(Bytes) 00 
COM1 Read(hex): 1(Bytes) A8 
COM1 Read(hex): 23(Bytes) 42 00 CD 0D 5B DC 80 C2 FF D6 C8 FF 13 AA 00 5E 00 00 BE 69 00 24 10 
COM1 Write(hex): 2(Bytes)9F FF 

Tag Protcole Message

上午 07:34:18 Protected Data Read Command Successful 

Protected Data : 
	Battery Low Count = 0
	General Status    = 0
	Received Bytes    = 0
	Receive Errors    = 0
	Serial Number     = 38525
	Transmit Errors   = 0
	Transmitted Bytes = 0
	
-----------------------------------------------------------------

\\\\\\\\\LCD Related Functions \\\\\\\\\\

(Write LCD Data)->123
成功，結果一樣，但是過程與內容都不同
COM1 Write(hex): 2(Bytes)FC FF 
COM1 Read(hex): 1(Bytes) CA 
COM1 Write(hex): 14(Bytes)FF FF 1F FF 77 FF FF 07 B0 FF BB FF 7F FF 
COM1 Read(hex): 1(Bytes) 60 
COM1 Write(hex): 251(Bytes)FF FF D0 FF F5 FF FF FF FF FF 7F FF FF FF 8F FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF EB 3F FF FF 87 FF FF FF 5F FF FF FF FF FF FF FF 3B 3F FF FF 7F FF FF FF 3F FF FF FF 7F FF FF FF 5B 3F FF FF BF FF FF FF BF FF FF FF BF FF FF FF 9B 3F FF FF 3F FF FF FF 7F FF FF FF 3F FF FF FF 1B 3F FF FF DF FF FF FF 7F FF FF FF DF FF FF FF 6F FF FF FF 5F FF FF FF 07 3F FF FF DF FF FF FF 1B 3F FF FF 5F FF FF FF FF FF FF FF 5F FF FF FF 1F FF FF FF 9F FF FF FF 7B 3F FF FF 5F FF FF FF EB 3F FF FF 9F FF FF FF FF FF FF FF 9F FF FF FF 9F FF FF FF 1F FF FF FF BB 3F FF FF 9F FF FF FF EB 3F FF FF 1F FF FF FF FF FF FF FF 1F FF FF FF 5F FF FF FF 6F FF FF FF 73 B3 33 FB FB FB FB FB FB FB FB FB FB FB FB FB FB FB FB FB FB FB FB FB FB FB FB FB FB FB FB FB F4 3C FF 1F FF 

Tag Protcole Message

上午 08:37:30 Write LCD Data Command Successful 

-----------------------------------------------------------------

(Write LCD Data)->123
成功，From Jason
COM1 Write(hex): 2(Bytes)FC FF 
COM1 Read(hex): 2(Bytes) FC FF 
COM1 Write(hex): 2(Bytes)B5 FF 
COM1 Read(hex): 2(Bytes) B5 FF 
COM1 Read(hex): 1(Bytes) CA 
COM1 Write(hex): 14(Bytes)FF FF 1F FF 77 FF FF 07 B0 FF BB FF 7F FF 
COM1 Read(hex): 1(Bytes) 60 
COM1 Write(hex): 251(Bytes)FF FF D0 FF F5 FF FF FF FF FF 7F FF FF FF 8F FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF EB 3F FF FF 87 FF FF FF 5F FF FF FF FF FF FF FF 3B 3F FF FF 7F FF FF FF 3F FF FF FF 7F FF FF FF 5B 3F FF FF BF FF FF FF BF FF FF FF BF FF FF FF 9B 3F FF FF 3F FF FF FF 7F FF FF FF 3F FF FF FF 1B 3F FF FF DF FF FF FF 7F FF FF FF DF FF FF FF 6F FF FF FF 5F FF FF FF 07 3F FF FF DF FF FF FF 1B 3F FF FF 5F FF FF FF FF FF FF FF 5F FF FF FF 1F FF FF FF 9F FF FF FF 7B 3F FF FF 5F FF FF FF EB 3F FF FF 9F FF FF FF FF FF FF FF 9F FF FF FF 9F FF FF FF 1F FF FF FF BB 3F FF FF 9F FF FF FF EB 3F FF FF 1F FF FF FF FF FF FF FF 1F FF FF FF 5F FF FF FF 6F FF FF FF 73 B3 33 FB FB FB FB FB FB FB FB FB FB FB FB FB FB FB FB FB FB FB FB FB FB FB FB FB FB FB FB FB F4 3C FF 1F FF 

Tag Protcole Message

上午 07:38:06 Write LCD Data Command Successful 

-----------------------------------------------------------------

(Read LCD Data)
都成功，但是過程與內容都不同
COM1 Write(hex): 2(Bytes)FC FF 
COM1 Read(hex): 2(Bytes) FC FF 
COM1 Read(hex): 1(Bytes) CA 
COM1 Write(hex): 15(Bytes)FF FF EF FF F7 FF FF 07 EF 7F 7F 63 FF FF FF 
COM1 Read(hex): 1(Bytes) 60 
COM1 Read(hex): 1(Bytes) 80 
COM1 Read(hex): 1(Bytes) 80 
COM1 Read(hex): 259(Bytes) 0A 00 00 00 00 80 00 00 00 70 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 14 C0 00 00 78 00 00 00 A0 00 00 00 00 00 00 00 C4 C0 00 00 80 00 00 00 C0 00 00 00 80 00 00 00 A4 C0 00 00 40 00 00 00 40 00 00 00 40 00 00 00 64 C0 00 00 C0 00 00 00 80 00 00 00 C0 00 00 00 E4 C0 00 00 20 00 00 00 80 00 00 00 20 00 00 00 90 00 00 00 A0 00 00 00 F8 C0 00 00 20 00 00 00 E4 C0 00 00 A0 00 00 00 00 00 00 00 A0 00 00 00 E0 00 00 00 60 00 00 00 84 C0 00 00 A0 00 00 00 14 C0 00 00 60 00 00 00 00 00 00 00 60 00 00 00 60 00 00 00 E0 00 00 00 44 C0 00 00 60 00 00 00 14 C0 00 00 E0 00 00 00 00 00 00 00 E0 00 00 00 A0 00 00 00 90 00 00 00 8C 4C CC 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04 0B 30 D6 C8 FD 7A 1A 08 5B DC CD 0D AA 00 43 CD 28 70 
COM1 Write(hex): 2(Bytes)9F FF 
COM1 Read(hex): 1(Bytes) 00 
COM1 Read(hex): 1(Bytes) 90 
COM1 Read(hex): 11(Bytes) 0A C7 E2 58 DF C3 9B 5E 7B C1 A0  << 後兩碼是 checksum
COM1 Write(hex): 2(Bytes)9F FF 

Tag Protcole Message

上午 08:38:15 Read LCD Data Command Successful LCD Data = 123

-----------------------------------------------------------------
(Read LCD Data)
成功，From Jason
COM1 Write(hex): 2(Bytes)FC FF 
COM1 Read(hex): 2(Bytes) FC FF 
COM1 Write(hex): 2(Bytes)B5 FF 
COM1 Read(hex): 2(Bytes) B5 FF 
COM1 Read(hex): 1(Bytes) CA 
COM1 Write(hex): 15(Bytes)FF FF EF FF F7 FF FF 07 EF 7F 7F 63 FF FF FF 
COM1 Read(hex): 1(Bytes) 60 
COM1 Read(hex): 1(Bytes) 80 
COM1 Read(hex): 1(Bytes) 80 
COM1 Read(hex): 259(Bytes) 0A 00 00 00 00 80 00 00 00 70 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 14 C0 00 00 78 00 00 00 A0 00 00 00 00 00 00 00 C4 C0 00 00 80 00 00 00 C0 00 00 00 80 00 00 00 A4 C0 00 00 40 00 00 00 40 00 00 00 40 00 00 00 64 C0 00 00 C0 00 00 00 80 00 00 00 C0 00 00 00 E4 C0 00 00 20 00 00 00 80 00 00 00 20 00 00 00 90 00 00 00 A0 00 00 00 F8 C0 00 00 20 00 00 00 E4 C0 00 00 A0 00 00 00 00 00 00 00 A0 00 00 00 E0 00 00 00 60 00 00 00 84 C0 00 00 A0 00 00 00 14 C0 00 00 60 00 00 00 00 00 00 00 60 00 00 00 60 00 00 00 E0 00 00 00 44 C0 00 00 60 00 00 00 14 C0 00 00 E0 00 00 00 00 00 00 00 E0 00 00 00 A0 00 00 00 90 00 00 00 8C 4C CC 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04 0B 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04 C5 90 
COM1 Write(hex): 2(Bytes)9F FF 
COM1 Read(hex): 1(Bytes) 00 
COM1 Read(hex): 1(Bytes) 90 
COM1 Read(hex): 11(Bytes) 0A 00 00 00 00 00 00 00 00 0A 00 << 後兩碼是 checksum
COM1 Write(hex): 2(Bytes)9F FF 

Tag Protcole Message

上午 07:39:30 Read LCD Data Command Successful LCD Data = 123             

-----------------------------------------------------------------

(LCD Display ON)
都成功，結果一樣，但是過程不同
COM1 Write(hex): 2(Bytes)FC FF 
COM1 Read(hex): 1(Bytes) CA 
COM1 Write(hex): 11(Bytes)FF FF DF FF F9 FF FF F9 FF FF FF 
COM1 Read(hex): 1(Bytes) 60 

Tag Protcole Message

上午 08:39:24  Issuing LCD ON Command...

上午 08:39:24 LCD Display ON Command Successful 

-----------------------------------------------------------------
(LCD Display ON)
成功，From Jason
COM1 Write(hex): 2(Bytes)FC FF 
COM1 Write(hex): 2(Bytes)B5 FF 
COM1 Read(hex): 2(Bytes) B5 FF 
COM1 Read(hex): 1(Bytes) CA 
COM1 Write(hex): 11(Bytes)FF FF DF FF F9 FF FF F9 FF FF FF 
COM1 Read(hex): 1(Bytes) 60 

Tag Protcole Message

上午 07:41:34  Issuing LCD ON Command...

上午 07:41:35 LCD Display ON Command Successful 

-----------------------------------------------------------------

(LCD Display OFF)
都成功，結果一樣，但是過程不同
COM1 Write(hex): 2(Bytes)FC FF 
COM1 Read(hex): 1(Bytes) CA 
COM1 Write(hex): 11(Bytes)FF FF DF FF 33 FF FF 33 FF FF FF 
COM1 Read(hex): 1(Bytes) 60 

Tag Protcole Message

上午 08:40:01  Issuing LCD OFF Command...

上午 08:40:01 LCD Display OFF Command Successful 

-----------------------------------------------------------------
(LCD Display OFF)
成功，From Jason
COM1 Write(hex): 2(Bytes)FC FF 
COM1 Write(hex): 2(Bytes)B5 FF 
COM1 Read(hex): 2(Bytes) B5 FF 
COM1 Read(hex): 1(Bytes) CA 
COM1 Write(hex): 11(Bytes)FF FF DF FF 33 FF FF 33 FF FF FF 
COM1 Read(hex): 1(Bytes) 60 

Tag Protcole Message

上午 07:44:03  Issuing LCD OFF Command...

上午 07:44:03 LCD Display OFF Command Successful 

-----------------------------------------------------------------

(Set LCD Time Out)
都失敗
Active Tag Error !!
Set LCD Time Out Functionaly is not currently Supported by Active Tag

-----------------------------------------------------------------

(Clear LCD)
都成功，結果一樣，但是過程不同
COM1 Write(hex): 2(Bytes)FC FF 
COM1 Read(hex): 1(Bytes) CA 
COM1 Write(hex): 11(Bytes)FF FF DF FF D3 FF FF D3 FF FF FF 
COM1 Read(hex): 1(Bytes) 60 

Tag Protcole Message

上午 08:42:16  Issuing Clear LCD Command...

上午 08:42:17 Clear LCD Command Successful 

-----------------------------------------------------------------
(Clear LCD)
成功，From Jason
COM1 Write(hex): 2(Bytes)FC FF 
COM1 Write(hex): 2(Bytes)B5 FF 
COM1 Read(hex): 2(Bytes) B5 FF 
COM1 Read(hex): 1(Bytes) CA 
COM1 Write(hex): 11(Bytes)FF FF DF FF D3 FF FF D3 FF FF FF 
COM1 Read(hex): 1(Bytes) 60 

Tag Protcole Message


上午 07:48:04  Issuing Clear LCD Command...

上午 07:48:04 Clear LCD Command Successful 

-----------------------------------------------------------------
(Enable Time Out)
都成功，結果一樣，但是過程不同，不附 Jason過程
COM1 Write(hex): 2(Bytes)FC FF 
COM1 Read(hex): 1(Bytes) CA 
COM1 Write(hex): 11(Bytes)FF FF DF FF B3 FF FF B3 FF FF FF 
COM1 Read(hex): 1(Bytes) 60 

Tag Protcole Message

上午 08:42:53  Issuing Enable Timeout Command...

上午 08:42:54 Enable Time Out Command Successful 

-----------------------------------------------------------------

(Disable Time Out)
都成功，結果一樣，但是過程不同，不附Jason過程
COM1 Write(hex): 2(Bytes)FC FF 
COM1 Read(hex): 1(Bytes) CA 
COM1 Write(hex): 11(Bytes)FF FF DF FF 73 FF FF 73 FF FF FF 
COM1 Read(hex): 1(Bytes) 60 

Tag Protcole Message

上午 08:43:26  Issuing Disable Timeout Command...

上午 08:43:26 Enable Time Out Command Successful 

-----------------------------------------------------------------

(Display Serial Number)
都成功，結果一樣，但是過程不同，不附Jason過程
COM1 Write(hex): 2(Bytes)FC FF 
COM1 Read(hex): 1(Bytes) CA 
COM1 Write(hex): 11(Bytes)FF FF DF FF 09 FF FF 09 FF FF FF 
COM1 Read(hex): 1(Bytes) 60 

Tag Protcole Message


上午 08:43:59  Issuing Display Serial Number Command...

上午 08:43:59 Display Serial Number Command Successful 

-----------------------------------------------------------------

\\\\\\\\\Format Commands \\\\\\\\\\

(Get Tag Format)
成功，但是 Jason 的TAG失敗
COM1 Write(hex): 2(Bytes)FC FF 
COM1 Read(hex): 1(Bytes) CA 
COM1 Write(hex): 15(Bytes)FF FF EF FF F7 FF 7F FF 87 FF 7F F3 FF FF FF 
COM1 Read(hex): 1(Bytes) 60 
COM1 Read(hex): 1(Bytes) 00 
COM1 Read(hex): 1(Bytes) F8 
COM1 Read(hex): 33(Bytes) 0A 9B 6A 8D AF 77 DC D5 F1 EB FF 12 C5 D0 E7 FF D5 F3 BB BF 6F 73 27 F6 4F DC B9 19 CC 6F DE D5 28 
COM1 Write(hex): 2(Bytes)9F FF 

Tag Protcole Message

上午 08:44:32 Get Tag Format Command Successful 
Format Type = NOFORMATTag Label = 揥梆???H??峙毻

-----------------------------------------------------------------

(Format Tag)->1 -> No Format
成功，但是 Jason 的TAG失敗
COM1 Write(hex): 2(Bytes)FC FF 
COM1 Read(hex): 1(Bytes) CA 
COM1 Write(hex): 14(Bytes)FF FF 1F FF 77 FF 7F FF F7 FF BB FF FF FF 
COM1 Read(hex): 1(Bytes) 60 
COM1 Write(hex): 25(Bytes)FF FF B7 FF F5 FF 73 FB FB FB FB FB FB FB FB FB FB FB FB FB FB 7D FF BF FF 
COM1 Read(hex): 1(Bytes) 60 

Tag Protcole Message

上午 08:45:34 Format Tag Command Successful 

-----------------------------------------------------------------

(Format Tag)->1 -> 8K Format
都失敗
Null

Tag Protcole Message

上午 08:46:10 Format Tag Command Failed 

-----------------------------------------------------------------

(Format Tag)->1 -> 32K Format
都失敗
Null

Tag Protcole Message

上午 08:46:51 Format Tag Command Failed

-----------------------------------------------------------------

\\\\\\\\\Tag Read/Write \\\\\\\\\\

(Read Tag)
都失敗
Null

-----------------------------------------------------------------

(Write Tag)
都失敗
Null 

-----------------------------------------------------------------
