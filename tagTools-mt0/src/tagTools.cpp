// http://libserial.sourceforge.net/x27.html
#include <iostream>
#include <string>
#include <cstdlib>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <signal.h>
#include <dlfcn.h>
#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>

#include "rs232ioctls.h"
#include "cmd.h"
#include "utils.h"

////////// #define //////////
#define clrscr() printf("\e[1;1H\e[2J");

#define DISPLAY_NONE	0
#define DISPLAY_RAW	1
#define DISPLAY_HEX	2
#define MAX_B		10

////////// 整體變數區 ///////////

int ret = 0;
std::string res="";
int cfg_display_com_in_mode = DISPLAY_RAW;
int cfg_timeout = 1000000; // 0 for unlimited, unit is us (1.0e-6 s)

////////// 自訂函數區 ///////////

void menu() {
  std::string showRes;
  switch (cfg_display_com_in_mode) {
    case DISPLAY_HEX: showRes = utils::toHex(res, true); break;
    case DISPLAY_RAW: showRes = utils::toAscii(res, '.'); break;
    default: showRes = "";
  }

  printf ("o) 切換顯示從 Com Port 讀到的資料的模式 : None / Hex / Raw(%s)\n"
          "a) 增加遞延時間(%d)\n"
          "m) 減少遞延時間(%d)\n"
          "c) cmd: Clear LCD\n"
          "+) cmd: LCD Display On\n"
          "-) cmd: LCD Display Off\n"
          "r) cmd: Read LCD Data\n"
          "w) cmd: Write LCD Data\n"
          "t) cmd: Display Time\n"
          "T) cmd: Read Time\n"
          "s) cmd; Set Time\n"
          "n) cmd: Normal Status Read\n"
          "1) cmd: Extended Status1 Read\n"
          "2) cmd: Extended Status2 Read\n"
          "p) cmd: Protected Data Read\n"
          "e) cmd: Enable Timeout\n"
          "d) cmd: Disable Timeout\n"
          "#) cmd: Display Serial Number\n"
          "f) cmd: Get Tag Format\n"
          "F) cmd: Format Tag\n"
          "x) pkg: PK_START_CMD     (FC FF)\n"
          "y) pkg: PK_READ_LCD_DATA (FF FF EF FF F7 FF FF 07 EF 7F 7F 63 FF FF FF)\n"
          "z) pkg: PK_ACK_START_CMD (CA)\n" // PK_GET_TAG_FORMAT(FF FF EF FF F7 FF 7F FF 87 FF 7F F3 FF FF FF)\n"
          "q) Quit\n"
          "(%d) '%s' > ",
          (cfg_display_com_in_mode==DISPLAY_NONE?"None": (cfg_display_com_in_mode==DISPLAY_RAW?"Raw":"Hex")),
          cfg_timeout, cfg_timeout, ret, utils::toHex(showRes, true).c_str()
  );
  fflush(stdout);
}

int kb_hit(void) {
  struct termios oldt, newt;
  struct timeval tv;
  fd_set fds;
  int hit, oldf;

  // 設定成 non-blocking，並存起來，後面要還原
  tcgetattr(STDIN_FILENO, &oldt);
  newt = oldt;
  newt.c_lflag &= ~(ICANON | ECHO);
  tcsetattr(STDIN_FILENO, TCSANOW, &newt);
  oldf = fcntl(STDIN_FILENO, F_GETFL, 0);
  fcntl(STDIN_FILENO, F_SETFL, oldf | O_NONBLOCK);

  while (1) {
    tv.tv_sec = 0;
    tv.tv_usec = 0;
    FD_ZERO(&fds);
    FD_SET(STDIN_FILENO, &fds);
    select(STDIN_FILENO+1, &fds, NULL, NULL, &tv);
    if ((hit = FD_ISSET(STDIN_FILENO, &fds))) {
      // 然後把該按鍵傳回去
      int ch = fgetc(stdin);
      if (ch == 27) {
        ch = fgetc(stdin);
        if (ch == 79 || ch == 91) ch = fgetc(stdin); // ESC 鍵是複合鍵，鍵值有時是兩個 bytes, 我們不處理
        FD_SET(STDIN_FILENO, &fds);
        select(STDIN_FILENO+1, &fds, NULL, NULL, &tv);
      }
      while (FD_ISSET(STDIN_FILENO, &fds)) {
        fgetc(stdin);
        FD_SET(STDIN_FILENO, &fds);
        select(STDIN_FILENO+1, &fds, NULL, NULL, &tv);
      }
      // 還原 stdin 的 non-blocking 模式為正常模式
      fcntl(STDIN_FILENO, F_SETFL, oldf);
      tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
      return ch;
    }
  }
}

int main(int argc, char **argv)
{
  int speed;
  struct timeval timeout;
  cmd c = cmd();
  packet p;
  int ch;

  if (argc != 3) {
    cerr << "Usage: " << argv[0] << " PORT SPEED\n\tPORT: /dev/ttyUSB0, SPEED: 9600\n";
    exit(1);
  }

  if ((tty_fd = term_open(argv[1])) < 0) {
    fprintf(stderr, "Error opening: %s", argv[1]);
    return (255);
  }

  term_start(tty_fd);

  rs232_setspeed(tty_fd, 9600);

  /* loop for input */
  while (true) {
    // clear window
    clrscr();
    menu();
    res = "";

    /* 讓 stdin + Com Port 設為 0, block till input */
    FD_CLR(tty_fd, &readfs);
    FD_ZERO(&readfs);
    FD_SET(tty_fd, &readfs);
    timeout.tv_sec=cfg_timeout/1000000;
    timeout.tv_usec = cfg_timeout - timeout.tv_sec * 1000000;
    select(tty_fd+1, &readfs, NULL, NULL, cfg_timeout == 0 ? NULL : &timeout); // 如果是標準輸入/輸出，仍然需要等 Enter

    // 有按鍵時
    ch = kb_hit();
    switch (ch) {
      case 'o':		// 切換顯示從 Com Port 讀到的資料的模式 : None / Hex / Raw
        cfg_display_com_in_mode = (cfg_display_com_in_mode + 1) % 3;
        continue;	// 這邊用 continue 則會立刻下一個迴圈，不會執行 switch() 後面的敘述
      case 'a':		// 增加遞延時間
        if (cfg_timeout == 0) cfg_timeout = 1000;
        else if (cfg_timeout < 512000) cfg_timeout <<= 1;
        else cfg_timeout += 100000;
        continue;	// 這邊用 continue 則會立刻下一個迴圈，不會執行 switch() 後面的敘述
      case 'm':		// 減少遞延時間
        if (cfg_timeout <= 1000) cfg_timeout = 0;
        else if (cfg_timeout <= 512000) cfg_timeout >>= 1;
        else cfg_timeout -= 100000;
        continue;	// 這邊用 continue 則會立刻下一個迴圈，不會執行 switch() 後面的敘述
      case 'c':		// cmd: Clear LCD
        cout << endl;
        c = cmd(CMD_CLEAR_LCD);
        ret = c.doCmd(tty_fd);
        continue;
      case '+':		// cmd: LCD Display On
        cout << endl;
        c = cmd(CMD_LCD_DISPLAY_ON);
        ret = c.doCmd(tty_fd);
        continue;
      case '-':		// cmd: LCD Display Off
        cout << endl;
        c = cmd(CMD_LCD_DISPLAY_OFF);
        ret = c.doCmd(tty_fd);
        continue;
      case 'r':		// cmd: Read LCD Data
        cout << endl;
        c = cmd(CMD_READ_LCD_DATA);
        ret = c.doCmd(tty_fd);
        continue;
      case 'w':		// cmd: Write LCD Data
        cout << endl;
        c = cmd(CMD_WRITE_LCD_DATA);
        ret = c.doCmd(tty_fd);
        continue;
      case 't':		// cmd: Display Time
        cout << endl;
        c = cmd(CMD_DISPLAY_TIME);
        ret = c.doCmd(tty_fd);
        continue;
      case 'T':		// cmd: Read Time
        cout << endl;
        c = cmd(CMD_READ_TIME);
        ret = c.doCmd(tty_fd);
        continue;
      case 's':		// cmd: Set Time
        cout << endl;
        c = cmd(CMD_SET_TIME);
        ret = c.doCmd(tty_fd);
        continue;
      case 'n':		// cmd: Normal Status Read
        cout << endl;
        c = cmd(CMD_NORMAL_STATUS_READ);
        ret = c.doCmd(tty_fd);
        continue;
      case '1':		// cmd: Extended Status1 Read
        cout << endl;
        c = cmd(CMD_EXTENDED_STATUS1_READ);
        ret = c.doCmd(tty_fd);
        continue;
      case '2':		// cmd: Extended Status2 Read
        cout << endl;
        c = cmd(CMD_EXTENDED_STATUS2_READ);
        ret = c.doCmd(tty_fd);
        continue;
      case 'p':		// cmd: Protected Data Read
        cout << endl;
        c = cmd(CMD_PROTECTED_DATA_READ);
        ret = c.doCmd(tty_fd);
        continue;
      case 'e':		// cmd: Enable Timeout
        cout << endl;
        c = cmd(CMD_ENABLE_TIMEOUT);
        ret = c.doCmd(tty_fd);
        continue;
      case 'd':		// cmd: Disnable Timeout
        cout << endl;
        c = cmd(CMD_DISABLE_TIMEOUT);
        ret = c.doCmd(tty_fd);
        continue;
      case '#':		// cmd: Display Serial Number
        cout << endl;
        c = cmd(CMD_DISPLAY_SERIAL_NUMBER);
        ret = c.doCmd(tty_fd);
        continue;
      case 'f':		// cmd: Get Tag Format
        cout << endl;
        c = cmd(CMD_GET_TAG_FORMAT);
        ret = c.doCmd(tty_fd);
        continue;
      case 'F':		// cmd: Format Tag
        cout << endl;
        c = cmd(CMD_FORMAT_TAG);
        ret = c.doCmd(tty_fd);
        continue;
      case 'x':		// send packet : Start Command = FC FF
        ret = write(tty_fd, packets[PK_START_CMD].data(), packets[PK_START_CMD].length());
        cout << std::endl << "<PK_START_CMD> " << utils::toHex(packets[PK_START_CMD]) << endl;
        res = c.readCmd(tty_fd);
        continue;
      case 'y':		// read packet : PK_READ_LCD_DATA = FF FF EF FF F7 FF FF 07 EF 7F 7F 63 FF FF FF
        ret = write(tty_fd, packets[PK_READ_LCD_DATA].data(), packets[PK_READ_LCD_DATA].length());
        cout << std::endl << "<PK_READ_LCD_DATA> " << utils::toHex(packets[PK_READ_LCD_DATA]) << endl;
        res = c.readCmd(tty_fd);
        continue;
      case 'z':		// send packet : PK_GET_TAG_FORMAT = FF FF EF FF F7 FF 7F FF 87 FF 7F F3 FF FF FF
        ret = write(tty_fd, packets[PK_GET_TAG_FORMAT].data(), packets[PK_GET_TAG_FORMAT].length());
        cout << std::endl << "<PK_GET_TAG_FORMAT> " << utils::toHex(packets[PK_GET_TAG_FORMAT]) << endl;
        res = c.readCmd(tty_fd);
        continue;
      case 'q':
        close(tty_fd);
        exit (0);
      default:
        ret = -1;
        continue;
    };
  }
  close(tty_fd);
  return 0;
}
