/**************************************************************************
 *                  rs232ioctls.c                                         *
 *                                                                        *
 *   This file is distributed under the GNU LGPL License, see LICENSE.TXT *
 *   for further details.                                                 *
 *                                                                        *
 *   2004 Santiago Palomino S�nchez-Manjavacas sps@acm.asoc.fi.upm.es     *
 **************************************************************************/

#include <sys/param.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <stdio.h>
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>

// DOS Version:
#ifndef TCGETS
//#warning "DOS Version compilation"
#include <termios.h>
#define TCGETS TCGETA
#define TCSETS TCSETA
#define TIOCGETD TIOCMGET
#define TIOCSETD TIOCMSET

/*  int cfmakeraw(struct termios *termios_p)
  {
    termios_p->c_iflag &= ~(IGNBRK|BRKINT|PARMRK|ISTRIP
			    |INLCR|IGNCR|ICRNL|IXON);
    termios_p->c_oflag &= ~OPOST;
    termios_p->c_lflag &= ~(ECHO|ECHONL|ICANON|ISIG|IEXTEN);
    termios_p->c_cflag &= ~(CSIZE|PARENB);
    termios_p->c_cflag |= CS8;
    return 1;
  }*/
#endif

extern "C" {

#include "rs232ioctls.h"

#include <termios.h>
  struct SPEEDS {
    const int os_code;
    const int speed;
  } speeds[] = {
    {
    B50, 50}, {
    B75, 75}, {
    B110, 110}, {
    B300, 300}, {
    B600, 600}, {
    B1200, 1200}, {
    B2400, 2400}, {
    B4800, 4800}, {
    B9600, 9600},
#ifdef B14400
    {
    B14400, 14400},
#endif
    {
    B19200, 19200}, {
    B38400, 38400}, {
    B57600, 57600}, {
    B115200, 115200}, {
  0, 0}};

  int speedint2os(int spd) {
    int i = 0;
    while (speeds[i].speed) {
      if (speeds[i].speed == spd) {
        return speeds[i].os_code;
      }
      i++;
    }
    return 0;
  }

  struct ttynode {
    int fd;
    struct termios ttydata;
    struct termios ttyolddata;
    struct ttynode *next;
  };

  struct ttynode *terms = NULL;

  int term_open(char *devname) {
    return open(devname, O_RDWR); // | O_NDELAY);
  }
// TODO: use malloc instead.
  struct termios tty[10];
  struct termios tty_b[10];

  int term_start(int fd) {
    int err;
    struct ttynode *aux;
    if (!terms) {
      terms = (struct ttynode *)malloc(sizeof(struct ttynode));
      aux = terms;
    } else {
      aux = terms;
      while (aux->next) {
        aux = aux->next;
      }
      aux->next = (struct ttynode *)malloc(sizeof(struct ttynode));
      aux = aux->next;
    }

    aux->fd = fd;
    aux->next = NULL;

    err = ioctl(fd, TCGETS, &(aux->ttydata));
    if (err < 0)
      return 1;

    aux->ttyolddata = aux->ttydata;

    cfmakeraw(&(aux->ttydata));

    ioctl(fd, TCSETS, &(aux->ttydata));
    if (err < 0)
      return 2;

    return 0;
  }

  int term_stop() {
    struct ttynode *aux = terms;
    struct ttynode *oldaux;
    int err;
    while (aux) {
      err = ioctl(aux->fd, TCSETS, &(aux->ttyolddata));
      if (err < 0) {
        perror("Error stoping terminals!");
        return 1;
      }
      oldaux = aux;
      aux = aux->next;
      free(oldaux);
    }
    terms = NULL;               /* close all terms */
    return 0;
  }

  int rs232_setspeed(int fd, int spd) {
    // TODO: I need some backup!

    struct termios tty_line;
    //  fcntl(fd,F_SETFL,O_NONBLOCK);   

    if (ioctl(fd, TCGETS, &tty_line) < 0)
      return 1;
    //  fprintf(stderr, "Setting tty line speed to %d %d\n", spd, speedint2os(spd));
    cfsetospeed(&tty_line, speedint2os(spd));
    cfsetispeed(&tty_line, speedint2os(spd));
    /*cfmakeraw(&tty_line);  Is done in other place */

    if (ioctl(fd, TCSETS, &tty_line) < 0)
      return 1;

    return 0;
  }

  int rs232_getspeed(int fd);
}
