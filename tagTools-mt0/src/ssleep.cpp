#include "ssleep.h"
#include <math.h>
#include <stdio.h>

time_t aux1, aux2;
#define SYNC(); time(&aux1); time(&aux2); while (aux1 == aux2) {time(&aux2);}

int iterations_per_msec;
// TODO hacer versiones fast, med y slow seg�n CPU.
int sscalibrate(int it)
{
  iterations_per_msec = it;
  return it;
}

int bogoloop(int it)
{
  int i = 1;
  int b = 1;
  int c = 1;
  for (i = it; i > 0; i--) {
    it *= 4;
    b = it / 3;
    c = it * b;
    it = it * (int)sin(3.141516 / 2);
    b = b * ((int)(cos(0) + 1));
  }
  return it + i;
}

/* Well, I know there is some instrumentation overhead, and also, Linux isn't a hard RTOS,
   so ... Don't use this unless your you understand what it does. */
int scalibrate()
{
  int iterations = 1024 * 1024; // * 32;
  time_t last;
  time_t nw;
  int finished = 0;
  int oldval = -1, newval = 0;

  while (!finished) {
    SYNC();
    time(&last);
    bogoloop(iterations);
    time(&nw);

    if (nw != last) {
      newval = iterations / (nw - last) / 1000;
    }

    printf("Calibrate: %ld %d it / msec %d O-> %d\n", nw - last, iterations, newval, oldval);
    fflush(stdout);
    if (oldval != 0)
      if (newval == oldval)
        finished++;
    oldval = newval;
    iterations *= 2;

  }
  iterations_per_msec = newval;
  return newval;
}

int ssleep(int usecs)
{
  //  fprintf(stderr, "ssleep: %d %d", usecs, iterations_per_msec);
  bogoloop(usecs * (iterations_per_msec / 1000) + 1);
  return iterations_per_msec > 1000;
}
