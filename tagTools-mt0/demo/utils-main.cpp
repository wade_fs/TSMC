#include <iostream>
#include <string>
#include "utils.h"

using namespace std;

int main(int argc, char* argv[])
{
  string s = "I am wade.\xE0\xE1\xF0\xFF";
  string s2 = utils::toHex(s, false);

  cout << s << endl;
  cout << utils::toHex(s) << endl;
  cout << utils::fromHex(s2, false) << endl;

  return 0;
}
