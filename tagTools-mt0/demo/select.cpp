#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/select.h>
#include <unistd.h>

int main(void)
{
  fd_set read_fds;
  struct timeval timeout;
  int rv;
  char buff[100];
  int len = 100;
  char msg[] = "請按鍵並送出 Enter\r\n";

  FD_ZERO(&read_fds);
  FD_SET(0, &read_fds); /* add our file descriptor to the set */

  timeout.tv_sec = 3;
  timeout.tv_usec = 0;

  write(0, msg, strlen(msg));
  select(0+1, &read_fds, NULL, NULL, &timeout);
  rv = FD_ISSET(0, &read_fds);
  if(rv == -1)
    perror("select"); /* an error accured */
  else if(rv == 0)
    printf("timeout\n"); /* a timeout occured */
  else {
    rv = read(0, buff, len ); /* there was data to read */
    printf ("read : %s\n", buff);
  }
  printf ("done %d\n", rv);
  return rv;
}
