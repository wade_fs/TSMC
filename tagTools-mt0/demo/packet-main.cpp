#include <stdlib.h>
#include <iostream>
#include "packet.h"
using namespace std;

int main(int argc, char* argv[])
{
  if (argc == 1) {
    cout << "Usage: " << argv[0] << " #\r\n\twhere # is packet id\r\n";
    exit(0);
  }
  int id = atoi(argv[1]);
  packet p = packet(id);

  cout << "id=" << id << "/" << p.getId() << ", dir=" << p.dir() << ": '" << p.getPinAscii() << "' : '" << p.getPinHex(' ') << "'\r\n";

  return 0;
}

