// future::wait_for
#include <iostream>       // std::cout
#include <future>         // std::async, std::future
#include <chrono>         // std::chrono::milliseconds
#include <math.h>
using namespace std;
// a non-optimized way of checking for prime numbers:
// long long n = 700020007LL; 
long long n = 15484279L; 
// long long n = 32416190071LL;
// long long n = 160479503LL;
// long long n = 2038072819LL;
bool is_prime (long long x) {
  // 這邊是故意寫的很沒效率.....不然可以用 sqrt()
  for (long long i=2; i<x; ++i) if (x%i==0) return false;
  return true;
}

int main ()
{
  // call function asynchronously:
  std::future<bool> fut = std::async (is_prime,n); 

  // do something while waiting for function to set future:
  std::cout << "checking, please wait" << endl;
  std::chrono::milliseconds span (1000);
  while (fut.wait_for(span)==std::future_status::timeout)
    cout << '.' << flush;

  bool x = fut.get();

  std::cout << "\n" << n << " " << (x?"is":"is not") << " prime.\n";

  return 0;
}
