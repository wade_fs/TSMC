#include <iostream>
#include <string>
#include <cstdlib>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <signal.h>
#include <dlfcn.h>
#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>

#include "rs232ioctls.h"
#include "cmd.h"
#include "utils.h"

int main(int argc, char** argv)
{   
  int speed;
  int ret = 0;
  int cfg_timeout = 1000000; // 0 for unlimited, unit is us (1.0e-6 s)

  struct timeval timeout;
  cmd c = cmd();
  packet p;
  int ch;
  std::string res="";

  if (argc != 3) {
    std::cerr << "Usage: " << argv[0] << " PORT SPEED\n\tPORT: /dev/ttyUSB0, SPEED: 9600\n";
    exit(1);
  }

  if ((tty_fd = term_open(argv[1])) < 0) {
    std::cerr << "Error opening: " << argv[1] << std::endl;
    return (255);
  }

  term_start(tty_fd);
  rs232_setspeed(tty_fd, 9600);

  /* 讓 stdin + Com Port 設為 0, block till input */
  FD_ZERO(&readfs);
  FD_SET(tty_fd, &readfs);
  timeout.tv_sec=cfg_timeout/1000000;
  timeout.tv_usec = cfg_timeout - timeout.tv_sec * 1000000;
  select(tty_fd+1, &readfs, NULL, NULL, cfg_timeout == 0 ? NULL : &timeout); // 如果是標準輸入/輸出，仍然需要等 Enter

  std::cout << "Reading...." << std::endl;

  res = c.readCmd(tty_fd);
  std::cout << "Read :\t" << utils::toHex(res) << std::endl;

  usleep(25000);
  //////////////////////////////////

  std::string write_string="";
  write_string.reserve(256);
  for (int c=255; c>=0; c--) write_string += std::string(1, static_cast<char>(c));

  std::cout << "Writing...." << std::endl;
  ret = write(tty_fd, write_string.data(), write_string.size());
  sync();
  std::cout << "Write:\t" << utils::toHex(write_string) << std::endl;
}
