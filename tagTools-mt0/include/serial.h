
#ifndef __SERIAL__H__
#define __SERIAL__H__

#include "sersync.h"
#include "rs232ioctls.h"

#define NOT_IMPLEMENTED -1

enum FLOW_TYPE {
  FLOW_NONE = 0,
  FLOW_SSLEEP = 1,
  FLOW_NANO = 2,
  FLOW_ECHO = 3,
  FLOW_HW = 4,
  FLOW_XON = 5
};

/* This class represents the logic-glue of the serial port.
   The control relays on Watcher, and the ioctls on low level routines.
*/
class serial_port {
  watcher *W;                   // watcher class.
  int realtty;                  // tty's for rs232ioctls.
  int rfd;
  int wfd;

  FLOW_TYPE flow_type;          // Flow Control Type.

  // Variable to determine whether or not we can read from port.
  pthread_cond_t condition_var;
  // Port global lock.
  pthread_mutex_t lock;
  int owner;                    // Indicates that the port is currently "owned" by a thread.
  //  int dataavailable; replaced by a call to watcher hasdataavailable.
public:
  int whendatabecomesavailable(); // callback to be used from child classes.
  bool set_flow(FLOW_TYPE);     // Set flow type.
  int acquire();
  int release();
  int acquirewithdata();

  int send(char *cad);
  int send(char *cad, unsigned long bytes);
  int gets(char *cad, unsigned int lim);
  int getstimeout(char *cad, unsigned int cmax);  // up to 1 second
  int flush_input();
  int flush_output();
  int terminate();

   serial_port(char *);
   serial_port(int r, int w);
  bool setspeed(int spd);
  ~serial_port();
  bool isopened();
};
#endif /* __SERIAL__H__ */
