#ifndef __DOCMD_H__
#define __DOCMD_H__

#include <string>
#include <vector>
#include "packet.h"

using namespace std;

/////////// 宣告區 ///////////////
enum CMD_ID {
  CMD_DISPLAY_TIME,
  CMD_READ_TIME,
  CMD_SET_TIME,
  CMD_NORMAL_STATUS_READ,
  CMD_EXTENDED_STATUS1_READ,
  CMD_EXTENDED_STATUS2_READ,
  CMD_PROTECTED_DATA_READ,
  CMD_WRITE_LCD_DATA,
  CMD_READ_LCD_DATA,
  CMD_LCD_DISPLAY_ON,
  CMD_LCD_DISPLAY_OFF,
  CMD_SET_LCD_TIMEOUT,
  CMD_CLEAR_LCD,
  CMD_ENABLE_TIMEOUT,
  CMD_DISABLE_TIMEOUT,
  CMD_DISPLAY_SERIAL_NUMBER,
  CMD_GET_TAG_FORMAT,
  CMD_FORMAT_TAG,
  CMD_READ_TAG,
  CMD_WRITE_TAG
};

/////////// 函式區 ///////////
// 應該先來想想什麼叫 packet, cmd, flow,
// 每個 packet 都包括 to/from, 但是並不是完整的命令
// 一個命令是像 LCD_ON 這樣，它其實是透過流程，將一系列的 packet + timeout 串起來
//   但是一個命令是有流程的，每個流程，包含失敗處理，會嘗試，會逾時
// 所謂的流程，就是一堆封包，在逾時前若成功，就進下一個命令，若逾時，則重覆
//   可以先假設每個命令都有重複次數
//   至於逾時值，我的感覺是所有命令都一樣的原理，有遞增性
class cmd {
private:
  int id;
public:
  cmd();
  cmd(int ID);
  int doCmd(int tty_fd);
////////////// 底下應該是測試才會用到 /////////////////////
  vector<int> getPacketIDs();
  vector<string> getPacketsString();
  vector<packet> getPackets();
  string readCmd(int tty_fd);
};

#endif
