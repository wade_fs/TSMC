#pragma version(1)
#pragma rs java_package_name(com.wade.wafercv)
#pragma rs_fp_relaxed
#include "rs_debug.rsh"

// 原圖(u8_4) -. blur(u8,u8) -. histEq(u8,u8) -. sobel(u8, float2) -. canny -. hysteresis
rs_allocation allocationTarget;     // 晶圓盤的「目標」照片
rs_allocation allocationSrc;        // 目前的照片
rs_allocation allocationOrg;        // 穩態的照片
rs_allocation allocationSobel;      // sobel(u8, float2)
rs_allocation allocationCanny;      // canny(float2, float2)
rs_allocation allocationHysteresis; // hysteresis(float2, rgba)
rs_allocation allocationOut;

uint32_t width;
uint32_t height;
int32_t histo[256];
float remapArray[256];
int size;
// Canny
uchar UPPER=35;
uchar LOWER=10;
const int MESSAGE_OK = 1;
// Hysteresis
float gradientThreshold;

// 用來判定穩態
typedef struct Variance {
  int32_t count;
} Variance_T;

Variance_T variance;

void init() {
    for (int i = 0; i < 256; i++) {
        histo[i] = 0;
        remapArray[i] = 0.0f;
    }
}

const static float3 gMonoMult = {0.299f, 0.587f, 0.114f};
// 目前同時設到 target 中
void RS_KERNEL calcSum(const uchar4 in, uint32_t x, uint32_t y) {
    uchar4 src = rsGetElementAt_uchar4(allocationSrc, x, y);
    uchar4 target = rsGetElementAt_uchar4(allocationTarget, x, y);
    if (src.g != target.g) {
        ++variance.count;
        src.g = src.b = 0;
    } else src.r = src.g = src.b = 255;
    rsSetElementAt_uchar4(allocationSrc, src, x, y);
}
void resetVariance() {
  variance.count = 0;
}
void getCount() {
    rsSendToClient(MESSAGE_OK, (void*)&variance, sizeof(variance));
}

uchar RS_KERNEL histEq(const uchar in, uint32_t x, uint32_t y) {
    rsAtomicInc(&histo[in]);
    return in;
}
void reset() {
  init();
}

// 要先算完 histo[] 並且呼叫過 remap, 才能讓 HistEq 使用調整對應
void createRemapArray() {
    //create map for y
    float sum = 0;
    for (int i = 0; i < 256; i++) {
        sum += histo[i];
        remapArray[i] = sum / (size);
    }
}
uchar RS_KERNEL remap(const uchar in, uint32_t x, uint32_t y) {
    float3 res = { remapArray[in], remapArray[in], remapArray[in] };
    uchar4 rgba = rsPackColorTo8888(res);
    return rgba.r;
}
uchar4 RS_KERNEL remap4(const uchar in, uint32_t x, uint32_t y) {
    float3 res = { remapArray[in], remapArray[in], remapArray[in] };
    uchar4 rgba = rsPackColorTo8888(res);
    return rgba;
}

float2 RS_KERNEL sobel(uint32_t x,uint32_t y){
  if(x>0 && y>0 && x<width-1 &&y<height-1){
    //get all values around pixel
    uchar topleft=     rsGetElementAt_uchar(allocationSobel, x-1,y-1);
    uchar left=        rsGetElementAt_uchar(allocationSobel, x-1,y);
    uchar bottomleft=  rsGetElementAt_uchar(allocationSobel, x-1,y+1);
    uchar top=         rsGetElementAt_uchar(allocationSobel, x,y-1);
    uchar bottom=      rsGetElementAt_uchar(allocationSobel, x,y+1);
    uchar topright=    rsGetElementAt_uchar(allocationSobel, x+1,y-1);
    uchar right=       rsGetElementAt_uchar(allocationSobel, x+1,y);
    uchar bottomright= rsGetElementAt_uchar(allocationSobel, x+1,y+1);
    //get x kernel value
    float xValue=(-topleft-2*left-bottomleft+topright+2*right+bottomright)/2;
    //get y kernel value
    float yValue=(-topleft-2*top-topright+bottomleft+2*bottom+bottomright)/2;
    return (float2){half_sqrt(xValue*xValue+yValue*yValue),(float)atan2pi((float)yValue, (float)xValue)};
  }else{
    return (float2){0,0};
  }
}

float2 RS_KERNEL canny(uint32_t x, uint32_t y){
    if(x>0 && y>0 && x<width-1 &&y<height-1){
                float2 input = rsGetElementAt_float2(allocationCanny, x, y);
                float d=input.y;
                uchar direction = (uchar)(round(d * 4.0f) + 4) % 4;
                uchar i=(uchar)input.x;
                uchar output;
        if (direction == 0) {
            // horizontal, check left and right
            float2 a = rsGetElementAt_float2(allocationCanny, x - 1, y);
            float2 b = rsGetElementAt_float2(allocationCanny, x + 1, y);
            output=(uchar)a.x < i && (uchar)b.x < i ? i : 0;
        } else if (direction == 2) {
            // vertical, check above and below
            float2 a = rsGetElementAt_float2(allocationCanny, x, y - 1);
            float2 b = rsGetElementAt_float2(allocationCanny, x, y + 1);
            output=(uchar)a.x < i && (uchar)b.x < i ? i : 0;
        } else if (direction == 1) {
            // NW-SE
            float2 a = rsGetElementAt_float2(allocationCanny, x - 1, y - 1);
            float2 b = rsGetElementAt_float2(allocationCanny, x + 1, y + 1);
            output=(uchar)a.x < i && (uchar)b.x < i ? i : 0;
        } else{
            // NE-SW
            float2 a = rsGetElementAt_float2(allocationCanny, x + 1, y - 1);
            float2 b = rsGetElementAt_float2(allocationCanny, x - 1, y + 1);
            output=(uchar)a.x < i && (uchar)b.x < i ? i : 0;
        }
        if(output>UPPER){
            return (float2){255,d};
        }else if(output<LOWER){
            return (float2){0,0};
        }else{
            return (float2){255,d};
        }
    }else{
        return (float2){0,0};
    }
}

uchar4 RS_KERNEL hysteresis(uint32_t x, uint32_t y){
  if(x>1 && y>1 && x<width-2 &&y<height-2){
    float2 input = rsGetElementAt_float2(allocationHysteresis, x, y);
    if(input.x == 255){
      if(input.y != 0){
        if(fabs(rsGetElementAt_float2(allocationHysteresis, x - 1, y - 1).y-input.y)       < gradientThreshold){
          return (uchar4){255,255,255,255};
        }else if(fabs(rsGetElementAt_float2(allocationHysteresis, x, y - 1).y-input.y)     < gradientThreshold ){
          return (uchar4){255,255,255,255};
        }else if(fabs(rsGetElementAt_float2(allocationHysteresis, x + 1, y - 1).y-input.y) < gradientThreshold ){
          return (uchar4){255,255,255,255};
        }else if(fabs(rsGetElementAt_float2(allocationHysteresis, x - 1, y).y-input.y)     < gradientThreshold ){
          return (uchar4){255,255,255,255};
        }else if(fabs(rsGetElementAt_float2(allocationHysteresis, x + 1, y).y-input.y)     < gradientThreshold ){
          return (uchar4){255,255,255,255};
        }else if(fabs(rsGetElementAt_float2(allocationHysteresis, x - 1, y + 1).y-input.y) < gradientThreshold){
          return (uchar4){255,255,255,255};
        }else if(fabs(rsGetElementAt_float2(allocationHysteresis, x, y + 1).y-input.y)     < gradientThreshold){
          return (uchar4){255,255,255,255};
        }else if(fabs(rsGetElementAt_float2(allocationHysteresis, x + 1, y + 1).y-input.y) < gradientThreshold){
          return (uchar4){255,255,255,255};
        }else{
          return (uchar4){0,0,0,255};
        }
      }else{
        return (uchar4){0,0,0,255};
      }
    }else if(input.x==0){
      return (uchar4){255,255,255,255};
    }else if(input.x==1){
      float2 a = rsGetElementAt_float2(allocationHysteresis, x - 1, y - 1);
      float2 b= rsGetElementAt_float2(allocationHysteresis, x, y - 1);
      float2 c= rsGetElementAt_float2(allocationHysteresis, x + 1, y - 1);
      float2 d= rsGetElementAt_float2(allocationHysteresis, x - 1, y);
      float2 e= rsGetElementAt_float2(allocationHysteresis, x + 1, y);
      float2 f= rsGetElementAt_float2(allocationHysteresis, x - 1, y + 1);
      float2 g= rsGetElementAt_float2(allocationHysteresis, x, y + 1);
      float2 h= rsGetElementAt_float2(allocationHysteresis, x + 1, y + 1);
      if(a.x+b.x+c.x+d.x+e.x+f.x+g.x+h.x>20){
        return (uchar4){0,0,0,255};
      }else{
        float2 a= rsGetElementAt_float2(allocationHysteresis, x - 2, y - 2);
        float2 b= rsGetElementAt_float2(allocationHysteresis, x - 1, y - 2);
        float2 c= rsGetElementAt_float2(allocationHysteresis, x, y - 2);
        float2 d= rsGetElementAt_float2(allocationHysteresis, x + 1, y - 2);
        float2 e= rsGetElementAt_float2(allocationHysteresis, x + 2, y - 2);
        float2 f= rsGetElementAt_float2(allocationHysteresis, x - 2, y - 1);
        float2 g= rsGetElementAt_float2(allocationHysteresis, x + 2, y - 1);
        float2 h= rsGetElementAt_float2(allocationHysteresis, x - 2, y);
        float2 i= rsGetElementAt_float2(allocationHysteresis, x + 2, y);
        float2 j= rsGetElementAt_float2(allocationHysteresis, x - 2, y + 1);
        float2 k= rsGetElementAt_float2(allocationHysteresis, x + 2, y + 1);
        float2 l= rsGetElementAt_float2(allocationHysteresis, x - 2, y + 2);
        float2 m= rsGetElementAt_float2(allocationHysteresis, x - 1, y + 2);
        float2 n= rsGetElementAt_float2(allocationHysteresis, x, y + 2);
        float2 o= rsGetElementAt_float2(allocationHysteresis, x + 1, y + 2);
        float2 p= rsGetElementAt_float2(allocationHysteresis, x + 2, y + 2);
        if(a.x+b.x+c.x+d.x+e.x+f.x+g.x+h.x+i.x+j.x+k.x+l.x+m.x+n.x+o.x+p.x>50){
          return (uchar4){0,0,0,255};
        }else{
          return (uchar4){255,255,255,255};
        }
      }
    }else{
      return (uchar4){255,255,255,255};
    }
  }else{
    return (uchar4){255,255,255,255};
  }
}
