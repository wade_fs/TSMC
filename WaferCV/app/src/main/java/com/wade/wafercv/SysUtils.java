package com.wade.wafercv;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.preference.PreferenceManager;
import android.widget.Toast;

import java.util.prefs.Preferences;

/**
 * Created by wade on 2018/3/27.
 */

public class SysUtils {
    Context context;
    double allowDays = 7;
    String version = "";

    SysUtils(Context context) {
        this.context = context;
    }
    public String getVersion() {
        PackageManager packageManager = context.getPackageManager();
        PackageInfo pInfo = null;
        try {
            pInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            version = "";
        }
        return version;
    }

    public void setAllowDays(double days) {
        allowDays = days;
    }

    public boolean isAllowable() {
        try {
            PackageManager packageManager = context.getPackageManager();
            PackageInfo pInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
            long installedTime = pInfo.lastUpdateTime; // firstInstallTime
            long now = System.currentTimeMillis();
            long WpD=8, MpD=30, YpD=365, DpH = 24, HpM = 60, MpS = 60, Spm = 1000;
            long allowedTime = (long)(allowDays * DpH * HpM * MpS * Spm);
            if ((now - installedTime) > allowedTime) {
                return false;
            }
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
        return true;
    }

    public void showMsgBox(String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }

    /*
        sysUtils.setPreference("MACHINE", "IMOBILE");
        sysUtils.showMsgBox(sysUtils.getPreference("MACHINE"));
     */
    public void setPreference(String Key, String Val) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        if (settings == null) {
            showMsgBox("No preference(IMOBILE)");
        } else {
            SharedPreferences.Editor editor = settings.edit();
            editor.putString(Key, Val);
            editor.commit();
        }
    }

    public String getPreference(String Key) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        if (settings == null) {
            showMsgBox("No preference(IMOBILE)");
            return "";
        } else {
            return settings.getString(Key, "XXXXX");
        }
    }
}
