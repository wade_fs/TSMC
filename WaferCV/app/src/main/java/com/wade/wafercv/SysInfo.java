package com.wade.wafercv;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;

/**
 * Created by wade on 2018/3/23.
 */

public class SysInfo {
    private Build build;
    private Context context;
    SysInfo(Context context) { this.context = context; this.build = new Build(); }
    public String get() {
        return Build.VERSION.SECURITY_PATCH+"|"+Build.VERSION.INCREMENTAL+"|"+Build.USER+"|"+Build.SERIAL+"|"+Build.DISPLAY;
    }
    public String getDate() { return Build.VERSION.SECURITY_PATCH; }
    public String getUser() { return Build.USER; }
    public String getVer() { return Build.VERSION.SECURITY_PATCH+"|"+Build.VERSION.INCREMENTAL; }
    public String getOS() { return Build.VERSION.BASE_OS; }
    public int getSDK() { return Build.VERSION.SDK_INT; }
    public String getInstalledDate() {
        PackageManager packageManager = context.getPackageManager();
        try {
            PackageInfo packageInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
            long installedTime = packageInfo.lastUpdateTime; // firstInstallTime
            long now = System.currentTimeMillis();
            long WpD=8, MpD=30, YpD=365, DpH = 24, HpM = 60, MpS = 60, Spm = 1000;
            long allowedTime = 7 * DpH * HpM * MpS * Spm;
            return "";
        } catch (PackageManager.NameNotFoundException e) {
            return "";
        }
    }
}
