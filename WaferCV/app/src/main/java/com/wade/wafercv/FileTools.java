package com.wade.wafercv;


import android.graphics.Bitmap;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by wade on 2018/2/22.
 */

public class FileTools {
    private String fileName;
    public FileTools(String fn) {
        fileName = fn;
    }
    public FileTools() {
        fileName = "";
    }
    public void setFileName(String fn) {
        fileName = fn;
    }
    public String loadLn() {
        File file = new File(fileName);
        if (!file.exists()) return "";
        try {
            FileReader f = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(f);
            String text = "";
            String buf="";
            while ((buf = bufferedReader.readLine()) != null) {
                text += buf+"\n";
            }
            bufferedReader.close();
            f.close();
            return text;
        } catch (IOException e) {
            Log.d("MyLog", "'"+fileName + "' 讀檔錯誤: "+e.getMessage());
        }
        return "";
    }

    public void delete() {
        File file = new File(fileName);
        file.delete();
    }

    private void write(String buf, boolean append) {
        try {
            File file = new File(fileName);
            FileWriter f  = new FileWriter(file, append);
            BufferedWriter bufferedWriter = new BufferedWriter(f);

            bufferedWriter.write(buf);
            bufferedWriter.flush();
            bufferedWriter.close();
            f.close();
        } catch (IOException e) {
            Log.d("MyLog", "'"+fileName + "' 寫檔錯誤: "+e.getMessage());
        }
    }

    public void save(String buf) {
        write(buf, false);
    }
    public void saveBitmap(Bitmap bitmap) {
        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            Log.d("MyLog", "Cannot write to external storage");
            return;
        }
        File file = new File (fileName);
        file.mkdirs();
        if (file.exists ()) file.delete ();
        try {
            FileOutputStream out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            Log.d("MyLog", "Error: Save to "+fileName);
        }
    }
    public void append(String buf) {
        write(buf, true);
        Log.d("MyLog", "append: '"+buf.trim()+"'");
    }
    public void empty() {
        write("", false);
    }
    public String read() {
        return read(0, 1024);
    }
    public String read(int offset, int len) {
        FileInputStream inputStream = null;
        BufferedReader bufferedReader;
        String res = "";
        try {
            inputStream = new FileInputStream(fileName);
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            char[] buf = new char[len];
            int n;
            if ((n = bufferedReader.read(buf, offset, len)) > 0) {
                res = new String(buf, 0, n);
            }
            bufferedReader.close();
            inputStream.close();
        } catch (FileNotFoundException e) {

        } catch (IOException e) {

        } finally {
            return res;
        }
    }
    public boolean fileExists() {
        File file = new File(fileName);
        return file.exists();
    }
}
