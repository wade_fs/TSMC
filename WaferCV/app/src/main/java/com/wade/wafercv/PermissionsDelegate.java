package com.wade.wafercv;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;

class PermissionsDelegate {

    private static final int REQUEST_CODE = 10;
    private static final int REQUEST_CODE2 = 20;
    private final MainActivity activity;

    PermissionsDelegate(MainActivity activity) { this.activity = activity; }

    boolean hasFilePermission() {
        int permissionCheckResult = ContextCompat.checkSelfPermission( activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return permissionCheckResult == PackageManager.PERMISSION_GRANTED;
    }
    void requestFilePermission() {
        ActivityCompat.requestPermissions( activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE2);
    }


    boolean hasCameraPermission() {
        int permissionCheckResult = ContextCompat.checkSelfPermission( activity, Manifest.permission.CAMERA);
        return permissionCheckResult == PackageManager.PERMISSION_GRANTED;
    }

    void requestCameraPermission() {
        ActivityCompat.requestPermissions( activity, new String[]{Manifest.permission.CAMERA}, REQUEST_CODE);
    }

    boolean resultGranted(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode != REQUEST_CODE && requestCode != REQUEST_CODE2) { return false; }
        if (grantResults.length < 1) { return false; }
        if (!(permissions[0].equals(Manifest.permission.CAMERA)) &&
            !(permissions[0].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE))) { return false; }
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            activity.showMsg("");
            return true;
        }

        requestCameraPermission();
        activity.showMsg("No Camera or File permission granted.\nPlease checking the Setup > App > Permission");
        return false;
    }
}
