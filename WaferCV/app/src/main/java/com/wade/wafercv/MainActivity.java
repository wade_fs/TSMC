package com.wade.wafercv;
import android.graphics.SurfaceTexture;
import android.os.Bundle;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.renderscript.Type;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;
import android.support.design.widget.CoordinatorLayout;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Locale;

import io.fotoapparat.Fotoapparat;
import io.fotoapparat.configuration.UpdateConfiguration;
import io.fotoapparat.error.CameraErrorListener;
import io.fotoapparat.exception.camera.CameraException;
import io.fotoapparat.parameter.ScaleType;
import io.fotoapparat.preview.Frame;
import io.fotoapparat.preview.FrameProcessor;
import io.fotoapparat.view.CameraView;

import static io.fotoapparat.log.LoggersKt.fileLogger;
import static io.fotoapparat.log.LoggersKt.logcat;
import static io.fotoapparat.log.LoggersKt.loggers;
import static io.fotoapparat.selector.FlashSelectorsKt.autoFlash;
import static io.fotoapparat.selector.FlashSelectorsKt.off;
import static io.fotoapparat.selector.FlashSelectorsKt.torch;
import static io.fotoapparat.selector.FocusModeSelectorsKt.autoFocus;
import static io.fotoapparat.selector.FocusModeSelectorsKt.fixed;
import static io.fotoapparat.selector.LensPositionSelectorsKt.back;
import static io.fotoapparat.selector.LensPositionSelectorsKt.front;
import static io.fotoapparat.selector.SelectorsKt.firstAvailable;
import static io.fotoapparat.selector.SensorSensitivitySelectorsKt.highestSensorSensitivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, TextureView.SurfaceTextureListener {

    private final PermissionsDelegate permissionsDelegate = new PermissionsDelegate(this);
    private boolean hasCameraPermission;
    private CameraView cameraView;
    private Button button_empty, button_bottom, button_top, button_full;
    private SurfaceTexture surfaceTexture;

    private Fotoapparat fotoapparat;

    private RenderScript mRS;
    private ScriptIntrinsicBlur si_blur;
    ScriptC_edge sc_edge;

    private Allocation allocationOut;
    private Allocation allocationRgbaTarget;
    private Allocation allocationRgbaTop;
    private Allocation allocationRgbaBottom;
    private Allocation allocationRgbaFull;
    private Allocation allocationRgbaSrc;
    private Allocation allocationU8;
    private Allocation allocationU8B;
    private Allocation allocationF32_2;
    private Allocation allocationF32_2B;

    DisplayMetrics displayMetrics;
    Variance variance;

    GuiHandler guiHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().clearFlags(
                WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
        SysUtils sysUtils = new SysUtils(this);
        sysUtils.setAllowDays(7);
        if (!sysUtils.isAllowable()) {
            sysUtils.showMsgBox("本應用已過期，如需要再次執行，可以重新安裝，或是洽研發部門取得正式版本");
            finishAffinity();
            return;
        }
/*      將來需要透過 Preference | File 來禁止用戶亂安裝
        FileTools fileTools = new FileTools(Environment.getExternalStorageDirectory().getAbsolutePath()+"/WaferCV.conf");
        if (fileTools.fileExists()) {
            sysUtils.showMsgBox("Old "+fileTools.loadLn());
        } else {
            fileTools.save("IMOBILE\n");
            sysUtils.showMsgBox("New "+fileTools.loadLn());
        }
*/

        setContentView(R.layout.activity_main);

        cameraView = findViewById(R.id.camera_view);
        button_empty = findViewById(R.id.button_empty);
        button_bottom = findViewById(R.id.button_bottom);
        button_top = findViewById(R.id.button_top);
        button_full = findViewById(R.id.button_full);

        hasCameraPermission = permissionsDelegate.hasCameraPermission();

        if (!hasCameraPermission) permissionsDelegate.requestCameraPermission();
        if (!permissionsDelegate.hasFilePermission()) permissionsDelegate.requestFilePermission();

        cameraView.setVisibility(View.VISIBLE);
        fotoapparat = createFotoapparat();

        takePictureOnClick();
        toggleTorchOnSwitch();

        mRS = RenderScript.create(this);

        TextureView textureView = findViewById(R.id.overlay);
        textureView.setSurfaceTextureListener(this);
        textureView.setOnClickListener(this);
        cameraView.setOnClickListener(this);

        cameraView.setVisibility(View.VISIBLE);
        displayMetrics = getResources().getDisplayMetrics();
        CoordinatorLayout.LayoutParams layoutParams = new CoordinatorLayout.LayoutParams((displayMetrics.widthPixels>>1), (displayMetrics.heightPixels>>1));
        layoutParams.gravity = Gravity.TOP | Gravity.END;
        cameraView.setLayoutParams(layoutParams);

        guiHandler = new GuiHandler(this);
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.overlay:
                if (cameraView.getVisibility() == View.VISIBLE) cameraView.setVisibility(View.INVISIBLE);
                else cameraView.setVisibility(View.VISIBLE);
                break;
            case R.id.camera_view:
                ViewGroup.LayoutParams layoutParams =  cameraView.getLayoutParams();
                if (displayMetrics.widthPixels != layoutParams.width) {
                    layoutParams = new CoordinatorLayout.LayoutParams(displayMetrics.widthPixels, displayMetrics.heightPixels);
                    cameraView.setLayoutParams(layoutParams);
                } else {
                    CoordinatorLayout.LayoutParams clParams = new CoordinatorLayout.LayoutParams((displayMetrics.widthPixels>>1), (displayMetrics.heightPixels>>1));
                    clParams.gravity = Gravity.TOP | Gravity.END;
                    cameraView.setLayoutParams(clParams);
                }
                break;
        }
    }
    private void takePictureOnClick() {
        button_empty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isTrained = true;
                afterTrained = false;
                guiHandler.setStatusTraining(GuiHandler.TARGET_ALLOCATION_EMPTY, 10000);
            }
        });
        button_bottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isTrained = true;
                afterTrained = false;
                guiHandler.setStatusTraining(GuiHandler.TARGET_ALLOCATION_BOTTOM, 10000);
            }
        });
        button_top.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isTrained = true;
                afterTrained = false;
                guiHandler.setStatusTraining(GuiHandler.TARGET_ALLOCATION_TOP, 10000);
            }
        });
        button_full.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isTrained = true;
                afterTrained = false;
                guiHandler.setStatusTraining(GuiHandler.TARGET_ALLOCATION_FULL, 10000);
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (permissionsDelegate.resultGranted(requestCode, permissions, grantResults)) {
            hasCameraPermission = true;
            fotoapparat.start();
            cameraView.setVisibility(View.VISIBLE);
        }
    }

    // 因為 Surface size 與 Camera 不一定一樣，我們就不在此設置
    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int width, int height) {
        this.surfaceTexture = surfaceTexture;
        Log.d("MyLog", "Surface size "+width + "x"+ height);
    }
    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i2) {
    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
    }

    private Fotoapparat createFotoapparat() {
        return Fotoapparat
                .with(this)
                .into(cameraView)
//                .photoResolution(standardRatio(lowestResolution()))
                .previewScaleType(ScaleType.CenterCrop)
                .focusMode(firstAvailable(
//                        continuousFocusPicture(),
                        fixed(),
                        autoFocus()
                ))
                .flash(firstAvailable(
                        off(),
                        torch(),
                        autoFlash()
                ))
//                .previewFpsRange(lowestFixedFps())
                .sensorSensitivity(highestSensorSensitivity())
                .lensPosition(firstAvailable(back(), front()))
                .frameProcessor(new SampleFrameProcessor())
                .logger(loggers(
                        logcat(),
                        fileLogger(this)
                ))
                .cameraErrorCallback(new CameraErrorListener() {
                    @Override
                    public void onError(@NotNull CameraException e) {
                        Toast.makeText(MainActivity.this, e.toString(), Toast.LENGTH_LONG).show();
                    }
                })
                .build();
    }

    private void toggleTorchOnSwitch() {
        SwitchCompat torchSwitch = findViewById(R.id.torchSwitch);

        torchSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                fotoapparat.updateConfiguration(
                        UpdateConfiguration.builder()
                                .flash(
                                        isChecked ? torch() : off()
                                )
                                .build()
                );
            }
        });
    }

/*
    public String takePicture(final String filename) {
        PhotoResult photoResult = fotoapparat.takePicture();
        // String filepath = getExternalFilesDir("photos").getAbsolutePath();
        String filepath = Environment.getExternalStorageDirectory().getAbsolutePath()+"/"+Environment.DIRECTORY_PICTURES+"/wafercv";

        File filePath = new File(filepath);
        filePath.mkdirs();
        if (null == filename || filename.isEmpty()) filepath += "photo.jpg";
        else filepath += "/"+filename+".jpg";
        photoResult.saveToFile(new File(filepath));

        photoResult
                .toBitmap(scaled(0.25f))
                .whenDone(new WhenDoneListener<BitmapPhoto>() {
                    @Override
                    public void whenDone(@Nullable BitmapPhoto bitmapPhoto) {
                        if (bitmapPhoto == null) {
                            Log.e(LOGGING_TAG, "Couldn't capture photo.");
                            return;
                        }
                        ImageView imageView = findViewById(R.id.result);

                        imageView.setImageBitmap(bitmapPhoto.bitmap);
                        imageView.setRotation(-bitmapPhoto.rotationDegrees);
                        imageView.setVisibility(View.VISIBLE);

                        guiHandler.setStatusHidePicView(3000);
                    }
                });
        return filepath;
    }
*/

    @Override
    protected void onStart() {
        super.onStart();
        if (hasCameraPermission) {
            fotoapparat.start();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (hasCameraPermission) {
            fotoapparat.stop();
        }
    }

    private Fps fps = new Fps();
    int width=0, height=0;
    float onePerSize = 0.0f;
    float lastRatio = 0.0f;

    private void reset(int w, int h) {
        width = w;
        height = h;
        onePerSize = 1.0f/(w*h);
        lastRatio = 0.0f;
        Type u8    = new Type.Builder(mRS, Element.U8(mRS)).setX(width).setY(height).create();
        Type u8_4  = new Type.Builder(mRS, Element.U8_4(mRS)).setX(width).setY(height).create();
        Type f32_2 = new Type.Builder(mRS, Element.F32_2(mRS)).setX(width).setY(height).create();

        allocationOut = Allocation.createTyped(mRS, u8_4, Allocation.USAGE_SCRIPT | Allocation.USAGE_IO_OUTPUT);
        allocationRgbaTarget = Allocation.createTyped(mRS, u8_4, Allocation.USAGE_SCRIPT);
        allocationRgbaBottom = Allocation.createTyped(mRS, u8_4, Allocation.USAGE_SCRIPT);
        allocationRgbaTop = Allocation.createTyped(mRS, u8_4, Allocation.USAGE_SCRIPT);
        allocationRgbaFull = Allocation.createTyped(mRS, u8_4, Allocation.USAGE_SCRIPT);
        allocationRgbaSrc = Allocation.createTyped(mRS, u8_4, Allocation.USAGE_SCRIPT);

        allocationU8 = Allocation.createTyped(mRS, u8, Allocation.USAGE_SCRIPT);
        allocationU8B = Allocation.createTyped(mRS, u8, Allocation.USAGE_SCRIPT);

        allocationF32_2 = Allocation.createTyped(mRS, f32_2, Allocation.USAGE_SCRIPT);
        allocationF32_2B = Allocation.createTyped(mRS, f32_2, Allocation.USAGE_SCRIPT);
        allocationOut.setSurface(new Surface(surfaceTexture));

        si_blur         = ScriptIntrinsicBlur.create(mRS, Element.U8(mRS));
        si_blur.setRadius(3);

        sc_edge = new ScriptC_edge(mRS);
        sc_edge.set_height(height);
        sc_edge.set_width(width);
        sc_edge.set_size(width*height);
        sc_edge.set_UPPER((short)100);
        sc_edge.set_LOWER((short)20);
        sc_edge.set_gradientThreshold(0.f);
        sc_edge.set_allocationSrc(allocationRgbaSrc); // 需要一直設定嗎？

        variance = new Variance(sc_edge);
        mRS.setMessageHandler(new RenderScript.RSMessageHandler() {
            @Override
            public void run() {
                switch(mID) {
                    case ScriptC_edge.const_MESSAGE_OK:
                        variance.set(mData);
                        if (guiHandler.getStatus() == GuiHandler.STATE_COMPLETED) {
                            Log.d("MyLog", "COMPLETED 2");

                            lastRatio = variance.count * onePerSize;
                        }
                        break;
                }
            }
        });
    }

    boolean isTrained = false;
    boolean afterTrained = false;
    private class SampleFrameProcessor implements FrameProcessor {
        @Override
        public void process(@NotNull Frame frame) {
            if (null == surfaceTexture) return;
            if (width == 0 || height == 0) reset(frame.getSize().width, frame.getSize().height);

            // 第一步，先變黑白，順便變成 u8
            allocationU8.copy1DRangeFrom(0, width * height, frame.getImage());
            sc_edge.invoke_reset(); // 每次 histeq 都必須重設統計表

            // 第二步，先 Blur, 同時先 HistEq 統計
            si_blur.setInput(allocationU8);
            si_blur.forEach(allocationU8B);
            // 再來正式 HistEq 映照表查詢
            sc_edge.forEach_histEq(allocationU8B, allocationU8);
            sc_edge.invoke_createRemapArray();
            sc_edge.forEach_remap(allocationU8, allocationU8B);

            // 第三步，sobel
            sc_edge.set_allocationSobel(allocationU8B);
            sc_edge.forEach_sobel(allocationF32_2);

            // 第四步，Canny
            sc_edge.set_allocationCanny(allocationF32_2);
            sc_edge.forEach_canny(allocationF32_2B);

            // 第五步，Hysteresis
            sc_edge.set_allocationHysteresis(allocationF32_2B);
            sc_edge.forEach_hysteresis(allocationOut);

            // 第六步，計算差異點
            allocationRgbaSrc.copyFrom(allocationOut);

            fps.add();

            // 新想法：啟動後首先要先訓練空盤狀態
            if (!isTrained) { // Training 相當於照相
                allocationOut.ioSend();
                guiHandler.setStatusTraining(GuiHandler.TARGET_ALLOCATION_EMPTY, 10000);
                isTrained = true;
                afterTrained = false;
                fps.reset();
                return;
            }
            Log.d("MyLog", "after "+afterTrained+", status "+guiHandler.getStatusString());
            if (!afterTrained && guiHandler.getStatus() == GuiHandler.STATE_COMPLETED) {
                Log.d("MyLog", "COMPLETED 1");
                allocationOut.ioSend();
                afterTrained = true;
                switch (guiHandler.getTargetAllocation()) {
                    case GuiHandler.TARGET_ALLOCATION_EMPTY:
                        allocationRgbaTarget.copyFrom(allocationOut);
                        sc_edge.set_allocationTarget(allocationRgbaTarget);
                        break;
                    case GuiHandler.TARGET_ALLOCATION_BOTTOM:
                        allocationRgbaBottom.copyFrom(allocationOut);
                        sc_edge.set_allocationTarget(allocationRgbaBottom);
                        break;
                    case GuiHandler.TARGET_ALLOCATION_TOP:
                        allocationRgbaTop.copyFrom(allocationOut);
                        sc_edge.set_allocationTarget(allocationRgbaTop);
                        break;
                    case GuiHandler.TARGET_ALLOCATION_FULL:
                        allocationRgbaFull.copyFrom(allocationOut);
                        sc_edge.set_allocationTarget(allocationRgbaFull);
                        break;
                }
                sc_edge.invoke_resetVariance();
                sc_edge.forEach_calcSum(allocationOut);
                sc_edge.invoke_getCount(); // 原本在 getCount() 中有複製 src -> Org, 目前取消
                return;
            } else if (isTrained && afterTrained &&
                    (guiHandler.getStatus() == GuiHandler.STATE_NONE || guiHandler.getStatus() == GuiHandler.STATE_SHOW_MSG))
            {
                // 會流到這裡來的，應該要已經設過 allocationTarget
                // 如果還沒有目標圖，記住它，同時也是 Org
                sc_edge.invoke_resetVariance();
                sc_edge.forEach_calcSum(allocationOut);
                sc_edge.invoke_getCount(); // 原本在 getCount() 中有複製 src -> Org, 目前取消
                allocationOut.copyFrom(allocationRgbaSrc);
                allocationOut.ioSend();

//                Log.d("MyLog", String.format("Variance count = %d, ratio = %.5f(%.5f)",
//                        variance.count, (variance.count*onePerSize), lastRatio));

                // 如果變異太大就重新訓練,
                // 但是發現不同手機的值相差甚大，主要應該跟相機品質、解析度等等有關
//                        if (variance.count > 0.45) {
//                            Log.d("MyLog", "go to training??? status = " + guiHandler.getStatusString());
//                            isTrained = false;
//                        }
            } else { // 會跑到這兒，是正常訓練中....
                allocationOut.ioSend();
            }
        }
    }

    private class Fps {
        private ArrayList<Long> data;
        Fps() {
            data = new ArrayList<Long>() {};
        }
        public void reset() {
            data.clear();
        }
        public void add() {
            data.add(System.currentTimeMillis());
            if (data.size() > 100) data.remove(0);
            int status = guiHandler.getStatus();
            if (status == GuiHandler.STATE_NONE)
                guiHandler.setStatusShowMsg(String.format(Locale.ENGLISH, "FPS = %.2f", get()),1000);
        }
        private double get() {
            if (data.size() <= 1) return 0;
            double diff = data.get(data.size()-1) - data.get(0);
            return (data.size()-1)*1000 / diff;
        }
    }

    private class Variance {
        int count;
        Variance(ScriptC_edge scriptCEdge) {
            ScriptField_Variance.Item item = new ScriptField_Variance.Item();
            item.count = 0;
            scriptCEdge.set_variance(item);
        }
        public void set(int[] data) {
            count = data[0];
        }
    }

    public void showMsg(String msg) {
        TextView textView = findViewById(R.id.tvMsg);
        textView.setText(msg);
    }
}
