// http://libserial.sourceforge.net/x27.html
#include <iostream>
#include <cstdlib>
#include <string>
#include <iomanip>

#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include "SerialPort.h"
#include "packet.h"
#include "utils.h"

using namespace LibSerial;

////////// #define //////////

////////// 整體變數區 ///////////

int ret=0;
int cfg_timeout = 1000000; // 0 for unlimited, unit is us (1.0e-6 s)

std::string readCmd(SerialPort& serial_port) {
  std::string buf="";
  std::cout.put(':').flush();
  while(!serial_port.IsDataAvailable()) {
    usleep(1000);
  }
  try {
    serial_port.Read(buf, 0, 100000);
  } catch (ReadTimeout e) {
    std::cerr << "\ncatch read timeout exception!" << std::endl;
  }
  std::cout << utils::toHex(buf, true) << std::endl;
  return buf;
}

////////// 主程式區 //////////

int main(int argc, char* argv[])
{
  if (argc != 2) {
    std::cerr << "Usage: " << argv[0] << " PORT\r\n\tPORT: /dev/ttyUSB0 or /dev/ttyS2\r\n";
    exit(1);
  }
  SerialPort serial_port;
  serial_port.Open(argv[1]);
  if ( !serial_port.IsOpen() ) {
    std::cerr << "[" << __FILE__ << ":" << __LINE__ << "] "
         << "Error: Could not open serial port."
         << std::endl ;
    exit(2) ;
  }
  
  serial_port.SetBaudRate(BaudRate::BAUD_9600);
  serial_port.SetCharacterSize(CharacterSize::CHAR_SIZE_8 ) ;
  serial_port.SetParity(Parity::PARITY_NONE ) ;
  serial_port.SetStopBits(StopBits::STOP_BITS_1);
  serial_port.SetFlowControl(FlowControl::FLOW_CONTROL_NONE);

  while (true) {
    std::string res = readCmd(serial_port);
    if (res.compare(packets[PK_START_CMD]) == 0) {
      serial_port.Write(packets[PK_ACK_START_CMD]);
      std::cout << "<PK_START_CMD>     " << utils::toHex(packets[PK_ACK_START_CMD], true) << std::endl;
    } else if (res.compare(packets[PK_ACK_START_CMD]) == 0) {
      std::cout << "<PK_ACK_START_CMD> ";
      for (unsigned char i=0; i<255; ++i) {
        serial_port.WriteByte(i);
        std::cout << std::setw(2) << std::setfill(' ') << std::hex << i;
      }
      std::cout << std::endl;
    } else {
      serial_port.Write(packets[PK_CMD_DONE]);
      std::cout << "<Others>           " << utils::toHex(packets[PK_CMD_DONE], true) << std::endl;
    }
    usleep(25000) ;
  };

  std::cerr << std::endl;
  return EXIT_SUCCESS;
}
