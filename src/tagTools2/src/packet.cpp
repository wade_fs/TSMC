#include <iostream>
#include <unistd.h>
#include "packet.h"

using namespace std;

////////// Global Variables //////////

unordered_map <int, string> packets = {
  { PK_START_CMD,    string("\xFC\xFF") },								// 0
  { PK_DISPLAY_TIME, string("\xFF\xFF\xDF\xFF\xBB\xFF\xFF\xBB\xFF\xFF\xFF") },				// 1
  { PK_READ_TIME, string("\xFF\xFF\xDF\xFF\x3B\xFF\xFF\x3B\xFF\xFF\xFF") },				// 2
  { PK_READ_DONE, string("\x9F\xFF") },									// 3
  { PK_NORMAL_STATUS_READ, string("\xFF\xFF\xDF\xFF\xFD\xFF\x55\x56\xFF\xFF\xFF") },			// 4
  { PK_ENTENDED_STATUS2_READ, string("\xFF\xFF\xDF\xFF\xFD\xFF\x5A\x58\xFF\xFF\xFF") },			// 5
  { PK_PROTECTED_DATA_READ, string("\xFF\xFF\xDF\xFF\xFD\xFF\x55\x56\xFF\xFF\xFF") },			// 6
  { PK_WRITE_LCD_DATA, string("\xFF\xFF\x1F\xFF\x77\xFF\xFF\x07\xB0\xFF\xBB\xFF\x7F\xFF") },		// 7
  { PK_READ_LCD_DATA, string("\xFF\xFF\xEF\xFF\xF7\xFF\xFF\x07\xEF\x7F\x7F\x63\xFF\xFF\xFF") },		// 8
  { PK_LCD_DISPLAY_ON, string("\xFF\xFF\xDF\xFF\xF9\xFF\xFF\xF9\xFF\xFF\xFF") },			// 9
  { PK_LCD_DISPLAY_OFF, string("\xFF\xFF\xDF\xFF\x33\xFF\xFF\x33\xFF\xFF\xFF") },			// 10
  { PK_CLEAR_LCD, string("\xFF\xFF\xDF\xFF\xD3\xFF\xFF\xD3\xFF\xFF\xFF") },				// 11
  { PK_ENABLE_TIME_OUT, string("\xFF\xFF\xDF\xFF\xB3\xFF\xFF\xB3\xFF\xFF\xFF") },			// 12
  { PK_DISABLE_TIME_OUT, string("\xFF\xFF\xDF\xFF\x73\xFF\xFF\x73\xFF\xFF\xFF") },			// 13
  { PK_DISPLAY_SERIAL_NUMBER, string("\xFF\xFF\xDF\xFF\x09\xFF\xFF\x09\xFF\xFF\xFF") },			// 14
  { PK_GET_TAG_FORMAT, string("\xFF\xFF\xEF\xFF\xF7\xFF\x7F\xFF\x87\xFF\x7F\xF3\xFF\xFF\xFF") },	// 15
  { PK_FORMAT_TAG, string("\xFF\xFF\x1F\xFF\x77\xFF\x7F\xFF\xF7\xFF\xBB\xFF\xFF\xFF") },		// 16
  { PK_START_CMDr, string("\xFC\xFF") },								// 100
  { PK_ACK_START_CMD, string("\xCA") },									// 101
  { PK_CMD_DONE, string("\x60") },									// 102
  { PK_FORMAT, string("\xFF\xFF\xB7\xFF\xF5\xFF\x73\xFB\xFB\xFB\xFB\xFB\xFB\xFB\xFB\xFB\xFB\xFB\xFB\xFB\xFB\x7D\xFF\xBF\xFF") }, // 封包不變，會跟著參數 // 1000
  { PK_SET_TIME, string("\xFF\xFF\xAF\xFF\x5B\xFF\x5D\x17\x3F\x1B\x77\x7F\x5B\xB8\xFF\xFF\xFF") },	// 1001
  { PK_LCD_DATAv, string("\xFF\xFF\xD0", 3) },								// 1002
  { PK_READ_TIME1, string("\x00\x10\x24", 3) }, // 要分成三段						// 1100
  { PK_NORMAL_STATUS, string("\x00\xA8\x42", 3) },							// 1101
  { PK_PROTECTED_DATA, string("\x00\xA8\x42", 3) },							// 1102
  { PK_EXTENDED_STATUS2, string("\x00\xE8\x42", 3) },							// 1103
  { PK_LCD_DATAvr, string("\x80\x80\x0A", 3) },								// 1104
  { PK_LCD_DATA21, string("\x00\x90\x0A", 3) },								// 1105
  { PK_LCD_DATA22, string("\x00\x50\x0A", 3) }, // 我怕會有別的值，所以這邊要思考一下			// 1106
  { PK_TAG_FORMAT, string("\x00\xF8\x0A", 3) },								// 1107
};

////////// class packet //////////

int packet::PK_OK = 0;
packet::packet(int ID) {
  if (ID >= 0 && (packets.find(ID) != packets.end()))
    id = ID;
  else id = -1;
}

int packet::getId() { return id; }
void packet::setId(int ID) { id = ID; }
int packet::dir() { return (id%1000) < 100; }
int packet::var() { return (id >= 1000); }

vector<char> packet::getP() {
  vector<char> ca = {};
  if (id >= 0 && (packets.find(id) != packets.end())) {
    string p = packets[id];
    copy(p.begin(), p.end(), back_inserter(ca));
  }
  return ca;
}

string packet::getPinAscii() {
  string res="";
  if (id >= 0 && (packets.find(id) != packets.end())) {
    string p = packets[id];
    for (unsigned int i=0; i<p.length(); ++i) {
      char c = p[i];
      if (isprint(c)) res.push_back(c);
      else res.push_back('.');
    }
    return res;
  } else return "x";
}

string packet::getPinHex(char delim) {
  static const char* const lut = "0123456789ABCDEF";
  string p;
  if (id >= 0 && (packets.find(id) != packets.end())) p = packets[id];
  if (p.length() == 0) return "y";

  size_t len = p.length();

  string output;
  output.reserve(3 * len);
  for (size_t i=0; i<len; ++i) {
    const unsigned char c = p[i];
    output.push_back(lut[c >> 4]);
    output.push_back(lut[c & 0x0F]);
    if (delim != '\0' && i<len-1) output.push_back(delim);
  }
  return output;
}
