/**
 *  @example serial_port_read_write.cpp
 */

#include <SerialPort.h>
#include "utils.h"

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <unistd.h>

using namespace LibSerial;

/**
 * @brief This example demonstrates multiple methods to read and write
 *        serial stream data.
 */
int main(int argc, char* argv[])
{
  SerialPort serial_port_1;

  serial_port_1.Open(argc==2?argv[1]:"/dev/ttyUSB0");

  if (!serial_port_1.IsOpen()) {
    std::cerr << "The serial ports did not open correctly." << std::endl;
    return EXIT_FAILURE;
  }

  // Set the serial port
  serial_port_1.SetBaudRate(BaudRate::BAUD_115200);
  serial_port_1.SetCharacterSize(CharacterSize::CHAR_SIZE_8);
  serial_port_1.SetFlowControl(FlowControl::FLOW_CONTROL_NONE);
  serial_port_1.SetParity(Parity::PARITY_NONE);
  serial_port_1.SetStopBits(StopBits::STOP_BITS_1);

  std::string write_string_1 = "";
  write_string_1.reserve(256);
  for (unsigned char c=0; c<=255; ++c) write_string_1 += std::string(1, static_cast<char>(c));

  std::string read_string_1 = "";
  read_string_1.reserve(write_string_1.size());

  size_t timeout_milliseconds = 10000;

  serial_port_1.Write(write_string_1);
  std::cout << "\tsent:\t"     << utils::toHex(write_string_1) << std::endl;

  try {
    serial_port_1.Read(read_string_1, write_string_1.size(), timeout_milliseconds);
    std::cout << "\tRead:\t" << utils::toHex(read_string_1) << std::endl;
  } catch (ReadTimeout) {
    std::cerr << "The Read() call has timed out." << std::endl;
  }

  serial_port_1.Close();

  std::cout << "The example program successfully completed!" << std::endl;
  return EXIT_SUCCESS;
}
