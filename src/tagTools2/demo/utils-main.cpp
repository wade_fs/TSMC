#include <iostream>
#include <string>
#include "utils.h"

using namespace std;

int main(int argc, char* argv[])
{
  string ss = "  I am wade.\xE0\xE1\xF0\xFF  ";
  string s = "    ";
  string s0 = utils::trim(s);
  string s1 = utils::trim(s, 1);
  string s2 = utils::trim(s, 2);

  cout << "Orgin: '" << s  << "'" << endl;
  cout << "trim2: '" << s0 << "'" << endl;
  cout << "trimH: '" << s1 << "'" << endl;
  cout << "trimT: '" << s2 << "'" << endl;

  return 0;
}
