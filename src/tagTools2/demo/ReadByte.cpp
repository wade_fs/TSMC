#include <SerialPort.h>
#include <utils.h>

#include <iostream>
#include <unistd.h>
#include <cstdlib>

using namespace LibSerial;

int main(int argc, char* argv[])
{
  SerialPort serial_port;
  size_t ms_timeout = 25;
  char data_byte;

  serial_port.Open(argc==2?argv[1]:"/dev/ttyMT2");

  // Set the SerialPort
  serial_port.SetBaudRate(BaudRate::BAUD_115200);
  serial_port.SetCharacterSize(CharacterSize::CHAR_SIZE_8);
  serial_port.SetFlowControl(FlowControl::FLOW_CONTROL_NONE);
  serial_port.SetParity(Parity::PARITY_NONE);
  serial_port.SetStopBits(StopBits::STOP_BITS_1);
  
  while(!serial_port.IsDataAvailable()) {
    usleep(1000);
  }

  std::string read_string=""; read_string.reserve(1024);

  while(serial_port.IsDataAvailable()) {
    try {
      serial_port.ReadByte(data_byte, ms_timeout);
      read_string += data_byte;
    } catch (ReadTimeout) {
      std::cerr << "The ReadByte() call has timed out." << std::endl;
    }

    usleep(1000);
  }

  std::cout << "Read:\t" << utils::toHex(read_string) << std::endl;
  return EXIT_SUCCESS;
}
