#include <SerialPort.h>
#include <utils.h>

#include <iostream>
#include <fstream>
#include <cstdlib>

using namespace LibSerial;

int main(int argc, char** argv)
{   
  SerialPort serial_port;

  serial_port.Open(argc==2?argv[1]:"/dev/ttyUSB0");

  // Set the Serial Port
  serial_port.SetBaudRate(BaudRate::BAUD_115200);
  serial_port.SetCharacterSize(CharacterSize::CHAR_SIZE_8);
  serial_port.SetFlowControl(FlowControl::FLOW_CONTROL_NONE);
  serial_port.SetParity(Parity::PARITY_NONE);
  serial_port.SetStopBits(StopBits::STOP_BITS_1);

  std::string write_string="";
  write_string.reserve(256);
  for (int c=0; c<=255; c++) write_string += std::string(1, static_cast<char>(c));

  for (std::string::iterator it = write_string.begin(); it < write_string.end(); ++it) {
    serial_port.WriteByte(*it);
  }

  std::cout << std::endl << "Write:\t" << utils::toHex(write_string) << std::endl;
  return EXIT_SUCCESS;
}
