#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <errno.h>

unsigned char inv(unsigned char m) {
  int r, i=8;
  for (; i--; r=r*2|m&1,m>>=1);
  return (unsigned char)r;
}

long int readHex(char* inps) { 
  static char *inpstr=NULL, *str=NULL;
  char *endptr;
  long int val;
  
  // new string, reset
  if (inps != inpstr) { inpstr = str = inps; }
  
  errno = 0; val = strtol(str, &endptr, 16); // reset errno to 0 before strtol()
  
  // input string with wrong format
  if ((errno == ERANGE && (val == LONG_MAX || val == LONG_MIN)) || (errno != 0 && val == 0)) {
    inpstr = str = NULL;
    return errno;
  }
  
  // read at end of string 
  if (str != endptr) { str = endptr; }
  else {
    inpstr = str = NULL; val = -LONG_MAX;
  }
  return val;
}

unsigned short int bytes2Int(unsigned char a, unsigned char b) {
  return (unsigned short int)(a << 8 || b);
}

// FC FF FF FF 1F FF 77 FF FF 7B 7E 7F D2 FF FF FF FF FF[40 FF|
struct CMD {
  unsigned char head1[2]; // FC FF
  unsigned char head2[6]; // FF FF 1F FF 77 FF
  // multi: 無切割07，後面不會有 B5 FF FF FF, 最後是 block index(1) + checksum(4)
  //        有切割7B, 封包的最後以 FC FF 結尾, 它的後面有 B5 FF FF FF 開始的其他封包，用意不明
  unsigned char multi[2];
  union {
    unsigned short int usi;
    unsigned char uc[2]; // 這應該有效 MSG data 的最大字串長度，實際長度依格式分析而得
  } totalLength;
  unsigned char unknown1[2]; // 這兩個 bytes 完全不知道意義，只知道 [0] 有變化，[1] = FF，有可能是某一種 checksum
  // 好像跟資料封包分段有關，但是意義跟前面不同，目前待測:
  // FF FF 指的是後面資料封包分兩段，意思是有多段資料，每段後面直接接 8 bytes(4 bytes checksum+4bytes), multi[] 一定是 FF 7B
  // 7F FF 目前意義不明確，所以被歸為 unknown2[]
  unsigned char unknown2[2]; // 好像跟資料封包分段有關，但是意義跟前面不同, 感覺是一代二代的格式
  unsigned char tail[2]; // FF FF
};
#define DUMPCMD
#ifdef DUMPCMD
void dump_cmd(struct CMD* cmd) {
  printf ("%02X %02X ", (unsigned char)~inv(cmd->head1[0]), (unsigned char)~inv(cmd->head1[1]));
  printf ("%02X %02X %02X %02X %02X %02X ", (unsigned char)~inv(cmd->head2[0]), (unsigned char)~inv(cmd->head1[1]),
          (unsigned char)~inv(cmd->head1[2]), (unsigned char)~inv(cmd->head1[3]),
          (unsigned char)~inv(cmd->head1[4]), (unsigned char)~inv(cmd->head1[5]));
  printf ("%02X %02X ", (unsigned char)~inv(cmd->multi[0]), (unsigned char)~inv(cmd->multi[1]));
  printf ("%02X %02X ", (unsigned char)~inv(cmd->totalLength.uc[0]), (unsigned char)~inv(cmd->totalLength.uc[1]));
  printf ("%02X %02X ", (unsigned char)~inv(cmd->unknown1[0]), (unsigned char)~inv(cmd->unknown1[1]));
  printf ("%02X %02X ", (unsigned char)~inv(cmd->unknown2[0]), (unsigned char)~inv(cmd->unknown2[1]));
  printf ("%02X %02X ", (unsigned char)~inv(cmd->tail[0]), (unsigned char)~inv(cmd->tail[1]));
  printf ("\t: ");

  printf ("%02X %02X ", cmd->head1[0], cmd->head1[1]);
  printf ("%02X %02X %02X %02X %02X %02X ", cmd->head2[0], cmd->head1[1], cmd->head1[2], cmd->head1[3], cmd->head1[4], cmd->head1[5]);
  printf ("%02X %02X ", cmd->multi[0], cmd->multi[1]);
  printf ("(%02X %02X)=(%d) ", cmd->totalLength.uc[0], cmd->totalLength.uc[1], (unsigned short int)cmd->totalLength.usi);
  printf ("%02X %02X ", cmd->unknown1[0], cmd->unknown1[1]);
  printf ("%02X %02X ", cmd->unknown2[0], cmd->unknown2[1]);
  printf ("%02X %02X ", cmd->tail[0], cmd->tail[1]);
  printf ("\n");
}
#endif
struct DATA {
  union {
    unsigned short int usi;
    unsigned char uc[2]; // 長度的index, 即 length-1: 含這兩個 bytes, 直到 checksum(不含) 之前
  } length;
  unsigned char head[2]; // F5 FF
};

unsigned char block_index;
struct CHECKSUM { // checksum 為 8 bits, HHHHLLLL 切割成 4 bytes
  unsigned char low[2]; // [0] 有效，  [1] 永遠是 FF
  unsigned char high[2];  // [0] 有效，  [1] 永遠是 FF
};

int process_single(char* buf, char* MSG) {
  struct DATA *data;
  unsigned char blockIndex;
  union {
    unsigned char usc[2];
    unsigned short int usi;
  } check_sum;
  int cs;
  char *cp;

  data = (struct DATA*)((char*)buf+18);
  if (data->head[0] != 0x50 || data->head[1] != 0x00) {
    printf ("Single wrong head %02X %02X\n", data->head[0], data->head[1]);
    return -2; // ERROR head, must be 50=inv(comp(F5)) && 00=inv(comp(FF))
  }
  blockIndex = (unsigned char)buf[18+data->length.usi];
  check_sum.usc[0] = (unsigned char)buf[18+data->length.usi+1];
  check_sum.usc[1] = (unsigned char)buf[18+data->length.usi+3];
  // checksum 不包括最前面的 length(所以初始值是 18+2), 但是有包括 blockIndex, 所以是 cp < buf+18+data->length.usi+1
  for (cs = 0, cp = buf+18+2; cp < buf+18+data->length.usi+1; cp++) {
    cs += (unsigned char)*cp;
  }
  if (cs == check_sum.usi) {
    strncpy(MSG, buf+18+2+2+blockIndex, 224-blockIndex);
    return 224-blockIndex;
  }
  else {
    printf ("Single wrong checksum, length = %d, checksum = %d / %d\n", data->length.usi, check_sum.usi, cs);
    return -1; // ERROR check sum
  }
}

int process_multi(char* buf, int n, char* MSG) {
  struct DATA *data;
  union {
    unsigned char usc[2];
    unsigned short int usi;
  } check_sum;
  int cs;
  char *cp, *data2;
  int ret=0, len=0, datalen, last=0;

  data2 = (char*)((char*)buf+18);
  data  = (struct DATA*)data2;
  datalen = data->length.usi - 1;
  if (data->head[0] != 0x50 || data->head[1] != 0x00) {
    printf ("M> head %02X %02X\n", data->head[0], data->head[1]);
    return -2; // ERROR head, must be 50=inv(comp(F5)) && 00=inv(comp(FF))
  }
  do {
    if (len >= 1024 || ((unsigned char)data2[datalen+6] == 0xC0 && (unsigned char)data2[datalen+7] == 0x00)) last = 1;
    check_sum.usc[0] = (unsigned char)data2[datalen+2];
    check_sum.usc[1] = (unsigned char)data2[datalen+4];
    // 資料前面有四個 bytes, 二個是 length, 二個是 head,
    // checksum 不包括最前面的 length(所以初始值是 data2+2), 結束端不像 Single, Multi 沒有 blockIndex, 所以是 cp < data2+(datalen+2) << 後面的 2 是 head F5 FF
    for (cs = 0, cp = data2+2; cp < data2+2+datalen; cp++) {
      cs += (unsigned char)*cp;
    }
    if (cs == check_sum.usi) {
      strncpy(MSG+len, data2+2+2, datalen-2);
      len += datalen-2;
      // 將 data2 指向下一個封包，如果有的話
      data2 = (char *)(data2 + datalen + 8);
      data  = (struct DATA*)data2;
      datalen = data->length.usi - 1; // length 數目怪怪的，要少 1
    }
    else {
      ret = -1; // ERROR check sum
      break;
    }
  } while (!last);
  if (len > 0) {
    ret = len;
  }
  return ret;
}

int main(int argc, char* argv[])
{
  char* input;
  char buf[1224]; // 不會這麼長
  char MSG[1024];
  struct CMD* cmd;

  if (argc != 2) {
    printf ("Usage: %s \"INPUT STRING\"\n", argv[0]);
    return 1;
  }

  if (argv[1][0] == '#') { // just for comment line
    return 0;
  }

  // 00. 底下一行，視作從 RS232 讀進資訊
  input = strndup(argv[1], strlen(argv[1]));
  bzero(MSG, 1024);
  bzero(buf, 1224);

  // 01. 將輸入字串，進行反向+補數處理
  long int res;
  int pos;
  pos = 0;
  while ((res = readHex(input)) >= 0) {
    buf[pos] = ~inv((unsigned char)res);
    pos++;
  }
  cmd = (struct CMD*)buf;

  // 02. 根據 cmd->multi[1] == (1F=inv(comp(7F)) single) 或 (21 = inv(comp(7B)) multi)
  int length;
  if (cmd->multi[1] == 0x1F) { // single
    // 03. 處理單一封包
    length = process_single(buf, MSG);
    printf ("S> %d\t\"%s\"\n", length, MSG);
  } else if (cmd->multi[1] == 0x21) { // multi
    // 04. 處理多個封包
    length = process_multi(buf, pos, MSG);
    printf ("M> %d\t\"%s\"\n", length, MSG);
  }
  return 0;
}
