#include <stdio.h>

unsigned inv(unsigned m) {
  unsigned int r, i=8;
  m &= 0xFF;
  for (; i--; r=r*2|m&1,m>>=1);
  return (unsigned)(r & 0xFF);
}

int main(int argc, char* argv[])
{
  unsigned inp;
  int c=0;

  while (scanf("%x", &inp) == 1) {
    unsigned ch = (~inv(inp) & 0xFF);
    printf (" %c%c", (ch>=32 && ch<=126)?(unsigned char)ch:'?' , ((c+1) % 8 == 0?'|':' '));
    if ((++c)%32 == 0) printf ("\n");
  }
  printf ("\n");
  return 0;
}
