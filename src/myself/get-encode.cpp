#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <errno.h>

unsigned char inv(unsigned char m) {
  int r, i=8;
  for (; i--; r=r*2|m&1,m>>=1);
  return (unsigned char)r;
}

inline void printCA() { printf ("CA 60 "); }
inline void printLength(int len) { printf ("%02X %02X ", (unsigned char) inv(len & 0xFF), (unsigned char) inv((len>>8) & 0xFF)); }
inline void print0A() { printf ("0A "); }
inline void printOK() { printf ("00 90 0A 00 00 00 00 00 00 00 00 0A 00 "); } // 這也是一個完整封包，格式為 Length(x2) HEAD 0A, ... Checksum(x2)
inline void println() { printf ("\n"); }

void printMsgBuf(char* buf, int bufLen, int blockIndex) {
  int i, len=0, checksum=80; // checksum init with 80 = inv(0A)
  // 前 224 是放資料的，後面有 16 個空白, 再後面是...
  for (i=0; i<blockIndex; i++) printf ("00 ");
  for (len=blockIndex, i=0; i<bufLen; i++) {
    printf ("%02X ", (unsigned char)inv(buf[i]));
    checksum += (unsigned int) buf[i];
  }
  // 這邊是放空白直到 240 結尾
  for (len+=bufLen; len<240; len++) {
    printf ("04 ");
    checksum += (unsigned int) 0x20; // 0x20 = inv(0x04)
  }
  // 240 之後是 blockIndex 50.....FF 00 00 共 16 bytes
  printf ("%02X 50 00 00 00 00 00 00 00 00 00 00 00 FF 00 00 ", (unsigned char)inv(blockIndex));
  checksum += (unsigned int)(blockIndex + 10 + 255); // 10 = inv(0x50), 255 = inv(0xFF)
  printf ("%02X %02X ", (unsigned)inv(checksum & 0xFF), (unsigned)inv((checksum>>8) & 0xFF));
}

void printFullMsg (char* buf, int bufLen) {
  int i, checksum=80; // checksum init with 80 = inv(0A)
  for (i=0; i<bufLen; i++) {
    printf ("%02X ", (unsigned char)inv(buf[i]));
    checksum += (unsigned int) buf[i];
  }
  printf ("%02X %02X ", (unsigned)inv(checksum & 0xFF), (unsigned)inv((checksum>>8) & 0xFF));
}

void printNullPackage() {
  int i, checksum=80; // checksum init with 80 = inv(0A)
  // 前 224 是放資料的，後面有 14 個空白, 再後面是...
  for (i=0; i<240; i++) {
    printf ("04 ");
    checksum += (unsigned int) 0x20; // 0x20 = inv(0x04)
  }
  printf ("57 50 00 00 00 00 00 00 00 00 00 00 00 FF 00 00 ");
  checksum += (unsigned int)(234 + 10 + 255); // 234 = inv(57), 10 = inv(0x50), 255 = inv(0xFF)
  printf ("%02X %02X ", (unsigned)inv(checksum & 0xFF), (unsigned)inv((checksum>>8) & 0xFF));
}

int main(int argc, char* argv[])
{
  // 00. message buffer 目前假設存在 argv[1] 中
  // 實際應用應該是類似 buf[] 中
  if (argc != 2) {
    printf ("usage: %s \"訊息字串\"\n", argv[0]);
    return 1;
  }
  int bufLen = strlen(argv[1]);

  // 01. 先判斷訊息長度, 分三種狀況
  // 1.1. <= 224
  if (bufLen <= 224) {
    printCA();
    printLength(256+1); // where 1 for 0A
    print0A();
    // 02. 計算 blockIndex 並印出整個訊息
    printMsgBuf(argv[1], bufLen, 224 - ((1+ (bufLen-1) / 16)<< 4));
    printOK();
    println();
  }
  // 1.2. <= 256
  else {
    // 03. 先印出整個空訊息，對，就這麼怪
    printCA();
    printLength(256+1); // 1 is for 0A
    print0A();
    printNullPackage();
    printOK();

    printCA();
    // 04. 接下來才是完整的封包, 它沒有後面的 printOK() ....
    char* pch = argv[1];
    int packLen;
    while (pch < argv[1] + bufLen) {
      if (strlen(pch) > 256) {
        packLen = 256;
      }
      else packLen = strlen(pch);
      printLength(packLen+1); // length = 0A + data length
      print0A();
      printFullMsg(pch, packLen); // 後面不必接 printOk()
      pch += 256;
    }
    println();
  }
  return 0;
}
