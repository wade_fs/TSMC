#include <stdio.h>
#include <string.h>

// 將 byte 的每個 bit 反向
unsigned char inv(unsigned char m)
{
  unsigned char curByte=m, resByte=0;
  for (int bit=0; bit<8; bit++) {
    if ((curByte & (1<<bit)) != 0) {
      resByte |= ( (1<<(7-bit)) & 0xFF);
    }
  }
  return resByte;
}

void Dump(unsigned char *data, int n, int invert, int revert)
{
  int s, e, d;
  if (revert) {
    if (invert) { for (int i=n-1; i>=0; i--) { printf ("%02X ", (unsigned char)inv(data[i])); } }
    else        { for (int i=n-1; i>=0; i--) { printf ("%02X ", (unsigned char)data[i]); } }
  }
  else {
    if (invert) { for (int i=0; i<n; i++) { printf ("%02X ", (unsigned char)inv(data[i])); } }
    else        { for (int i=0; i<n; i++) { printf ("%02X ", (unsigned char)data[i]); } }
  }
  printf ("\n");
}

int main(int argc, char* argv[])
{
  unsigned short int SUM=0;
  unsigned char *dataP;
  int pos, inplen, packNum;
#pragma pack(push)  /* push current alignment to stack */
#pragma pack(1)     /* set alignment to 1 byte  */
  struct {
    unsigned char head[4]; // CA, 60, 80, 80
    union {
      struct {
        unsigned char HEAD; // 0A
        unsigned char d[14][16];
        unsigned char spaces[16]; // 04 04 04 04 04 04 04 04 04 04 04 04 04, 04 04 04
        unsigned char position[2]; // [1] = 50, [0] is position block index
        unsigned char unknown[14]; // 最後一個 byte 有可能是 00 或 FF
      } data;
      unsigned char buf[257];
    } inputData;
    union {
      unsigned short int checkSum;
      unsigned char cs[2];
    } checksum;
    unsigned char tail[13]; // 00 90 0A 00 00 00 00 00 00 00 00 0A 00
  } package;
#pragma pack(pop)   /* restore original alignment from stack */

  if (argc != 2) {
    printf ("Usage: %s \"INPUT STRING\"\n", argv[0]);
    return 1;
  }

  // 第0步 初始化結構
  package.head[0] = 0x53; package.head[1] = 0x06; package.head[2] = 0x01; package.head[3] = 0x01;
  bzero(package.inputData.data.d, sizeof(package.inputData.data.d));
  package.inputData.data.HEAD = 0x50;
  memset(package.inputData.data.spaces, 0x20, 16);
  package.inputData.data.position[1] = 0x0A;
  memset(package.inputData.data.unknown, 0x00, 14); // package.inputData.data.unknown[12] = 0xFF;
  memset(package.tail, 0x00, 13); package.tail[1] = 0x09; package.tail[2] = 0x50; package.tail[11] = 0x50;

  // 第一步，先計算資料組位置, 並初始化為 0x20
  inplen = strlen(argv[1]);
  if (inplen == 0) {
    printf ("Usage: %s \"INPUT STRING\"\n\tWhere INPUT STRING could not be empty string\n", argv[0]);
    return 2;
  }
  packNum = 1+ (inplen-1) / 16;
  pos = 224 - (packNum << 4);
  package.inputData.data.position[0] = (unsigned char)pos;
  dataP = (unsigned char*)&(package.inputData.data.d[14-packNum]);
  memset(dataP, 0x20, packNum * 16);

  // 第二步，把資料丟入 package.data.d
  strncpy ((char*)dataP, (const char*)argv[1], (size_t)strlen(argv[1]));

  // 第三步，計算 checksum
  SUM = 0;
  for (int i=0; i<sizeof(package.inputData.buf); i++) {
    SUM += (unsigned short int)package.inputData.buf[i];
  }
  package.checksum.checkSum = SUM;

  Dump((unsigned char*)&package, (int)sizeof(package), 1, 0);
  return 0;
}
