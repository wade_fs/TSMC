#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <errno.h>

unsigned char inv(unsigned char m) {
  int r, i=8;
  for (; i--; r=r*2|m&1,m>>=1);
  return (unsigned char)r;
}

int main(int argc, char* argv[])
{
  unsigned char buf[1024];
  unsigned char ch, *bufp;
  unsigned int inp;
  int blockIndex;
  int isStartCA = 0, isStart0A = 0, isStartPack = 0, isEndPack = 0;
  int pch = 0, len = 0, lenByte = 0, bufLen = 0;
  int checksum = 0, checksumByte = 0, myChecksum = 80; // 80 = inv(0x0A) for init
  int dataSecNum = 0;

  // 00. 這邊有一段應該初始化變數，目前寫在宣告區
  // ....... init() ........

  bzero(buf, sizeof(buf));
  while (scanf("%x", (unsigned int*)&inp) == 1) {
    ch = (unsigned char)(inp & 0xFF);
    // 01. 讀開頭的 CA XX 
    if (!isStartCA) {
      if (ch == (unsigned char)0xCA) {
        isStartCA = 1;
        lenByte = 0;

        // 這邊需要跳過一個 byte, CA 後面的 byte 意義不明
        scanf("%x", (unsigned int*)&inp);
      }
      continue;
    }
    // 02. 讀 Len
    if (!isStart0A) {
      if (lenByte < 2) { // wait for length
        len = (len << 8) | inv(ch);
        ++lenByte;
      }
      if (lenByte < 2) continue;
      // 03. 讀 0A
      if (ch == (unsigned char)0x0A) { // 到這邊時，照理應該要已經讀到長度了
        --len;
        isStart0A = 1;
      }
      continue;
    }
    // 04. 讀進封包資料
    if (!isEndPack) {
      if (pch < len) {
        buf[bufLen] = inv(ch);
        myChecksum += buf[bufLen];
        ++pch; ++bufLen;
      }
      if (pch >= len) {
        if (dataSecNum == 0) {
          blockIndex = buf[240];
        }
        ++dataSecNum;
        isEndPack = 1;
      }
      continue;
    }
    // 05 到這邊要讀進兩個 bytes 的 checksum
    if (checksumByte == 0) {
      checksum = inv(ch);
      checksumByte++;
      continue;
    }
    if (checksumByte == 1) {
      checksum |= (inv(ch) << 8);
      checksumByte++;
    }
    // 5.1 檢查 checksum 是否正確
    if (checksum != myChecksum) {
      printf ("Error checksum %d != %d\n", checksum, myChecksum);
      break;
    }
    // 06. 檢查封包型態
    if (dataSecNum == 1) {
      if (blockIndex < 234) { // 如果長度 <= 224, 只讀這麼一筆
        break;
      }
      else // Multi 型要把緩衝區重設
        bufLen = 0;
    }
    // 07, 處理完一個封包，繼續下一個
    isStart0A = isStartPack = isEndPack = pch = len = lenByte = checksum = checksumByte = 0;
    if (dataSecNum == 1) isStartCA = 0;
    myChecksum = 80; // 80 = inv(0x0A) for init
  }
  // 08. 印訊息
  printf ("\"");
  if ((dataSecNum == 1) && (blockIndex < 234)) { // 只有一個 data section
    for (pch = blockIndex; pch < (256 - 16); pch++) {
      printf ("%c", buf[pch]);
    }
  }
  else {
    for (pch = 0; pch < bufLen; pch++) {
      printf ("%c", buf[pch]);
    }
  }
  printf ("\"\n");
  return 0;
}
