#include <stdio.h>

unsigned inv(unsigned m) {
  unsigned int r, i=8;
  m &= 0xFF;
  for (; i--; r=r*2|m&1,m>>=1);
  return (unsigned)(r & 0xFF);
}

struct INT {
  char a; char b; char c; char d;
};
int main(int argc, char* argv[])
{
  unsigned char a, b, c, d;
  struct INT i;

  if (scanf("%x %x %x %x", &a, &b, &c, &d) == 4) {
    i.a = a; i.b = b; i.c = c; i.d = d;
    printf ("%08X = %d\n", i, i);
    i.a = c; i.b = d; i.c = a; i.d = b;
    printf ("%08X = %d\n", i, i);
    i.a = b; i.b = a; i.c = d; i.d = c;
    printf ("%08X = %d\n", i, i);
    i.a = a; i.b = c; i.c = 0; i.d = 0;
    printf ("%08X = %d\n", i, i);
    i.a = c; i.b = a; i.c = 0; i.d = 0;
    printf ("%08X = %d\n", i, i);
  }
  return 0;
}
