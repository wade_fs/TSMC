/***
 * This example expects the serial port has a loopback on it.
 *
 * Alternatively, you could use an Arduino:
 *
 * <pre>
 *  void setup() {
 *    Serial.begin(<insert your baudrate here>);
 *  }
 *
 *  void loop() {
 *    if (Serial.available()) {
 *      Serial.write(Serial.read());
 *    }
 *  }
 * </pre>
 */

#include <string>
#include <iostream>
#include <cstdio>

// OS Specific sleep
#ifdef _WIN32
#include <windows.h>
#else
#include <unistd.h>
#endif

#include "serial/serial.h"

using std::string;
using std::exception;
using std::cout;
using std::cerr;
using std::endl;
using std::vector;

void my_sleep(unsigned long milliseconds) {
#ifdef _WIN32
      Sleep(milliseconds);
#else
      usleep(milliseconds*1000);
#endif
}

void print_usage()
{
  cerr << "Usage: test_serial {-e|<serial port address>} ";
    cerr << "<baudrate> [test string]" << endl;
}

int run2(int argc, char **argv)
{
  int timeout = 10000; // 10,000 ms = 10 s
  if(argc < 2) {
    print_usage();
    return 0;
  }

  // Argument 1 is the serial port or enumerate flag
  string port(argv[1]);

  if (argc < 3) {
    print_usage();
    return 1;
  }

  // Argument 2 is the baudrate
  unsigned long baud = 0;
#if defined(WIN32) && !defined(__MINGW32__)
  sscanf_s(argv[2], "%lu", &baud);
#else
  sscanf(argv[2], "%lu", &baud);
#endif

  // port, baudrate, timeout in milliseconds
  serial::Serial my_serial(port, baud, serial::Timeout::simpleTimeout(timeout));

  cout << "Is the serial port open?";
  if(my_serial.isOpen())
    cout << " Yes." << endl;
  else
    cout << " No." << endl;

  // Get the Test string
  int count = 0;
  string test_string;
  if (argc == 4) {
    test_string = argv[3];
  } else {
    test_string = "ACK";
  }

  // my_serial.setTimeout(serial::Timeout::max(), 10000, 0, 10000, 0);
  cout << "Timeout == " << timeout << "ms, asking for 1 more byte than written." << endl;

  my_serial.flushInput();
  string result = my_serial.read(255);

  cout << "Bytes read: ";
  cout << result.size() << ", String read: " << result << endl;

  my_sleep(25);
  size_t bytes_wrote = my_serial.write((const uint8_t*)test_string.data(), test_string.size());
  my_serial.flushOutput();

  cout << "Bytes written: ";
  cout << bytes_wrote << endl;
}

int main(int argc, char **argv) {
  try {
    return run2(argc, argv);
  } catch (exception &e) {
    cerr << "Unhandled Exception: " << e.what() << endl;
  }
}
