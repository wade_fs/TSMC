#pragma version(1)
#pragma rs java_package_name(com.wade.tsmcbyrenderscript)

uchar4 RS_KERNEL root(uchar4 in) {
        float4 f4 = rsUnpackColor8888(in);

        float average = (f4.r + f4.g + f4.b) / 3;
        float3 output = {average, average, average};
        return rsPackColorTo8888(output);
}

void init(){
        rsDebug("MyLog: Called init() @ grayscale", rsUptimeMillis());
}
