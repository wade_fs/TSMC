#pragma version(1)
#pragma rs java_package_name(ca.tutortutor.embossimage)

rs_allocation out;
rs_allocation in;

rs_script script;

void root(const uchar4* v_in, uchar4* v_out, const void* usrData, uint32_t x,
          uint32_t y)
{
   float4 current = rsUnpackColor8888(*v_in);
   float4 upperLeft = { 0, 0, 0, 0 };
   if (x > 0 && y > 0)
      upperLeft = rsUnpackColor8888(*(uchar*) rsGetElementAt(in, x-1, y-1));
   float rDiff = current.r-upperLeft.r;
   float gDiff = current.g-upperLeft.g;
   float bDiff = current.b-upperLeft.b;
   float diff = rDiff;
   if (fabs(gDiff) > fabs(diff))
      diff = gDiff;
   if (fabs(bDiff) > fabs(diff))
      diff = bDiff;
   float grayLevel = fmax(fmin(0.5f+diff, 1.0f), 0);
   current.r = grayLevel;
   current.g = grayLevel;
   current.b = grayLevel;
   *v_out = rsPackColorTo8888(current.r, current.g, current.b, current.a);
}

void filter()
{
   rsDebug("RS_VERSION = ", RS_VERSION);
#if !defined(RS_VERSION) || (RS_VERSION < 14)
   rsForEach(script, in, out, 0);
#else
   rsForEach(script, in, out);
#endif
}