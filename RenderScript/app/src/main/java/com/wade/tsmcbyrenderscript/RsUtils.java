package com.wade.tsmcbyrenderscript;

import android.content.ContentValues;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.v8.renderscript.Allocation;
import android.support.v8.renderscript.Element;
import android.support.v8.renderscript.RSRuntimeException;
import android.support.v8.renderscript.RenderScript;
import android.support.v8.renderscript.ScriptIntrinsicBlur;
import android.support.v8.renderscript.ScriptIntrinsicBlend;
import android.util.Log;
import android.widget.ImageView;

import ca.tutortutor.embossimage.ScriptC_emboss;

/**
 * Created by wade on 2018/2/26.
 * RsUtils 剛好當範例，RenderScript 除了內建的 Allocation, Element, RenderScript 外，還有底下
 * ScriptIntrinsic3DLUT
 • ScriptIntrinsicBlend
 • ScriptIntrinsicBlur
 • ScriptIntrinsicColorMatrix
 • ScriptIntrinsicConvolve3x3
 • ScriptIntrinsicConvolve5x5
 • ScriptIntrinsicLUT
 • ScriptIntrinsicYuvToRGB
 • ScriptGroup
 */
public class RsUtils {

    private static final float BITMAP_SCALE = 0.4f;

    //Set the radius of the Blur. Supported range 0 < radius <= 25
    private static float BLUR_RADIUS = 10.5f;
    RenderScript mRS;
    Context me;

    public RsUtils(Context context) {
        me = context;
        mRS = RenderScript.create(me);
    }

    /**************************************************
     單點處理，平行處理
     **************************************************/
    public Bitmap mono(Bitmap image) {
        Bitmap bitmapOut = Bitmap.createBitmap(image.getWidth(), image.getHeight(),
                image.getConfig());

        Allocation allocationIn = Allocation.createFromBitmap(mRS, image,
                Allocation.MipmapControl.MIPMAP_NONE,
                Allocation.USAGE_SCRIPT);
        Allocation allocationOut = Allocation.createFromBitmap(mRS, bitmapOut,
                Allocation.MipmapControl.MIPMAP_NONE,
                Allocation.USAGE_SCRIPT);
        ScriptC_mono scriptMe = new ScriptC_mono(mRS);
        scriptMe.forEach_root(allocationIn, allocationOut);
        allocationOut.copyTo(bitmapOut);

        allocationIn.destroy();
        allocationOut.destroy();
        scriptMe.destroy();

        return bitmapOut;
    }

    public Bitmap histEq(Bitmap image) {
        int width = image.getWidth();
        int height = image.getHeight();
        Bitmap bitmapOut = image.copy(image.getConfig(), true);

        Allocation allocationA = Allocation.createFromBitmap(mRS, bitmapOut);
        Allocation allocationB = Allocation.createTyped(mRS, allocationA.getType());

        ScriptC_histEq scriptMe = new ScriptC_histEq(mRS);
        scriptMe.set_size(width*height);
        scriptMe.forEach_root(allocationA, allocationB);
        scriptMe.invoke_createRemapArray();
        scriptMe.forEach_remaptoRGB(allocationB, allocationA);
        allocationA.copyTo(bitmapOut);

        allocationA.destroy();
        allocationB.destroy();
        scriptMe.destroy();

        return bitmapOut;
    }

    public Bitmap blur(Bitmap image, float blurRadius) {
        Bitmap bitmapOut = null;

        if (image != null) {
            if (blurRadius == 0) { return image; }
            if (blurRadius < 1) { blurRadius = 1; }
            if (blurRadius > 25) { blurRadius = 25; }

            Log.d("MyLog1", "blur R="+blurRadius);
            BLUR_RADIUS = blurRadius;

            int width = Math.round(image.getWidth() * BITMAP_SCALE);
            int height = Math.round(image.getHeight() * BITMAP_SCALE);

            Bitmap bitmapIn = Bitmap.createScaledBitmap(image, width, height, false);
            bitmapOut = Bitmap.createBitmap(bitmapIn);

            ScriptIntrinsicBlur scriptIntrinsicBlur = ScriptIntrinsicBlur.create(mRS, Element.U8_4(mRS));
            Allocation allocationIn = Allocation.createFromBitmap(mRS, bitmapIn);
            Allocation allocationOut = Allocation.createFromBitmap(mRS, bitmapOut);
            scriptIntrinsicBlur.setRadius(BLUR_RADIUS);
            scriptIntrinsicBlur.setInput(allocationIn);
            scriptIntrinsicBlur.forEach(allocationOut);
            allocationOut.copyTo(bitmapOut);

            allocationIn.destroy();
            allocationOut.destroy();
            scriptIntrinsicBlur.destroy();
        }

        return bitmapOut;
    }

    // 變明亮，因為是與原圖合成，最暗跟原圖一樣
    /*
      filter 有很多種:
      Color Fx        | Geometry   |    *       | Borders
      Color transform | Crop       | Contrast   | Image-based
                        Straighten | Saturation | Parametric
                        Rotate     | Local
                        Mirror     | Vignette

     */
    public Bitmap bright(Bitmap bitmap, float b) {
        Bitmap bitmapOut = null;
        if (bitmap != null) {
            ScriptIntrinsicBlend blend = ScriptIntrinsicBlend.create(mRS, Element.U8_4(mRS));
            bitmapOut = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(),
                    bitmap.getConfig());
            Allocation allocationIn = Allocation.createFromBitmap(mRS, bitmap,
                    Allocation.MipmapControl.MIPMAP_NONE,
                    Allocation.USAGE_SCRIPT);
            Allocation allocationOut = Allocation.createFromBitmap(mRS, bitmapOut,
                    Allocation.MipmapControl.MIPMAP_NONE,
                    Allocation.USAGE_SCRIPT);
            ScriptC_bright filter = new ScriptC_bright(mRS);
            filter.set_brightPassThreshold(b);
            filter.forEach_brightPass(allocationIn, allocationOut);

            blend.forEachAdd(allocationIn, allocationOut);
            allocationOut.copyTo(bitmapOut);

            allocationIn.destroy();
            allocationOut.destroy();
            filter.destroy();
            blend.destroy();
        }
        return bitmapOut;
    }

    public Bitmap binarize(Bitmap image) {
        Bitmap bitmapOut = Bitmap.createBitmap(image.getWidth(), image.getHeight(),
                image.getConfig());

        Allocation allocationIn = Allocation.createFromBitmap(mRS, image,
                Allocation.MipmapControl.MIPMAP_NONE,
                Allocation.USAGE_SCRIPT);
        Allocation allocationOut = Allocation.createFromBitmap(mRS, bitmapOut,
                Allocation.MipmapControl.MIPMAP_NONE,
                Allocation.USAGE_SCRIPT);
        ScriptC_binarize scriptMe = new ScriptC_binarize(mRS);
        scriptMe.forEach_root(allocationIn, allocationOut);
        allocationOut.copyTo(bitmapOut);

        allocationIn.destroy();
        allocationOut.destroy();
        scriptMe.destroy();

        return bitmapOut;
    }

    public Bitmap grayscale(Bitmap image) {
        Bitmap bitmapOut = Bitmap.createBitmap(image.getWidth(), image.getHeight(),
                image.getConfig());

        Allocation allocationIn = Allocation.createFromBitmap(mRS, image,
                Allocation.MipmapControl.MIPMAP_NONE,
                Allocation.USAGE_SCRIPT);
        Allocation allocationOut = Allocation.createFromBitmap(mRS, bitmapOut,
                Allocation.MipmapControl.MIPMAP_NONE,
                Allocation.USAGE_SCRIPT);
        ScriptC_grayscale scriptMe = new ScriptC_grayscale(mRS);
        scriptMe.forEach_root(allocationIn, allocationOut);
        allocationOut.copyTo(bitmapOut);

        allocationIn.destroy();
        allocationOut.destroy();
        scriptMe.destroy();

        return bitmapOut;
    }

    public Bitmap sketch(Bitmap image) {
        Bitmap bitmapOut = Bitmap.createBitmap(image.getWidth(), image.getHeight(), image.getConfig());
        Allocation allocationIn = Allocation.createFromBitmap(mRS, image,
                Allocation.MipmapControl.MIPMAP_NONE,
                Allocation.USAGE_SCRIPT);
        Allocation allocationOut = Allocation.createFromBitmap(mRS, bitmapOut,
                Allocation.MipmapControl.MIPMAP_NONE,
                Allocation.USAGE_SCRIPT);

        ScriptC_sketch scriptMe = new ScriptC_sketch(mRS);
        scriptMe.forEach_root(allocationIn, allocationOut);
        allocationOut.copyTo(bitmapOut);

        allocationIn.destroy();
        allocationOut.destroy();
        scriptMe.destroy();

        return bitmapOut;
    }

    public Bitmap magnifier(Bitmap image, int x, int y) {
        if (x <= 0 || y <= 0) return Bitmap.createBitmap(image);

        Bitmap bitmapOut = Bitmap.createBitmap(image.getWidth(), image.getHeight(), image.getConfig());
        Allocation allocationIn = Allocation.createFromBitmap(mRS, image,
                Allocation.MipmapControl.MIPMAP_NONE,
                Allocation.USAGE_SCRIPT);
        Allocation allocationOut = Allocation.createFromBitmap(mRS, bitmapOut,
                Allocation.MipmapControl.MIPMAP_NONE,
                Allocation.USAGE_SCRIPT);

        ScriptC_magnifier scriptMe = new ScriptC_magnifier(mRS);
        scriptMe.set_inputAllocation(allocationIn);
        scriptMe.set_atX(x);
        scriptMe.set_atY(y);
        scriptMe.set_radius(100);
        scriptMe.set_scale(2);

        scriptMe.forEach_root(allocationIn, allocationOut);
        allocationOut.copyTo(bitmapOut);

        allocationIn.destroy();
        allocationOut.destroy();
        scriptMe.destroy();

        return bitmapOut;
    }

    /**************************************************
     影像處理，非平行處理
     **************************************************/

    private Allocation createAllocation(Bitmap bitmap) {
        int[] pixels = new int[bitmap.getWidth() * bitmap.getHeight()];
        bitmap.getPixels(pixels, 0, bitmap.getWidth(), 0, 0, bitmap.getWidth(), bitmap.getHeight());

        Allocation allocation = Allocation.createSized(mRS,
                Element.I32(mRS), bitmap.getWidth() * bitmap.getHeight(), Allocation.USAGE_SCRIPT);
        allocation.copyFrom(pixels);

        return allocation;
    }

    private Bitmap getBitmapFromAllocation(Allocation allocation, Bitmap bitmap) {
        int[] pixels = new int[bitmap.getWidth() * bitmap.getHeight()];
        allocation.copyTo(pixels);
        bitmap.setPixels(pixels, 0, bitmap.getWidth(), 0, 0, bitmap.getWidth(), bitmap.getHeight());

        return bitmap;
    }

    public Bitmap sobel(Bitmap image) throws RSRuntimeException {
        Bitmap bitmapOut = Bitmap.createBitmap(image.getWidth(), image.getHeight(), image.getConfig());
        ScriptC_sobel scriptMe = new ScriptC_sobel(mRS);
        Allocation allocationIn = createAllocation(image);
        Allocation allocationOut = Allocation.createTyped(mRS, allocationIn.getType());

        scriptMe.set_gIn(allocationIn);
        scriptMe.set_gOut(allocationOut);
        scriptMe.set_mImageHeight(image.getHeight());
        scriptMe.set_mImageWidth(image.getWidth());
        scriptMe.set_gScript(scriptMe);
        scriptMe.bind_gOutPixels(allocationOut);

        scriptMe.invoke_compute();

        bitmapOut = getBitmapFromAllocation(allocationOut, bitmapOut);
        Log.d("MyLog", "sobel Input("+image.getWidth()+","+image.getHeight()
                +"), Output("+bitmapOut.getWidth()+","+bitmapOut.getHeight()+")");
//        allocationIn.destroy();
//        allocationOut.destroy();
//        scriptMe.destroy();

        return bitmapOut;
    }

    public Bitmap emboss(Bitmap image) {
        Bitmap bitmapOut = Bitmap.createBitmap(image.getWidth(), image.getHeight(), image.getConfig());
        ScriptC_emboss scriptMe = new ScriptC_emboss(mRS);
        Allocation allocationIn = Allocation.createFromBitmap(mRS, image,
                Allocation.MipmapControl.MIPMAP_NONE,
                Allocation.USAGE_SCRIPT);
        Allocation allocationOut = Allocation.createTyped(mRS, allocationIn.getType());

        scriptMe.set_in(allocationIn);
        scriptMe.set_out(allocationOut);
        scriptMe.set_script(scriptMe);

        scriptMe.invoke_filter();
        allocationOut.copyTo(bitmapOut);

        allocationIn.destroy();
        allocationOut.destroy();
        scriptMe.destroy();

        return bitmapOut;
    }
}
