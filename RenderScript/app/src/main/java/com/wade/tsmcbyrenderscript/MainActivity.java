package com.wade.tsmcbyrenderscript;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v8.renderscript.Allocation;
import android.support.v8.renderscript.RenderScript;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

public class MainActivity extends Activity {
    private Bitmap mBitmapIn;
    private RenderScript mRS;
    private RsUtils rsUtils;
    private float blur = 10.5f;
    private float bright = 0.15f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        mBitmapIn = loadBitmap(R.drawable.data);
        rsUtils = new RsUtils(this);

        ImageView in = (ImageView) findViewById(R.id.displayin);
        in.setImageBitmap(mBitmapIn);

        createScript();
    }

    private void createScript() {
        mRS = RenderScript.create(this);

        createScriptMono();
        createScriptHistEq();
        createScriptBlur();
        createScriptBright();
        createScriptInvert();

        createScriptBinarize();
        createScriptGrayScale();
        createScriptSobel();
        createScriptSketch();
        createScriptMagnifier();
        createScriptEmboss();

        mRS.destroy();
    }

    private Bitmap loadBitmap(int resource) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        return BitmapFactory.decodeResource(getResources(), resource, options);
    }

    // 轉成黑白
    private void createScriptMono() {
        ImageView imageViewMe = (ImageView) findViewById(R.id.displaymono);
        imageViewMe.setImageBitmap(rsUtils.mono(mBitmapIn));
    }

    // 做 HistogramEqualization
    private void createScriptHistEq() {
        ImageView imageViewMe = (ImageView) findViewById(R.id.displayhisteq);
        imageViewMe.setImageBitmap(rsUtils.histEq(mBitmapIn));
    }

    // 模糊化, Blur 把 code 移至 RsUtils class, 而且沒有獨立的 .rs
    private void createScriptBlur() {
        ImageView imageViewMe = (ImageView) findViewById(R.id.displayblur);
        imageViewMe.setImageBitmap(rsUtils.blur(mBitmapIn, blur));
        // 按圖的左半部會變清晰，右半部會變模糊
        imageViewMe.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: break;
                    case MotionEvent.ACTION_MOVE: break;
                    case MotionEvent.ACTION_UP:
                        float W = v.getWidth();
                        int x = (int)event.getX();
                        v.performClick();
                        if (x < W/2) {
                            blur /= 1.1;
                        } else blur *= 1.1;
                        ((ImageView)v).setImageBitmap(rsUtils.blur(mBitmapIn, blur));
                        break;
                    default:
                        break;
                }
                return true;
            }
        });
    }

    // 變亮
    private void createScriptBright() {
        ImageView imageViewMe = (ImageView) findViewById(R.id.displaybright);
        imageViewMe.setImageBitmap(rsUtils.bright(mBitmapIn, bright));
        // 按圖的左半部會變清晰，右半部會變模糊
        imageViewMe.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: break;
                    case MotionEvent.ACTION_MOVE: break;
                    case MotionEvent.ACTION_UP:
                        float W = v.getWidth();
                        int x = (int)event.getX();
                        v.performClick();
                        if (x < W/2) {
                            bright /= 1.1;
                        } else bright *= 1.1;
                        Log.d("MyLog1", "Bright = "+bright);
                        ((ImageView)v).setImageBitmap(rsUtils.bright(mBitmapIn, bright));
                        break;
                    default:
                        break;
                }
                return true;
            }
        });
    }

    // 轉成黑白
    private void createScriptInvert() {
        Bitmap imageViewMe = Bitmap.createBitmap(mBitmapIn.getWidth(), mBitmapIn.getHeight(),
                mBitmapIn.getConfig());

        Allocation mInAllocation = Allocation.createFromBitmap(mRS, mBitmapIn,
                Allocation.MipmapControl.MIPMAP_NONE,
                Allocation.USAGE_SCRIPT);
        Allocation mInvertAllocation = Allocation.createFromBitmap(mRS, imageViewMe,
                Allocation.MipmapControl.MIPMAP_NONE,
                Allocation.USAGE_SCRIPT);
        ScriptC_invert mScriptInvert = new ScriptC_invert(mRS);
        mScriptInvert.forEach_invert(mInAllocation, mInvertAllocation);
        mInvertAllocation.copyTo(imageViewMe);

        ImageView blackwhite = (ImageView) findViewById(R.id.displayinvert);
        blackwhite.setImageBitmap(imageViewMe);

        mInAllocation.destroy();
        mInvertAllocation.destroy();
    }

    private void createScriptBinarize() {
        ImageView imageViewMe = (ImageView) findViewById(R.id.displaybinarize);
        imageViewMe.setImageBitmap(rsUtils.binarize(mBitmapIn));
    }

    private void createScriptGrayScale() {
        ImageView imageViewMe = (ImageView) findViewById(R.id.displaygrayscale);
        imageViewMe.setImageBitmap(rsUtils.grayscale(mBitmapIn));
    }

    private void createScriptSketch() {
        ImageView imageViewMe = (ImageView) findViewById(R.id.displaysketch);
        imageViewMe.setImageBitmap(rsUtils.sketch(mBitmapIn));
    }

    // 放大鏡
    private void createScriptMagnifier() {
        ImageView imageViewMe = (ImageView) findViewById(R.id.displaymagnifier);
        imageViewMe.setImageBitmap(rsUtils.magnifier(mBitmapIn, -1, -1));
        // 按圖的左半部會變清晰，右半部會變模糊
        imageViewMe.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: break;
                    case MotionEvent.ACTION_MOVE: break;
                    case MotionEvent.ACTION_UP:
                        v.performClick();

                        float W = v.getWidth();
                        int vx = (int)event.getX();
                        int vy = (int)event.getY();
                        int iw = (int)mBitmapIn.getWidth();
                        int ih = (int)mBitmapIn.getHeight();
                        int vw = (int)v.getWidth();
                        int vh = (int)v.getHeight();
                        int x, y;

                        if (iw*vh < vw*ih) { // 按高度比, 橫向有間隔
                            int tw = (int)(iw * vh / ih);
                            int th = vh;
                            x = (vx - (vw-tw)/2) * ih / vh;
                            y = vy * ih / vh;

//                            Log.d("MyLog1", String.format("Magnifier<@(%d,%d)/(%d,%d)/(%d,%d) image(%d,%d), view(%d,%d)",
//                                    vx, vy, tw, th, x, y, iw, ih, vw, vh));
                        } else {
                            int tw = vw;
                            int th = (int)(ih * vw / iw);
                            x = vx * iw / vw;
                            y = (vy - (vh-th)/2) * iw / vw;

//                            Log.d("MyLog1", String.format("Magnifier>(%.1f,%.1f)@(%d,%d)/(%d,%d)/(%d,%d) image(%d,%d), view(%d,%d)",
//                                    vx, vy, tw, th, x, y, iw, ih, vw, vh));
                        }

                        ((ImageView)v).setImageBitmap(rsUtils.magnifier(mBitmapIn, x, y));
                        break;
                    default:
                        break;
                }
                return true;
            }
        });
    }

    /**************************************************
     影像處理，非平行處理
     **************************************************/

    private void createScriptSobel() {
        ImageView imageViewMe = (ImageView) findViewById(R.id.displaysobel);
        imageViewMe.setImageBitmap(rsUtils.sobel(mBitmapIn));
    }

    private void createScriptEmboss() {
        ImageView imageViewMe = (ImageView) findViewById(R.id.displayemboss);
        imageViewMe.setImageBitmap(rsUtils.emboss(rsUtils.mono(mBitmapIn)));
    }
}
