package com.wade.tsmcbyrenderscript;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v8.renderscript.Allocation;
import android.support.v8.renderscript.RenderScript;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
    ImageView mImageView;
    private RenderScript mRs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mImageView = (ImageView) findViewById(R.id.imageView);
        mRs = RenderScript.create(this);
        Background b = new Background();
        b.execute();
    }
    class Background extends AsyncTask<Void, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(Void... params) {
            Bitmap img = Bitmap.createBitmap(1080, 1920, Bitmap.Config.ARGB_8888);
            Allocation alloc = Allocation.createFromBitmap(mRs, img);
            ScriptC_mono foo = new ScriptC_mono(mRs);
            foo.forEach_bar(alloc);
            alloc.copyTo(img);
            return img;
        }
        @Override
        protected void onPostExecute(Bitmap bitmap) {
            mImageView.setImageBitmap(bitmap);
        }
    }
}
