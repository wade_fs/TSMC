#include <string.h>
#include "usage.h"
#include "rs232.h"

extern char* cmdFn;
extern char port[];
extern char baudrate[];

void usage(int argc, char *argv[]) {
  printf ("Usage: %s [-h] [-l] [-p #] [-b BAUDRATE] [CMD FILE]\n\
\t-h          : show this meesage\n\
\t-l          : list COM ports\n\
\t-p #        : assign serial port number, could be 0, 1, 2, 3, ...\n\
\t-b BAUDRATE : assign baudrate, could be 9600, 19200, 38400, 57600, 115200, 230400\n\
\tCMD FILE    : assign CMD file\n", argv[0]);
  exit(0);
}
int myGetopt(int argc, char *argv[]) {
  int opt;
  while ((opt = getopt (argc, argv, "hlp:b:")) != -1) {
    switch (opt) {
      case 'h':
        usage(argc, argv);
        break;
      case 'l':
        listPorts();
        break;
      case 'p':
        strcpy(port, optarg);
        break;
      case 'b':
        strcpy(baudrate, optarg);
        break;
    }
  }

  for ( ; optind < argc; optind++) {
    if (access(argv[optind], R_OK)) {
      cmdFn = strdup(argv[optind]);
      break;
    }
  }

  return 0;
}
