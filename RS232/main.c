#include <stdio.h>
#include "usage.h"
#include "rs232.h"

struct COM com;

char* cmdFn;
char port[80], path[80];
char baudrate[10];

int main(int argc, char* argv[])
{
  port[0] = baudrate[0] = 0;
  sprintf(port, "/dev/ttyS0");
  myGetopt(argc, argv);
  if (cmdFn != NULL) printf("CMD=%s\n", cmdFn);
  if (strlen(port)> 0) printf("port=%s\n", port);
  if (strlen(baudrate)> 0) printf("baudrate=%s\n", baudrate);
  if (openPort(&com, getPortPath(path, port), baudFromStr(baudrate)) != NULL) {
    printf ("port path = %s\n", com.path);
    closePort(&com);
  } else {
    printf ("無法開啟 port %s\n", com.path);
  }
  return 0;
}

/*
    char *portname = "/dev/ttyS0";
    int fd;
    int wlen;

    fd = open(portname, O_RDWR | O_NOCTTY | O_SYNC);
    if (fd < 0) {
        printf("Error opening %s: %s\n", portname, strerror(errno));
        return -1;
    }
    setPort(fd, B9600);
    set_mincount(fd, 0, 5);                // set to pure timed read //

    // simple output //
    wlen = write(fd, "Hello!\n", 7);
    if (wlen != 7) {
        printf("Error from write: %d, %d\n", wlen, errno);
    }
    tcdrain(fd);    // delay for output //


    // simple noncanonical input //
    do {
        unsigned char buf[80];
        int rdlen;

        rdlen = read(fd, buf, sizeof(buf) - 1);
        if (rdlen > 0) {
            unsigned char   *p;
            printf("Read %d:", rdlen);
            for (p = buf; rdlen-- > 0; p++)
                printf(" 0x%x", *p);
            printf("\n");
        } else if (rdlen < 0) {
            printf("Error from read: %d: %s\n", rdlen, strerror(errno));
        }
        // repeat read to get full message //
    } while (1);
*/

