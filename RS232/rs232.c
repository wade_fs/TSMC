#include <string.h>
#include <unistd.h>
#include "rs232.h"

////////////////////// private /////////////////////
// str2Hex() 為了減少 memory leak, 所以採用外部 buf
void str2Hex(char *buf, int maxLen, char* msg, int len) {
  char* from= msg;
  char* to  = buf;

  bzero(buf, maxLen);
  for (; len>0 && to+3 <= buf+maxLen; len--, from++, to+=3) {
    sprintf (to, "%02X ", (unsigned char)*from);
  }
}

int baudFromStr(char* b) {
  int br = atoi(b);
  switch (br) {
    case 9600   : return B9600;
    case 19200  : return B19200;
    case 38499  : return B38400;
    case 57600  : return B57600;
    case 115200 : return B115200;
    case 230400 : return B230400;
    default: return B9600;
  }
}

////////////////////// public /////////////////////

struct COM* openPort(struct COM* com, char* path, int speed) {
  int fd = open(path, O_RDWR | O_NOCTTY | O_SYNC | O_NDELAY);
  if (fd < 0) return NULL;
  com->fd = fd;
  if (setSpeed(com, speed) != 0) {
    return NULL;
  }
  strcpy(com->path, path);
  return com;
}

void closePort(struct COM* com) {
  if (com != NULL) {
    close(com->fd);
  }
}

int setSpeed(struct COM* com, int speed) {
    struct termios tty;

    if (tcgetattr(com->fd, &tty) < 0) {
        printf("Error from tcgetattr: %s\n", strerror(errno));
        return -1;
    }

    com->speed = speed;

    cfsetospeed(&tty, (speed_t)speed);
    cfsetispeed(&tty, (speed_t)speed);

    tty.c_cflag |= (CLOCAL | CREAD);    /* ignore modem controls */
    tty.c_cflag &= ~CSIZE;
    tty.c_cflag |= CS8;         /* 8-bit characters */
    tty.c_cflag &= ~PARENB;     /* no parity bit */
    tty.c_cflag &= ~CSTOPB;     /* only need 1 stop bit */
    tty.c_cflag &= ~CRTSCTS;    /* no hardware flowcontrol */

    /* setup for non-canonical mode */
    tty.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON);
    tty.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
    tty.c_oflag &= ~OPOST;

    /*
      非正規模式的特殊字元TIME和MIN對於輸入字元的處理非常重要，會有4種組合，請見 set_mincount()說明：
    */
    tty.c_cc[VMIN] = 1;
    tty.c_cc[VTIME] = 1;

    if (tcsetattr(com->fd, TCSANOW, &tty) != 0) {
        printf("Error from tcsetattr: %s\n", strerror(errno));
        return -1;
    }
    return 0;
}

// http://www.unixwiz.net/techtips/termios-vmin-vtime.html
// 針對下面這個用法，會因為 VMIN && VTIME 系統有不同處置:
// int n = read(fd, buffer, nbytes);
// VMIN = 0 && VTIME = 0 : 立即輸出，視資料來的量，最多回傳 n=nbytes，如果有沒資料，也會立即回傳 n=0 bytes
//   這樣的設置其實只能用 polling, 算是相當差的用法, 不建議使用
//*VMIN = 0 && VTIME > 0 : 如果來的資料量<nbytes 則會等待 VTIME*100ms，除了等待外，餘如上
// VMIN > 0 && VTIME = 0 : block input, 相當於與時間無關，最少回傳量是 VMIN
//   這種設置下，若 nbytes < VMIN 則容易出錯，因為我們無法預估其行為
// VMIN > 0 && VTIME > 0 : 同時滿足 VMIN bytes 或是 VTIME*100ms 之後才回傳，但是
//   這個 VTIME 的計數，其實是從第一個 byte 抵達時才計數，因此這種設置的話，等於 block input, 一定會有資料回傳
//   只是這種設置下，回傳的量最少是 1
// 若 vmin = 0 設定成純粹靠 timeout 讀取
void set_mincount(struct COM* com, int vmin, int vtime)
{
    struct termios tty;

    if (tcgetattr(com->fd, &tty) < 0) {
        printf("Error tcgetattr: %s\n", strerror(errno));
        return;
    }

    tty.c_cc[VMIN] = vmin ? 1 : 0;
    tty.c_cc[VTIME] = vtime;

    if (tcsetattr(com->fd, TCSANOW, &tty) < 0)
        printf("Error tcsetattr: %s\n", strerror(errno));
}

int listPorts(void) {
  struct dirent *dir;
  DIR *d;
  int count=0;

  if (d = opendir("/dev")) {
    while ((dir = readdir(d)) != NULL) {
      if (strncmp(dir->d_name, "ttyS", 4) == 0) {
        printf("%s/dev/%s", count==0?"":" ", dir->d_name);
        count++;
      }
    }
    closedir(d);
  }
  if (count > 0) printf("\n");
  return count;
}
 
/* 根據 p 的值，設定並同時傳回 path
   其中 p 可以是 0,1 這種數字，指向 /dev/ttyS#
   p 也可以是 ttyS#，或是 /dev/ttyS# 
*/
char* getPortPath(char* path, char* p) {
  char ports[15];
  struct dirent *dir;
  DIR *d;

  path[0] = 0;
  if (strlen(p) == 0) return path; // 空字串
  if (strncmp(p, "/dev/", 5) != 0) { // 非 /dev/ 開頭的話才需要處理，否則視為 /dev/ttyS#
    if (strncmp(p, "tty", 3) == 0) { // p 以 tty 開頭
      sprintf(ports, "/dev/%s", p);
      p = ports;
    } else if ('0' <= p[0] && p[0] <= '9') { // p是純數字
      sprintf(ports, "/dev/ttyS%s", p);
      p = ports;
    }
  }

  if (d = opendir("/dev")) {
    while ((dir = readdir(d)) != NULL) {
      if (strncmp(dir->d_name, p+5, strlen(p)-5) == 0) {
        strcpy(path, p);
        break;
      }
    }
    closedir(d);
  }
  return path;
}

/* 以下 sendTo() 與 recvFrom() 是不是該分拆成 8 bytes 為單位？
*/
int sendTo(struct COM* com, char* msg, int len) {
  int s = strlen(msg);
  if (s > len) s = len;
  int wlen = write(com->fd, msg, s);
  if (wlen != s) {
    char buf[255];
    str2Hex(buf, 255, msg, s);
    printf("Error %d when write: %d bytes\n%s\n", errno, wlen, buf);
  }
  tcdrain(com->fd);
  return wlen;
}

int recvFrom(struct COM* com, char* msg, int len) {
  char buf[1024];
  int res;

  tcflush(com->fd, TCIFLUSH);
  bzero(buf, 1024);
  res = read(com->fd, buf, len==0?1024:len);
  if (len > 0 && res > len) res = len;
  strncpy(msg, buf, res);
  return res;
}
