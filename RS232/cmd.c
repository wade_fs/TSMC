#include "rs232.h"

/**********************************************************************************************
 * 內部命令區，僅供對外命令使用
 **********************************************************************************************/
// cmdSystemReset() 是內部使用的, 由 cmdStartCmd() 呼叫
// COM3 Write(hex): 7(Bytes)00 05 08 00 00 08 00
// COM3 Read(hex): 8(Bytes) 06 00 05 09 00 00 09 00
// COM3 Write(hex): 1(Bytes)06

int cmdSystemReset(struct COM* com) {
  char cmd[] = { 0x00, 0x05, 0x08, 0x00, 0x00, 0x08, 0x00 };
  char buf[10];
  if (sendTo(com, cmd, 7) != 7) return -1;
  // 接下來應該有兩段(1+7)，或是一段(=8)
  bzero(buf, 10);
  // 這邊應該要讀到 0x06
  if (recvFrom(com, buf, 1) != 1 || buf[0] != 6) return -1;
  if (recvFrom(com, buf, 7) != 7 || !(buf[0]==0 && buf[1]==5 &&
      buf[2]==9 && buf[3]==0 && buf[4]==0 && buf[5]==9 && buf[6]==0)) return -1;
  cmd[0] = 0x06;
  if (sendTo(com, cmd, 1) != 1) return -1;
  return 0;
}
// 目前認定，每個命令之前會送 StartCmd....所以，這個函式也是內部使用的，由各命令呼叫
// COM1 Write(hex): 4(Bytes)FC FF B5 FF
// COM1 Read(hex): 1(Bytes) CA
int cmdStartCmd(struct COM* com) {
  char cmd[] = { 0xFC, 0xFF, 0xB5, 0xFF };
  char buf[10];
  int ok = 0;

  for (int i=0; i<4; i++) {
    if (sendTo(com, cmd, 4) != 4) break;
    bzero(buf, 10);
    // 這邊應該要讀到 0xCA, 如果沒讀到，持續送
    if (recvFrom(com, buf, 1) != 1 || buf[0] != 0xCA) continue;
    else { ok = 1; break; }
  }
  if (ok) return 0;
  else return cmdSystemReset(com);
}

/**********************************************************************************************
 * 真正提供的命令區
 **********************************************************************************************/
/* TODO: 最後面用到遞迴呼叫可能有點危險，而且所有通訊都沒有 delay, 我猜也會出問題
COM1 Write(hex): 11(Bytes)FF FF DF FF F9 FF FF F9 FF FF FF
COM1 Read(hex): 1(Bytes) 60
*/
int cmdLcdOn(struct COM* com) {
  char cmd[] = { 0xFF, 0xFF, 0xFD, 0xFF, 0xF9, 0xFF, 0xFF, 0xF9, 0xFF, 0xFF, 0xFF };
  char buf[10];
  int ok = 0;

  if (cmdStartCmd(com) == 0 && sendTo(com, cmd, 11) == 11) {
    bzero(buf, 10);
    // 這邊應該要讀到 0x60...
    if (recvFrom(com, buf, 1) == 1 && buf[0] == 0x60) return 0;
  }
  if (cmdSystemReset(com) == 0) cmdLcdOn(com);
}

/* READ TAG
COM1 Write(hex): 8(Bytes)FF FF EF FF F7 FF FF 07
COM1 Write(hex): 7(Bytes)F0 FF 7F FB FF 7F FF
COM1 Read(hex): 246(Bytes)
60 00 8F 0A 00 00 00 00 80 00 00 00 70 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 8D 80 00 00
78 00 00 00 A2 B2 0A 5C 2A A2 CA 2A 04 04 04 04 04 04 04 04 04 0C F4 2A A2 CA 2A FA 32 F2 2A 04
04 F4 0C 04 04 04 04 04 04 04 04 04 F4 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04
04 04 44 04 32 F2 C2 5C 04 04 04 04 04 04 04 04 04 04 04 04 72 F2 4A B2 32 5C 4C 0C 8C EC F4 8C
8C F4 8C 2C 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04 8A 22 AA A2 5C 04 04 04 04 04 04 04
04 04 04 04 72 F2 2A A2 8C 5C 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04
04 04 04 04 72 F2 2A A2 4C 5C 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04 04
04 04 04 04 6A A6 4E CE 96 F6 76 04 CC 74 EC 04 04 04 04 04 DE A4
COM1 Write(hex): 2(Bytes)9F FF
*/
