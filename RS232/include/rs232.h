#ifndef __RS232__
#define __RS232__
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <dirent.h>

////////// 結構 //////////
struct COM {
  int fd;
  int speed;
  char path[15];
};

////////// provate //////////
void str2Hex(char *buf, int maxLen, char* msg, int len);
int baudFromStr(char* b);

////////// public //////////
struct COM* openPort(struct COM* com, char* path, int speed);
void closePort(struct COM* com);
int setSpeed(struct COM* com, int speed);
void set_mincount(struct COM* com, int vmin, int vtime);
int listPorts(void);
char* getPortPath(char* path, char* p);
int sendTo(struct COM* com, char* msg, int len);
int recvFrom(struct COM* com, char* msg, int len);

#endif
