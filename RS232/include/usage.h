#ifndef __USAGE__
#define __USAGE__
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

void usage(int argc, char *argv[]);
int myGetopt(int argc, char *argv[]);
#endif
