#ifndef __CMD__
#define __CMD__

/* 內部使用的命令 */
int cmdSystemReset(struct COM* com);
int cmdStartCmd(struct COM* com);

/* 公開命令 */
int cmdLcdOn(struct COM* com);
  
#endif
