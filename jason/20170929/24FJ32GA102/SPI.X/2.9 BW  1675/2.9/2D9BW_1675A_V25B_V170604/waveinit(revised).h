/*
//2.1��
//Creat by [Waveform Look Up Table.exe]
//2011-12-08 09:53:53
const unsigned char init_data[]={
0x82,0x00,0x00,0x00,0xAA,0x00,0x00,0x00,
0xAA,0xAA,0x00,0x00,0xAA,0xAA,0xAA,0x00,
0x55,0xAA,0xAA,0x00,0x55,0x55,0x55,0x55,
0xAA,0xAA,0xAA,0xAA,0x55,0x55,0x55,0x55,
0xAA,0xAA,0xAA,0xAA,0x15,0x15,0x15,0x15,
0x05,0x05,0x05,0x05,0x01,0x01,0x01,0x01,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x41,0x45,0xF1,0xFF,0x5F,0x55,0x01,0x00,
0x00,0x00,};
*/

//2.9��
//Creat by Yuhu Lin
//2014-04-16 
const unsigned char init_data[]={
0x50,
0xAA,
0x55,
0xAA,
0x11,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0xFF,
0xFF,
0x1F,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,

};

//OTP, WS0, Default
const unsigned char init_data0[]={
//30byte
0x50,	//OTP-byte 1
0xAA,	//OTP-byte 2
0x55,	//OTP-byte 3
0xAA,	//OTP-byte 4
0x11,	//OTP-byte 5
0x00,	//OTP-byte 6
0x00,	//OTP-byte 7
0x00,	//OTP-byte 8
0x00,	//OTP-byte 9
0x00,	//OTP-byte 10
0x00,	//OTP-byte 11
0x00,	//OTP-byte 12
0x00,	//OTP-byte 13
0x00,	//OTP-byte 14
0x00,	//OTP-byte 15
0x00,	//OTP-byte 16
0x00,	//OTP-byte 17
0x00,	//OTP-byte 18
0x00,	//OTP-byte 19
0x00,	//OTP-byte 20
0xFF,	//OTP-byte 21
0xFF,	//OTP-byte 22
0x1F,	//OTP-byte 23
0x00,	//OTP-byte 24
0x00,	//OTP-byte 25
0x00,	//OTP-byte 26
0x00,	//OTP-byte 27
0x00,	//OTP-byte 28
0x00,	//OTP-byte 29
0x00,	//OTP-byte 30
0x0A,	//VSH/VSL
0x00,	//NA
};

//OTP, WS1, TR1
const unsigned char init_data1[]={
//30byte
0x50,	//OTP-byte 1
0xAA,	//OTP-byte 2
0x55,	//OTP-byte 3
0xAA,	//OTP-byte 4
0x11,	//OTP-byte 5
0x00,	//OTP-byte 6
0x00,	//OTP-byte 7
0x00,	//OTP-byte 8
0x00,	//OTP-byte 9
0x00,	//OTP-byte 10
0x00,	//OTP-byte 11
0x00,	//OTP-byte 12
0x00,	//OTP-byte 13
0x00,	//OTP-byte 14
0x00,	//OTP-byte 15
0x00,	//OTP-byte 16
0x00,	//OTP-byte 17
0x00,	//OTP-byte 18
0x00,	//OTP-byte 19
0x00,	//OTP-byte 20
0xFF,	//OTP-byte 21
0xFF,	//OTP-byte 22
0x1F,	//OTP-byte 23
0x00,	//OTP-byte 24
0x00,	//OTP-byte 25
0x00,	//OTP-byte 26
0x00,	//OTP-byte 27
0x00,	//OTP-byte 28
0x00,	//OTP-byte 29
0x00,	//OTP-byte 30
0x0A,	//VSH/VSL
0x00,	//NA
};

//OTP, WS2, TR2
const unsigned char init_data2[]={
//30byte
0x50,	//OTP-byte 1
0xAA,	//OTP-byte 2
0x55,	//OTP-byte 3
0xAA,	//OTP-byte 4
0x11,	//OTP-byte 5
0x00,	//OTP-byte 6
0x00,	//OTP-byte 7
0x00,	//OTP-byte 8
0x00,	//OTP-byte 9
0x00,	//OTP-byte 10
0x00,	//OTP-byte 11
0x00,	//OTP-byte 12
0x00,	//OTP-byte 13
0x00,	//OTP-byte 14
0x00,	//OTP-byte 15
0x00,	//OTP-byte 16
0x00,	//OTP-byte 17
0x00,	//OTP-byte 18
0x00,	//OTP-byte 19
0x00,	//OTP-byte 20
0xFF,	//OTP-byte 21
0xFF,	//OTP-byte 22
0x1F,	//OTP-byte 23
0x00,	//OTP-byte 24
0x00,	//OTP-byte 25
0x00,	//OTP-byte 26
0x00,	//OTP-byte 27
0x00,	//OTP-byte 28
0x00,	//OTP-byte 29
0x00,	//OTP-byte 30
0x0A,	//VSH/VSL
0x00,	//NA
};

//OTP, WS3, TR3
const unsigned char init_data3[]={
//30byte
0x50,	//OTP-byte 1
0xAA,	//OTP-byte 2
0x55,	//OTP-byte 3
0xAA,	//OTP-byte 4
0x11,	//OTP-byte 5
0x00,	//OTP-byte 6
0x00,	//OTP-byte 7
0x00,	//OTP-byte 8
0x00,	//OTP-byte 9
0x00,	//OTP-byte 10
0x00,	//OTP-byte 11
0x00,	//OTP-byte 12
0x00,	//OTP-byte 13
0x00,	//OTP-byte 14
0x00,	//OTP-byte 15
0x00,	//OTP-byte 16
0x00,	//OTP-byte 17
0x00,	//OTP-byte 18
0x00,	//OTP-byte 19
0x00,	//OTP-byte 20
0xFF,	//OTP-byte 21
0xFF,	//OTP-byte 22
0x1F,	//OTP-byte 23
0x00,	//OTP-byte 24
0x00,	//OTP-byte 25
0x00,	//OTP-byte 26
0x00,	//OTP-byte 27
0x00,	//OTP-byte 28
0x00,	//OTP-byte 29
0x00,	//OTP-byte 30
0x0A,	//VSH/VSL
0x00,	//NA
};

//OTP, WS4, TR4
const unsigned char init_data4[]={
//30byte
0x50,	//OTP-byte 1
0xAA,	//OTP-byte 2
0x55,	//OTP-byte 3
0xAA,	//OTP-byte 4
0x11,	//OTP-byte 5
0x00,	//OTP-byte 6
0x00,	//OTP-byte 7
0x00,	//OTP-byte 8
0x00,	//OTP-byte 9
0x00,	//OTP-byte 10
0x00,	//OTP-byte 11
0x00,	//OTP-byte 12
0x00,	//OTP-byte 13
0x00,	//OTP-byte 14
0x00,	//OTP-byte 15
0x00,	//OTP-byte 16
0x00,	//OTP-byte 17
0x00,	//OTP-byte 18
0x00,	//OTP-byte 19
0x00,	//OTP-byte 20
0xFF,	//OTP-byte 21
0xFF,	//OTP-byte 22
0x1F,	//OTP-byte 23
0x00,	//OTP-byte 24
0x00,	//OTP-byte 25
0x00,	//OTP-byte 26
0x00,	//OTP-byte 27
0x00,	//OTP-byte 28
0x00,	//OTP-byte 29
0x00,	//OTP-byte 30
0x0A,	//VSH/VSL
0x00,	//NA
};

//OTP, WS5, TR5
const unsigned char init_data5[]={
0x50,	//OTP-byte 1
0xAA,	//OTP-byte 2
0x55,	//OTP-byte 3
0xAA,	//OTP-byte 4
0x11,	//OTP-byte 5
0x00,	//OTP-byte 6
0x00,	//OTP-byte 7
0x00,	//OTP-byte 8
0x00,	//OTP-byte 9
0x00,	//OTP-byte 10
0x00,	//OTP-byte 11
0x00,	//OTP-byte 12
0x00,	//OTP-byte 13
0x00,	//OTP-byte 14
0x00,	//OTP-byte 15
0x00,	//OTP-byte 16
0x00,	//OTP-byte 17
0x00,	//OTP-byte 18
0x00,	//OTP-byte 19
0x00,	//OTP-byte 20
0xFF,	//OTP-byte 21
0xFF,	//OTP-byte 22
0x1F,	//OTP-byte 23
0x00,	//OTP-byte 24
0x00,	//OTP-byte 25
0x00,	//OTP-byte 26
0x00,	//OTP-byte 27
0x00,	//OTP-byte 28
0x00,	//OTP-byte 29
0x00,	//OTP-byte 30
0x0A,	//VSH/VSL
0x00,	//NA
};

//OTP, WS6, TR6
const unsigned char init_data6[]={
0x50,	//OTP-byte 1
0xAA,	//OTP-byte 2
0x55,	//OTP-byte 3
0xAA,	//OTP-byte 4
0x11,	//OTP-byte 5
0x00,	//OTP-byte 6
0x00,	//OTP-byte 7
0x00,	//OTP-byte 8
0x00,	//OTP-byte 9
0x00,	//OTP-byte 10
0x00,	//OTP-byte 11
0x00,	//OTP-byte 12
0x00,	//OTP-byte 13
0x00,	//OTP-byte 14
0x00,	//OTP-byte 15
0x00,	//OTP-byte 16
0x00,	//OTP-byte 17
0x00,	//OTP-byte 18
0x00,	//OTP-byte 19
0x00,	//OTP-byte 20
0xFF,	//OTP-byte 21
0xFF,	//OTP-byte 22
0x1F,	//OTP-byte 23
0x00,	//OTP-byte 24
0x00,	//OTP-byte 25
0x00,	//OTP-byte 26
0x00,	//OTP-byte 27
0x00,	//OTP-byte 28
0x00,	//OTP-byte 29
0x00,	//OTP-byte 30
0x0A,	//VSH/VSL
0x00,	//NA
};

//OTP, Temperature range
const unsigned char init_data7[]={
//18byte
0xF8,	//F8-->TEMP1L[7:0]
0x0F,	//0 -->TEMP1H[3:0], F-->TEMP1L[11:8]
0x0A,	//0A-->TEMP1H[11:8]
0xA0,	//A0-->TEMP2L[7:0]
0x00,	//0 -->TEMP2H[3:0], 0-->TEMP2L[11:8]
0x14,	//14-->TEMP2H[11:8]
0x40,	//40-->TEMP3L[7:0]                   	
0x01,	//0 -->TEMP3H[3:0], 1-->TEMP3L[11:8] 
0x1E,	//1E-->TEMP3H[11:8]             
0xE0,	//E0-->TEMP4L[7:0]                   
0x01,	//0 -->TEMP4H[3:0], 1-->TEMP4L[11:8] 
0x2D,	//2D-->TEMP4H[11:8]                  
0x00,	//00-->TEMP5L[7:0]                   
0x00,	//0 -->TEMP5H[3:0], 0-->TEMP5L[11:8] 
0x00,	//00-->TEMP5H[11:8]                  
0x00,	//00-->TEMP6L[7:0]                   
0x00,	//0 -->TEMP6H[3:0], 0-->TEMP6L[11:8] 
0x00,	//00-->TEMP6H[11:8]                  
};










