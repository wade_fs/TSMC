******************************************************************************
* File Name: TagConfig.SML
* TAG Configuration Manager 
*
* Revision history:
* Rev.              By                      Comments        Date (mm/dd/yyyy)
* 1.0           Bhavin Shah                 		      08/24/1999
******************************************************************************


* Are you there request
S1F1 : 'S100F1' W.

* On line data
S1F2 : 'S100F2'
<L[2] 
  <A 'ERR'>  *MDLN   
  <A 'COMERR 1.00.00'>  *SOFTREV   
>.  

RUTHERENORMAL: 'S1F1' W.

RUTHERENORMALA: 'S1F2'
<L[2]
	<A 'ERR'> *MDLN
	<A 'COMERR'> *SOFTREV
>.                        


*
* Date and time read request
GetDate: 'S2F17' w.
*
GetDateA: 'S2F18'
  <A '990630140620'>. *TIME
*
*

SetDate: 'S100F31' W
<A '990630140620'>.	*TIME

SetDateA: 'S100F32'
<B 0>. *TIACK

* Equipment Constant Request
ECR:  'S100F13' w
<L[5]
  	<U1 1>       	*ECID
	<U1 2>		*ECID
	<U1 3>		*ECID
	<U1 4>		*ECID
	<U1 5>		*ECID
>.

* Equipment Constant Request
ECRA:  'S100F14'
<L[]
  <L[2]
    <U1 2>     *ECID
    <U1 3>     *ECV
  >
>.

* Set New Equipment Constatnt
ECS: 'S100F15' W
<L[5]
  <L[2]
    <U1 1>        *ECID1
    <U1 1>        *TIMEFORMAT
  >
  <L[2]
    <U1 2>   *ECID2
    <U1 0>   *HIINTEG
  >
  <L[2]
    <U1 3>   *ECID3
    <U1 1>   *AREYOUTHERE
  >
  <L[2]
    <U1 4>	*ECID4
    <U1 1>   	*RESETENA 	  
  >
  <L[2]
    <U1 5>	*ECID5
    <U1 1>	*PAGESCRO
  >
>.

ECSA: 'S100F16'
  <B 0>. *EAC
                           
* S100F105  - File Directory Data
Dir:   'S100F105' w 
  <U4 0006>. *TAGSER

DirA: 'S100F106'
<L[4]
  <B 0>  *ACK1
  <U4 06>  *TAGSER
  <U2 100>  *NFILES
  <L[]
    <L  
      <A 'FILE'>  *FILEID
      <U2 300>  *FSIZE
      <A '990630140620'>  *TIME
    >    
  >
>.  

* Send a file to the tag
FILEWRITE: 'S100F107' w
<L[5]
    <U4 6>       *TAGSER
    <U1 1>    *FILOVWT
    <A 'FILE'>      *FILEID
    <A '2000022823594500'>  *TIME
    <B 0>    *FILEDATA 
>.

FILEWRITEA: 'S100F108'
  <B 0>.  *ACK1  


DeleteFile:  'S100F111' w
<L[2]
  <U4 06> *TAGSER
  <A 'FILE'>    *FILEID
>.

DeleteFileA:  'S100F112'
    <B 0>.  *ACK1  


*Tag File Read
FileRead: 'S100F109' w
<L[2]
  <U4 0006>     *TAGSER
  <A 'FILE'>    *FILEID
>.

FileReadA: 'S100F110'
<L[2]
  <B 0>     *ACK1
  <A 'DATA'>    *FILEDATA
>.

DeleteAllFiles:  'S100F113' w
  <U4 0006>.    *TAGSER

DeleteAllFilesA:  'S100F114'
    <B 0>.  *ACK1  

* S100F115  - Format Directory Data
Format:   'S100F115' w 
  <U4 0006>.    *TAGSER

FormatA:   'S100F116'
  <B 0>.    *ACK1                     

* DFD - DESIGNATE FEATURE FILE AS DISPLAY FILE
FileToLCD: 'S100F117' w
<L[2]
    <U4 0006>  *TAGSER
    <A 'FILE'>  *FILEID
>.

FileToLCDA: 'S100F118'
    <B 0>.  *ACK1  

Mem:  'S100F123' w.

MemA:  'S100F124' 
  <U4 1234>.  *MEMSIZE

*Upgrade the tag firmware
CODEUPGRADE: 'S100F161' w
    <B 0x00>.    *FILEDATA 

CODEUPGRADEA: 'S100F162'
  <B 0>.  *ACK1  


* Tag Status Read 
TAGSTATUSREAD: 'S100F131' w.   

TAGSTATUSREADA: 'S100F132'
<L[11]
	<U4 6> *TAGSER
	<A 'VER'>  *SOFTREV
	<A 'HWVER'>  *HARDREV
	<U1 0> *BATSTAT
	<A 'FILE'>  *FILEID
	<U1 1> *LSFTEST
	<U1 1> *SOFTSTAT
	<U1 1> *TIMEFORMAT
	<U1 1> *HIINTEG
	<U1 1> *AREYOUTHERE 
	<U1 1> *RESETENA
>.  


* Tag Battery Status
S100F135: 'S100F135' w.


* Tag Odometer Read
OdometerRead: 'S100F133' w.

OdometerReadA: 'S100F134'
<L[9]
	<U4 6> *TAGSER
	<A '20000228223594500'> *TIME
	<U4 333> *RECODO
	<U4 232> *XMTODO
	<U4 222> *TAGONODO
	<U4 222> *LCDONODO
	<U4 233> *LEDONODO
	<U2 233> *BATTLOWCNT
	<U4 324> *WAKEUPCNT     
>.

TAGSTAT: 'S100F135' w.

TAGSTATA: 'S100F136'
<L[3]
	<U4 06> * TAGSER
	<U1 0> * BATSTAT
	<A  'FILE'> * FILEID
>.

SELFTEST: 'S100F137' W
<U4 06>. *TAGSER

SELFTESTA: 'S100F138'
<B 0>. *ACK1

* Tag Interrogation Message, Polled mode
TagInterrogation:   'S100F141'  w
<L[5]
  <U4 6>      *TAGSER
  <U2 05>     *MSGID
  <U1 10>     *TIMEOUT
  <U1 02>     *RESPMODE
  <A 0>         *LCDMSG
>.

TagInterrogationA: 'S100F142'
  <B 0>.  *ACK1  
 
* Get Interrogation Result
GetInterrogation: 'S100F143' w
   <U4 0006>.   *TAGSER
 
GetInterrogationA: 'S100F144' 
<L[2]
  <U2 5>      *MSGID
  <B  0>        *ACK3  
>.


SetBaudRate: 'S100F181' w
<L[7]
  <U4 4284481325>  *TAGSER
  <U1 2>  *BAUDRATE 
  <U1 5>  *TIMERT1
  <U1 8>  *TIMERT2
  <U1 3>  *TIMERT3
  <U1 3>  *TIMERT4
  <U1 3>  *COMRTY
>.

SetBaudRateA: 'S100F182'
  <B 0>.  *ACK5  

ReadCommSetup: 'S100F189' W.

ReadCommSetupA: 'S100F190' 
<L[7]
	<U4 6> *TAGSER
	<U1 2> *BAUDRATE
	<U1 1> *TIMERT1
	<U1 10> *TIMERT2
	<U1 45> *TIMERT3
	<U1 45> *TIMERT4
	<U1 3> *COMRTY
>.

* Read Tag Serial Number
TagReadSerialNo: 'S100F171' W.

TagReadSerialNoA: 'S100F172'
<U4 6>. *TAGSER

* Reset the tag.
RESET_TAG: 'S100F187' W
<L[2]
	<U4 6> *TAGSER
	<U2 3000> *BATTCAP
>.

RESET_TAGA: 'S100F188'
<B 0>. *ACK1

STATIC_WRITE:'S100F149' W
<L[3]
	<U4 0006> *TAGSER
	<U1 7> *LINENUM
	<A 'MSG'> *LCDMES
>.

STATIC_WRITEA:'S100F150'
<B 0>. *ACK2

CANCEL_STATIC:'S100F151' W
<U4 6>. *TAGSER

CANCEL_STATICA: 'S100F152'
<B 0>. *ACK2


LCDBLANK: 'S100F119' W
     <L[4]
	<U4 6>	*TAGSER
	<U1 1>	*FILOVWT
	<A '980226080400'> *TIME
	<B 
0xf0
0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 
0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 
0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 
0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 
0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 
0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 
0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 
0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 
0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 
0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 
0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 
0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 
0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 
0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 
0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 
0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 
0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 
0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f
0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 
0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 
0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 
0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f
0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f
0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f
0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f
0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f
0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f
0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f
0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f
0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f
0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f
0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f
0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f
0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f
0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 0x7f 
	> *FILEDATA
     >.


LCDBLANKA: 'S100F120'
<B 0>. *ACK1

WRITELCD: 'S100F119' W
     <L[4]
	<U4 6>	*TAGSER
	<U1 1>	*FILOVWT
	<A '980226080400'> *TIME
	<B 0>	*FILEDATA
>.

WRITELCDA: 'S100F120'
<B 0>. *ACK1

READLCDFILE: 'S100F121' W
<U4 06>. *TAGSER

READLCDFILEA: 'S100F122'
<L[2]
	<B 0 > *ACK1
	<A 'FILEDATA'> *FILEDATA
>.

WRITEFONT: 'S100F153' W
<L[3]
	<U1 14> *CHARHT
	<U1 14> *CHARWD
	<B  240> *FONT
>.

WRITEFONTA: 'S100F154'
<B 0>. *ACK2

BLINKLED: 'S100F157' W
<L[2]
	<U1 2> *LEDSTATE
	<U2 10> *LEDTIME
>.

BLINKLEDA: 'S100F158' 
<B 0>. *ACK1

SETLCDTO: 'S100F155' W
<U1 4>. *LCDTIME

SETLCDTOA: 'S100F156'
<B 0>. *ACK2

SELECTFONT: 'S100F159' W
<U1 0xF0>. *FONTCODE

SELECTFONTA: 'S100F160'
<B 0>. *ACK2

*Write Lot ID to the tag
WRITELOTID: 'S100F183' W
<L[2]
	<U4 6> *TAGSER
	<A '1234567890'> *LOTID
>.

WRITELOTIDA: 'S100F184'
<B 0x0>. *ACK1

*Read Lot ID From the tag
READLOTID: 'S100F185' W
<U4 6>. *TAGSER


*S100F186 Lot ID return
READLOTIDA: 'S100F186'
<L[2]
	<B > * ACK1
	<A 'LOTID'> * LOTID
>.

LCDON: 'S100F191' W.

LCDONA: 'S100F192'
<B 0>. *ACK1

READSTATIC: 'S100F193' W.

READSTATICA: 'S100F194'
<L[2]
	<B 00> * ACK2
	<A 'MSG'> * LCDMES
>.

LCDTEMPWRITE: 'S100F147' W
<L[2]
	<U1 30> *TIMEOUT
	<A ' Test Temp Write' > *LCDMES
>.

LCDTEMPWRITEA: 'S100F148'
<B 0>. *ACK2

SETSPACES:'S100F195' W
<L[4]
	<U4 06> *TAGSER
	<U1 240> *FONTCODE
	<U1 0> *CHARSPACE
	<U1 0> *LINESPACE
>.


RAR:'S18F1' W
<L[2]
	<A '42'> *TARGETID
	<L[]
		<A ' '> *ATTRID1
	>
>.

RARA:'S18F2'
<L[4]
	<A '42' > *TARGETID
	<A '1' > * SSACK
	<L[n]
		<A ' '> *ATTRID
	>
	<L[4]
		<A '0'> * STATUS1
		<A '0'> * STATUS2
		<A '0'> * STATUS3
		<A '0'> * STATUS4

	>
>.

WAR:'S18F3' W
<L[2]
	<A '42'> *TARGETID
	<L[n]
		<L[2]
			<A ' '> *ATTRID1
			<A ' '> *ATTRVAL1
		>
	>
>.

WARA:'S18F4'
<L[4]
	<A '42' > *TARGETID
	<A '1' > * SSACK
	<L[n]
		<A ' '> *STATUS1
	>
>.

LEDSSCMD:'S18F13' W
<L[3]
	<A '42' > *TARGETID
	<A '04' > *SSCMD
	<L[3]
		<A ' '> *LEDSTATE
		<A ' '> *TIMEOUT
		<A ' '> *LEDNO
	>
>.

LEDSSCMDA:'S18F14' 
<L[3]
	<A '42'> *TARGETID
	<A '0' > *SSACK
	<L[4]
		<A ' '> *STATUS1
		<A ' '> *STATUS2
		<A ' '> *STATUS3
		<A ' '> *STATUS4
	>
>.

DeleteAllFilesSSCMD:'S18F13' W
<L[3]
	<A '42' > *TARGETID
	<A '06' > *SSCMD
	<L[0]
	>
>.

DeleteAllFilesSSCMDA:'S18F14' 
<L[3]
	<A '42'> *TARGETID
	<A '0' > *SSACK
	<L[]
		<A ' '> *STATUS
	>
>.

SelfTest:'S18F13' W
<L[3]
	<A '42' > *TARGETID
	<A '07' > *SSCMD
	<L[0]
	>
>.

SelfTestA:'S18F14' 
<L[3]
	<A '42'> *TARGETID
	<A '0' > *SSACK
	<L[]
		<A ' '> *STATUS
	>
>.

LCDONSSCMD:'S18F13' W
<L[3]
	<A '42' > *TARGETID
	<A '03' > *SSCMD
	<L[1]
		<A '10'> *TIMEOUT
	>
>.

LCDONSSCMDA:'S18F14' 
<L[3]
	<A '42'> *TARGETID
	<A '0' > *SSACK
	<L[]
		<A ' '> *STATUS
	>
>.

ResetODOSSCMD:'S18F13' W
<L[3]
	<A '42' > *TARGETID
	<A '12' > *SSCMD
	<L[0]
	>
>.

ResetODOSSCMDA:'S18F14' 
<L[3]
	<A '42'> *TARGETID
	<A '0' > *SSACK
	<L[]
		<A ' '> *STATUS
	>
>.

ResetUNIT:'S18F13' W
<L[3]
	<A '42' > *TARGETID
	<A '13' > *SSCMD
	<L[0]
	>
>.

ResetUNITA:'S18F14' 
<L[3]
	<A '42'> *TARGETID
	<A '0' > *SSACK
	<L[]
		<A ' '> *STATUS
	>
>.

TagInterrogationSSCMD:'S18F13' W
<L[3]
	<A '42' > *TARGETID
	<A '8' > *SSCMD
	<L[3]
		<A '6'> *MSGID
		<A '10'> *TIMEOUT
		<A '1'> *RESPMODE
		<A 'M'> *MSG
	>
>.

TagInterrogationSSCMDA:'S18F14' 
<L[3]
	<A '42'> *TARGETID
	<A '0' > *SSACK
	<L[]
		<A ' '> *STATUS
	>
>.


STATIC_WRITESSCMD:'S18F13' W
<L[3]
	<A '42' > *TARGETID
	<A '9' > *SSCMD
	<L[3]
		<A '6'> *LINENUM
		<A 'M'> *LCDMSG
	>
>.

STATIC_WRITESSCMDA:'S18F14' 
<L[3]
	<A '42'> *TARGETID
	<A '0' > *SSACK
	<L[]
		<A ' '> *STATUS
	>
>.

CancelStaticSSCMD:'S18F13' W
<L[3]
	<A '42' > *TARGETID
	<A '10' > *SSCMD
	<L[0]
	>
>.

CancelStaticSSCMDA:'S18F14' 
<L[3]
	<A '42'> *TARGETID
	<A '0' > *SSACK
	<L[]
		<A ' '> *STATUS
	>
>.

LCDTEMPWRITESSCMD:'S18F13' W
<L[3]
	<A '42' > *TARGETID
	<A '11' > *SSCMD
	<L[2]
		<A '10'> *TIMEOUT
		<A ' '>  *LCDMSG
	>
>.

LCDTEMPWRITESSCMDA:'S18F14' 
<L[3]
	<A '42'> *TARGETID
	<A '0' > *SSACK
	<L[]
		<A ' '> *STATUS
	>
>.


DEFAULTFILESSCMD:'S18F13' W
<L[3]
	<A '42' > *TARGETID
	<A '1' > *SSCMD
	<L[1]
		<A 'FILE'> *FILEID
	>
>.

DEFAULTFILESSCMDA:'S18F14' 
<L[3]
	<A '42'> *TARGETID
	<A '0' > *SSACK
	<L[]
		<A ' '> *STATUS
	>
>.

DELETEFILESSCMD:'S18F13' W
<L[3]
	<A '42' > *TARGETID
	<A '2' > *SSCMD
	<L[1]
		<A 'FILE'> *FILEID
	>
>.

DELETEFILESSCMDA:'S18F14' 
<L[3]
	<A '42'> *TARGETID
	<A '0' > *SSACK
	<L[]
		<A ' '> *STATUS
	>
>.

ChangeStateSSCMD:'S18F13' W
<L[3]
	<A '42' > *TARGETID
	<A '15' > *SSCMD
	<L[1]
		<A 'M' > *OPMODE
	>
>.

ChangeStateSSCMDA:'S18F14' 
<L[3]
	<A '42'> *TARGETID
	<A '0' > *SSACK
	<L[]
		<A ' '> *STATUS
	>
>.


GetStatusSSCMD:'S18F13' W
<L[3]
	<A '42' > *TARGETID
	<A 'GetStatus' > *SSCMD
	<L
	>
>.

GetStatusSSCMDA:'S18F14' 
<L[3]
	<A '42'> *TARGETID
	<A '0' > *SSACK
	<L[]
		<A ' '> *STATUS
	>
>.

EventReport:'S18F71'
<L[4]
	<A '42'> *TARGETID
	<A '0' > *SSACK
	<A '1' > *CEID
	<L[]
		<A ' '> *DVNAME1
		<A ' '> *DVVAL1
	>
>.

ReadRequest:'S18F5' W
<L[3]
	<A '42'> *TARGETID
	<A '00'> *DATASEG
	<A '20'> *DATALENGTH
>.

ReadRequestA:'S18F6'
<L[3]
	<A '42'> *TARGETID
	<A '0'> *SSACK
	<A 'aa'> *DATA
>.


ReadRequest_E99:'S18F5' W
<L[3]
	<A '42'> *TARGETID
	<A > *DATASEG
	<U2> *DATALENGTH
>.

ReadRequest_E99A:'S18F6'
<L[3]
	<A '42'> *TARGETID
	<A '0'> *SSACK
	<A 'aa'> *DATA
>.

WriteDataRequest:'S18F7' W
<L[4]
	<A '42'> *TARGETID
	<A '00'> *DATASEG
	<A '20'> *DATALENGTH
	<A ' '> *DATA
>.

WriteDataRequestA:'S18F8' 
<L[3]
	<A '42'> *TARGETID
	<A '0' > *SSACK
	<L[]
		<A ' '> *STATUS
	>
>.

WriteDataRequest_E99:'S18F7' W
<L[4]
	<A '42'> *TARGETID
	<A '00'> *DATASEG
	<U2 '20'> *DATALENGTH
	<A ' '> *DATA
>.

WriteDataRequest_E99A:'S18F8' 
<L[3]
	<A '42'> *TARGETID
	<A '0' > *SSACK
	<L[]
		<A ' '> *STATUS
	>
>.

ReadMaterialID:'S18F9' W
<A '42'> *TARGETID

ReadMaterialIDA:'S18F10'
<L[4]
	<A '42'> *TARGETID
	<A '0' > *SSACK
	<A '8'> *MID
	<L[4]
		<A '0'> *STATUS1
		<A '0'> *STATUS2
		<A '0'> *STATUS3
		<A '0'> *STATUS4
	>
>.

WriteMaterialID:'S18F11' W
<L[2]
	<A '42'> *TARGETID
	<A '0' > *MID
>.

WriteMaterialIDA:'S18F12'
<L[3]
	<A '42'> *TARGETID
	<A '0' > *SSACK
	<L[]
		<A ' '> *STATUS
	>
>.

WriteHighIngerity:'S18F75' W
<L[5]
	<A '42'> *TARGETID
	<A '00'> *DATASEG
	<A '20'> *DATALENGTH
	<A '6'> *SERIALNUM
	<A ' '> *DATA
>.

WriteHighIngerityA:'S18F76' 
<L[3]
	<A '42'> *TARGETID
	<A '0' > *SSACK
	<L[]
		<A ' '> *STATUS
	>
>.

ReadHighIntegrity:'S18F73' W
<L[3]
	<A '42'> *TARGETID
	<A '00'> *DATASEG
	<A '20'> *DATALENGTH
	<A '6'> *SERIALNUM
>.

ReadHighIntegrityA:'S18F74'
<L[3]
	<A '42'> *TARGETID
	<A '0'> *SSACK
	<A 'aa'> *DATA
>.

ReadHighIntegrity_E99:'S18F73' W
<L[3]
	<A '42'> *TARGETID
	<A ''> *DATASEG
	<U2 > *DATALENGTH
	<A '6'> *SERIALNUM
>.

ReadHighIntegrity_E99A:'S18F74'
<L[3]
	<A '42'> *TARGETID
	<A '0'> *SSACK
	<A 'aa'> *DATA
>.

WRITEFONTCIDRW: 'S18F65' W
<L[6]
	<A '42'> *TARGETID
	<A '14'> *CHARHT
	<A '14'> *CHARWD
	<A '1'> *INTERCHARSPACE
	<A '0'> *INTERLINESPACE
	<B  240> *FONT
>.

WRITEFONTCIDRWA: 'S18F66'
<L[3]
	<A '42'> *TARGETID
	<A '0' > *SSACK
	<L[]
		<A ' '> *STATUS
	>
>.

CODEUPGRADECIDRW: 'S18F69' W
<L[2]
	<A '42'> *TARGETID
	<B  0x00> *FILEDATA
>.

CODEUPGRADECIDRWA: 'S18F70'
<L[3]
	<A '42'> *TARGETID
	<A '0' > *SSACK
	<L[]
		<A ' '> *STATUS
	>
>.

ReadOdometer: 'S18F77' W.
<A '42'>. *TARGETID

ReadOdometerA: 'S18F78'
<L[3]
	<A '42'> *TARGETID
	<A '0' > *SSACK
	<L[8]
		<A ' '> *ODODATA1
		<A ' '> *ODODATA2
		<A ' '> *ODODATA3
		<A ' '> *ODODATA4
		<A ' '> *ODODATA5
		<A ' '> *ODODATA6
		<A ' '> *ODODATA7
		<A ' '> *ODODATA8
	>
>.

ReadStatus: 'S18F79' W.
<A '42'>. *TARGETID

ReadStatusA: 'S18F80'
<L[3]
	<A '42'> *TARGETID
	<A 'NO'> *SSACK
	<A 'IDLE'> *STATUS
>.

ReadCFGFile: 'S18F81' W
<L[2]
	<A '00'> *TARGETID
	<A 'COM1:'> *PORT
>.

ReadCFGFileA:'S18F82'
<L[2]
	<A '00'> *TARGETID
	<L[]
		<A ' '> *PARAMVALUE
	>
>.

ReadCFGFile1A:'S18F82'
<L[2]
	<A '00'> *TARGETID
	<L[42]
		<A 'PORT' > * Port
		<A 'COM1:'> * RS232xx, CAN, or LAN
		<A 'PROTOCOL'> * Protocol
		<A 'SECS'> 	* SECS or ASCII, Default = ASCII
		<A 'HOST'> *Host
		<A 'YES'> * YES or NO, Default = NO
		<A 'BAUDRATE'>	* Baud rate, SECS or ASCII, Default = 9600
		<A '9600'> 	*BaudRate
		<A 'TRY'> *RETRY
		<A '3'>	* Re try limit, SECS or ASCII, Default = 3
		<A 'T1'> *T1 timeout 
		<A '5'>	 * Inter-character  Timeout, for SECS or ASCII, defualt = 
		<A 'T2'> *T2 Timeout
		<A '24'> * Protocol Time, for SECS only
		<A 'T3'> *T3 timeout
		<A '120'>* Response Timeout, for SECS or HSMS
		<A 'T4'> *T4 timeout
		<A '45'> *Interblock Timeout, for SECS only 
		<A 'T5'> *T5 timeout
		<A '45'> * connect separation timeout t, for HSMS only
		<A 'T6'> *T6 timeout
		<A '45'> * control transaction timeout, for HSMS only
		<A 'T7'> * T7 timeout
 		<A '40'> * Not Selected timeout, for HSMS only
		<A 'T8'> * T8 timeout
		<A '5'> * Network Inter-character  timeout, for HSMS only
		<A 'CONNECTMODE'> * Connection mode
		<A 'MASTER'>	* MASTER, SLAVE, ACTIVE or PASSIVE, for HSMS or SECS
		<A 'IPLOCAL'> * local ip address
		<A '128.5.10.5'>	* Local IP address, for HSMS only
		<A 'IPRemote'> * Remote ip address
		<A '128.5.10.10'>	* Remote IP address, for HSMS only
		<A 'IPPORT'> * IP port
		<A '5000'>	* IP Port, for HSMS only
		<A 'LINKTEST'> * Link test timer frequency
		<A '35'>	* Link Test timer, for HSMS only
		<A 'CHECKSUM'> * Check sum 
		<A 'EN'> 	* Checksum bytes, ENABLE or DISABLE, ASCII only
		<A 'POLARITY'> * Polarity
		<A 'TRUE'> * TRUE or FALSE, for SECS And ASCII
		<A 'EVENTS'> *Events 
		<A 'ON'> * Whether or not the Events are of ON or OFF.
	>
>.


WriteCFGFile:'S18F83' W
<L[2]
	<A '00'> *TARGETID
	<L[]
		<A 'PROPERTY' > * PROPERTY NAME OR VALUE
	>
>.

WriteCFGFileA: 'S18F84'
<L[3]
	<A '00'> *TARGETID
	<A 'NO'> *SSACK
	<L[]
		<A 'ERR Field"> *Erreneous Field
	>
>.