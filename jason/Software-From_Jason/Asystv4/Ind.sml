******************************************************************************
* File Name: LPI.SML
* LPI Simulator for SMARTComm Testing
*
* Revision history:
* Rev.              By                      Comments        Date (mm/dd/yy)
* 1.0           Venu Vemula              Created            9/15/97
* 1.1           Manoj Tamhankar          Added FL status message 9/11/98
******************************************************************************
AbortTransaction: 'S1F0'.

* S1F1 (From Host)
GetEquipmentOnline : 'S1F1' W

* On Line Data
GetEquipmentOnlineReply : 'S1F2'
	<L
		<A ''> * MDLN
		<A ''> * SOFTREV
	>.

* Status Request
StatusRequest : 'S1F5' W
<B[1]> * SFCD

* S1F5 (From Host)
* For SFCD = 0
GeneralStatus: 'S1F6'
	<L
		<B[1] 0> * SFCD
		<L
			<U1 0>	*PIO_MODE
			<U1 1>	*PIP
			<U1 1>	*INXOPS
			<U1 1>	*HOMEST
			<U1 0>	*LFUNC
			<U1 1>	*PRTST
			<U1 1>	*ELDN
			<U1 0>	*ELUP
			<U1 0>	*ELSTAGE
			<U1 0>	*SEATER_ST
			<U1 0>	*MODE
			<U1 0>	*SLOTPOS
		>
		<U2  25>	*ELPOS
		<L
			<U1 0>	*PIO_1
			<U1 0>	*PIO_2
			<U1 0>	*PIO_3
			<U1 0>	*PIO_4
			<U1 0>	*PIO_5
			<U1 1>	*PIO_ENABLE
		>
	>.
GeneralStatusFL: 'S1F6'
	<L
		<B[1] 0> * SFCD
		<L
			<U1 0>	*PIO_MODE
			<U1 1>	*CIP
			<U1 1>	*INXOPS
			<U1 1>	*HOMEST
			<U1 0>	*LFUNC
			<U1 1>	*PRTST
			<U1 1>	*ELDN
			<U1 0>	*ELUP
			<U1 0>	*ELSTAGE
			<U1 0>	*PODPOS
			<U1 0>	*MODE
			<U1 0>	*DOORST
			<U1 0>  *PAP
			<U1 0>  *LISTITEM14
		>
		<U2  25>	*ELPOS
		<L
			<U1 0>	*PIO_1
			<U1 0>	*PIO_2
			<U1 0>	*PIO_3
			<U1 0>	*PIO_4
			<U1 0>	*PIO_5
			<U1 1>	*PIO_ENABLE
		>
	>.

* For SFCD = 1
SoftWareRev_EventData: 'S1F6'
	<L	
		<B[1] 1>	*SFCD
		<A '12345678901234567890'> * SOFTREV
		<L
			<U1 0>		*EED
			<B[1] 0>		*ALED
			<U1 0>		*SORTMODE
		>
	>.

* For SFCD = 2
WaferMap: 'S1F6'
	<L
		<B 2>	*SFCD
		<L
			<U1 1>	*SLOT_STATUS
			<U1 1>	*SLOT_STATUS
			<U1 1>	*SLOT_STATUS
			<U1 1>	*SLOT_STATUS
			<U1 1>	*SLOT_STATUS
			<U1 1>	*SLOT_STATUS
			<U1 1>	*SLOT_STATUS
			<U1 1>	*SLOT_STATUS
			<U1 1>	*SLOT_STATUS
			<U1 1>	*SLOT_STATUS
			<U1 1>	*SLOT_STATUS
			<U1 1>	*SLOT_STATUS
			<U1 1>	*SLOT_STATUS
			<U1 1>	*SLOT_STATUS
			<U1 1>	*SLOT_STATUS
			<U1 1>	*SLOT_STATUS
			<U1 1>	*SLOT_STATUS
			<U1 1>	*SLOT_STATUS
			<U1 1>	*SLOT_STATUS
			<U1 1>	*SLOT_STATUS
			<U1 1>	*SLOT_STATUS
			<U1 1>	*SLOT_STATUS
			<U1 1>	*SLOT_STATUS
			<U1 1>	*SLOT_STATUS
			<U1 1>	*SLOT_STATUS
		>
	>.

* For SFCD = 3
StausCountData: 'S1F6'
	<L
		<B[1] 3> *SFCD
		<L
			<I4 20>	*TOTAL_PU
			<I4 70>	*TOTAL_STAGE
			<I4 58>	*TOTAL_HOME
			<I4 26>	*TOTAL_MAP
			<I4 10>	*TOTAL_WSEARCH
			<I4 22>	*TOTAL_SSEARCH
			<I4 24>	*TOTAL_RECERR
			<I4 50>	*TOTAL_NRECERR
		>
		<U2 0> *LAST_ERROR
	>.

* Equipment Constant Data request
EquipmentConstantReq: 'S2F13' W
<L
	<U1 4> *ExtHost
	<U1 6> *PIOMode
	<U1 9> *PIOEnable
	<U1 13> *MSCInterlock
	<U1 32> *MinWaferPos
	<U1 33> *StagePos
	<U1 34> *SlotCount
	<U1 35> *SlotPinch
	<U1 37> *SinglePort
	<U1 38> *HostPos
	<U1 39> *CasetteSize
	<U1 40> *DeviceID
	<U1 41> *BaudeRate
	<U1 46> *CasetteLanding
	<U1 55> *ReceiveTimeout
	<U1 56> *ButtonOption
	<U1 57> *PodInPlace
	<U1 58> *SquatOption
>
EquipmentConstantReq1: 'S2F13' W
<L
	<U1 0> *ECID
>
EquipmentConstantReq1Reply: 'S2F14'
<L
	<L
		<U1 0> *ECID
		<U1 0> *ECIDValue
	>
>
EquipmentConstantReq1Reply2: 'S2F14'
<L
	<L
		<U1 0> *ECID
		<U2 0> *ECIDValue
	>
>
* Equipment Constant Data
EquipmentConstantReply: 'S2F14'
<L
	<L
		<U1 4> *ExtHost
		<U1 4> *ExtHostValue
	>
	<L
		<U1 6> *PIOMode
		<U1 6> *PIOModeValue
	>
	<L
		<U1 9> *PIOEnable
		<U1 9> *PIOEnableValue
	>
	<L
		<U1 13> *MSCInterlock
		<U1 13> *MSCInterlockValue
	>
	<L
		<U1 32> *MinWaferPos
		<U2 32> *MinWaferPosValue
	>
	<L
		<U1 33> *StagePos
		<U2 33> *StagePosValue
	>
	<L
		<U1 34> *SlotCount
		<U2 34> *SlotCountValue
	>
	<L
		<U1 35> *SlotPinch
		<U2 35> *SlotPinchValue
	>
	<L
		<U1 37> *SinglePort
		<U1 37> *SinglePortValue
	>
	<L
		<U1 38> *HostPos
		<U1 38> *HostPosValue
	>
	<L
		<U1 39> *CasetteSize
		<U1 39> *CasetteSizeValue
	>
	<L
		<U1 40> *DeviceID
		<U2 40> *DeviceIDValue
	>
	<L
		<U1 41> *BaudeRate
		<U2 41> *BaudeRateValue
	>
	<L
		<U1 46> *CasetteLanding
		<U2 46> *CasetteLandingValue
	>
	<L
		<U1 46> *CasettePickup
		<U2 46> *CasettePickupValue
	>
	<L
		<U1 55> *ReceiveTimeout
		<U1 55> *ReceiveTimeoutValue
	>
	<L
		<U1 56> *ButtonOption
		<U1 56> *ButtonOptionValue
	>
	<L
		<U1 57> *PodInPlace
		<U1 57> *PodInPlaceValue
	>
	<L
		<U1 58> *SquatOption
		<U1 58> *SquatOptionValue
	>
>

* Equipment Constant Data set
EquipmentConstantSet: 'S2F15' W
<L
	<L
		<U1 4> *ExtHost
		<U1 4> *ExtHostValue
	>
	<L
		<U1 6> *PIOMode
		<U1 6> *PIOModeValue
	>
	<L
		<U1 9> *PIOEnable
		<U1 9> *PIOEnableValue
	>
	<L
		<U1 13> *MSCInterlock
		<U1 13> *MSCInterlockValue
	>
	<L
		<U1 32> *MinWaferPos
		<U2 32> *MinWaferPosValue
	>
	<L
		<U1 33> *StagePos
		<U2 33> *StagePosValue
	>
	<L
		<U1 34> *SlotCount
		<U2 34> *SlotCountValue
	>
	<L
		<U1 35> *SlotPinch
		<U2 35> *SlotPinchValue
	>
	<L
		<U1 37> *SinglePort
		<U1 37> *SinglePortValue
	>
	<L
		<U1 38> *HostPos
		<U1 38> *HostPosValue
	>
	<L
		<U1 39> *CasetteSize
		<U1 39> *CasetteSizeValue
	>
	<L
		<U1 40> *DeviceID
		<U2 40> *DeviceIDValue
	>
	<L
		<U1 41> *BaudeRate
		<U2 41> *BaudeRateValue
	>
	<L
		<U1 46> *CasetteLanding
		<U2 46> *CasetteLandingValue
	>
	<L
		<U1 46> *CasettePickup
		<U2 46> *CasettePickupValue
	>
	<L
		<U1 55> *ReceiveTimeout
		<U1 55> *ReceiveTimeoutValue
	>
	<L
		<U1 56> *ButtonOption
		<U1 56> *ButtonOptionValue
	>
	<L
		<U1 57> *PodInPlace
		<U1 57> *PodInPlaceValue
	>
	<L
		<U1 58> *SquatOption
		<U1 58> *SquatOptionValue
	>
>

EquipmentConstantSet11: 'S2F15' W
<L
	<L
		<U1 0> *ECID
		<U1 0> *ECIDValue
	>
>
EquipmentConstantSet12: 'S2F15' W
<L
	<L
		<U1 0> *ECID
		<U2 0> *ECIDValue
	>
>
* New Equipment constant acknowledge

EquipmentConstantSetAck: 'S2F15' : 'S2F16'
	<B[1] 0> *EAC

* Enable/Disable Events
EnableAllEvents : 'S2F37' W
<L
	<B[1] 1> *CEED
	<L
	>
>

EnableEvent : 'S2F37' W
<L
	<B[1] 1> *CEED
	<L
		<U1 31> *CEID
	>
>
* Enable/Disable Event Report Acknowledge
EnableAllEventsAck : 'S2F38'
	<B[1] 0>.

* HostCommandSend - Sam Port
RCS : 'S2F21' W
<U1 0> * RCMD

* Host Command Acknowledge Sma port
RCSA : 'S2F22'
	<B[1] 0>. * HCACK
* HostCommandSend
HCS : 'S2F41' W
<L
	<U1 0> * RCMD
	<L
	>
>
* HostCommandSend2
HCS2 : 'S2F41' W
<L
	<U1 0> * RCMD
	<L
		<U2 0> * PARAM1
		<U2 0> * PARAM2
		<U2 0> * PARAM3
	>
>
* Host Command Acknowledge
HCSA : 'S2F42'
	<B[1] 0>. * HCACK

* Alarm Report Send
AlarmReceived : 'S5F1'
	<L
		<B[1] 6>	* AlarmCode
		<U2 41>	* Alarm_ID
		<A 'Not home'> *AlarmMessage
	>.

* S5F3 (From Host)

* Enable/Disable Alarm Acknowledge
EnableAlarams : 'S5F3' W
<L
	<B[1] 0x80>	*Enable
	<U1 0>
>
DiableAlarams : 'S5F3' W
<L
	<B[1] 0>	*Enable
	<U1 0>
>
EnableAck : 'S5F4'
	<B 0>. * ACKC5

* Annoted Event Report Send
EventReceived :'S6F13' W
	<L
		<U1 1>	* DataID
		<U1 33>	* CEID
		<L>
	>.

EventReceivedAck :'S6F14'
	<B[1] 0> * ACKC6
