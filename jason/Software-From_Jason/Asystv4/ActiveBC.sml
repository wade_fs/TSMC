* -----------------------------------------------------------
* File    : SmartCom.sml
* Date    : Oct. 02, 98
* Edited  : neesha shah
* Remarks : SECS Message Set for Barcode through smart com
* -----------------------------------------------------------


* ----------------------------------
* S6F11 Event Report Send (ERS) (H<-E)
* Event sent when barcode is swiped
* ----------------------------------
ERS: 'S6F11' W
<A[] 'BarCode1'>	* BarCode
.


* ----------------------------------
* S6F11 Event Report Acknowledge (ERA) (H->E)
* Acknowledgement for event report
* ----------------------------------
ERA: 'S6F12' 
<U1 0>	* Ack5
.