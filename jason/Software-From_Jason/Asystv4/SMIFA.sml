******************************************************************************
* File Name: LPT.SML
* LPT message file for LPT OCX
*
* Revision history:
* Rev.              By                      Comments        Date (mm/dd/yy)
* 1.0           Manoj Tamhankar              Created            9/16/97
* 1.1			Manoj Tamhankar	  Added FSD2 for ALU(SPR 1)     3/3/98
* 1.2			Manoj Tamhankar		   Added ARA & D_H (SPR 2)	3/5/98
******************************************************************************
S1F0_abort: 'S1F0'.

* R U There?
R : 'S1F1' w.
	

* On Line Data
D : 'S1F2'
	<L
		<A  'LPTTST'> * MDLN
		<A  '5.420'> * SOFTREV
	>.
D_H : 'S1F2'
	<L>.

* SSR
SSR : 'S1F3' w
	<L>.

* Selected Equipment Status Data
SSD : 'S1F4'
<L[2]
	<L[25]
		<U1 0>	* SLOT1
		<U1 0>	* SLOT2
		<U1 0>	* SLOT3
		<U1 2>	* SLOT4
		<U1 0>	* SLOT5
		<U1 1>	* SLOT6
		<U1 0>	* SLOT7
		<U1 0>	* SLOT8
		<U1 2>	* SLOT9
		<U1 0>	* SLOT10
		<U1 0>	* SLOT11
		<U1 4>	* SLOT12
		<U1 0>	* SLOT13
		<U1 2>	* SLOT14
		<U1 0>	* SLOT15
		<U1 4>	* SLOT16
		<U1 1>	* SLOT17
		<U1 0>	* SLOT18
		<U1 0>	* SLOT19
		<U1 1>	* SLOT20
		<U1 0>	* SLOT21
		<U1 1>	* SLOT22
		<U1 0>	* SLOT23
		<U1 0>	* SLOT24
		<U1 0>	* SLOT25
	>
	<L
		<U1 19> * WAFERCOUNT
	>
>.

* Formatted status request
FSR : 'S1F5' w
	<B[1] 0>. * SFCD

* Formatted status data 
FSD1: 'S1F6'
	<L
	   <U1[20] 1 0 1 0 0 1 0 1 2 0 1 0 2 0 1 0 1 20 1 0 > * MODE PIP HOMEST FUNC COLST LFUNC GPST PRTST ELUP PLDN MARMUP MARMDN TILUP ELDN PLUP SWPOS DIPSW ARMTYP ECV ALED
	   <U2[4] 0 0 0 0 > * XPOS YPOS ELPOS TLTPOS
       <U1[1] 1> * RDYST1
	>.

FSD: 'S1F6'
	<L
	   <U1[20] 1 0 1 0 0 1 0 1 2 0 1 0 2 0 1 0 1 20 1 0 > * MODE PIP HOMEST FUNC COLST LFUNC GPST PRTST ELUP PLDN MARMUP MARMDN TILUP ELDN PLUP SWPOS DIPSW ARMTYP ECV ALED
	   <U2[4] 0 0 0 0 > * XPOS YPOS ELPOS TLTPOS
       <U1[1] 1> * RDYST1
	   <A '12345678901234567890'> * ARM_SW
       <U1[4] 0 1 0 1> * PIO_LU PIO_LRDY PIO_URDY PIO_LOCK
	>.

FSD2: 'S1F6'
	<L
		<U1[13] 1 0 1 0 1 0 1 0 1 0 1 0 1> * MODE PIP HOMEST FUNC LFUNC PRTST ELUP ELDN DIPSW ARMTYP ECV ALED RDYST1
		<U2[1] 1> * ELPOS
		<A '12345678901234567890'> * ARM_SW
	>.

* ECS ; New Equipment Constant Send
EnableEvents : 'S2F15' w
	<L
		<U1 1> * ECID
		<U1 1> * ECV
	>.

DisableEvents : 'S2F15' w
	<L
		<U1 1> * ECID
		<U1 0> * ECV
	>.

* New Equipment constant acknowledge
ECA : 'S2F16'
	<B[1] 0>. * EAC

* New Equipment constant acknowledge for 6 inch arm.
ECA2 : 'S2F16'
	<U1 0>. * EAC

* RIS Reset initialize
RIS_OR : 'S2F19' w
	<U1 1>. * RIC	

RIS_SEMI : 'S2F19' w
	<U1 65>. * RIC	

RIS_AND : 'S2F19' w
	<U1 64>. * RIC	

* Reset Acknowledge
RIA : 'S2F20'
	<U1 0>. * RAC

* Remote Commands
Lock : 'S2F21' w
	<U1 12>. * RCMD

UnLock : 'S2F21' w
	<U1 13>. * RCMD

EnableLoad : 'S2F21' w
	<U1 1>. * RCMD

Load : 'S2F21' w
	<U1 9>. * RCMD

EnableUnLoad : 'S2F21' w
	<U1 2>. * RCMD

UnLoad : 'S2F21' w
	<U1 10>. * RCMD

RCS : 'S2F21' w
	<U1 12>. * RCMD

* Remote Command Acknowledge
RCA : 'S2F22'
	<U1 0>. * CMDA 

* S2F25 (From Host)

* Loopback Diagnostic Data
*S2F26_LDD : 'S2F26'
*	<B[9] 01 02 03 04 05 06 07 08 09>.


* Alarm Report Send
ARS : 'S5F1' w
	<L
		<B[1] 6> * ALCD
		<U2 41> * ALID
		<A 'No Cassette'> * ALTX
	>.

ARS2 : 'S5F1' w
	<L
		<U1 6> * ALCD
		<U2 41> * ALID
		<A 'No Cassette'> * ALTX
	>.

* Alarm Report Acknowledge
ARA : 'S5F2'
	<B[1] 0>. * ACKC5

* S5F3
EnableAlarms : 'S5F3' w
	<L
		<B[1] 0x80>
		<U2[0]>
	>.

DisableAlarms : 'S5F3' w
	<L
		<B[1] 0x00> * ALED
		<U2[0]> * ALID
	>.

* Enable/Disable Alarm Acknowledge
EAA: 'S5F4'
	<B[1] 0>. *ACKC5
* Enable/Disable Alarm Acknowledge for 6 inch ARM
EAA2: 'S5F4'
	<U1 0>. *ACKC5

* Discrete Variable Data Send
DVS : 'S6F3' w
	<L
		<U1 0> * DATAID
		<U1 2> * CEID
	>.

* S6F4 (From Host)
DVA : 'S6F4'
	<B[1] 0> * ACKC5

* S9F1 
UDN : 'S9F1'
	<B[10] 0 0 0 0 0 0 0 0 0 0>. * MHEAD

* S9F3
USN : 'S9F3'
	<B[10] 0 0 0 0 0 0 0 0 0 0>. * MHEAD

* S9F5
UFN : 'S9F5'
	<B[10] 0 0 0 0 0 0 0 0 0 0>. * MHEAD
