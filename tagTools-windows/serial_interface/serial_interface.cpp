#include "serial_interface.hpp"

using namespace SI;

serial_variables::serial_variables()
{
	baud = CBR_38400;
	bits = 8;
	parity = NOPARITY;
	stop_bits = ONESTOPBIT;
}

serial_interface::SI_implementation::SI_implementation()
{
	m_handle = NULL;
}

int serial_interface::SI_implementation::configure_port(const char* port)
{	
	CloseHandle(m_handle);
	m_handle = CreateFile(TEXT(port),		// Pointer to the name of the port (USE "COM1:")
		GENERIC_READ | GENERIC_WRITE,	// Access (read-write) mode
		0,							// Share mode
		NULL,						// Pointer to the security attribute
		OPEN_EXISTING,				// How to open the serial port
		0,							// Port attributes
		NULL);						// Handle to port with attribute to copy
	if(m_handle != INVALID_HANDLE_VALUE)
		return COMM_SUCCESS;
	else 
	{
		m_handle = INVALID_HANDLE_VALUE;
		return COMM_FAIL;
	}
}

int serial_interface::SI_implementation::configure_comm(int baud, int bits, int stop_bits, int parity)
{
	if(m_handle != INVALID_HANDLE_VALUE)
	{
		DCB PortDCB;

		// Initialize the DCBlength member. 
		PortDCB.DCBlength = sizeof (DCB);

		// Get the default port setting information.
		GetCommState (m_handle, &PortDCB);

		// Change the DCB structure settings.
		PortDCB.BaudRate = baud;              // Current baud 
		PortDCB.fBinary = TRUE;               // Binary mode; no EOF check 
		PortDCB.fParity = TRUE;               // Enable parity checking 
		PortDCB.fOutxCtsFlow = FALSE;         // No CTS output flow control 
		PortDCB.fOutxDsrFlow = FALSE;         // No DSR output flow control 
		PortDCB.fDtrControl = DTR_CONTROL_ENABLE; 
											  // DTR flow control type 
		PortDCB.fDsrSensitivity = FALSE;      // DSR sensitivity 
		PortDCB.fTXContinueOnXoff = TRUE;     // XOFF continues Tx 
		PortDCB.fOutX = FALSE;                // No XON/XOFF out flow control 
		PortDCB.fInX = FALSE;                 // No XON/XOFF in flow control 
		PortDCB.fErrorChar = FALSE;           // Disable error replacement 
		PortDCB.fNull = FALSE;                // Disable null stripping 
		PortDCB.fRtsControl = RTS_CONTROL_ENABLE; 
											  // RTS flow control 
		PortDCB.fAbortOnError = FALSE;        // Do not abort reads/writes on 
											  // error
		PortDCB.ByteSize = bits;              // Number of bits/byte, 4-8 
		PortDCB.Parity = parity;	          // 0-4=no,odd,even,mark,space 
		PortDCB.StopBits = stop_bits;	      // 0,1,2 = 1, 1.5, 2 

		// Configure the port according to the specifications of the DCB 
		// structure.
		if (!SetCommState (m_handle, &PortDCB))
		{
		  // Could not configure the serial port.
			m_handle = INVALID_HANDLE_VALUE;
			return COMM_STATE_FAIL;
		}
		else
			return COMM_SUCCESS;
	}
	else
	{
		m_handle = INVALID_HANDLE_VALUE;
		return COMM_FAIL;
	}
}

int serial_interface::SI_implementation::configure_timeouts()
{
	if (m_handle != INVALID_HANDLE_VALUE)
	{
		// Retrieve the timeout parameters for all read and write operations
		// on the port. 
		COMMTIMEOUTS CommTimeouts;
		GetCommTimeouts (m_handle, &CommTimeouts);

		// Change the COMMTIMEOUTS structure settings.
		CommTimeouts.ReadIntervalTimeout = MAXDWORD;  
		CommTimeouts.ReadTotalTimeoutMultiplier = 0;  
		CommTimeouts.ReadTotalTimeoutConstant = 0;    
		CommTimeouts.WriteTotalTimeoutMultiplier = 10;  
		CommTimeouts.WriteTotalTimeoutConstant = 1000;    

		// Set the timeout parameters for all read and write operations
		// on the port. 
		if (!SetCommTimeouts (m_handle, &CommTimeouts))
		{
		  // Could not set the timeout parameters.
			m_handle = INVALID_HANDLE_VALUE;
			return COMM_TIMEOUT_FAIL;
		}
		else
			return COMM_SUCCESS;
	}
	else
	{
		m_handle = INVALID_HANDLE_VALUE;
		return COMM_FAIL;
	}
}

/* SERIAL INTERFACE PUBLIC FUNCTIONS 
*
*  HANDLE get_serial_handle();
*  This function returns the handle to the serial port so that
*  other functions can use it.
*
*  bool setup_serial_interface();
*  This functions sets the serial port's baud rate, flow control
*  parity and stop bits and returns true if successful and false
*  if it fails
*
*  static serial_interface* instance();
*  This function ensures the singleton operation of the class.
*  It might make sense to not make this a Singleton though.
*  END SERIAL INTERFACE PUBLIC FUNCTIONS */

HANDLE serial_interface::get_serial_handle()
{
	return m_imp->m_handle;
}

serial_variables serial_interface::get_serial_interface()
{
	if(m_imp->m_handle != INVALID_HANDLE_VALUE)	// if the handle is already assigned
	{											// return currently set values
		DCB PortDCB;
		PortDCB.DCBlength = sizeof (DCB);

		// Get the default port setting information.
		GetCommState (m_imp->m_handle, &PortDCB);	
		vars.baud = PortDCB.BaudRate;
		vars.bits = PortDCB.ByteSize;
		vars.parity = PortDCB.fParity;
		vars.stop_bits = PortDCB.StopBits;
		return vars;
	}
	else										// otherwise set and return defaults
	{
		vars.baud = CBR_38400;
		vars.baud = 8;
		vars.parity = NOPARITY;
		vars.stop_bits = ONESTOPBIT;
		return vars;
	}
}

bool serial_interface::setup_serial_interface(const char* port, serial_variables vars)
{
	return setup_serial_interface(port, vars.baud, vars.bits, vars.stop_bits, vars.parity);
}

bool serial_interface::setup_serial_interface(std::string port, serial_variables vars)
{
	return setup_serial_interface(port.c_str(), vars.baud, vars.bits, vars.stop_bits, vars.parity);
}

bool serial_interface::setup_serial_interface(const char* port, int baud, int bits, int stop_bits, int parity)
{
	int success_code = m_imp->configure_port(port);
	if(success_code)
	{
		success_code = m_imp->configure_comm(baud, bits, stop_bits, parity);
		if (success_code)
		{
			success_code = m_imp->configure_timeouts();
			if (success_code)
				return true;
			else
			{
				// set error
				return false;
			}
		}
		else
		{
			// set error
			return false;
		}
	}
	else
	{
		// set error
		return false;
	}
}

serial_interface* serial_interface::m_instance = 0;

serial_interface* serial_interface::instance()
{
	if (m_instance == 0)
		m_instance = new serial_interface;
	return m_instance;
}

/* SERIAL INTERFACE PROTECTED FUNCTIONS
*
*  serial_interface();
*  This is the constructor.  It sets up the serial port with default values
*
*  ~serial_interface();
*  The destructor closes the port and nullifies the object
*  END SERIAL INTERFACE PROTECTED FUNCTIONS */

serial_interface::serial_interface()
{
	m_imp = new SI_implementation();
	setup_serial_interface(COM1, vars);
}

serial_interface::serial_interface(const char* port, serial_variables vars)
{
	m_imp = new SI_implementation();
	setup_serial_interface(port, vars);
}

serial_interface::~serial_interface()
{
	CloseHandle(m_imp->m_handle);
}