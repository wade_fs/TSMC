#ifndef SERIAL_INTERFACE_DEFINED
#define SERIAL_INTERFACE_DEFINED

// #include "card_holder.hpp"
// #include "boost/shared_ptr.hpp"
#include <windows.h>
#include <string>

namespace SI
{
	const std::string COM1 = "COM1:";
	const std::string COM2 = "COM2:";
	const std::string COM3 = "COM3:";

	const int COMM_SUCCESS = 1;
	const int COMM_FAIL = 0;
	const int COMM_STATE_FAIL = -1;
	const int COMM_TIMEOUT_FAIL = -2;

	struct serial_variables
	{
		int baud, parity, bits, stop_bits;
		serial_variables();
	};

	class serial_interface
	{
	public:
		// typedef boost::shared_ptr<serial_interface> serial_pointer;
		HANDLE get_serial_handle();
		serial_variables get_serial_interface();
		bool setup_serial_interface(const char*, serial_variables);
		bool setup_serial_interface(std::string, serial_variables);
		bool setup_serial_interface(const char*, int, int, int, int);
		static serial_interface* instance();
	protected:
		serial_interface();
		serial_interface(const char*, serial_variables);
		~serial_interface();
		class SI_implementation;
	private:
		SI_implementation *m_imp;
		static serial_interface *m_instance;
		serial_variables vars;
	}; // end class serial_interface

	class serial_interface::SI_implementation
	{
	public:
		HANDLE m_handle;
		SI_implementation();
		int configure_port(const char*);
		int configure_comm(int, int, int, int);
		int configure_timeouts();
	};
}; // end namespace SI

#endif // SERIAL_INTERFACE_DEFINED