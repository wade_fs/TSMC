#include <windows.h>
#include <iostream>
#include <string>
#include <fstream>
//#include <conio.h>
//#include <cstdio>
//#include <cstdlib>
#include "serial/serial.h"
#include "cmd.h"

using std::string;
using std::exception;
using std::cout;
using std::cerr;
using std::endl;
using std::vector;

void print_usage(char* c)
{
	cerr << "Usage: " << c << "<serial port address> <command file> [baudrate]" << endl;
}

int run(int argc, char **argv)
{
	bool Running = true, debug=false;
	int res = -1, key = 0;
	Cmd mycmd;
	string cmd = "";
	string port = "";
	unsigned long baud = 9600;
	fstream file;
	cerr << "Step 1" << endl;
	for (int i = 1; i < argc; ++i) {
		string arg(argv[i]);
		cerr << "Step 2: arg=" << arg << endl;
		if (arg == "-v") debug = true;
		else if (arg.find("COM") != std::string::npos) port = arg;
		else if (arg.find("00") != std::string::npos) baud = std::stol(arg);
		else if (arg == "-h") {
			print_usage(argv[0]);
			return 0;
		}
		else {
			struct stat buffer;
			if (stat(arg.c_str(), &buffer) == 0) {
				cerr << "step 3, open file " << arg << endl;
				file.open(arg.c_str(), std::fstream::in);
			}
		}
	}
	if (port.size() == 0) {
		print_usage(argv[0]);
		return 0;
	}

	//// port, baudrate, timeout in milliseconds
	//serial::Serial ser(port, baud, serial::Timeout::simpleTimeout(1000));

	//if (!ser.isOpen()) {
	//	cout << "Please check whether the " << port << " exists, and check its access permission.\n";
	//	exit(1);
	//}
	serial::Serial ser=NULL;

	string line;
	//std::vector<std::string> myLines;
	//std::copy(std::istream_iterator<std::string>(myfile),
	//	std::istream_iterator<std::string>(),
	//	std::back_inserter(myLines));
	if (!file.is_open()) {
		cerr << "No cmd file" << endl;
		exit(3);
	}
	while (Running && std::getline(file, line)) {
		// line = Cmd::trim(line);
		cout << "Line " << line << endl;
		continue;
		if (line.size() == 0) continue;
		switch (line[0]) {
		case '#': break;
			// NORMAL command
		case 'a': res = mycmd.doCmd(ser, ENUM_CMD_DISPLAY_TIME);			cmd = "DISPLAY_TIME";  break;
		case 'b': res = mycmd.doCmd(ser, ENUM_CMD_LCD_DISPLAY_ON);			cmd = "LCD_DISPLAY_ON"; break;
		case 'c': res = mycmd.doCmd(ser, ENUM_CMD_LCD_DISPLAY_OFF);			cmd = "LCD_DISPLAY_OFF";  break;
		case 'd': res = mycmd.doCmd(ser, ENUM_CMD_CLEAR_LCD);				cmd = "CLEAR_LCD";  break;
		case 'e': res = mycmd.doCmd(ser, ENUM_CMD_ENABLE_TIMEOUT);			cmd = "ENABLE_TIMEOUT"; break;
		case 'f': res = mycmd.doCmd(ser, ENUM_CMD_DISABLE_TIMEOUT);			cmd = "DISABLE_TIMEOUT"; break;
		case 'g': res = mycmd.doCmd(ser, ENUM_CMD_DISPLAY_SERIAL_NUMBER);	cmd = "DISPLAY_SERIAL_NUMBER"; break;
			// READ
		case 'h': res = mycmd.doCmd(ser, ENUM_CMD_NORMAL_STATUS_READ);		cmd = "NORMAL_STATUS_READ"; break;
		case 'i': res = mycmd.doCmd(ser, ENUM_CMD_EXTENDED_STATUS2_READ);	cmd = "EXTENDED_STATUS2_READ"; break;
		case 'j': res = mycmd.doCmd(ser, ENUM_CMD_PROTECTED_DATA_READ);		cmd = "PROTECTED_DATA_READ"; break;
		case 'k': res = mycmd.doCmd(ser, ENUM_CMD_GET_TAG_FORMAT);			cmd = "GET_TAG_FORMAT"; break;
			// FORMAT
		case 'l': res = mycmd.doCmd(ser, ENUM_CMD_FORMAT_TAG);				cmd = "FORMAT_TAG"; break;
			// READ LCD Data
		case 'm': res = mycmd.doCmd(ser, ENUM_CMD_READ_LCD_DATA);			cmd = "READ_LCD_DATA"; break;
			// WRITE LCD Data
		case 'n': res = mycmd.doCmd(ser, ENUM_CMD_WRITE_LCD_DATA);			cmd = "WRITE_LCD_DATA"; break;
			// DISPLAY_TIME ����
		case 'A': // DISPLAY_TIME TEST
			cmd = "DISPLAY_TIME TEST";
			cout << endl << "Wait for testing " << cmd << "...." << flush;
			res = mycmd.doCmd(ser, ENUM_CMD_LCD_DISPLAY_ON);
			if (res == 0) {
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_DISPLAY_TIME);
			}
			else break;
			if (res == 0) {
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_LCD_DISPLAY_OFF);
			}
			break;
		case 'B': // CLEAR_LCD TEST
			cmd = "CLEAR_LCD TEST";
			cout << endl << "Wait for testing " << cmd << "...." << flush;
			res = mycmd.doCmd(ser, ENUM_CMD_LCD_DISPLAY_ON);
			if (res == 0) {
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_CLEAR_LCD);
			}
			else break;
			if (res == 0) {
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_LCD_DISPLAY_OFF);
			}
			break;
		case 'C': // DISPLAY_SERIAL_NUMBER TEST
			cmd = "DISPLAY_SERIAL_NUMBER TEST";
			cout << endl << "Wait for testing " << cmd << "...." << flush;
			res = mycmd.doCmd(ser, ENUM_CMD_LCD_DISPLAY_ON);
			if (res == 0) {
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_DISPLAY_SERIAL_NUMBER);
			}
			else break;
			if (res == 0) {
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_LCD_DISPLAY_OFF);
			}
			break;
		case 'D': // GET_TAG_FORMAT TEST
			cmd = "GET_TAG_FORMAT TEST";
			cout << endl << "Wait for testing " << cmd << "...." << flush;
			res = mycmd.doCmd(ser, ENUM_CMD_LCD_DISPLAY_ON);
			if (res == 0) {
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_GET_TAG_FORMAT);
			}
			else break;
			if (res == 0) {
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_LCD_DISPLAY_OFF);
			}
			break;
		case 'E': // READ_LCD_DATA TEST
			cmd = "READ_LCD_DATA TEST";
			cout << endl << "Wait for testing " << cmd << "...." << flush;
			res = mycmd.doCmd(ser, ENUM_CMD_LCD_DISPLAY_ON);
			if (res == 0) {
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_READ_LCD_DATA);
			}
			else break;
			if (res == 0) {
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_LCD_DISPLAY_OFF);
			}
			break;
		case 'F': // TIMEOUT TEST
			cmd = "TIMEOUT TEST";
			cout << endl << "Wait for testing " << cmd << "...." << flush;
			res = mycmd.doCmd(ser, ENUM_CMD_LCD_DISPLAY_ON);
			if (res == 0) {
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_ENABLE_TIMEOUT);
			}
			else break;
			if (res == 0) {
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_DISABLE_TIMEOUT);
			}
			else break;
			if (res == 0) {
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_LCD_DISPLAY_OFF);
			}
			break;
			// QUIT
		case 'G': // STATUS_READ TEST
			cmd = "STATUS_READ TEST";
			cout << endl << "Wait for testing " << cmd << "...." << flush;
			res = mycmd.doCmd(ser, ENUM_CMD_LCD_DISPLAY_ON);
			if (res == 0) {
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_NORMAL_STATUS_READ);
			}
			else break;
			if (res == 0) {
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_EXTENDED_STATUS2_READ);
			}
			else break;
			if (res == 0) {
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_PROTECTED_DATA_READ);
			}
			else break;
			if (res == 0) {
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_LCD_DISPLAY_OFF);
			}
			break;
		case 'X': // ALL TEST
			cmd = "ALL TEST";
			cout << endl << "Wait for testing " << cmd << "...." << flush;
			res = mycmd.doCmd(ser, ENUM_CMD_LCD_DISPLAY_ON);
			if (res == 0) {
				cmd = "DISPLAY_TIME TEST";
				cout << endl << "Wait for testing " << cmd << "...." << flush;
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_DISPLAY_TIME);
				cmd = "ALL";
			}
			else break;
			if (res == 0) {
				cmd = "CLEAR_LCD TEST";
				cout << endl << "Wait for testing " << cmd << "...." << flush;
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_CLEAR_LCD);
				cmd = "ALL";
			}
			else break;
			if (res == 0) {
				cmd = "DISPLAY_SERIAL_NUMBER TEST";
				cout << endl << "Wait for testing " << cmd << "...." << flush;
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_DISPLAY_SERIAL_NUMBER);
				cmd = "ALL";
			}
			else break;
			if (res == 0) {
				cmd = "GET_TAG_FORMAT TEST";
				cout << endl << "Wait for testing " << cmd << "...." << flush;
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_GET_TAG_FORMAT);
				cmd = "ALL";
			}
			else break;
			if (res == 0) {
				cmd = "READ_LCD_DATA TEST";
				cout << endl << "Wait for testing " << cmd << "...." << flush;
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_READ_LCD_DATA);
				cmd = "ALL";
			}
			else break;
			if (res == 0) {
				cmd = "ENABLE_TIMEOUT TEST";
				cout << endl << "Wait for testing " << cmd << "...." << flush;
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_ENABLE_TIMEOUT);
				cmd = "ALL";
			}
			else break;
			if (res == 0) {
				cmd = "DISABLE_TIMEOUT TEST";
				cout << endl << "Wait for testing " << cmd << "...." << flush;
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_DISABLE_TIMEOUT);
				cmd = "ALL";
			}
			else break;
			if (res == 0) {
				cmd = "NORMAL_STATUS_READ TEST";
				cout << endl << "Wait for testing " << cmd << "...." << flush;
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_NORMAL_STATUS_READ);
				cmd = "ALL";
			}
			else break;
			if (res == 0) {
				cmd = "EXTENDED_STATUS2_READ TEST";
				cout << endl << "Wait for testing " << cmd << "...." << flush;
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_EXTENDED_STATUS2_READ);
				cmd = "ALL";
			}
			else break;
			if (res == 0) {
				cmd = "PROTECTED_DATA_READ TEST";
				cout << endl << "Wait for testing " << cmd << "...." << flush;
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_PROTECTED_DATA_READ);
				cmd = "ALL";
			}
			else break;
			if (res == 0) {
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_LCD_DISPLAY_OFF);
			}
			cmd = "ALL";
			break;
			// QUIT
		case 'q':
			mycmd.writeByteArray(ser, { 'q' });
			Running = false;
			break;
		default: res = -1; cmd = "";
		}
	}
	cout << "end..." << endl;
	// ser.close();
	file.close();
	return 0;
}

int main(int argc, char **argv) {
	run(argc, argv);
	//try {
	//	return run(argc, argv);
	//}
	//catch (serial::PortNotOpenedException &e) {
	//	cerr << "serial::PortNotOpenedException: " << e.what() << endl;
	//}
	//catch (serial::IOException &e) {
	//	cerr << "serial::IOException: " << e.what() << endl;
	//}
	//catch (serial::SerialException &e) {
	//	cerr << "serial::SerialException: " << e.what() << endl;
	//}
}
