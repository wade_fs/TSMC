/***
 * This example expects the serial port has a loopback on it.
 *
 * Alternatively, you could use an Arduino:
 *
 * <pre>
 *  void setup() {
 *    Serial.begin(<insert your baudrate here>);
 *  }
 *
 *  void loop() {
 *    if (Serial.available()) {
 *      Serial.write(Serial.read());
 *    }
 *  }
 * </pre>
 */

#include <string>
#include <iostream>
#include <cstdio>
#include <windows.h>

#include "serial/serial.h"
#include "cmd.h"

using std::string;
using std::exception;
using std::cout;
using std::cerr;
using std::endl;
using std::vector;
static const uint8_t _lcdData[] = {
	0xFF, 0xFF, 0xD0, 0xFF, 0xF5, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0x7F, 0xFF, 0xFF, 0xFF, 0x8F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xEB, 0x3F, 0xFF, 0xFF, 0x87, 0xFF, 0xFF, 0xFF, 0x5F, 0xFF,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x3B, 0x3F, 0xFF, 0xFF,
	0x7F, 0xFF, 0xFF, 0xFF, 0x3F, 0xFF, 0xFF, 0xFF, 0x7F, 0xFF,
	0xFF, 0xFF, 0x5B, 0x3F, 0xFF, 0xFF, 0xBF, 0xFF, 0xFF, 0xFF,
	0xBF, 0xFF, 0xFF, 0xFF, 0xBF, 0xFF, 0xFF, 0xFF, 0x9B, 0x3F,
	0xFF, 0xFF, 0x3F, 0xFF, 0xFF, 0xFF, 0x7F, 0xFF, 0xFF, 0xFF,
	0x3F, 0xFF, 0xFF, 0xFF, 0x1B, 0x3F, 0xFF, 0xFF, 0xDF, 0xFF,
	0xFF, 0xFF, 0x7F, 0xFF, 0xFF, 0xFF, 0xDF, 0xFF, 0xFF, 0xFF,
	0x6F, 0xFF, 0xFF, 0xFF, 0x5F, 0xFF, 0xFF, 0xFF, 0x07, 0x3F,
	0xFF, 0xFF, 0xDF, 0xFF, 0xFF, 0xFF, 0x1B, 0x3F, 0xFF, 0xFF,
	0x5F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x5F, 0xFF,
	0xFF, 0xFF, 0x1F, 0xFF, 0xFF, 0xFF, 0x9F, 0xFF, 0xFF, 0xFF,
	0x7B, 0x3F, 0xFF, 0xFF, 0x5F, 0xFF, 0xFF, 0xFF, 0xEB, 0x3F,
	0xFF, 0xFF, 0x9F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0x9F, 0xFF, 0xFF, 0xFF, 0x9F, 0xFF, 0xFF, 0xFF, 0x1F, 0xFF,
	0xFF, 0xFF, 0xBB, 0x3F, 0xFF, 0xFF, 0x9F, 0xFF, 0xFF, 0xFF,
	0xEB, 0x3F, 0xFF, 0xFF, 0x1F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFF, 0xFF, 0x1F, 0xFF, 0xFF, 0xFF, 0x5F, 0xFF, 0xFF, 0xFF,
	0x6F, 0xFF, 0xFF, 0xFF, 0x73, 0xB3, 0x33, 0xFB, 0xFB, 0xFB,
	0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB,
	0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB,
	0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xF4, 0x3C, 0xFF, 0x1F,
	0xFF };
vector<uint8_t> lcdData(_lcdData, _lcdData+sizeof(_lcdData));  // arr, arr + sizeof(arr) / sizeof(arr[0])
/*
vector <int> theVector = {1, 2, 3, 4, 5};
===>
static const int arr[] = {1, 2, 3, 4, 5};
vector<int> theVector (arr, arr + sizeof(arr) / sizeof(arr[0]) );
*/

static const uint8_t _Status[] = {
	0x00, 0xA8, 0x42, 0x00, 0xCD, 0x0D, 0x5B, 0xDC, 0x80, 0xC2,
	0xFF, 0xD6, 0xC8, 0xFF, 0x13, 0xAA, 0x00, 0x5E, 0x00, 0x00,
	0xBE, 0x69, 0x00, 0x24, 0x10 };
vector<uint8_t> Status(_Status, _Status+sizeof(_Status) / sizeof(_Status[0]));

BOOL CtrlHandler(DWORD fdwCtrlType){
	switch (fdwCtrlType) {
		// Handle the CTRL-C signal. 
	case CTRL_C_EVENT:
		return(TRUE);
		// Pass other signals to the next handler. 
	case CTRL_BREAK_EVENT:
		return FALSE;
	case CTRL_LOGOFF_EVENT:
		return FALSE;
	case CTRL_SHUTDOWN_EVENT:
		return FALSE;
	default:
		return FALSE;
	}
}

void my_sleep(unsigned long milliseconds) {
      Sleep(milliseconds); // 100 ms
}

void enumerate_ports()
{
	vector<serial::PortInfo> devices_found = serial::list_ports();

	vector<serial::PortInfo>::iterator iter = devices_found.begin();

	while( iter != devices_found.end() )
	{
		serial::PortInfo device = *iter++;

		printf( "(%s, %s, %s)\n", device.port.c_str(), device.description.c_str(),
     device.hardware_id.c_str() );
	}
}

void print_usage(char* c)
{
	cerr << "Usage: " << c << "{-l|<serial port address>} <baudrate>" << endl;
}

int run(int argc, char **argv)
{
	if (argc < 2) {
		print_usage(argv[0]);
		return 0;
	}

	// Argument 1 is the serial port or enumerate flag
	string port(argv[1]);

	if (port == "-l") {
		enumerate_ports();
		return 0;
	}
	else if (argc < 3) {
		print_usage(argv[0]);
		return 1;
	}

	// Argument 2 is the baudrate
	unsigned long baud = 0;
	sscanf_s(argv[2], "%lu", &baud);

	// port, baudrate, timeout in milliseconds
	serial::Serial ser(port, baud, serial::Timeout::simpleTimeout(1000));

	if (!ser.isOpen()) {
		cout << "請檢查 " << port << "，例如是否存在，以及權限是否可讀寫\n";
		exit(1);
	}

	string pkt;
	bool Running = true;
	int key = 0;
	Cmd mycmd;

	while (Running) {
		int cmdId = 0;
		printf("waiting packet...\n");
		// 前4個封包算是固定的，跟 cmdId 無關
		// 第1個封包 read
		pkt = mycmd.readPkt(ser, 0); // 第1個封包肯定是 ENUM_PK_START_CMD
		// 有點 bug, 有時第1個byte 會突然跑出 00，或者說，後面會突然多出一個 00 byte
		if (pkt.size() == 1 && pkt[0] == '\x00') {
			printf("只有一個 00");
			continue;
		}
		// QUIT
		if (pkt.size() == 1 && pkt[0] == 'q') {
			break;
		}
		// 底下是專門給測試封包用的
		if (pkt.size() == 3 && pkt[0] == '\x00') {
			printf("測試封包");
			ser.write(mycmd.packets[ENUM_PK_CMD_DONE]);
			continue;
		}
		// 第2個封包 write
		if (mycmd.pktEqual(pkt, mycmd.cmds[cmdId][0])) {
			mycmd.writePkt(ser, mycmd.cmds[cmdId][1]);
		}
		else {
			continue;
		}

		// 第3個封包 read, 此時就可以判斷是哪個命令
		pkt = mycmd.readPkt(ser, 30);
		cmdId = mycmd.getCmdId(pkt);
		if (cmdId < 0) {
			cout << "cannot found cmdId for pkt : " << mycmd.toHex(pkt) << endl;
			continue;
		}
		printf("cmdId = %d\n", cmdId);

		// 第4個封包 write
		if (mycmd.pktEqual(pkt, mycmd.cmds[cmdId][2])) {
			mycmd.writePkt(ser, mycmd.cmds[cmdId][3]);
		}

		// 前7個命令到此為止
		if (cmdId < ENUM_CMD_NORMAL_STATUS_READ) {
			continue;
			// 第7個命令之後會有額外封包
		}
		else if (cmdId < ENUM_CMD_FORMAT_TAG) { // 命令 7, 8, 9, 10 是 read, 所以需要再回傳內容，然後收到 ENUM_PK_READ_DONE。這邊是固>定值
			mycmd.usleep(40000);
			mycmd.writePkt(ser, mycmd.cmds[cmdId][4]);
			mycmd.usleep(2000);
			string pkt = mycmd.readPkt(ser, 40); // 這邊是最後一個封包, 目前是 READ 命令，所以是 ENUM_PK_READ_DONE
			if (mycmd.pktEqual(pkt, mycmd.cmds[cmdId][5])) { // 此時的 cmd.cmds[cmdId][5] 都只有3bytes，比對時會自動裁切
				printf("Cmd %d DONE\n", cmdId);
			}
			else {
				printf("Cmd %d BAD\n", cmdId);
			}
		}
		else if (cmdId == ENUM_CMD_FORMAT_TAG) { // 乍看跟 7/8/9/10 一樣，其實是相反方向的，還需要讀、寫(ENUM_PK_CMD_DONE)一次
			pkt = mycmd.readPkt(ser, 40);
			if (mycmd.pktEqual(pkt, mycmd.cmds[cmdId][4])) {
				mycmd.writePkt(ser, mycmd.cmds[cmdId][5]);
			}
			else {
				continue;
			}
		}
		else if (cmdId == ENUM_CMD_WRITE_LCD_DATA) {
			string pkt = mycmd.readPkt(ser, 40);
			lcdData.clear();
			lcdData.assign(pkt.begin(), pkt.end());
			// 這邊應該要檢查 CRC
		}
		else { // READ_LCD_DATA 比較特殊, 還有 ENUM_PK_LCD_DATAvr, ENUM_PK_READ_DONE, ENUM_PK_LCD_DATA21, ENUM_PK_READ_DONE
			mycmd.writeByteArray(ser, lcdData);
			pkt = mycmd.readPkt(ser, 40);
			if (!mycmd.pktEqual(pkt, mycmd.cmds[cmdId][5])) {
				printf("Cmd %d BAD\n", cmdId);
				continue;
			}
			mycmd.writePkt(ser, mycmd.cmds[cmdId][6]);
			pkt = mycmd.readPkt(ser, 40);
			if (mycmd.pktEqual(pkt, mycmd.cmds[cmdId][7])) {
				printf("Cmd %d DONE\n", cmdId);
			}
			else {
				printf("Cmd %d BAD\n", cmdId);
			}
		}
	}

	return 0;
}

int main(int argc, char **argv) {
	SetConsoleCtrlHandler((PHANDLER_ROUTINE)CtrlHandler, TRUE);
	try {
		return run(argc, argv);
	}
	catch (serial::PortNotOpenedException &e) {
		cerr << "serial::PortNotOpenedException: " << e.what() << endl;
	}
	catch (serial::IOException &e) {
		cerr << "serial::IOException: " << e.what() << endl;
	}
	catch (serial::SerialException &e) {
		cerr << "serial::SerialException: " << e.what() << endl;
	}
}
