/***
 * This example expects the serial port has a loopback on it.
 *
 * Alternatively, you could use an Arduino:
 *
 * <pre>
 *  void setup() {
 *    Serial.begin(<insert your baudrate here>);
 *  }
 *
 *  void loop() {
 *    if (Serial.available()) {
 *      Serial.write(Serial.read());
 *    }
 *  }
 * </pre>
 */
#include <windows.h>
#include <iostream>
#include <string>
#include <conio.h>
#include <cstdio>
#include <cstdlib>
#include "serial/serial.h"
#include "cmd.h"

using std::string;
using std::exception;
using std::cout;
using std::cerr;
using std::endl;
using std::vector;

void enumerate_ports()
{
	vector<serial::PortInfo> devices_found = serial::list_ports();

	vector<serial::PortInfo>::iterator iter = devices_found.begin();

	while (iter != devices_found.end())
	{
		serial::PortInfo device = *iter++;

		printf("port = %s\n\tDescription = %s\n\tHardware ID = %s\n", device.port.c_str(), device.description.c_str(),
			device.hardware_id.c_str());
	}
}

void print_usage(char* c)
{
	cerr << "Usage: " << c << "{-l|<serial port address>} <baudrate>" << endl;
}

int menu(int res, string cmd, bool debug=false) {
	if (!debug) system("cls");
	// 底下7個命令都是固定的4封包格式 : 相當於 > Start, < ACK, > CMD, < DONE
	printf("a) cmd : Display Time\n");
	printf("b) cmd : LCD Display On\n");
	printf("c) cmd : DISPLAY_SERIAL_NUMBER\n");
	printf("d) cmd : Clear LCD\n");
	printf("e) cmd : Enable Timeout\n");
	printf("f) cmd : Disable Timeout\n");
	printf("g) cmd : Display Serial Number\n");
	// 底下5個命令算是同一類，前4個封包後面，接上變動長度的內容，最後再加上 ENUM_PK_READ_DONE/ENUM_PK_CMD_DONE
	printf("h) cmd : Normal Status Read\n");
	printf("i) cmd : Extended Status2 Read\n");
	printf("j) cmd : Protected Data Read\n");
	printf("k) cmd : Get Tag Format\n");
	printf("l) cmd : Format Tag\n");
	// 底下命令特殊的地方是，後面並沒有 ..._DONE 的確認封包
	printf("m) cmd : Write LCD Data\n");
	// 底下命令特殊的地方是，後面有兩段來回確認，我猜是因為資料被切成2段
	printf("n) cmd : Read LCD Data\n");
	printf("A) TEST: DISPLAY_TIME\n");
	printf("B) TEST: CLEAR_LCD\n");
	printf("C) TEST: DISPLAY_SERIAL_NUMBER\n");
	printf("D) TEST: GET_TAG_FORMAT\n");
	printf("E) TEST: READ_LCD_DATA\n");
	printf("F) TEST: TIMEOUT\n");
	printf("G) TEST: STATUS_READ\n");
	printf("X) TEST: ALL\n");
	if (res == 0) {
		printf("q) Quit [CMD (%s) OK] >", cmd.c_str());
	}
	else if (res > 0) {
		printf("q) Quit [CMD (%s) ERROR] >", cmd.c_str());
	}
	else {
		printf("q) Quit >");
	}
	cout << flush;
	int _key = _getche();
	return _key;
}
int run(int argc, char **argv)
{
	bool Running = true, debug=false;
	int res = -1, key = 0;
	Cmd mycmd;
	string cmd = "";

	if (argc < 2) {
		print_usage(argv[0]);
		return 0;
	}

	// Argument 1 is the serial port or enumerate flag
	string port(argv[1]);

	if (port == "-l") {
		enumerate_ports();
		return 0;
	}
	else if (argc < 3) {
		print_usage(argv[0]);
		return 1;
	}

	// Argument 2 is the baudrate
	unsigned long baud = 0;
	sscanf_s(argv[2], "%lu", &baud);
	if (argc == 4) {
		mycmd.setDebug(true);
		debug = true;
	}

	// port, baudrate, timeout in milliseconds
	serial::Serial ser(port, baud, serial::Timeout::simpleTimeout(1000));

	if (!ser.isOpen()) {
		cout << "請檢查 " << port << "，例如是否存在，以及權限是否可讀寫\n";
		exit(1);
	}

	while (Running) {
		key = menu(res, cmd, debug);
		switch (key) {
			// NORMAL command
		case 'a': res = mycmd.doCmd(ser, ENUM_CMD_DISPLAY_TIME);			cmd = "DISPLAY_TIME";  break;
		case 'b': res = mycmd.doCmd(ser, ENUM_CMD_LCD_DISPLAY_ON);			cmd = "LCD_DISPLAY_ON"; break;
		case 'c': res = mycmd.doCmd(ser, ENUM_CMD_LCD_DISPLAY_OFF);			cmd = "LCD_DISPLAY_OFF";  break;
		case 'd': res = mycmd.doCmd(ser, ENUM_CMD_CLEAR_LCD);				cmd = "CLEAR_LCD";  break;
		case 'e': res = mycmd.doCmd(ser, ENUM_CMD_ENABLE_TIMEOUT);			cmd = "ENABLE_TIMEOUT"; break;
		case 'f': res = mycmd.doCmd(ser, ENUM_CMD_DISABLE_TIMEOUT);			cmd = "DISABLE_TIMEOUT"; break;
		case 'g': res = mycmd.doCmd(ser, ENUM_CMD_DISPLAY_SERIAL_NUMBER);	cmd = "DISPLAY_SERIAL_NUMBER"; break;
			// READ
		case 'h': res = mycmd.doCmd(ser, ENUM_CMD_NORMAL_STATUS_READ);		cmd = "NORMAL_STATUS_READ"; break;
		case 'i': res = mycmd.doCmd(ser, ENUM_CMD_EXTENDED_STATUS2_READ);	cmd = "EXTENDED_STATUS2_READ"; break;
		case 'j': res = mycmd.doCmd(ser, ENUM_CMD_PROTECTED_DATA_READ);		cmd = "PROTECTED_DATA_READ"; break;
		case 'k': res = mycmd.doCmd(ser, ENUM_CMD_GET_TAG_FORMAT);			cmd = "GET_TAG_FORMAT"; break;
			// FORMAT
		case 'l': res = mycmd.doCmd(ser, ENUM_CMD_FORMAT_TAG);				cmd = "FORMAT_TAG"; break;
			// READ LCD Data
		case 'm': res = mycmd.doCmd(ser, ENUM_CMD_READ_LCD_DATA);			cmd = "READ_LCD_DATA"; break;
			// WRITE LCD Data
		case 'n': res = mycmd.doCmd(ser, ENUM_CMD_WRITE_LCD_DATA);			cmd = "WRITE_LCD_DATA"; break;
			// DISPLAY_TIME 測試
		case 'A': // DISPLAY_TIME TEST
			cmd = "DISPLAY_TIME TEST";
			cout << endl << "Wait for testing " << cmd << "...." << flush;
			res = mycmd.doCmd(ser, ENUM_CMD_LCD_DISPLAY_ON);
			if (res == 0) {
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_DISPLAY_TIME);
			}
			else break;
			if (res == 0) {
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_LCD_DISPLAY_OFF);
			}
			break;
		case 'B': // CLEAR_LCD TEST
			cmd = "CLEAR_LCD TEST";
			cout << endl << "Wait for testing " << cmd << "...." << flush;
			res = mycmd.doCmd(ser, ENUM_CMD_LCD_DISPLAY_ON);
			if (res == 0) {
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_CLEAR_LCD);
			}
			else break;
			if (res == 0) {
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_LCD_DISPLAY_OFF);
			}
			break;
		case 'C': // DISPLAY_SERIAL_NUMBER TEST
			cmd = "DISPLAY_SERIAL_NUMBER TEST";
			cout << endl << "Wait for testing " << cmd << "...." << flush;
			res = mycmd.doCmd(ser, ENUM_CMD_LCD_DISPLAY_ON);
			if (res == 0) {
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_DISPLAY_SERIAL_NUMBER);
			}
			else break;
			if (res == 0) {
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_LCD_DISPLAY_OFF);
			}
			break;
		case 'D': // GET_TAG_FORMAT TEST
			cmd = "GET_TAG_FORMAT TEST";
			cout << endl << "Wait for testing " << cmd << "...." << flush;
			res = mycmd.doCmd(ser, ENUM_CMD_LCD_DISPLAY_ON);
			if (res == 0) {
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_GET_TAG_FORMAT);
			}
			else break;
			if (res == 0) {
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_LCD_DISPLAY_OFF);
			}
			break;
		case 'E': // READ_LCD_DATA TEST
			cmd = "READ_LCD_DATA TEST";
			cout << endl << "Wait for testing " << cmd << "...." << flush;
			res = mycmd.doCmd(ser, ENUM_CMD_LCD_DISPLAY_ON);
			if (res == 0) {
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_READ_LCD_DATA);
			}
			else break;
			if (res == 0) {
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_LCD_DISPLAY_OFF);
			}
			break;
		case 'F': // TIMEOUT TEST
			cmd = "TIMEOUT TEST";
			cout << endl << "Wait for testing " << cmd << "...." << flush;
			res = mycmd.doCmd(ser, ENUM_CMD_LCD_DISPLAY_ON);
			if (res == 0) {
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_ENABLE_TIMEOUT);
			}
			else break;
			if (res == 0) {
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_DISABLE_TIMEOUT);
			}
			else break;
			if (res == 0) {
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_LCD_DISPLAY_OFF);
			}
			break;
			// QUIT
		case 'G': // STATUS_READ TEST
			cmd = "STATUS_READ TEST";
			cout << endl << "Wait for testing " << cmd << "...." << flush;
			res = mycmd.doCmd(ser, ENUM_CMD_LCD_DISPLAY_ON);
			if (res == 0) {
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_NORMAL_STATUS_READ);
			}
			else break;
			if (res == 0) {
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_EXTENDED_STATUS2_READ);
			}
			else break;
			if (res == 0) {
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_PROTECTED_DATA_READ);
			}
			else break;
			if (res == 0) {
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_LCD_DISPLAY_OFF);
			}
			break;
		case 'X': // ALL TEST
			cmd = "ALL TEST";
			cout << endl << "Wait for testing " << cmd << "...." << flush;
			res = mycmd.doCmd(ser, ENUM_CMD_LCD_DISPLAY_ON);
			if (res == 0) {
				cmd = "DISPLAY_TIME TEST";
				cout << endl << "Wait for testing " << cmd << "...." << flush;
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_DISPLAY_TIME);
				cmd = "ALL";
			}
			else break;
			if (res == 0) {
				cmd = "CLEAR_LCD TEST";
				cout << endl << "Wait for testing " << cmd << "...." << flush;
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_CLEAR_LCD);
				cmd = "ALL";
			}
			else break;
			if (res == 0) {
				cmd = "DISPLAY_SERIAL_NUMBER TEST";
				cout << endl << "Wait for testing " << cmd << "...." << flush;
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_DISPLAY_SERIAL_NUMBER);
				cmd = "ALL";
			}
			else break;
			if (res == 0) {
				cmd = "GET_TAG_FORMAT TEST";
				cout << endl << "Wait for testing " << cmd << "...." << flush;
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_GET_TAG_FORMAT);
				cmd = "ALL";
			}
			else break;
			if (res == 0) {
				cmd = "READ_LCD_DATA TEST";
				cout << endl << "Wait for testing " << cmd << "...." << flush;
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_READ_LCD_DATA);
				cmd = "ALL";
			}
			else break;
			if (res == 0) {
				cmd = "ENABLE_TIMEOUT TEST";
				cout << endl << "Wait for testing " << cmd << "...." << flush;
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_ENABLE_TIMEOUT);
				cmd = "ALL";
			}
			else break;
			if (res == 0) {
				cmd = "DISABLE_TIMEOUT TEST";
				cout << endl << "Wait for testing " << cmd << "...." << flush;
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_DISABLE_TIMEOUT);
				cmd = "ALL";
			}
			else break;
			if (res == 0) {
				cmd = "NORMAL_STATUS_READ TEST";
				cout << endl << "Wait for testing " << cmd << "...." << flush;
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_NORMAL_STATUS_READ);
				cmd = "ALL";
			}
			else break;
			if (res == 0) {
				cmd = "EXTENDED_STATUS2_READ TEST";
				cout << endl << "Wait for testing " << cmd << "...." << flush;
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_EXTENDED_STATUS2_READ);
				cmd = "ALL";
			}
			else break;
			if (res == 0) {
				cmd = "PROTECTED_DATA_READ TEST";
				cout << endl << "Wait for testing " << cmd << "...." << flush;
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_PROTECTED_DATA_READ);
				cmd = "ALL";
			}
			else break;
			if (res == 0) {
				mycmd.usleep(1000000);
				res = mycmd.doCmd(ser, ENUM_CMD_LCD_DISPLAY_OFF);
			}
			cmd = "ALL";
			break;
			// QUIT
		case 'q': {
			const uint8_t _q[] = { 'q' };
			vector <uint8_t> q(_q, _q+sizeof(_q)/sizeof(_q[0]));
			mycmd.writeByteArray(ser, q);
			Running = false;
			break; }
		default: res = -1; cmd = "";
		}
	}
	ser.close();
	return 0;
}

int main(int argc, char **argv) {
	try {
		return run(argc, argv);
	}
	catch (serial::PortNotOpenedException &e) {
		cerr << "serial::PortNotOpenedException: " << e.what() << endl;
	}
	catch (serial::IOException &e) {
		cerr << "serial::IOException: " << e.what() << endl;
	}
	catch (serial::SerialException &e) {
		cerr << "serial::SerialException: " << e.what() << endl;
	}
}
