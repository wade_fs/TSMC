@echo off
setLocal EnableDelayedExpansion
set PORT=COM3
set LIST="0"
set DEBUG=
set A=
for %%a in (%1 %2 %3 %4) do (
  set A=%%a
  if not "x!A:COM=!"=="x!A!" (
    set PORT=!A!
  )
  if "!A!"=="-l" ( set LIST="-l" )
  if "!A!"=="-v" set DEBUG=1 )
  if "!A!"=="-h" ( goto help )
  if "!A!"=="-help" ( goto help )
  if "!A!"=="/h" ( goto help )
  if "!A!"=="/help" ( goto help )
)
if %LIST%=="-l" goto list
:NORMAL
client.exe %PORT% 9600 %DEBUG%
goto end
:list
client.exe -l
goto end
:help

@echo Usage: start.bat [-h] [-l] [-v] [PORT]
@echo Example and Description :
@echo 	start.bat	: Run client.exe in Standard Mode
@echo 	start.bat -v	: Run client.exe in Verbose Mode
@echo 	start.bat -h	: Print this message
@echo 	start.bat -l	: list COM Ports in system
@echo 	start.bat COM3	: Run client.exe by using COM3
:end
endlocal
