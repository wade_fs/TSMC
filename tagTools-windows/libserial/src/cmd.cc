#include <windows.h>
#include <unordered_map>
#include <iostream>
#include <ctype.h>
#include "cmd.h"

const string Cmd::lut = "0123456789ABCDEF";
string _PK_START_CMD = "\xFC\xFF";                                                              // 0
string _PK_DISPLAY_TIME = "\xFF\xFF\xDF\xFF\xBB\xFF\xFF\xBB\xFF\xFF\xFF";                          // 1
string _PK_READ_TIME = "\xFF\xFF\xDF\xFF\x3B\xFF\xFF\x3B\xFF\xFF\xFF";                             // 2
string _PK_READ_DONE = "\x9F\xFF";                                                                 // 3
string _PK_NORMAL_STATUS_READ = "\xFF\xFF\xDF\xFF\xFD\xFF\x55\x56\xFF\xFF\xFF";                    // 4
string _PK_EXTENDED_STATUS2_READ = "\xFF\xFF\xDF\xFF\xFD\xFF\x5A\x58\xFF\xFF\xFF";                 // 5
string _PK_PROTECTED_DATA_READ = "\xFF\xFF\xDF\xFF\xFD\xFF\x55\x56\xFF\xFF\xFF";                   // 6
string _PK_WRITE_LCD_DATA = "\xFF\xFF\x1F\xFF\x77\xFF\xFF\x07\xB0\xFF\xBB\xFF\x7F\xFF";            // 7
string _PK_READ_LCD_DATA = "\xFF\xFF\xEF\xFF\xF7\xFF\xFF\x07\xEF\x7F\x7F\x63\xFF\xFF\xFF";         // 8
string _PK_LCD_DISPLAY_ON = "\xFF\xFF\xDF\xFF\xF9\xFF\xFF\xF9\xFF\xFF\xFF";                        // 9
string _PK_LCD_DISPLAY_OFF = "\xFF\xFF\xDF\xFF\x33\xFF\xFF\x33\xFF\xFF\xFF";                       // 10
string _PK_CLEAR_LCD = "\xFF\xFF\xDF\xFF\xD3\xFF\xFF\xD3\xFF\xFF\xFF";                             // 11
string _PK_ENABLE_TIME_OUT = "\xFF\xFF\xDF\xFF\xB3\xFF\xFF\xB3\xFF\xFF\xFF";                       // 12
string _PK_DISABLE_TIME_OUT = "\xFF\xFF\xDF\xFF\x73\xFF\xFF\x73\xFF\xFF\xFF";                      // 13
string _PK_DISPLAY_SERIAL_NUMBER = "\xFF\xFF\xDF\xFF\x09\xFF\xFF\x09\xFF\xFF\xFF";                 // 14
string _PK_GET_TAG_FORMAT = "\xFF\xFF\xEF\xFF\xF7\xFF\x7F\xFF\x87\xFF\x7F\xF3\xFF\xFF\xFF";        // 15
string _PK_FORMAT_TAG = "\xFF\xFF\x1F\xFF\x77\xFF\x7F\xFF\xF7\xFF\xBB\xFF\xFF\xFF";                // 16
string _PK_START_CMDr = "\xFC\xFF";                                                                // 17
string _PK_ACK_START_CMD = "\xCA";                                                                 // 18
string _PK_CMD_DONE = "\x60";                                                                      // 19
string _PK_FORMAT = "\xFF\xFF\xB7\xFF\xF5\xFF\x73\xFB\xFB\xFB\xFB\xFB\xFB\xFB\xFB\xFB\xFB\xFB\xFB\xFB\xFB\x7D\xFF\xBF\xFF"; // 封包不變，會跟著參數 // 20
string _PK_SET_TIME = "\xFF\xFF\xAF\xFF\x5B\xFF\x5D\x17\x3F\x1B\x77\x7F\x5B\xB8\xFF\xFF\xFF";      // 21
string _PK_LCD_DATAv = "\xFF\xFF\xD0";                                                          // 22
string _PK_READ_TIME1 = "\x00\x10\x24"; // 要分成三段                                           // 23
string _PK_NORMAL_STATUS = "\x00\xA8\x42";                                                      // 24
string _PK_PROTECTED_DATA = "\x00\xA8\x42";                                                     // 25
string _PK_EXTENDED_STATUS2 = "\x00\xE8\x42";                                                   // 26
string _PK_LCD_DATAvr = "\x80\x80\x0A";                                                         // 27
string _PK_LCD_DATA21 = "\x00\x90\x0A";                                                         // 28
string _PK_LCD_DATA22 = "\x00\x50\x0A";		// 我怕會有別的值，所以這邊要思考一下                  // 29
string _PK_TAG_FORMAT = "\x00\xF8\x0A";                                                         // 30

unordered_map <int, string> create_packets() {
	unordered_map <int, string> m;
	m[ENUM_PK_START_CMD] = string("\xFC\xFF");
	m[ENUM_PK_DISPLAY_TIME] = string("\xFF\xFF\xDF\xFF\xBB\xFF\xFF\xBB\xFF\xFF\xFF");
	m[ENUM_PK_READ_TIME] = string("\xFF\xFF\xDF\xFF\x3B\xFF\xFF\x3B\xFF\xFF\xFF") ;                             // 2
	m[ENUM_PK_READ_DONE] = string("\x9F\xFF") ;                                                                 // 3
	m[ENUM_PK_NORMAL_STATUS_READ] = string("\xFF\xFF\xDF\xFF\xFD\xFF\x55\x56\xFF\xFF\xFF") ;                    // 4
	m[ENUM_PK_EXTENDED_STATUS2_READ] = string("\xFF\xFF\xDF\xFF\xFD\xFF\x5A\x58\xFF\xFF\xFF") ;                 // 5
	m[ENUM_PK_PROTECTED_DATA_READ] = string("\xFF\xFF\xDF\xFF\xFD\xFF\x55\x56\xFF\xFF\xFF") ;                   // 6
	m[ENUM_PK_WRITE_LCD_DATA] = string("\xFF\xFF\x1F\xFF\x77\xFF\xFF\x07\xB0\xFF\xBB\xFF\x7F\xFF") ;            // 7
	m[ENUM_PK_READ_LCD_DATA] = string("\xFF\xFF\xEF\xFF\xF7\xFF\xFF\x07\xEF\x7F\x7F\x63\xFF\xFF\xFF") ;         // 8
	m[ENUM_PK_LCD_DISPLAY_ON] = string("\xFF\xFF\xDF\xFF\xF9\xFF\xFF\xF9\xFF\xFF\xFF") ;                        // 9
	m[ENUM_PK_LCD_DISPLAY_OFF] = string("\xFF\xFF\xDF\xFF\x33\xFF\xFF\x33\xFF\xFF\xFF") ;                       // 10
	m[ENUM_PK_CLEAR_LCD] = string("\xFF\xFF\xDF\xFF\xD3\xFF\xFF\xD3\xFF\xFF\xFF") ;                             // 11
	m[ENUM_PK_ENABLE_TIME_OUT] = string("\xFF\xFF\xDF\xFF\xB3\xFF\xFF\xB3\xFF\xFF\xFF") ;                       // 12
	m[ENUM_PK_DISABLE_TIME_OUT] = string("\xFF\xFF\xDF\xFF\x73\xFF\xFF\x73\xFF\xFF\xFF") ;                      // 13
	m[ENUM_PK_DISPLAY_SERIAL_NUMBER] = string("\xFF\xFF\xDF\xFF\x09\xFF\xFF\x09\xFF\xFF\xFF") ;                 // 14
	m[ENUM_PK_GET_TAG_FORMAT] = string("\xFF\xFF\xEF\xFF\xF7\xFF\x7F\xFF\x87\xFF\x7F\xF3\xFF\xFF\xFF") ;        // 15
	m[ENUM_PK_FORMAT_TAG] = string("\xFF\xFF\x1F\xFF\x77\xFF\x7F\xFF\xF7\xFF\xBB\xFF\xFF\xFF") ;                // 16
	m[ENUM_PK_START_CMDr] = string("\xFC\xFF") ;                                                                // 17
	m[ENUM_PK_ACK_START_CMD] = string("\xCA") ;                                                                 // 18
	m[ENUM_PK_CMD_DONE] = string("\x60") ;                                                                      // 19
	m[ENUM_PK_FORMAT] = string("\xFF\xFF\xB7\xFF\xF5\xFF\x73\xFB\xFB\xFB\xFB\xFB\xFB\xFB\xFB\xFB\xFB\xFB\xFB\xFB\xFB\x7D\xFF\xBF\xFF") ; // 封包不變，會跟著參數 // 20
	m[ENUM_PK_SET_TIME] = string("\xFF\xFF\xAF\xFF\x5B\xFF\x5D\x17\x3F\x1B\x77\x7F\x5B\xB8\xFF\xFF\xFF") ;      // 21
	m[ENUM_PK_LCD_DATAv] = string("\xFF\xFF\xD0", 3) ;                                                          // 22
	m[ENUM_PK_READ_TIME1] = string("\x00\x10\x24", 3) ; // 要分成三段                                            // 23
	m[ENUM_PK_NORMAL_STATUS] = string("\x00\xA8\x42", 3) ;                                                      // 24
	m[ENUM_PK_PROTECTED_DATA] = string("\x00\xA8\x42", 3) ;                                                     // 25
	m[ENUM_PK_EXTENDED_STATUS2] = string("\x00\xE8\x42", 3) ;                                                   // 26
	m[ENUM_PK_LCD_DATAvr] = string("\x80\x80\x0A", 3) ;                                                         // 27
	m[ENUM_PK_LCD_DATA21] = string("\x00\x90\x0A", 3) ;                                                         // 28
	m[ENUM_PK_LCD_DATA22] = string("\x00\x50\x0A", 3) ; // 我怕會有別的值，所以這邊要思考一下					   // 29
	m[ENUM_PK_TAG_FORMAT] = string("\x00\xF8\x0A", 3) ;                                                         // 30

	return m;
};
unordered_map <int, string> Cmd::packets = create_packets();

unordered_map <int, vector<int>> create_cmds() {
	/* 底下命令相當於 > Start, < ACK, > CMD, < DONE
	*/
	unordered_map <int, vector<int>> m;
	static const int _CMD_DISPLAY_TIME[] = {
		ENUM_PK_START_CMD,
		ENUM_PK_ACK_START_CMD,
		ENUM_PK_DISPLAY_TIME,
		ENUM_PK_CMD_DONE
	};
	vector <int> v_CMD_DISPLAY_TIME(_CMD_DISPLAY_TIME, _CMD_DISPLAY_TIME+sizeof(_CMD_DISPLAY_TIME)/sizeof(_CMD_DISPLAY_TIME[0]));
	m[ENUM_CMD_DISPLAY_TIME] =  v_CMD_DISPLAY_TIME;

	static const int _CMD_LCD_DISPLAY_ON[] = {
		ENUM_PK_START_CMD,
		ENUM_PK_ACK_START_CMD,
		ENUM_PK_LCD_DISPLAY_ON,
		ENUM_PK_CMD_DONE
	};
	vector <int> v_CMD_LCD_DISPLAY_ON(_CMD_LCD_DISPLAY_ON, _CMD_LCD_DISPLAY_ON+sizeof(_CMD_LCD_DISPLAY_ON) / sizeof(_CMD_LCD_DISPLAY_ON[0]));
	m[ENUM_CMD_LCD_DISPLAY_ON] =  v_CMD_LCD_DISPLAY_ON;
	
	static const int _CMD_LCD_DISPLAY_OFF[] = {
		ENUM_PK_START_CMD,
		ENUM_PK_ACK_START_CMD,
		ENUM_PK_LCD_DISPLAY_OFF,
		ENUM_PK_CMD_DONE
	};
	vector <int> v_CMD_LCD_DISPLAY_OFF(_CMD_LCD_DISPLAY_OFF, _CMD_LCD_DISPLAY_OFF+sizeof(_CMD_LCD_DISPLAY_OFF) / sizeof(_CMD_LCD_DISPLAY_OFF[0]));
	m[ENUM_CMD_LCD_DISPLAY_OFF] = v_CMD_LCD_DISPLAY_OFF;

	static const int _CMD_CLEAR_LCD[] = {
		ENUM_PK_START_CMD,
		ENUM_PK_ACK_START_CMD,
		ENUM_PK_CLEAR_LCD,
		ENUM_PK_CMD_DONE
	};
	vector <int> v_CMD_CLEAR_LCD(_CMD_CLEAR_LCD, _CMD_CLEAR_LCD+sizeof(_CMD_CLEAR_LCD) / sizeof(_CMD_CLEAR_LCD[0]));
	m[ENUM_CMD_CLEAR_LCD] = v_CMD_CLEAR_LCD;

	static const int _CMD_ENABLE_TIMEOUT[] = {
		ENUM_PK_START_CMD,
		ENUM_PK_ACK_START_CMD,
		ENUM_PK_ENABLE_TIME_OUT,
		ENUM_PK_CMD_DONE
	};
	vector <int> v_CMD_ENABLE_TIMEOUT(_CMD_ENABLE_TIMEOUT, _CMD_ENABLE_TIMEOUT+sizeof(_CMD_ENABLE_TIMEOUT) / sizeof(_CMD_ENABLE_TIMEOUT[0]));
	m[ENUM_CMD_ENABLE_TIMEOUT] = v_CMD_ENABLE_TIMEOUT;

	static const int _CMD_DISABLE_TIMEOUT[] = {
		ENUM_PK_START_CMD,
		ENUM_PK_ACK_START_CMD,
		ENUM_PK_DISABLE_TIME_OUT,
		ENUM_PK_CMD_DONE
	};
	vector <int> v_CMD_DISABLE_TIMEOUT(_CMD_DISABLE_TIMEOUT, _CMD_DISABLE_TIMEOUT+sizeof(_CMD_DISABLE_TIMEOUT) / sizeof(_CMD_DISABLE_TIMEOUT[0]));
	m[ENUM_CMD_DISABLE_TIMEOUT] = v_CMD_DISABLE_TIMEOUT;

	static const int _CMD_DISPLAY_SERIAL_NUMBER[] = {
		ENUM_PK_START_CMD,
		ENUM_PK_ACK_START_CMD,
		ENUM_PK_DISPLAY_SERIAL_NUMBER,
		ENUM_PK_CMD_DONE
	};
	vector <int> v_CMD_DISPLAY_SERIAL_NUMBER(_CMD_DISPLAY_SERIAL_NUMBER, _CMD_DISPLAY_SERIAL_NUMBER+sizeof(_CMD_DISPLAY_SERIAL_NUMBER) / sizeof(_CMD_DISPLAY_SERIAL_NUMBER[0]));
	m[ENUM_CMD_DISPLAY_SERIAL_NUMBER] = v_CMD_DISPLAY_SERIAL_NUMBER;

	/* 底下命令都是讀資料，所以在 前面的通訊完畢後，還會加上 < RESPONSE, > READ_DONE
	 * 其中 RESPONSE 只是開頭3 bytes，用來比對
	 */
	static const int _CMD_NORMAL_STATUS_READ[] = {
		ENUM_PK_START_CMD,
		ENUM_PK_ACK_START_CMD,
		ENUM_PK_NORMAL_STATUS_READ,
		ENUM_PK_CMD_DONE,
		ENUM_PK_NORMAL_STATUS,
		ENUM_PK_READ_DONE
	};
	vector <int> v_CMD_NORMAL_STATUS_READ(_CMD_NORMAL_STATUS_READ, _CMD_NORMAL_STATUS_READ+sizeof(_CMD_NORMAL_STATUS_READ)/sizeof(_CMD_NORMAL_STATUS_READ[0]));
	m[ENUM_CMD_NORMAL_STATUS_READ] = v_CMD_NORMAL_STATUS_READ;

	static const int _CMD_EXTENDED_STATUS2_READ[] = {
		ENUM_PK_START_CMD,
		ENUM_PK_ACK_START_CMD,
		ENUM_PK_EXTENDED_STATUS2_READ,
		ENUM_PK_CMD_DONE,
		ENUM_PK_EXTENDED_STATUS2,
		ENUM_PK_READ_DONE
	};
	vector <int> v_CMD_EXTENDED_STATUS2_READ(_CMD_EXTENDED_STATUS2_READ, _CMD_EXTENDED_STATUS2_READ+sizeof(_CMD_EXTENDED_STATUS2_READ) / sizeof(_CMD_EXTENDED_STATUS2_READ[0]));
	m[ENUM_CMD_EXTENDED_STATUS2_READ] = v_CMD_EXTENDED_STATUS2_READ;

	static const int _CMD_PROTECTED_DATA_READ[] = {
		ENUM_PK_START_CMD,
		ENUM_PK_ACK_START_CMD,
		ENUM_PK_PROTECTED_DATA_READ,
		ENUM_PK_CMD_DONE,
		ENUM_PK_PROTECTED_DATA,
		ENUM_PK_READ_DONE
	};
	vector <int> v_CMD_PROTECTED_DATA_READ(_CMD_PROTECTED_DATA_READ, _CMD_PROTECTED_DATA_READ+sizeof(_CMD_PROTECTED_DATA_READ) / sizeof(_CMD_PROTECTED_DATA_READ[0]));
	m[ENUM_CMD_PROTECTED_DATA_READ] = v_CMD_PROTECTED_DATA_READ;

	static const int _CMD_GET_TAG_FORMAT[] = {
		ENUM_PK_START_CMD,
		ENUM_PK_ACK_START_CMD,
		ENUM_PK_GET_TAG_FORMAT,
		ENUM_PK_CMD_DONE,
		ENUM_PK_TAG_FORMAT,
		ENUM_PK_READ_DONE
	};
	vector <int> v_CMD_GET_TAG_FORMAT(_CMD_GET_TAG_FORMAT, _CMD_GET_TAG_FORMAT+sizeof(_CMD_GET_TAG_FORMAT) / sizeof(_CMD_GET_TAG_FORMAT[0]));
	m[ENUM_CMD_GET_TAG_FORMAT] = v_CMD_GET_TAG_FORMAT;

	static const int _CMD_FORMAT_TAG[] = {
		ENUM_PK_START_CMD,
		ENUM_PK_ACK_START_CMD,
		ENUM_PK_FORMAT_TAG,
		ENUM_PK_CMD_DONE,
		ENUM_PK_FORMAT,
		ENUM_PK_CMD_DONE
	};
	vector <int> v_CMD_FORMAT_TAG(_CMD_FORMAT_TAG, _CMD_FORMAT_TAG+sizeof(_CMD_FORMAT_TAG) / sizeof(_CMD_FORMAT_TAG[0]));
	m[ENUM_CMD_FORMAT_TAG] = v_CMD_FORMAT_TAG;

	/* CMD_WRITE_LCD_DATA 通訊過程跟以上都不同, 後面還需要再一段真正的 LCD Data
	 */
	static const int _CMD_WRITE_LCD_DATA[] = {
		ENUM_PK_START_CMD,
		ENUM_PK_ACK_START_CMD,
		ENUM_PK_WRITE_LCD_DATA,
		ENUM_PK_CMD_DONE
	};
	vector <int> v_CMD_WRITE_LCD_DATA(_CMD_WRITE_LCD_DATA, _CMD_WRITE_LCD_DATA+sizeof(_CMD_WRITE_LCD_DATA) / sizeof(_CMD_WRITE_LCD_DATA[0]));
	m[ENUM_CMD_WRITE_LCD_DATA] = v_CMD_WRITE_LCD_DATA;

	/* CMD_READ_LCD_DATA 通訊過程相當詭異, 不止開始有問題，後面在傳完 Data 後，還有一段
	 * 我認為有問題，底下與 README-Aconfig.txt 記載並不相符
	 [ ENUM_PK_START_CMD, ENUM_PK_ACK_START_CMD, ENUM_PK_READ_LCD_DATA, ENUM_PK_CMD_DONE, ENUM_PK_LCD_DATAvr, ENUM_PK_READ_DONE, ENUM_PK_LCD_DATA21, ENUM_PK_READ_DONE ]
	 */
	static const int _CMD_READ_LCD_DATA[] = {
		ENUM_PK_START_CMD,
		ENUM_PK_ACK_START_CMD,
		ENUM_PK_READ_LCD_DATA,
		ENUM_PK_CMD_DONE,
		ENUM_PK_LCD_DATAvr,
		ENUM_PK_READ_DONE,
		ENUM_PK_LCD_DATA21,
		ENUM_PK_READ_DONE
	};
	vector <int> v_CMD_READ_LCD_DATA(_CMD_READ_LCD_DATA, _CMD_READ_LCD_DATA+sizeof(_CMD_READ_LCD_DATA) / sizeof(_CMD_READ_LCD_DATA[0]));
	m[ENUM_CMD_READ_LCD_DATA] = v_CMD_READ_LCD_DATA;
	return m;
};
unordered_map <int, vector<int>> Cmd::cmds = create_cmds();

static const uint8_t _lcdData[] = {
	0xFF, 0xFF, 0xD0, 0xFF, 0xF5, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0x7F, 0xFF, 0xFF, 0xFF, 0x8F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xEB, 0x3F, 0xFF, 0xFF, 0x87, 0xFF, 0xFF, 0xFF, 0x5F, 0xFF,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x3B, 0x3F, 0xFF, 0xFF,
	0x7F, 0xFF, 0xFF, 0xFF, 0x3F, 0xFF, 0xFF, 0xFF, 0x7F, 0xFF,
	0xFF, 0xFF, 0x5B, 0x3F, 0xFF, 0xFF, 0xBF, 0xFF, 0xFF, 0xFF,
	0xBF, 0xFF, 0xFF, 0xFF, 0xBF, 0xFF, 0xFF, 0xFF, 0x9B, 0x3F,
	0xFF, 0xFF, 0x3F, 0xFF, 0xFF, 0xFF, 0x7F, 0xFF, 0xFF, 0xFF,
	0x3F, 0xFF, 0xFF, 0xFF, 0x1B, 0x3F, 0xFF, 0xFF, 0xDF, 0xFF,
	0xFF, 0xFF, 0x7F, 0xFF, 0xFF, 0xFF, 0xDF, 0xFF, 0xFF, 0xFF,
	0x6F, 0xFF, 0xFF, 0xFF, 0x5F, 0xFF, 0xFF, 0xFF, 0x07, 0x3F,
	0xFF, 0xFF, 0xDF, 0xFF, 0xFF, 0xFF, 0x1B, 0x3F, 0xFF, 0xFF,
	0x5F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x5F, 0xFF,
	0xFF, 0xFF, 0x1F, 0xFF, 0xFF, 0xFF, 0x9F, 0xFF, 0xFF, 0xFF,
	0x7B, 0x3F, 0xFF, 0xFF, 0x5F, 0xFF, 0xFF, 0xFF, 0xEB, 0x3F,
	0xFF, 0xFF, 0x9F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0x9F, 0xFF, 0xFF, 0xFF, 0x9F, 0xFF, 0xFF, 0xFF, 0x1F, 0xFF,
	0xFF, 0xFF, 0xBB, 0x3F, 0xFF, 0xFF, 0x9F, 0xFF, 0xFF, 0xFF,
	0xEB, 0x3F, 0xFF, 0xFF, 0x1F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFF, 0xFF, 0x1F, 0xFF, 0xFF, 0xFF, 0x5F, 0xFF, 0xFF, 0xFF,
	0x6F, 0xFF, 0xFF, 0xFF, 0x73, 0xB3, 0x33, 0xFB, 0xFB, 0xFB,
	0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB,
	0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB,
	0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xFB, 0xF4, 0x3C, 0xFF, 0x1F,
	0xFF
};
vector<uint8_t> Cmd::lcdData(_lcdData, _lcdData+sizeof(_lcdData) / sizeof(_lcdData[0]));

void Cmd::setDebug(bool d) { debug = d;  }
bool Cmd::isDebug() { return debug;  }

int Cmd::getCmdId(string pkt) {
	for (size_t c = 0; c < cmds.size(); ++c) {
		for (size_t p = 0; p < cmds[c].size(); ++p) {
			if (pkt.compare(packets[p]) == 0) {
				return p;
			}
		}
	}
	return -1;
};

string Cmd::trim(const std::string& s, int which) {
	size_t first = 0, last = s.size() - 1;
	if (last < 0) return "";
	if (which != 2) { // head
		first = s.find_first_not_of(' ');
	}
	if (which != 1) { // tail
		last = s.find_last_not_of(' ');
	}
	if (first < last)
		return s.substr(first, (last - first + 1));
	else return "";
}

string Cmd::toHex(vector<uint8_t>s, bool withSpace) {
	// string res(s.begin(), s.end());
	// return toHex(res, withSpace);
	int n = s.size();
	string output;
	output.reserve((withSpace ? 3 : 2) * n);
	for (int i = 0; i < n; ++i) {
		const unsigned char c = s[i];
		output.push_back(lut[c >> 4]);
		output.push_back(lut[c & 0x0F]);
		if (withSpace) output.push_back(' ');
	}
	return trim(output);
}

string Cmd::toHex(string s, bool withSpace) {
	//vector<uint8_t> v(s.begin(), s.end());
	//return toHex(v, withSpace);
	int n = s.size();
	string output;
	output.reserve((withSpace ? 3 : 2) * n);
	for (int i = 0; i < n; ++i) {
		const unsigned char c = s[i];
		output.push_back(lut[c >> 4]);
		output.push_back(lut[c & 0x0F]);
		if (withSpace) output.push_back(' ');
	}
	return trim(output);
}

string Cmd::readPkt(serial::Serial& ser, uint32_t timeout, size_t s) {
	string line = "", input = "";
	// 第一次應該是等候較長時間，後面會再確認
	// uint32_t inter_byte_timeout, uint32_t read_timeout_constant, uint32_t read_timeout_multiplier, uint32_t write_timeout_constant, uint32_t write_timeout_multiplier
	ser.setTimeout(serial::Timeout::max(), timeout, 1, timeout, 1);
	size_t max = s > 32 ? 32 : s;
	while (line.size() < s) {
		input = ser.read(max); // 有時封包會超過 255
		usleep(30000);
		line += input;
		if (input.size() < max) break;
	}
	if (debug) cout << "read(" << line.size() << "/" << s << ") : " << toHex(line) << endl;
	return line;
}

void Cmd::writePkt(serial::Serial& ser, int pktId) {
	if (debug) cout << "write: " << toHex(packets[pktId]) << endl;
	ser.write(packets[pktId]);
}

void Cmd::writeByteArray(serial::Serial& ser, const std::vector<uint8_t>& data) {
	ser.write(data);
}

bool Cmd::pktEqual(string pkt, int pktId) {
	if (debug) cout << "pktEqual: " << toHex(pkt) << " v.s. " << toHex(packets[pktId]) << endl;

	if (pkt.size() < packets[pktId].size()) { // 收到的 packet 只能比內建資料長
		return false;
	}
	if (pkt.size() > packets[pktId].size()) { // 如果收到的 packet 比內建資料長，只比對內建資料長度
		pkt = pkt.substr(0, packets[pktId].size());
	}
	return pkt.compare(packets[pktId]) == 0;
}

void Cmd::usleep(__int64 usec) {
	HANDLE timer;
	LARGE_INTEGER ft;

	ft.QuadPart = -(10 * usec); // Convert to 100 nanosecond interval, negative value indicates relative time

	timer = CreateWaitableTimer(NULL, TRUE, NULL);
	SetWaitableTimer(timer, &ft, 0, NULL, NULL, 0);
	WaitForSingleObject(timer, INFINITE);
	CloseHandle(timer);
}

void Cmd::msleep(DWORD milliseconds) {
	Sleep(milliseconds);
}

int Cmd::doCmd(serial::Serial& ser, int cmdId) {
	// 前4個封包算是固定的，跟 cmdId 無關
	// pack 1 : write packet
	writePkt(ser, cmds[cmdId][0]); // 每一個命令的前2個封包，其實都一樣，這邊是 ENUM_PK_START_CMD
	// pack 2 : read packet
	string res;
	res = readPkt(ser, 40, packets[cmds[cmdId][1]].size());
	if (!pktEqual(res, cmds[cmdId][1])) {
		return 1;
	}
	// pack 3: write packet
	writePkt(ser, cmds[cmdId][2]);
	// pack 4 : read packet
	res = readPkt(ser, 40, packets[cmds[cmdId][3]].size());
	if (!pktEqual(res, cmds[cmdId][3])) {
		return 1;
	}
	// 接下來需要區分 cmdId
	if (cmdId < ENUM_CMD_NORMAL_STATUS_READ) {
		// READ 之前是一般命令，只有兩次一來一回
		return 0;
	}
	else if (cmdId < ENUM_CMD_FORMAT_TAG) {
		// 命令 7, 8, 9, 10 是 read, 所以需要再讀回傳內容，然後傳送 ENUM_PK_READ_DONE。這邊是固定值
		usleep(2000);
		res = readPkt(ser, 40, 255);
		if (!pktEqual(res, cmds[cmdId][4])) {
			return 1;
		}
		writePkt(ser, cmds[cmdId][5]);
		return 0;
	}
	else if (cmdId == ENUM_CMD_FORMAT_TAG) { // 乍看跟 7 / 8 / 9 / 10 一樣，其實是相反方向的，還需要送命令、讀回(ENUM_PK_CMD_DONE)一次
		writePkt(ser, cmds[cmdId][4]);
		res = readPkt(ser, 40, packets[cmds[cmdId][5]].size());
		if (pktEqual(res, cmds[cmdId][5])) {
			return 0;
		}
	}
	else if (cmdId == ENUM_CMD_WRITE_LCD_DATA) {
		writeByteArray(ser, lcdData);
		return 0;
	}
	else { // READ_LCD_DATA 比較特殊, 還有 < ENUM_PK_LCD_DATAvr, > ENUM_PK_READ_DONE, < ENUM_PK_LCD_DATA21, >ENUM_PK_READ_DONE
		res = readPkt(ser, 1000, 255); // ENUM_PK_LCD_DATAvr
		if (res.size() < 10) { // || !pktEqual(res, cmds[cmdId][4])) {
			return 1;
		}
		writePkt(ser, cmds[cmdId][5]); // ENUM_PK_READ_DONE
		res = readPkt(ser, 40, 255); // ENUM_PK_LCD_DATA21
		if (res.size() < 3) { // || !pktEqual(res, cmds[cmdId][6])) {
			return 1;
		}
		writePkt(ser, cmds[cmdId][7]);
		return 0;
	}
	return 1;
}
