#ifndef __CMD_H__
#define __CMD_H__

#include <string>
#include <vector>
#include <unordered_map>

#ifndef __SERIAL_H__
#include "serial/serial.h"
#endif

using namespace std;

/////////// 宣告區 ///////////////
typedef enum {
	ENUM_PK_START_CMD				= 0,
	ENUM_PK_DISPLAY_TIME			= 1,
	ENUM_PK_READ_TIME				= 2,
	ENUM_PK_READ_DONE				= 3,
	ENUM_PK_NORMAL_STATUS_READ		= 4,
	ENUM_PK_EXTENDED_STATUS2_READ	= 5,
	ENUM_PK_PROTECTED_DATA_READ		= 6,
	ENUM_PK_WRITE_LCD_DATA			= 7,
	ENUM_PK_READ_LCD_DATA			= 8,
	ENUM_PK_LCD_DISPLAY_ON			= 9,
	ENUM_PK_LCD_DISPLAY_OFF			= 10,
	ENUM_PK_CLEAR_LCD				= 11,
	ENUM_PK_ENABLE_TIME_OUT			= 12,
	ENUM_PK_DISABLE_TIME_OUT		= 13,
	ENUM_PK_DISPLAY_SERIAL_NUMBER	= 14,
	ENUM_PK_GET_TAG_FORMAT			= 15,
	ENUM_PK_FORMAT_TAG				= 16,
	ENUM_PK_START_CMDr				= 17,
	ENUM_PK_ACK_START_CMD			= 18,
	ENUM_PK_CMD_DONE				= 19,
	ENUM_PK_FORMAT					= 20,
	ENUM_PK_SET_TIME				= 21,
	ENUM_PK_LCD_DATAv				= 22,
	ENUM_PK_READ_TIME1				= 23,
	ENUM_PK_NORMAL_STATUS			= 24,
	ENUM_PK_PROTECTED_DATA			= 25,
	ENUM_PK_EXTENDED_STATUS2		= 26,
	ENUM_PK_LCD_DATAvr				= 27,
	ENUM_PK_LCD_DATA21				= 28,
	ENUM_PK_LCD_DATA22				= 29,
	ENUM_PK_TAG_FORMAT				= 30
} PACKET_ID;

typedef enum {
	/// 底下命令相當於 > Start, < ACK, > CMD, < DONE
	ENUM_CMD_DISPLAY_TIME			= 0,
	ENUM_CMD_LCD_DISPLAY_ON			= 1,
	ENUM_CMD_LCD_DISPLAY_OFF		= 2,
	ENUM_CMD_CLEAR_LCD				= 3,
	ENUM_CMD_ENABLE_TIMEOUT			= 4,
	ENUM_CMD_DISABLE_TIMEOUT		= 5,
	ENUM_CMD_DISPLAY_SERIAL_NUMBER	= 6,
	/// 底下命令都是讀資料，所以在 前面的通訊完畢後，還會加上 < RESPONSE, > READ_DONE
	/// 其中 RESPONSE 只是開頭3 bytes，用來比對
	ENUM_CMD_NORMAL_STATUS_READ		= 7,
	ENUM_CMD_EXTENDED_STATUS2_READ	= 8,
	ENUM_CMD_PROTECTED_DATA_READ	= 9,
	ENUM_CMD_GET_TAG_FORMAT			= 10,
	ENUM_CMD_FORMAT_TAG				= 11,
	/// CMD_WRITE_LCD_DATA 通訊過程跟以上都不同, 後面還需要再一段真正的 LCD Data
	ENUM_CMD_WRITE_LCD_DATA			= 12,
	/// CMD_READ_LCD_DATA 通訊過程相當詭異, 不止開始有問題，後面在傳完 Data 後，還有一段
	/// 我認為有問題，底下與 README - Aconfig.txt 記載並不相符
	ENUM_CMD_READ_LCD_DATA			= 13
} CMD_ID;

/////////// 函式區 ///////////
// 應該先來想想什麼叫 packet, cmd, flow,
// 每個 packet 都包括 to/from, 但是並不是完整的命令
// 一個命令是像 LCD_ON 這樣，它其實是透過流程，將一系列的 packet + timeout 串起來
//   但是一個命令是有流程的，每個流程，包含失敗處理，會嘗試，會逾時
// 所謂的流程，就是一堆封包，在逾時前若成功，就進下一個命令，若逾時，則重覆
//   可以先假設每個命令都有重複次數
//   至於逾時值，我的感覺是所有命令都一樣的原理，有遞增性
class Cmd {
private:
	static const string lut;
	static vector<uint8_t> lcdData;
	static std::string trim(const std::string& s, int which = 0); // 0 : both, 1 : head, 2 : tail
	bool debug;
public:
	static unordered_map <int, string> packets;
	static unordered_map <int, vector<int>> cmds;

	static void usleep(__int64 usec);
	static void msleep(DWORD milliseconds);
	static string toHex(string s, bool withSpace = true);
	static string toHex(vector<uint8_t> s, bool withSpace = true);

	Cmd() { debug = false;  }
	bool isDebug();
	void setDebug(bool d);
	int getCmdId(string pkt);
	string readPkt(serial::Serial& ser, uint32_t timeout, size_t s=255);
	void writePkt(serial::Serial& ser, int pktId);
	int doCmd(serial::Serial& ser, int cmdId);
	void writeByteArray(serial::Serial& ser, const std::vector<uint8_t>& data);
	bool pktEqual(string pkt, int pktId);
};

#endif